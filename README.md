# EMR Web App

## Code Structure

.next\
 .vscode\
 node modules\
public\
 &nbsp; _-_ fonts\
 &nbsp; _-_ icons\

## To Install

- clone repo
- run `yarn`
- run `yarn dev` to start development server
- or run `yarn build` to open production environment
