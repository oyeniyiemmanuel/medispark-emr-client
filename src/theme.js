import { extendTheme } from "@chakra-ui/react";
import { createBreakpoints } from "@chakra-ui/theme-tools";
import { BeatLoader } from "react-spinners";

const breakpoints = createBreakpoints({
  sm: "576px",
  md: "768px",
  lg: "992px",
  xl: "1200px",
});

const theme = extendTheme({
  colors: {
    black: "#16161D",
    gray: {
      30: "#FBFBFB",
      70: "#F7F8F8",
    },
    primary: {
      50: "#def6ff",
      100: "#b6defa",
      200: "#8cc7f2",
      300: "#61b0e9",
      400: "#379ae2",
      500: "#1d80c8",
      600: "#11649d",
      700: "#054771",
      800: "#002b47",
      900: "#000f1d",
    },
    secondary: {
      30: "#d9ffff",
      50: "#acfffe",
      100: "#7dfefb",
      200: "#4efefa",
      300: "#28fef8",
      400: "#17e4de",
      500: "#01b1ad",
      600: "#007f7c",
      700: "#004d4b",
      800: "#001b1b",
      900: "#001b1b",
    },
    primaryDark: {
      50: "#bfd2f4",
      100: "#97b3e8",
      200: "#6f95dd",
      300: "#4777d2",
      400: "#2e5db9",
      500: "#224990",
      600: "#163468",
      700: "#0b1f41",
      800: "#010a1b",
      900: "#010a1b",
    },
  },
  fonts: {
    body: "Sen, sans-serif",
    heading: "Sen, sans-serif",
    mono: "Sen, sans-serif",
  },
  breakpoints,
  icons: {
    logo: {
      path: (
        <svg
          width="3000"
          height="3163"
          viewBox="0 0 3000 3163"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect width="3000" height="3162.95" fill="none" />
          <path
            d="M1470.89 1448.81L2170 2488.19H820V706.392H2170L1470.89 1448.81ZM1408.21 1515.37L909.196 2045.3V2393.46H1998.84L1408.21 1515.37Z"
            fill="currentColor"
          />
        </svg>
      ),
      viewBox: "0 0 3000 3163",
    },
    button: <BeatLoader size={8} color="white" />,
  },
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  radii: { md: "4px" },
});

export default theme;
