import { Text } from "@chakra-ui/layout";
import Axios from "axios";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { BASE_API_URL, separateWithComma } from "../utils";
import CustomSelect from "./multi-select";

export default function SelectService({
  valueChecker,
  serviceCategory,
  ...rest
}) {
  const { services } = useSelector((state) => state.services);
  const { selectedPatient } = useSelector((state) => state.patients);
  const [selectedServiceDetails, setSelectedServiceDetails] = useState(null);
  const { userDetails } = useSelector((state) => state.user);

  const showResults = (results) => {
    return [...results]
      .filter((service) =>
        serviceCategory ? service.category == serviceCategory : true
      )
      .slice(0, 100)
      .map((service) => {
        return servicesOptions(service);
      });
  };

  const promiseOptions = (inputValue) =>
    // inputValue &&
    Axios.get(`${BASE_API_URL}/billing/service/search`, {
      params: {
        branch_id: userDetails.branch_id,
        keywords: inputValue || "",
      },
      headers: {
        Authorization: `Bearer ${localStorage.getItem(
          "medispark_acccess_token"
        )}`,
      },
    })
      .then(({ data: { data } }) => {
        return showResults(data);
      })
      .catch((error) => {
        console.log(error);
      });

  const servicesOptions = (service) => ({
    ...service,
    value: service.name,
    label: (
      <Text fontSize="sm">
        {service.name} (₦
        {separateWithComma(
          service.prices?.find(
            ({ retainer_id }) => retainer_id == selectedPatient?.retainer_id
          )?.amount || 0
        )}
        )
      </Text>
    ),
  });

  return (
    <CustomSelect
      isAsync
      cacheOptions
      placeholder="Select Service"
      styles={false}
      defaultOptions
      loadOptions={promiseOptions}
      value={selectedServiceDetails}
      {...rest}
      onChange={(medication) => {
        rest.onChange(medication);
        setSelectedServiceDetails(medication);
      }}
    />
  );
}
