import { Box } from "@chakra-ui/layout";
import React from "react";

export default function TextEditor({ onChange, value, placeholder }) {
  if (typeof window !== "undefined") {
    var ReactQuill = require("react-quill");
  }
  return typeof window !== "undefined" && ReactQuill ? (
    <Box w="full" className="editor">
      <ReactQuill
        theme="snow"
        onChange={onChange}
        value={value}
        modules={TextEditor.modules}
        formats={TextEditor.formats}
        bounds={".app"}
        placeholder={placeholder}
      />
    </Box>
  ) : (
    <textarea />
  );
}
TextEditor.modules = {
  toolbar: [
    // [{ header: "1" }, { header: "2" }],
    // [{ size: [] }],
    [
      "bold",
      "italic",
      "underline",
      // "strike",
      "blockquote",
    ],
    [
      { list: "ordered" },
      { list: "bullet" },
      { indent: "-1" },
      { indent: "+1" },
    ],
    [
      "link",
      //  "image",
      //   "video"
    ],
    // ["clean"],
  ],
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false,
  },
};
/*
 * Quill editor formats
 * See https://quilljs.com/docs/formats/
 */
TextEditor.formats = [
  //   "header",
  //   "font",
  "size",
  "bold",
  "italic",
  "underline",
  //   "strike",
  "blockquote",
  "list",
  "bullet",
  "indent",
  "link",
  //   "image",
  //   "video",
];
