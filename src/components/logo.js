import React, { memo } from "react";
import { Image, Stack } from "@chakra-ui/react";
import Router from "next/router";

export const EMRIcon = ({ width, disabled, white, ...rest }) => {
  return (
    <Image
      cursor={disabled ? "initial" : "pointer"}
      onClick={() => {
        !disabled && Router.push("/");
      }}
      w={width || "100px"}
      src={white ? "/images/emr-icon-white.png" : "/images/emr-icon.png"}
      {...rest}
    />
  );
};

function getLogoVariant(variant) {
  switch (variant) {
    case "white":
      return "/images/emr-logo-white.png";

    case "grey":
      return "/images/emr-logo-grey.svg";

    case "gray":
      return "/images/emr-logo-grey.svg";

    default:
      return "/images/emr-logo.png";
  }
}

function EMRLogo({ width, disabled, variant, white, ...rest }) {
  return (
    <Image
      cursor={disabled ? "initial" : "pointer"}
      onClick={() => {
        !disabled && Router.push("/");
      }}
      w={width || "200px"}
      src={getLogoVariant(variant)}
      {...rest}
    />
  );
}

export default memo(EMRLogo);
