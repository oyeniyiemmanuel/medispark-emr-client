import { Flex } from "@chakra-ui/react";
import React from "react";
import { EMRIcon } from "./logo";

export default function PageLoadingAnimation() {
  return (
    <Flex h="100vh" align="center" justify="center" w="100vw">
      <EMRIcon className="zoom" w={["150px", "200px"]} disabled />
      <style jsx global>{`
        .zoom {
          animation: scale 2s ease-in-out infinite;
        }
        @keyframes scale {
          50% {
            -webkit-transform: scale(1.09);
            -moz-transform: scale(1.09);
            -ms-transform: scale(1.09);
            -o-transform: scale(1.09);
            transform: scale(1.09);
          }
        }
      `}</style>
    </Flex>
  );
}
