import { Box } from "@chakra-ui/react";
import React from "react";

export default function FakeIcon({ boxSize, ...rest }) {
  return <Box boxSize={boxSize || 4} bg="gray.300" {...rest} />;
}
