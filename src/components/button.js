import { Button as ChakraButton } from "@chakra-ui/react";
import React from "react";
export default function Button({ variant, ...rest }) {
  switch (variant) {
    case "primary":
      return <ChakraButton {...rest} />;
    default:
      return <ChakraButton {...rest} />;
  }
}
