import {
  Box,
  Center,
  Flex,
  Heading,
  Image,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Stack,
  Text,
} from "@chakra-ui/react";
import React from "react";

export default function UploadFile({
  isOpen,
  onClose,
  onOpen,
  fileToUpload,
  setFileToUpload,
  accept,
  ...rest
}) {
  const onFileChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();

    reader.addEventListener(
      "load",
      function () {
        // convert image file to base64 string
        // setPreviewUrl(reader.result);
      },
      false
    );
    if (file && file.name) {
      // if (Math.round(file.size / 1024) >= 1024) {
      //   toast({
      //     status: "error",
      //     title: "This image is too large",
      //     description: "Please upload an image less then 1MB",
      //     duration: 4000,
      //     isClosable: true,
      //     position: "bottom-left",
      //   });
      // } else {
      reader.readAsDataURL(file);
      //   ...........................
      // const formData = new FormData();
      // formData.append("image", file);
      setFileToUpload(file);
      onClose();
      // }
    }
  };

  return (
    <>
      <Modal
        closeOnOverlayClick={false}
        isCentered
        size="lg"
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay bgColor="rgba(0, 0, 0, 0.2)" />
        <ModalContent border="1px solid #C4C4C4" px={[2, 6]} pb={4} rounded={8}>
          <ModalCloseButton />

          <ModalBody py={10}>
            <Stack spacing={10} justify="center" align="center">
              <Stack justify="center" align="center">
                <Heading fontSize="xl" color="primary.600">
                  Upload File
                </Heading>
                <Text textAlign="center" maxW={72}>
                  Drag and drop file(s) here to upload <br /> (Max. 5MB)
                </Text>
              </Stack>
              <Flex w="full" pos="relative">
                <Center
                  cursor="pointer"
                  w="full"
                  h={56}
                  bg="primary.50"
                  rounded={8}
                >
                  <Stack align="center" justify="center">
                    <Image boxSize={20} src="/images/folder-icon.png" />
                  </Stack>
                </Center>
                <Box
                  as="input"
                  pos="absolute"
                  w="full"
                  accept={accept}
                  cursor="pointer"
                  h="full"
                  opacity={0}
                  type="file"
                  onChange={onFileChange}
                />
              </Flex>
            </Stack>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
