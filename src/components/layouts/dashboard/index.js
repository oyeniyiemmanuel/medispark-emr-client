import { Box, Flex } from "@chakra-ui/react";
import React, { useState } from "react";
import FeedBoard from "./queues";
import DashboardNavbar from "./navbar";
import ServicePopupModal from "../../popup-modal";
import ReactTooltip from "react-tooltip";
// import AuthenticateModal from "./re-authenticate";

export default function DashboardContainer({ active, children }) {
  const [openFeed, setOpenFeed] = useState(true); //For managing expanded state of feed-board

  return (
    <>
      <Box h="full" minH="100vh" bgColor="gray.70">
        <DashboardNavbar />
        <Flex h="full" justify="center">
          <Box w="full" h="full">
            <Box
              pt={16}
              w={
                // Responsive widths based on the openFeed state
                openFeed
                  ? [
                      "calc(100% - 0px)",
                      "calc(100% - 64px)",
                      "calc(100% - 300px)",
                      "calc(100% - 300px)",
                      "calc(100% - 370px)",
                    ]
                  : [
                      "calc(100% - 0px)",
                      "calc(100% - 64px)",
                      "calc(100% - 64px)",
                      "calc(100% - 64px)",
                      "calc(100% - 64px)",
                    ]
              }
              transition="0.2s all"
            >
              {children}
            </Box>
          </Box>
          <FeedBoard openFeed={openFeed} setOpenFeed={setOpenFeed} />
        </Flex>
        <ReactTooltip place="bottom" type="dark" effect="solid" id="main" />
        <ServicePopupModal />
      </Box>
    </>
  );
}
