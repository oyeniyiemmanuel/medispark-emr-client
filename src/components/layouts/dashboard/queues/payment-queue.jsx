import { DragHandleIcon } from "@chakra-ui/icons";
import {
  Avatar,
  Badge,
  Box,
  Center,
  Divider,
  Flex,
  HStack,
  IconButton,
  Stack,
  Table,
  Tbody,
  Td,
  Text,
  Tr,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import Moment from "react-moment";
import "moment-timezone";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";
import { setConnectedChannels } from "../../../../redux/slices/commonSlice";
import {
  fetchPatients,
  setSelectedPatient,
} from "../../../../redux/slices/patientSlice";
import {
  addToQueue,
  fetchBillingQueues,
} from "../../../../redux/slices/queueSlice";
import { fetchRetainers } from "../../../../redux/slices/retainerSlice";
import theme from "../../../../theme";
import { formatDateToDDMMYY, separateWithComma } from "../../../../utils";
import Router from "next/router";
import ReactTooltip from "react-tooltip";

export default function PaymentQueue({ openFeed, ...rest }) {
  const dispatch = useDispatch();
  const { userDetails } = useSelector((state) => state.user);
  const { retainers } = useSelector((state) => state.retainers);
  const { patients } = useSelector((state) => state.patients);
  const { connectedChannels } = useSelector((state) => state.common);
  const { billingQueue, loading, error, success } = useSelector(
    (state) => state.queues
  );
  const [combinedQueue, setCombinedQueue] = useState([]);
  const toast = useToast();

  useEffect(() => {
    check.emptyArray(billingQueue) &&
      dispatch(
        fetchBillingQueues({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
          // date: "2021-03-21",
          date: formatDateToDDMMYY("yyyy-mm-dd"),
        })
      );

    //Fetch Patients
    check.emptyArray(patients) &&
      dispatch(
        fetchPatients({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );

    //Fetch Patient retainers
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
  }, []);

  useEffect(() => {
    if (window.echo && !connectedChannels.billing) {
      var channel = window.echo.channel(
        `waitlist.service.billing.${formatDateToDDMMYY("yyyy-mm-dd")}`
      );

      channel.on("pusher:subscription_succeeded", function (members) {
        dispatch(setConnectedChannels({ billing: true }));
      });

      channel.listen(".new.user.on.waitlist", function (data) {
        dispatch(addToQueue({ type: "billing", data: data.result }));
        if (data.result?.service_category == "pharmacy") {
          dispatch(addToQueue({ type: "dispensary", data: data.result }));
        }
      });
    }
  }, []);

  useEffect(() => {
    if (check.nonEmptyArray(billingQueue)) {
      let newCombinedQueue = [];
      billingQueue.map((queueItem) => {
        const findIndex = newCombinedQueue.findIndex(
          (item) => item[0]?.patient_id == queueItem?.patient_id
        );
        if (findIndex >= 0) {
          console.log(findIndex);
          newCombinedQueue[findIndex]?.push(queueItem);
        } else {
          newCombinedQueue.push([queueItem]);
        }
        setCombinedQueue(newCombinedQueue);
      });
    }
  }, [billingQueue.length]);

  return (
    <Box {...rest}>
      {/* <Stack
        // as="a"
        // href="/patients/12"
        cursor="pointer"
        pt={4}
        px={4}
        // onClick={() => Router.push("/patients/12")}
        _hover={{ bg: "gray.30" }}
        spacing={4}
      >
        <Flex>
          <HStack spacing={4} align="start" w="full">
            <Center boxSize={10} rounded={4} bg="green.100" color="green.500">
              <Text fontSize="sm">Rcpt.</Text>
            </Center>
            <Box>
              <Text fontWeight="bold" color="primary.600">
                Richard Mofe Damijo
              </Text>
              <HStack fontWeight="bold" color="gray.600">
                <Text>
                  <Box as="span" fontSize={12} color="green.500">
                    MANSARD
                  </Box>
                  &nbsp;#200,000
                </Text>
              </HStack>
            </Box>
          </HStack>
          <IconButton
            icon={<DragHandleIcon />}
            variant="default"
            cursor="grab"
            size="sm"
            color="gray.500"
            fontSize="xs"
          />
        </Flex>
        <Flex w="full" align="baseline" justify="space-between">
          <HStack>
            <Checkbox
              color="green.500"
              size="sm"
              colorScheme="green"
              defaultIsChecked
            >
              Payment Received
            </Checkbox>
          </HStack>
          <Text color="gray.500" fontSize={12}>
            10w Ago
          </Text>
        </Flex>

        <Divider />
      </Stack> */}
      {loading === "FETCH_BILLING_QUEUE" && ( // While loading billing queues, show spinner
        <Flex w="full" justify="center">
          <BeatLoader size={8} color={theme.colors.primary[500]} />
        </Flex>
      )}
      {loading !== "FETCH_BILLING_QUEUE" &&
        check.emptyArray(billingQueue) &&
        openFeed && (
          <Flex pt={6} w="full" justify="center">
            <Text color="gray.500">No Tasks on this queue yet</Text>
          </Flex>
        )}
      {[...combinedQueue].reverse().map((eachList) => {
        const { id, patient_id, patient_retainer_id } = eachList[0];

        return (
          <Box key={id}>
            {/* <ReactTooltip
              multiline
              place="left"
              type="dark"
              effect="solid"
              id="main"
            /> */}
            {openFeed ? (
              <Stack
                as="a"
                onClick={(event) => {
                  event.preventDefault();
                  dispatch(
                    setSelectedPatient(
                      patients.find(({ id }) => id === patient_id)
                    )
                  );
                  Router.push(`/patients/${patient_id}?activity=bills`);
                }}
                href={`/patients/${patient_id}?activity=bills`}
                cursor="pointer"
                pt={4}
                px={4}
                _hover={{ bg: "gray.30" }}
                spacing={2}
              >
                <Flex>
                  <HStack spacing={4} align="start" w="full">
                    <Avatar
                      size="md"
                      showBorder={false}
                      name={
                        patients.find(({ id }) => id === patient_id)?.full_name
                      }
                      src={
                        patients.find(({ id }) => id === patient_id)?.avatar
                          ?.file_path
                      }
                    />
                    <Box>
                      <Text fontWeight="bold" color="primary.600">
                        {
                          patients.find(({ id }) => id === patient_id)
                            ?.full_name
                        }
                      </Text>
                      <Text
                        color="primary.500"
                        textTransform="uppercase"
                        fontSize="xs"
                      >
                        {
                          retainers.find(({ id }) => id === patient_retainer_id)
                            ?.name
                        }
                      </Text>
                    </Box>
                  </HStack>
                  <IconButton
                    icon={<DragHandleIcon />}
                    variant="default"
                    cursor="grab"
                    size="sm"
                    color="gray.500"
                    fontSize="xs"
                  />
                </Flex>

                <Table variant="unstyled">
                  <Tbody>
                    {[...eachList]
                      .reverse()
                      .map(
                        ({
                          created_at,
                          id,
                          service_name,
                          deposit_request_reason,
                          deposit_request_id,
                          price,
                        }) => (
                          <Tr key={id}>
                            <Td px={0} py={1}>
                              <a
                                data-tip={`${
                                  deposit_request_id
                                    ? "Deposit request for"
                                    : ""
                                } 
                                ${service_name || deposit_request_reason}`}
                                data-for="main"
                              >
                                <Text
                                  noOfLines={1}
                                  fontSize={12}
                                  color="gray.500"
                                >
                                  {deposit_request_id
                                    ? "Deposit request for"
                                    : ""}{" "}
                                  {service_name || deposit_request_reason}
                                </Text>
                              </a>
                            </Td>
                            <Td px={3} py={1} isNumeric>
                              <Text
                                fontSize={12}
                                color="gray.600"
                                fontWeight="bold"
                              >
                                ₦{separateWithComma(price)}
                              </Text>
                            </Td>
                            <Td px={0} py={1}>
                              <Text color="gray.500" isTruncated fontSize={12}>
                                <Moment fromNow ago>
                                  {new Date(created_at)}
                                </Moment>
                              </Text>
                            </Td>
                          </Tr>
                        )
                      )}
                  </Tbody>
                </Table>

                <Divider />
              </Stack>
            ) : (
              <Flex py={3} w="full" justify="center">
                <Avatar
                  boxSize={10}
                  showBorder={false}
                  name={patients.find(({ id }) => id === patient_id)?.full_name}
                  src={
                    patients.find(({ id }) => id === patient_id)?.avatar
                      ?.file_path
                  }
                />
              </Flex>
            )}
          </Box>
        );
      })}
    </Box>
  );
}
