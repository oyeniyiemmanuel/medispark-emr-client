import { DragHandleIcon } from "@chakra-ui/icons";
import {
  Avatar,
  Badge,
  Box,
  Center,
  Divider,
  Flex,
  HStack,
  IconButton,
  Stack,
  Text,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import Moment from "react-moment";
import "moment-timezone";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";
import {
  openPopup,
  setConnectedChannels,
} from "../../../../redux/slices/commonSlice";
import {
  fetchPatients,
  setSelectedPatient,
} from "../../../../redux/slices/patientSlice";
import {
  addToQueue,
  fetchDispensaryQueues,
} from "../../../../redux/slices/queueSlice";
import { fetchRetainers } from "../../../../redux/slices/retainerSlice";
import theme from "../../../../theme";
import { formatDateToDDMMYY, separateWithComma } from "../../../../utils";
import { fetchPrescriptions } from "../../../../redux/slices/prescriptionSlice";
import Router from "next/router";

export default function DispensaryQueue({ openFeed, ...rest }) {
  const dispatch = useDispatch();
  const { userDetails } = useSelector((state) => state.user);
  const { retainers } = useSelector((state) => state.retainers);
  const { patients } = useSelector((state) => state.patients);
  const { connectedChannels } = useSelector((state) => state.common);
  const { dispensaryQueue, loading, error, success } = useSelector(
    (state) => state.queues
  );
  const { staff } = useSelector((state) => state.staff);
  const toast = useToast();

  useEffect(() => {
    check.emptyArray(dispensaryQueue) &&
      dispatch(
        fetchDispensaryQueues({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
          date: formatDateToDDMMYY("yyyy-mm-dd"),
          service_category: "pharmacy",
        })
      );

    //Fetch Patients
    check.emptyArray(patients) &&
      dispatch(
        fetchPatients({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );

    //Fetch Patient retainers
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
  }, []);

  // useEffect(() => {
  //   if (window.echo && !connectedChannels.dispensary) {
  //     var echoChannel = window.echo.channel(
  //       `waitlist.service.billing.${formatDateToDDMMYY("yyyy-mm-dd")}`
  //     );

  //     echoChannel.on("pusher:subscription_succeeded", function (members) {
  //       dispatch(setConnectedChannels({ dispensary: true }));
  //     });

  //     echoChannel.listen(".new.user.on.waitlist", function (data) {
  //       console.log(data.result);
  //       if (data.result?.service_category == "pharmacy") {
  //         dispatch(addToQueue({ type: "dispensary", data: data.result }));
  //       }
  //     });
  //   }
  // }, []);

  return (
    <Box {...rest}>
      {loading === "FETCH_DISPENSARY_QUEUE" && ( // While loading dispensary queues, show spinner
        <Flex w="full" justify="center">
          <BeatLoader size={8} color={theme.colors.primary[500]} />
        </Flex>
      )}
      {loading !== "FETCH_DISPENSARY_QUEUE" &&
        check.emptyArray(dispensaryQueue) &&
        openFeed && (
          <Flex pt={6} w="full" justify="center">
            <Text color="gray.500">No Tasks on this queue yet</Text>
          </Flex>
        )}
      {[...dispensaryQueue]
        .reverse()
        .map(
          ({
            branch_id,
            created_at,
            id,
            organization_id,
            patient_id,
            staff_id,
            patient_retainer_id,
            service_category,
            service_duration,
            service_id,
            service_name,
            service_user_id,
            comment,
            status,
            price,
            updated_at,
          }) => (
            <Box key={id}>
              {openFeed ? (
                <Stack
                  as="a"
                  cursor="pointer"
                  pt={4}
                  px={4}
                  onClick={(event) => {
                    event.preventDefault();
                    dispatch(
                      setSelectedPatient(
                        patients.find(({ id }) => id === patient_id)
                      )
                    );

                    dispatch(
                      fetchPrescriptions({
                        serviceuser_id: service_user_id,
                      })
                    );
                    dispatch(
                      openPopup({
                        popupType: "Dispensary",
                        popupId: id,
                      })
                    );
                    Router.push(`/patients/${patient_id}?activity=treatments`);
                  }}
                  href={`/patients/${patient_id}?activity=treatments`}
                  _hover={{ bg: "gray.30" }}
                  spacing={1}
                >
                  <Flex>
                    <HStack spacing={4} align="start" w="full">
                      <Avatar
                        size="md"
                        showBorder={false}
                        name={
                          patients.find(({ id }) => id === patient_id)
                            ?.full_name
                        }
                        src={
                          patients.find(({ id }) => id === patient_id)?.avatar?.file_path
                        }
                      />
                      <Box>
                        <Text fontWeight="bold" color="primary.600">
                          {
                            patients.find(({ id }) => id === patient_id)
                              ?.full_name
                          }
                        </Text>
                        <Text
                          color="primary.500"
                          textTransform="uppercase"
                          fontSize="xs"
                        >
                          {
                            retainers.find(
                              ({ id }) => id === patient_retainer_id
                            )?.name
                          }
                        </Text>
                        {/* <Text fontSize="sm" noOfLines={1} color="gray.500">
                          8 Prescriptions in total
                        </Text> */}
                      </Box>
                    </HStack>
                    <IconButton
                      icon={<DragHandleIcon />}
                      variant="default"
                      cursor="grab"
                      size="sm"
                      color="gray.500"
                      fontSize="xs"
                    />
                  </Flex>
                  {/* <Flex pl={16} w="full">
                    <Text color="gray.600" fontWeight="bold">
                      ₦{separateWithComma(price)}
                    </Text>
                  </Flex> */}

                  <Flex
                    pl={16}
                    w="full"
                    align="baseline"
                    justify="space-between"
                  >
                    <Text fontSize="xs" color="gray.600">
                      Prescribed by:{" "}
                      <Box as="span" fontWeight="bold">
                        {staff.find(({ id }) => id === staff_id)?.full_name}
                      </Box>
                    </Text>
                    <Text color="gray.500" fontSize={12}>
                      <Moment fromNow>{new Date(created_at)}</Moment>
                    </Text>
                  </Flex>

                  <Divider />
                </Stack>
              ) : (
                <Flex py={3} w="full" justify="center">
                  <Avatar
                    boxSize={10}
                    showBorder={false}
                    name={
                      patients.find(({ id }) => id === patient_id)?.full_name
                    }
                    src={patients.find(({ id }) => id === patient_id)?.avatar?.file_path}
                  />
                </Flex>
              )}
            </Box>
          )
        )}
    </Box>
  );
}
