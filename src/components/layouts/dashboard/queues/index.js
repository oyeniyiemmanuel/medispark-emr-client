import {
  Box,
  Button,
  Flex,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Text,
} from "@chakra-ui/react";
import "moment-timezone";
import React, { useState } from "react";
import {
  BiCalendarEvent,
  BiChevronDown,
  BiChevronLeft,
  BiChevronRight,
} from "react-icons/bi";
import { MdUnfoldMore } from "react-icons/md";
import Moment from "react-moment";
import { useSelector } from "react-redux";
import ConsultationQueue from "./consultation-queue";
import DispensaryQueue from "./dispensary-queue";
import PaymentQueue from "./payment-queue";

export default function FeedBoard({ openFeed, setOpenFeed, ...rest }) {
  const { userDetails } = useSelector((state) => state.user);

  const showQueueTypeByRole = (role) => {
    switch (role) {
      case "front-desk":
        return "Consultation Queue";

      case "billing":
        return "Bills Queue";

      case "procedure":
        return "Procedure Queue";

      case "pharmacy":
        return "Dispensary Queue";

      default:
        return "Consultation Queue";
    }
  };

  const allRoles = ["front-desk", "billing", "procedure", "pharmacy"];

  const [department, setDepartment] = useState(
    showQueueTypeByRole(userDetails.roles && userDetails.roles[0])
  );

  return (
    //Make expanded or hidden states of Feed board to be responsive based on the state of 'openFeed'
    <Box
      w={openFeed ? [300, 300, 300, 300, 370] : 16}
      transition="0.2s all"
      zIndex={[29, 29, 0]}
      shadow={["md", "md", "none"]}
      h={[openFeed ? "100vh" : "initial", "100vh"]}
      pos="fixed"
      overflowY="scroll"
      className="container-body"
      right={0}
      borderLeft="1px solid lightgrey"
      mt={16}
      pb={20}
      bg="white"
      {...rest}
    >
      <Flex justify="space-between" p={4}>
        {openFeed && (
          <Menu>
            <MenuButton
              as={Button}
              variant="ghost"
              leftIcon={<BiChevronDown />}
            >
              {department}
            </MenuButton>
            <MenuList>
              {
                // userDetails?.roles?
                allRoles.map((role, index) => (
                  <MenuItem
                    onClick={() => setDepartment(showQueueTypeByRole(role))}
                    key={index}
                  >
                    {showQueueTypeByRole(role)}
                  </MenuItem>
                ))
              }
            </MenuList>
          </Menu>
        )}
        <IconButton
          size="sm"
          fontSize="xl"
          variant="ghost"
          onClick={() => setOpenFeed(!openFeed)}
          icon={openFeed ? <BiChevronRight /> : <BiChevronLeft />}
        />
      </Flex>

      <Flex
        borderTop="1px solid lightgrey"
        borderBottom="1px solid lightgrey"
        d={[openFeed ? "flex" : "none", "flex"]}
        h={8}
        px={4}
        bg="gray.30"
      >
        {openFeed && (
          <Flex
            w="full"
            color="gray.400"
            justify="space-between"
            align="center"
          >
            <Text fontSize="sm">
              <Moment format="MMMM D, YYYY ">
                {new Date().toDateString()}
              </Moment>
            </Text>
            <HStack>
              <IconButton
                size="sm"
                variant="default"
                icon={<BiCalendarEvent />}
                fontSize="lg"
              />
              <IconButton
                size="sm"
                variant="default"
                icon={<MdUnfoldMore />}
                fontSize="lg"
              />
            </HStack>
          </Flex>
        )}
      </Flex>

      <Stack pt={2} spacing={0} w="full">
        <ConsultationQueue
          display={department === "Consultation Queue" ? "initial" : "none"}
          openFeed={openFeed}
        />
        <PaymentQueue
          display={department === "Bills Queue" ? "initial" : "none"}
          openFeed={openFeed}
        />
        <DispensaryQueue
          display={department === "Dispensary Queue" ? "initial" : "none"}
          openFeed={openFeed}
        />
      </Stack>
    </Box>
  );
}
