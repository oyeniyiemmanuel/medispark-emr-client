import { AiOutlineDoubleLeft, AiOutlineDoubleRight } from "react-icons/ai";
import {
  Avatar,
  Badge,
  Box,
  Button,
  Center,
  Checkbox,
  Divider,
  Flex,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Text,
} from "@chakra-ui/react";
import React, { useState } from "react";
import Moment from "react-moment";
import "moment-timezone";
import { DragHandleIcon, Icon } from "@chakra-ui/icons";
import {
  BiCalendarEvent,
  BiChevronDown,
  BiChevronLeft,
  BiChevronRight,
} from "react-icons/bi";
import { MdUnfoldMore } from "react-icons/md";
import Router from "next/router";

const PatientQueueItems = () =>
  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((index) => (
    <Stack
      as="a"
      href="/patients/12"
      cursor="pointer"
      pt={4}
      px={4}
      // onClick={() => Router.push("/patients/12")}
      key={index}
      _hover={{ bg: "gray.30" }}
      spacing={4}
    >
      <Flex>
        <HStack align="start" w="full">
          <Avatar
            size="md"
            name="Richard Mofe Damijo"
            showBorder={false}
            src="https://bit.ly/prosper-baba"
          />
          <Stack spacing={0}>
            <Text color="primary.500">Richard Mofe Damijo</Text>
            <Text color="primary.500" fontSize="xs">
              RETAINER
            </Text>
            <Text fontSize="sm" noOfLines={1} color="gray.400">
              Short description of what may be the situation of the application
            </Text>
          </Stack>
        </HStack>
        <IconButton
          icon={<DragHandleIcon />}
          variant="default"
          cursor="grab"
          size="sm"
          color="gray.500"
          fontSize="xs"
        />
      </Flex>
      <Divider />
    </Stack>
  ));
const PaymentQueueItems = () => (
  <>
    <Stack
      // as="a"
      // href="/patients/12"
      cursor="pointer"
      pt={4}
      px={4}
      // onClick={() => Router.push("/patients/12")}
      _hover={{ bg: "gray.30" }}
      spacing={4}
    >
      <Flex>
        <HStack spacing={4} align="start" w="full">
          <Center boxSize={10} rounded={4} bg="green.100" color="green.500">
            <Text fontSize="sm">Rcpt.</Text>
          </Center>
          <Box>
            <Text fontWeight="bold" color="primary.600">
              Richard Mofe Damijo
            </Text>
            <HStack fontWeight="bold" color="gray.600">
              <Text>
                <Box as="span" fontSize={12} color="green.500">
                  MANSARD
                </Box>
                &nbsp;#200,000
              </Text>
            </HStack>
          </Box>
        </HStack>
        <IconButton
          icon={<DragHandleIcon />}
          variant="default"
          cursor="grab"
          size="sm"
          color="gray.500"
          fontSize="xs"
        />
      </Flex>
      <Flex w="full" align="baseline" justify="space-between">
        <HStack>
          <Checkbox
            color="green.500"
            size="sm"
            colorScheme="green"
            defaultIsChecked
          >
            Payment Received
          </Checkbox>
        </HStack>
        <Text color="gray.500" fontSize={12}>
          10w Ago
        </Text>
      </Flex>

      <Divider />
    </Stack>

    <Stack
      as="a"
      // href="/patients/12"
      cursor="pointer"
      pt={4}
      px={4}
      // onClick={() => Router.push("/patients/12")}
      _hover={{ bg: "gray.30" }}
      spacing={4}
    >
      <Flex>
        <HStack spacing={4} align="start" w="full">
          <Center boxSize={10} rounded={4} bg="orange.100" color="orange.500">
            <Text fontSize="sm">Inv.</Text>
          </Center>
          <Box>
            <Text fontWeight="bold" color="primary.600">
              Richard Mofe Damijo
            </Text>
            <HStack fontWeight="bold" color="gray.600">
              <Text>
                <Box as="span" fontSize={12} color="green.500">
                  RECEIPT 124
                </Box>
                &nbsp;#200,000
              </Text>
            </HStack>
            <Badge
              colorScheme="blue"
              textTransform="capitalize"
              color="primary.500"
              fontWeight="normal"
            >
              Part-Payment
            </Badge>
          </Box>
        </HStack>
        <IconButton
          icon={<DragHandleIcon />}
          variant="default"
          cursor="grab"
          size="sm"
          color="gray.500"
          fontSize="xs"
        />
      </Flex>
      <Flex w="full" align="baseline" justify="space-between">
        <HStack></HStack>
        <Text color="gray.500" fontSize={12}>
          10w Ago
        </Text>
      </Flex>

      <Divider />
    </Stack>
  </>
);

export default function QueueItems({ department }) {
  return (
    <>
      {department === "Consultation Queues" && <PatientQueueItems />}
      {department === "Payment Queues" && <PaymentQueueItems />}
    </>
  );
}
