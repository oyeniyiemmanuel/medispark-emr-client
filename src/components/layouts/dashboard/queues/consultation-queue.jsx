import { DragHandleIcon } from "@chakra-ui/icons";
import {
  Avatar,
  Box,
  Center,
  Divider,
  Flex,
  HStack,
  IconButton,
  Stack,
  Text,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import "moment-timezone";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";
import { setConnectedChannels } from "../../../../redux/slices/commonSlice";
import {
  fetchPatients,
  setSelectedPatient,
} from "../../../../redux/slices/patientSlice";
import {
  addToQueue,
  fetchConsultationQueues,
} from "../../../../redux/slices/queueSlice";
import { fetchRetainers } from "../../../../redux/slices/retainerSlice";
import theme from "../../../../theme";
import { formatDateToDDMMYY } from "../../../../utils";

export default function ConsultationQueue({ openFeed, ...rest }) {
  const dispatch = useDispatch();
  const { userDetails } = useSelector((state) => state.user);
  const { retainers } = useSelector((state) => state.retainers);
  const { patients } = useSelector((state) => state.patients);
  const { connectedChannels } = useSelector((state) => state.common);
  const { consultationQueue, loading, error, success } = useSelector(
    (state) => state.queues
  );
  const toast = useToast();

  useEffect(() => {
    //Fetch Patients

    check.emptyArray(consultationQueue) &&
      dispatch(
        fetchConsultationQueues({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
          date: formatDateToDDMMYY("yyyy-mm-dd"),
          service_category: "consultation",
        })
      );

    check.emptyArray(patients) &&
      dispatch(
        fetchPatients({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );

    //Fetch Patient retainers
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
  }, []);

  useEffect(() => {
    if (window.echo && !connectedChannels.consultation) {
      var channel = window.echo.channel(
        `waitlist.service.consultation.${formatDateToDDMMYY("yyyy-mm-dd")}`
      );

      channel.on("pusher:subscription_succeeded", function (members) {
        dispatch(setConnectedChannels({ consultation: true }));
      });

      channel.listen(".new.user.on.waitlist", function (data) {
        dispatch(addToQueue({ type: "consultation", data: data.result }));
      });
    }
  }, []);

  return (
    <Box {...rest}>
      {loading === "FETCH_CONSULTATION_QUEUE" && ( // While loading appointments, show spinner
        <Flex w="full" justify="center">
          <BeatLoader size={8} color={theme.colors.primary[500]} />
        </Flex>
      )}
      {loading !== "FETCH_CONSULTATION_QUEUE" &&
        check.emptyArray(consultationQueue) &&
        openFeed && (
          <Flex pt={6} w="full" justify="center">
            <Text color="gray.500">No Tasks on this queue yet</Text>
          </Flex>
        )}
      {[...consultationQueue]
        .reverse()
        .map(
          ({
            branch_id,
            created_at,
            id,
            organization_id,
            patient_id,
            patient_retainer_id,
            service_category,
            service_duration,
            service_id,
            service_name,
            service_user_id,
            comment,
            status,
            updated_at,
          }) => (
            <Box key={id}>
              {openFeed ? (
                <Stack
                  as="a"
                  onClick={(event) => {
                    event.preventDefault();
                    dispatch(
                      setSelectedPatient(
                        patients.find(({ id }) => id === patient_id)
                      )
                    );
                    Router.push(`/patients/${patient_id}`);
                  }}
                  href={`/patients/${patient_id}`}
                  cursor="pointer"
                  pt={4}
                  px={4}
                  _hover={{ bg: "gray.30" }}
                  spacing={4}
                >
                  <Flex>
                    <HStack align="start" w="full">
                      <Avatar
                        size="md"
                        showBorder={false}
                        name={
                          patients.find(({ id }) => id === patient_id)
                            ?.full_name
                        }
                        src={
                          patients.find(({ id }) => id === patient_id)?.avatar
                            ?.file_path
                        }
                      />
                      <Stack spacing={0}>
                        <Text color="primary.500">
                          {
                            patients.find(({ id }) => id === patient_id)
                              ?.full_name
                          }
                        </Text>
                        <Text
                          color="primary.500"
                          textTransform="uppercase"
                          fontSize="xs"
                        >
                          {
                            retainers.find(
                              ({ id }) => id === patient_retainer_id
                            )?.name
                          }
                        </Text>
                        <Text
                          pt={2}
                          fontSize="sm"
                          noOfLines={1}
                          color="gray.500"
                        >
                          {service_name}
                        </Text>
                        <Text fontSize="sm" noOfLines={1} color="gray.400">
                          {comment}
                        </Text>
                      </Stack>
                    </HStack>
                    <IconButton
                      icon={<DragHandleIcon />}
                      variant="default"
                      cursor="grab"
                      size="sm"
                      onClick={(event) => {
                        event.preventDefault();
                        event.stopPropagation();
                      }}
                      color="gray.500"
                      fontSize="xs"
                    />
                  </Flex>
                  <Divider />
                </Stack>
              ) : (
                <Flex py={3} w="full" justify="center">
                  <Avatar
                    boxSize={10}
                    showBorder={false}
                    name={
                      patients.find(({ id }) => id === patient_id)?.full_name
                    }
                    src={patients.find(({ id }) => id === patient_id)?.avatar?.file_path}
                  />
                </Flex>
              )}
            </Box>
          )
        )}
    </Box>
  );
}
