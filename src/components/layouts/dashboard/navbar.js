import {
  Avatar,
  Box,
  Button,
  Center,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
  useMediaQuery,
} from "@chakra-ui/react";
import Link from "next/link";
import Router from "next/router";
import React, { memo, useEffect } from "react";
import EMRLogo, { EMRIcon } from "../../logo";
import { RiSettings4Line } from "react-icons/ri";
import { BiLogOut } from "react-icons/bi";
import { IoMdSettings } from "react-icons/io";
import { MdNotifications } from "react-icons/md";
import { AiFillCaretDown, AiOutlineInfoCircle } from "react-icons/ai";
import {
  fetchUserDetails,
  setUserDetails,
} from "../../../redux/slices/userSlice";
import { useDispatch, useSelector } from "react-redux";
import { DragHandleIcon, Icon } from "@chakra-ui/icons";
import check from "check-types";
import { fetchOrganizationDetails } from "../../../redux/slices/organizationSlice";

function DashboardNavbar({ ...rest }) {
  const [isOverflowing] = useMediaQuery("(max-width: 991px)");
  const dispatch = useDispatch();
  const { userDetails, loading, error, success } = useSelector(
    (state) => state.user
  );
  const { organizationDetails } = useSelector((state) => state.organization);

  useEffect(() => {
    if (check.emptyObject(organizationDetails)) {
      dispatch(
        fetchOrganizationDetails({
          organization_id: userDetails.organization_id,
        })
      );
    }
  }, [userDetails]);

  const logout = () => {
    // Perform Logout Action
    localStorage.removeItem("medispark_acccess_token");
    dispatch(setUserDetails({}));
    Router.push("/login");
  };
  return (
    <Box pos="fixed" w="full" zIndex={30} top={0} {...rest}>
      <Flex align="center" color="white">
        <IconButton
          icon={<EMRIcon white width="30px" />}
          colorScheme="primary"
          color="white"
          fontSize="lg"
          h={16}
          onClick={() => Router.push("/")}
          w={16}
          rounded={0}
          bg="primary.600"
        />
        <Flex
          w="full"
          py={3}
          pr="4%"
          pl={4}
          bg="primary.500"
          align="center"
          color="white"
          justify={["flex-end", "space-between"]}
        >
          <HStack
            d={["none", "flex"]}
            color="white"
            className={isOverflowing ? "container-body" : ""}
            overflowX={isOverflowing ? "scroll" : "hidden"}
            overflowY="hidden"
            fontSize={isOverflowing ? "md" : "xl"}
            spacing={isOverflowing ? 5 : 7}
            pb={2}
            px={4}
          >
            <Link href="/patients">
              <Text cursor="pointer">Clients</Text>
            </Link>
            <Link href="/schedules">
              <Text cursor="pointer">Schedules</Text>
            </Link>
            <Link href="#">
              <Text cursor="pointer">Departments</Text>
            </Link>
            <Link href="#">
              <Text cursor="pointer">Analytics</Text>
            </Link>
            <Link href="#">
              <Text cursor="pointer">Accounts</Text>
            </Link>
            <Link href="#">
              <Text cursor="pointer">Admin</Text>
            </Link>
          </HStack>

          <HStack spacing={4} align="center">
            <HStack spacing={2} color="white" d={["none", "flex"]}>
              <IconButton
                fontSize="2xl"
                variant="default"
                icon={<MdNotifications />}
              />
              <IconButton
                fontSize="2xl"
                variant="default"
                icon={<AiOutlineInfoCircle />}
              />
            </HStack>
            <Divider
              d={["none", "block"]}
              orientation="vertical"
              color="white"
              h="30px"
            />

            <Box>
              <Menu closeOnBlur>
                <MenuButton
                  variant="ghost"
                  colorScheme="default"
                  color="white"
                  as={Button}
                >
                  <HStack align="center">
                    {userDetails?.first_name && (
                      <Avatar
                        size="sm"
                        name={`${userDetails?.first_name} ${userDetails?.last_name}`}
                        src={userDetails?.image}
                      />
                    )}
                    <Text textTransform="capitalize">
                      {userDetails?.first_name}
                    </Text>
                    <Icon as={AiFillCaretDown} />
                  </HStack>
                </MenuButton>
                <MenuList placement="bottom-end">
                  <MenuItem p={0}>
                    <Button
                      justifyContent="left"
                      color="gray.600"
                      isFullWidth
                      leftIcon={<RiSettings4Line />}
                      // onClick={() => Router.push("/settings")}
                      variant="ghost"
                    >
                      Settings
                    </Button>
                  </MenuItem>
                  <MenuItem p={0}>
                    <Button
                      justifyContent="left"
                      colorScheme="red"
                      isFullWidth
                      onClick={logout}
                      leftIcon={<BiLogOut />}
                      variant="ghost"
                    >
                      Logout
                    </Button>
                  </MenuItem>
                </MenuList>
              </Menu>
            </Box>
          </HStack>
        </Flex>
      </Flex>
    </Box>
  );
}

export default memo(DashboardNavbar);
