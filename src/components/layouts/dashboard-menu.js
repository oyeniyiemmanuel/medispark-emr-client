import {
  Box,
  Flex,
  Heading,
  Icon,
  IconButton,
  Image,
  Link as ChakraLink,
  Stack,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import Link from "next/link";
import Router from "next/router";
import React, { useEffect } from "react";
import { BsChatDots, BsFillCalendarFill } from "react-icons/bs";
import { FaHandHoldingHeart } from "react-icons/fa";
import { GiHealthPotion } from "react-icons/gi";
import { IoMdPeople } from "react-icons/io";
import { BiFileFind, BiHome } from "react-icons/bi";
import {
  HiOutlineBriefcase,
  HiOutlineDotsVertical,
  HiOutlineUsers,
} from "react-icons/hi";
import { useDispatch, useSelector } from "react-redux";
import { FiSettings } from "react-icons/fi";
// import { fetchUserDetails } from "../redux/slices/userSlice";

export default function DashboardMenu({ active, isMobile, ...rest }) {
  // const userDetails = useSelector((state) => state.user.userDetails);
  // const { loading, error, success } = useSelector((state) => state.user);
  // const dispatch = useDispatch();
  // const toast = useToast();

  // useEffect(() => {
  //   if (error) {
  //     toast({
  //       title: "An error occurred",
  //       description: error.errorMessage && error.errorMessage?.data?.error,
  //       status: "error",
  //       duration: 4000,
  //       position: "bottom-left",
  //       isClosable: true,
  //     });
  //   }
  // }, [error]);
  // useEffect(() => {
  //   check.emptyObject(userDetails) && dispatch(fetchUserDetails());
  // }, []);

  const menu = [
    {
      name: "Pharmacy",
      link: "/pharmacy",
      icon: BiHome,
    },
    {
      name: "Finance",
      link: "/finance",
      icon: HiOutlineBriefcase,
    },
    {
      name: "Find a Feature",
      link: "/faf",
      icon: BiFileFind,
    },
    {
      name: "Memos",
      link: "/memos",
      icon: BsChatDots,
    },
    {
      name: "Patients",
      link: "/patients",
      icon: HiOutlineUsers,
    },
    {
      name: "Settings",
      link: "/settings",
      icon: FiSettings,
    },
  ];

  return (
    <Flex {...rest} h="calc(100% - 148px)" align="center">
      <Stack w="full" spacing={[2, 2, 4]}>
        <Box py={5} px={4} bg="white" rounded={16}>
          {/* {check.nonEmptyObject(userDetails) && ( */}
          <Stack align="center" isInline justify="space-between">
            <Stack pl={0} cursor="pointer" align="center" isInline spacing={3}>
              {/* {userDetails.profile?.image && ( */}
              <Image
                objectFit="cover"
                rounded={8}
                boxSize={10}
                src={"https://picsum.photos/200/300"}
              />
              {/* )} */}

              <Stack spacing={1}>
                <Heading fontSize="md">{`Rosa Name`}</Heading>
                <ChakraLink
                  color="gray.400"
                  href="mailto:info@rosa.com"
                  fontSize="sm"
                >
                  info@rosa.com
                </ChakraLink>
              </Stack>
            </Stack>
            <IconButton icon={<HiOutlineDotsVertical />} />
          </Stack>
          {/* )} */}
        </Box>
        <Box py={[6, 6, 8]} bg="white" rounded={16}>
          <Stack spacing={[4, 4, 4, 4, 0]}>
            {menu.map(({ name, link, icon }, index) => (
              <Flex
                key={index}
                transition="0.4s all"
                cursor="pointer"
                onClick={() => Router.push(link)}
                _hover={{ bg: "gray.100" }}
                bg={index + 1 === active ? "purple.50" : "transparent"}
              >
                <Flex
                  h="2.8rem"
                  w={1}
                  bg={index + 1 === active ? "purple.500" : "transparent"}
                ></Flex>
                <Stack py={3} px={4} align="center" isInline spacing={3}>
                  <Icon
                    color={index + 1 === active ? "purple.500" : "gray.400"}
                    fontSize="xl"
                    as={icon}
                  />
                  <Heading
                    fontWeight={index + 1 === active ? "bold" : "normal"}
                    fontSize="md"
                    color={index + 1 === active ? "purple.500" : "gray.400"}
                  >
                    {name}
                  </Heading>
                </Stack>
              </Flex>
            ))}
          </Stack>
        </Box>
      </Stack>
    </Flex>
  );
}
