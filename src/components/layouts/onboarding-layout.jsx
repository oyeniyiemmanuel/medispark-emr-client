import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  Stack,
  Text,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import React from "react";
import { GiHelp } from "react-icons/gi";
import EMRLogo from "../logo";

export default function OnboardingLayout({ active, children, ...rest }) {
  return (
    <Box {...rest} w="full" p={["6%", "10%", 7]}>
      <Button
        pos="absolute"
        right={[4, 16]}
        top={[10, 7]}
        variant="link"
        fontWeight="normal"
        colorScheme="primary"
        rightIcon={<GiHelp />}
      >
        Need Help
      </Button>
      <HStack spacing={[0, 0, 20]} align="center">
        <Box
          w="370px"
          display={["none", "none", "block"]}
          py={8}
          h="calc(100vh - 3.5rem)"
          bg="primary.500"
          rounded={8}
          px={12}
          pos="relative"
        >
          <EMRLogo mt={8} variant="white" pos="absolute" top={0} />
          <Flex h="full" align="center">
            <Stack color="white" spacing={12}>
              <Heading fontWeight="normal">
                Create your Organization in just a few Clicks
              </Heading>
              <Text fontSize="xl">
                Unlock the mysteries of the medical world, the power of digital
                medical records, at the tip of your fingers.
              </Text>
            </Stack>
          </Flex>
        </Box>
        {children}
      </HStack>
    </Box>
  );
}
