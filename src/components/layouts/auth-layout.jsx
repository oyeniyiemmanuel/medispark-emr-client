import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  Image,
  Link as ChakraLink,
  Stack,
  Text,
  useMediaQuery,
} from "@chakra-ui/react";
import React from "react";
import EMRLogo from "../logo";
import Link from "next/link";
import { AddIcon } from "@chakra-ui/icons";

export default function AuthLayout({ active, children, ...rest }) {
  const [isMobile] = useMediaQuery("(max-width: 767px)");
  const [isSmallMobile] = useMediaQuery("(max-height: 570px)");

  const AuthNav = ({ active, ...rest }) => (
    <Flex
      align="start"
      justify="space-between"
      px={["6%", "9%", "13%"]}
      // bg="gray.200"
      py={8}
      pos="absolute"
      top={0}
      w="full"
      {...rest}
    >
      <EMRLogo variant={isMobile ? "default" : "white"} />

      {/* <HStack d={["none", "flex"]} spacing={[4, 8, 16]} align="start">
        {authLinks.map(({ href, id, text }) => (
          <Link key={id} href={href}>
            <ChakraLink
              borderBottom={active === id ? "4px solid" : 0}
              borderBottomColor="primary.500"
              pb={4}
              fontWeight={active === id ? "bold" : "normal"}
              textDecoration="none !important"
              color="primary.500"
            >
              {text}
            </ChakraLink>
          </Link>
        ))}
      </HStack> */}
    </Flex>
  );

  return (
    <Box {...rest} minH="100vh" pos="relative">
      <AuthNav active={active} />
      <Flex
        justify="center"
        align="center"
        minH={[isSmallMobile ? "130vh" : "100vh", "100vh", "auto"]}
      >
        <Flex align="center" w="full" px={["6%", "9%"]}>
          <Image
            d={["none", "none", "block"]}
            maxW="50%"
            objectFit="cover"
            // h="calc(100vh - 80px)"
            src="/images/onboarding-doctor.png"
          />
          <Box
            px={[5, 12, 10, 10, 16]}
            py={[12, 12, 8, 10, 12]}
            //   mb={10}
            w="full"
            maxW="635px"
            bg="gray.50"
          >
            {children}
          </Box>
        </Flex>
      </Flex>

      <Flex
        // pos="absolute"
        bottom={0}
        h="80px"
        pt={3}
        fontSize="sm"
        px={["6%", "9%", "13%"]}
      >
        <HStack spacing={[4, 8, 16]}>
          <Text color="gray.400">
            © MediSpark {new Date().getFullYear()}, All Rights Reserved
          </Text>
          <Link href="/privacy-policy">
            <ChakraLink color="gray.400">Privacy Policy</ChakraLink>
          </Link>
          <Link href="/terms-and-conditions">
            <ChakraLink color="gray.400">Terms & Conditions</ChakraLink>
          </Link>
        </HStack>
      </Flex>
    </Box>
  );
}
