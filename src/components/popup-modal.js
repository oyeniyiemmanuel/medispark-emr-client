import { AddIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Divider,
  Heading,
  HStack,
  IconButton,
  Select,
  Stack,
} from "@chakra-ui/react";
import React from "react";
import { BiExpand } from "react-icons/bi";
import { MdClose } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { Rnd } from "react-rnd";
import CreateEncounterView from "../modules/patients/each-patient/encounter/create-encounter-new";
import { closePopup } from "../redux/slices/commonSlice";
import RecordNotes from "../modules/patients/each-patient/documentations/record-notes";
import RecordVitals from "../modules/patients/each-patient/documentations/record-vitals";
import EditEncounterView from "../modules/patients/each-patient/encounter/edit-encounter";
import EditNotes from "../modules/patients/each-patient/documentations/edit-notes";
import EditVitals from "../modules/patients/each-patient/documentations/edit-vitals";
import DispenseDrugs from "../modules/patients/each-patient/dispense-drugs";

export default function ServicePopupModal({ ...rest }) {
  const dispatch = useDispatch();
  const { patientTimeline } = useSelector((state) => state.patients);
  const { dispensaryQueue } = useSelector((state) => state.queues);
  const {
    popup: { isOpen, popupType, popupId, isNew },
  } = useSelector((state) => state.common);

  return isOpen ? (
    <Rnd
      default={{
        x: 700,
        y: 150,
        width: 650,
        height: "max-content",
      }}
      style={{ zIndex: 50 }}
      minWidth={650}
      minHeight="max-content"
      dragHandleClassName={"handle"}
      bounds="window"
    >
      <Box
        border="1px solid lightgrey"
        boxShadow="md"
        borderRadius={4}
        w="full"
        pb={4}
        h="full"
        bg="gray.70"
      >
        <HStack p={4} className="handle" justify="space-between">
          <Heading pl={3} fontSize="xl">
            {popupType}
          </Heading>
          <HStack>
            <IconButton
              pos="initial"
              size="sm"
              color="gray.500"
              fontSize="2xl"
              variant="ghost"
              icon={<BiExpand />}
            />
            <IconButton
              pos="initial"
              size="sm"
              color="gray.500"
              onClick={() => dispatch(closePopup())}
              fontSize="2xl"
              variant="ghost"
              icon={<MdClose />}
            />
          </HStack>
        </HStack>
        <Box px={4}>
          {popupType === "Encounter" && <CreateEncounterView />}
          {popupType === "Edit Encounter" && (
            <EditEncounterView
              encounter={patientTimeline?.encounters?.find(
                ({ id }) => id == popupId
              )}
            />
          )}

          {popupType === "Add New Note" && <RecordNotes />}
          {popupType === "Edit Note" && (
            <EditNotes
              note={patientTimeline?.notes?.find(({ id }) => id == popupId)}
            />
          )}
          {popupType === "Routine Vitals" && <RecordVitals />}
          {popupType === "Edit Vitals" && (
            <EditVitals
              vital={patientTimeline?.vitals?.find(({ id }) => id == popupId)}
            />
          )}
          {popupType === "Dispensary" && (
            <DispenseDrugs
              prescription={dispensaryQueue?.find(({ id }) => id == popupId)}
            />
          )}
        </Box>
      </Box>
    </Rnd>
  ) : null;
}
