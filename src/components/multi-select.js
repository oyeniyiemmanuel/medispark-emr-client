import React, { useState } from "react";
import Select, { components } from "react-select";
import AsyncSelect from "react-select/async";
import AsyncCreatableSelect from "react-select/async-creatable";
import appTheme from "../theme";
import makeAnimated from "react-select/animated";
import { Text } from "@chakra-ui/layout";

const customStyles = {
  control: (base) => ({
    ...base,
    border: 0,
    boxShadow: "none",
  }),
  multiValue: (provided, state) => ({
    ...provided,
    backgroundColor: appTheme.colors.primary[50],
    borderRadius: 16,
    borderColor: appTheme.colors.primary[100],
    borderWidth: "1px",
  }),
};

const { ValueContainer } = components;

const CustomValueContainer = ({ children, ...rest }) => {
  const [toExpandRecipients, setToExpandRecipients] = useState(false);

  const selectedCount = rest?.getValue().length;
  const conditional = selectedCount < 6;

  let firstFiveOptions = [];

  if (!conditional) {
    firstFiveOptions = [...[...children[0]].slice(0, 5), children[1]];
  }

  return (
    <ValueContainer {...rest}>
      {!conditional && !toExpandRecipients ? (
        <>
          {firstFiveOptions}
          <Text onClick={() => setToExpandRecipients(true)}>
            and {selectedCount - 5} others
          </Text>
        </>
      ) : (
        children
      )}
    </ValueContainer>
  );
};

export default function CustomSelect({ ...rest }) {
  // This commented code collapses the expanded recipients
  // const usePrevious = (value) => {
  //   const ref = useRef();
  //   useEffect(() => {
  //     ref.current = value;
  //   });
  //   return ref.current;
  // };

  // const prevRecipients = usePrevious(recipients);

  // useEffect(() => {
  //   if (prevRecipients < recipients) {
  //     // Your logic here
  //     setToExpandRecipients(false);
  //   }
  // }, [recipients]);

  return (
    <CustomSelectComponent
      components={{
        ValueContainer: CustomValueContainer,
      }}
      styles={customStyles}
      theme={(theme) => ({
        ...theme,
        colors: {
          ...theme.colors,
          primary25: appTheme.colors.gray[50],
          primary: appTheme.colors.primary[500],
        },
      })}
      {...rest}
    />
  );
}

export const CustomSelectComponent = ({ ...rest }) => {
  if (rest.isAsync) {
    return <AsyncSelect {...rest} />;
  }
  if (rest.isAsyncCreatable) {
    return <AsyncCreatableSelect {...rest} />;
  }
  return <Select {...rest} />;
};
