import Head from "next/head";
import React from "react";
import DashboardContainer from "../../components/layouts/dashboard";
import BillingTransactionsView from "../../modules/billing/transactions";
import { withAuth } from "../../utils/withAuth";

function BillingTransactions() {
  return (
    <>
      <Head>
        <title>Billing Transactions - MediSpark </title>
      </Head>
      <DashboardContainer>
        <BillingTransactionsView />
      </DashboardContainer>
    </>
  );
}

export default withAuth(BillingTransactions);
