import Head from "next/head";
import CreateOrganization from "../../modules/onboarding/create";

const InitiateNewRegistration = () => (
  <>
    <Head>
      <title>Welcome - MediSpark </title>
    </Head>
    <CreateOrganization />
  </>
);

export default InitiateNewRegistration;
