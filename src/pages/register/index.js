import { Box, Heading } from "@chakra-ui/react";
import Head from "next/head";
import ChooseOnboardingOption from "../../modules/onboarding";

const InitiateRegistration = () => (
  <>
    <Head>
      <title>Welcome - MediSpark </title>
    </Head>
    <ChooseOnboardingOption />
  </>
);

export default InitiateRegistration;
