import Head from "next/head";
import ShowAllPatients from "../../modules/patients/patient-list";
import { withAuth } from "../../utils/withAuth";

const PatientsList = () => (
  <>
    <Head>
      <title>Patients - MediSpark</title>
    </Head>
    <ShowAllPatients />
  </>
);

export default withAuth(PatientsList);
