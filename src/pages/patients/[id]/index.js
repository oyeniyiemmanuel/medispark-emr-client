import Head from "next/head";
import DashboardContainer from "../../../components/layouts/dashboard";
import EachPatientView from "../../../modules/patients/each-patient";
import { withAuth } from "../../../utils/withAuth";

const EachPatient = () => (
  <DashboardContainer>
    <EachPatientView />
  </DashboardContainer>
);

export default withAuth(EachPatient);
