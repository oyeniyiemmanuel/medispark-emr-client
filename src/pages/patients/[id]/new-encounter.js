import DashboardContainer from "../../../components/layouts/dashboard";
import OldCreateEncounterView from "../../../modules/patients/each-patient/encounter/create-encounter-old";
import { withAuth } from "../../../utils/withAuth";

const NewEncounter = () => (
  <DashboardContainer>
    <OldCreateEncounterView />
  </DashboardContainer>
);

export default withAuth(NewEncounter);
