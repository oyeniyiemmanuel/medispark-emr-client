import DashboardContainer from "../../../../components/layouts/dashboard";
import PatientProfileSettings from "../../../../modules/patients/each-patient/profile-settings";
import PatientProfileContainer from "../../../../modules/patients/each-patient/profile-settings/shared/profile-container";
import { withAuth } from "../../../../utils/withAuth";

const PatientProfile = () => (
  <DashboardContainer>
    <PatientProfileContainer>
      <PatientProfileSettings />
    </PatientProfileContainer>
  </DashboardContainer>
);

export default withAuth(PatientProfile);
