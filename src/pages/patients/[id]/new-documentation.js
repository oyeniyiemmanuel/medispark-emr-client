import Head from "next/head";
import DashboardContainer from "../../../components/layouts/dashboard";
import NewClerkingView from "../../../modules/patients/each-patient/documentations";
import { withAuth } from "../../../utils/withAuth";

const NewDocumentation = () => (
  <DashboardContainer>
    <NewClerkingView />
  </DashboardContainer>
);

export default withAuth(NewDocumentation);
