import DashboardContainer from "../../components/layouts/dashboard";
import { withAuth } from "../../utils/withAuth";
import CreateNewPatientView from "../../modules/patients/add-patient/add-patient-page";

const CreateNewPatient = () => (
  <DashboardContainer>
    <CreateNewPatientView />
  </DashboardContainer>
);

export default withAuth(CreateNewPatient);
