import { ChakraProvider, ColorModeProvider } from "@chakra-ui/react";
import Echo from "laravel-echo";
import Pusher from "pusher-js";
import Head from "next/head";
import Router from "next/router";
import nProgress from "nprogress";
import { useEffect, useState } from "react";
import { Provider } from "react-redux";
import { DarkModeSwitch } from "../components/dark-mode-switch";
import store from "../redux/store";
import theme from "../theme";

nProgress.configure({
  easing: "ease",
  speed: 1600,
  trickleSpeed: 5000,
  showSpinner: true,
}); //nprogress configurations

function MyApp({ Component, pageProps, router }) {
  useEffect(() => {
    window.echo = new Echo({
      broadcaster: "pusher",
      key: "d321ebf6b2a85ad58dc4",
      cluster: "eu",
      forceTLS: true,
    });
  }, []);

  if (typeof window !== "undefined") {
    var insertionQ = require("insertion-query"); //Import only after the page is fully loaded
  }

  const [toBlur, setToBlur] = useState(false);

  if (typeof window !== "undefined" && insertionQ) {
    //Automatically check if a modal has been opened and then blur the background
    insertionQ(".chakra-portal").every(function (element) {
      setToBlur(true); //callback
    });
  }

  useEffect(() => {
    window.scrollTo(0, 0); //Always start at the top when a page changes
  }, [router.route]);

  //Automatically check if a modal has been closed and then sharpen the background back to original
  function onRemove(el, callback) {
    new MutationObserver((mutations, observer) => {
      if (!document.body.contains(el)) {
        observer.disconnect();
        callback();
      }
    }).observe(document.body, { childList: true });
  }
  if (typeof window !== "undefined") {
    onRemove(document?.querySelector(".chakra-portal"), function () {
      setToBlur(false);
    });
  }
  const load = () => {
    nProgress.start(); //Start nprogress
  };

  const stop = () => {
    nProgress.done(); // Stop nprogress
  };
  useEffect(() => {
    //Listen to router events
    Router.events.on("routeChangeStart", load);
    Router.events.on("routeChangeComplete", stop);
    Router.events.on("routeChangeError", stop);

    return () => {
      Router.events.off("routeChangeStart", load);
      Router.events.off("routeChangeComplete", stop);
      Router.events.off("routeChangeError", stop);
    };
  }, []);

  return (
    <>
      <Head>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Sen&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://unpkg.com/react-quill@1.3.3/dist/quill.snow.css"
        ></link>
        <link rel="stylesheet" href="/styles/global.css" />
        <link rel="stylesheet" href="/styles/nprogress.css" />
        <link rel="icon" href="/emr-icon.png" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-title" content="MediSpark" />
      </Head>
      <ChakraProvider resetCSS theme={theme}>
        {/* Chakra UI configs */}
        <ColorModeProvider
          options={{
            initialColorMode: "light",
            useSystemColorMode: false,
          }}
        >
          <DarkModeSwitch />
          {/* Import Redux store */}
          <Provider store={store}>
            <Component {...pageProps} />
          </Provider>
        </ColorModeProvider>
      </ChakraProvider>
      <style jsx global>{`
        #__next {
          /* Blur background if toBlur is true */
          filter: ${toBlur ? "blur(10px)" : "none"};
        }
        /*
        ::-webkit-scrollbar {
          width: 0px;
        }
        ::-moz-scrollbar {
          width: 0px;
        }
*/
        .container-body::-webkit-scrollbar {
          width: 5px;
          height: 6px;
        }
        .container-body::-moz-scrollbar {
          width: 5px;
          height: 6px;
        }
        /* Track */
        .container-body::-webkit-scrollbar-track {
          background: ${theme.colors.gray[100]};
        }

        /* Handle */
        .container-body::-webkit-scrollbar-thumb {
          background: ${theme.colors.gray[300]};
          border-radius: 5px;
        }

        /* Handle on hover */
        .container-body::-webkit-scrollbar-thumb:hover {
          background: ${theme.colors.gray[400]};
        }
      `}</style>
    </>
  );
}

export default MyApp;
