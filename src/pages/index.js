import Head from "next/head";
import { useSelector } from "react-redux";
import DashboardContainer from "../components/layouts/dashboard";
import BillingDashboard from "../modules/billing/dashboard";
import FrontDeskDashboard from "../modules/front-desk/dashboard";
import { withAuth } from "../utils/withAuth";

function Index() {
  const { userDetails } = useSelector((state) => state.user);

  const showDashboardByRole = () => {
    if (userDetails?.roles) {
      switch (userDetails?.roles[0]) {
        // case "billing":
        //   return <BillingDashboard />;

        case "front-desk":
          return <FrontDeskDashboard />;

        default:
          return <FrontDeskDashboard />;
      }
    }
  };

  return (
    <>
      <Head>
        <title>Dashboard - MediSpark</title>
      </Head>
      <DashboardContainer>{showDashboardByRole()}</DashboardContainer>
    </>
  );
}

export default withAuth(Index);
