import Head from "next/head";
import LogInModule from "../modules/login";

const SignIn = () => (
  <>
    <Head>
      <title>Welcome Back - MediSpark </title>
    </Head>
    <LogInModule />
  </>
);

export default SignIn;
