import Head from "next/head";
import React from "react";
import DashboardContainer from "../../components/layouts/dashboard";
import ComposeMessageView from "../../modules/messages/compose-new";
import { withAuth } from "../../utils/withAuth";

function ComposeMessage() {
  return (
    <>
      <Head>
        <title>Compose New Message - MediSpark </title>
      </Head>
      <DashboardContainer>
        <ComposeMessageView />
      </DashboardContainer>
    </>
  );
}

export default withAuth(ComposeMessage);
