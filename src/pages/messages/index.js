import Head from "next/head";
import React from "react";
import DashboardContainer from "../../components/layouts/dashboard";
import BillingDashboard from "../../modules/billing/dashboard";
import { withAuth } from "../../utils/withAuth";

function Billing() {
  return (
    <>
      <Head>
        <title>Billing Dashboard - MediSpark </title>
      </Head>
      <DashboardContainer>
        <BillingDashboard />
      </DashboardContainer>
    </>
  );
}

export default withAuth(Billing);
