import Head from "next/head";
import React from "react";
import DashboardContainer from "../../components/layouts/dashboard";
import SchedulesView from "../../modules/schedules";
import { withAuth } from "../../utils/withAuth";

function Schedules() {
  return (
    <>
      <Head>
        <title>Schedules - MediSpark </title>
        <link
          rel="stylesheet"
          href="https://unpkg.com/react-day-picker/lib/style.css"
        />
      </Head>
      <DashboardContainer>
        <SchedulesView />
      </DashboardContainer>
    </>
  );
}
export default withAuth(Schedules);
