import Head from "next/head";
import React from "react";
import DashboardContainer from "../../components/layouts/dashboard";
import QueuesView from "../../modules/queues";
import { withAuth } from "../../utils/withAuth";

function Queues() {
  return (
    <>
      <Head>
        <title>Queues - MediSpark </title>
      </Head>
      <DashboardContainer>
        <QueuesView />
      </DashboardContainer>
    </>
  );
}
export default withAuth(Queues);
