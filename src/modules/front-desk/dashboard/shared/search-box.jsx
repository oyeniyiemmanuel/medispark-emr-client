import { SearchIcon } from "@chakra-ui/icons";
import {
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  Input,
  InputGroup,
  InputLeftElement,
  Stack,
  Text,
  useToast,
} from "@chakra-ui/react";
import { Table, Tbody, Td, Tr } from "@chakra-ui/table";
import check from "check-types";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import FilterResults from "react-filter-search";
import { useDispatch, useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";
import { openAppointmentModal } from "../../../../redux/slices/commonSlice";
import {
  clearStates,
  fetchPatients,
  setSelectedPatient,
} from "../../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../../redux/slices/retainerSlice";
import theme from "../../../../theme";
import { calculateAge, toastError } from "../../../../utils";
import AddPatient from "../../../patients/add-patient/add-patient-modal";

export default function SearchBox({ ...rest }) {
  const [searchValue, setSearchValue] = useState("");
  const [inputInFocus, setInputInFocus] = useState(false);
  const [hoveringOnResults, setHoveringOnResults] = useState(false);
  const [hoveredOnPatient, setHoveredOnPatient] = useState(null);
  const { userDetails } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const { retainers } = useSelector((state) => state.retainers);
  const { patients, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const toast = useToast();

  useEffect(() => {
    //Fetch Patients
    check.emptyArray(patients) &&
      dispatch(
        fetchPatients({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );

    //Fetch Patient retainers
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
  }, []);

  useEffect(() => {
    if (error && error.errorType === "FETCH_PATIENTS") {
      // If an error occurs while fetching patient,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  const onSubmitSearch = (event) => {
    event.preventDefault();
    Router.push(`/patients?searchQuery=${searchValue}`);
  };

  return (
    <Stack
      bg="white"
      border="1px solid lightgrey"
      pt={[6, 6]}
      pb={6}
      pl={[6, 8, 8, 10]}
      pr={[6, 8, 8, 10]}
      spacing={6}
      rounded={8}
      {...rest}
    >
      <HStack spacing={6} w="full">
        <Stack w="full">
          <Stack>
            <Heading
              textTransform="capitalize"
              fontSize={["2xl", "3xl"]}
              color="primary.500"
            >
              Welcome {userDetails?.first_name},
            </Heading>
            <Text color="gray.500">
              To begin you can search for, or register a patient
            </Text>
          </Stack>
          <Stack pos="relative">
            <Stack direction={["column", "row"]} spacing={4}>
              <form
                onSubmit={onSubmitSearch}
                style={{ width: "100%" }}
                action="submit"
              >
                <InputGroup size="lg">
                  <InputLeftElement
                    pointerEvents="none"
                    children={<SearchIcon color="gray.300" />}
                  />
                  <Input
                    variant="filled"
                    type="text"
                    isRequired
                    onBlur={() => setInputInFocus(false)}
                    value={searchValue}
                    onChange={(event) => {
                      setSearchValue(event.target.value);
                      setInputInFocus(true);
                    }}
                    onFocus={() => searchValue && setInputInFocus(true)} //Open results box only when user has typed and input is in focus
                    placeholder="Search Patient"
                  />
                </InputGroup>
              </form>
            </Stack>
            <Stack
              onMouseEnter={() => setHoveringOnResults(true)} // Check If action is being done on the results,
              onMouseLeave={() => setHoveringOnResults(false)} // Check If... not
              w="full"
              h={48}
              bg="white"
              // Hide when (empty or not in focus) and when not hovering on results box
              zIndex={
                (searchValue && inputInFocus) || hoveringOnResults ? 3 : -1
              }
              opacity={
                (searchValue && inputInFocus) || hoveringOnResults ? 1 : 0
              }
              transform={
                (searchValue && inputInFocus) || hoveringOnResults
                  ? "scale(1)"
                  : "scale(1.08)"
              }
              transition="0.2s all"
              shadow="xl"
              rounded={8}
              pos="absolute"
              className="container-body"
              w="800px"
              top={[12, 16]}
              overflowX="hidden"
              overflowY="scroll"
            >
              {/* <HStack px={[4, 12]} spacing={4}>
            <Badge
              textTransform="initial"
              rounded={4}
              fontWeight="normal"
              colorScheme="primary"
            >
              Name
            </Badge>
            <Badge
              textTransform="initial"
              rounded={4}
              fontWeight="normal"
              colorScheme="orange"
            >
              Retainer
            </Badge>
            <Badge textTransform="initial" rounded={4} fontWeight="normal">
              Phone Number
            </Badge>
          </HStack>
      */}
              {loading && ( // While loading patients, show spinner
                <Flex w="full" justify="center">
                  <BeatLoader size={8} color={theme.colors.primary[500]} />
                </Flex>
              )}
              <Stack pt={4}>
                <Table variant="unstyled" colorScheme="gray" size="sm">
                  <Tbody
                    onMouseLeave={() => setHoveredOnPatient(null)}
                    color="gray.500"
                  >
                    <FilterResults
                      value={searchValue}
                      data={[...patients]}
                      renderResults={(results) =>
                        check.emptyArray(results) ? (
                          <Text wordBreak="break-word" textAlign="center">
                            No patient matches the search value
                          </Text>
                        ) : (
                          results.map(
                            ({
                              id,
                              first_name,
                              last_name,
                              phone,
                              gender,
                              dob,
                              retainer_id,
                            }) => (
                              <Tr
                                as="a"
                                href={`/patients/${id}`}
                                display="table-row"
                                onMouseEnter={() => setHoveredOnPatient(id)}
                                onClick={(event) => {
                                  event.preventDefault();
                                  Router.push(`/patients/${id}`);
                                }}
                                verticalAlign="inherit"
                                bg="transparent"
                                _hover={{ bg: "gray.70" }}
                                cursor="pointer"
                                key={id}
                              >
                                <Td pl={[4, 4]}>
                                  {first_name} {last_name}
                                </Td>
                                <Td>
                                  {dob && <>{calculateAge(dob)} years old</>}
                                </Td>
                                <Td textTransform="uppercase">{gender}</Td>
                                <Td isNumeric>{phone}</Td>
                                <Td>
                                  {
                                    retainers.find(
                                      ({ id }) => id === retainer_id
                                    )?.name
                                  }
                                </Td>
                                <Td>
                                  <HStack w={20}>
                                    {hoveredOnPatient == id && (
                                      <HStack>
                                        <Button
                                          // colorScheme="green"
                                          variant="outline"
                                          h={5}
                                          size="sm"
                                          onClick={(event) => {
                                            event.preventDefault();
                                            event.stopPropagation();
                                            dispatch(
                                              setSelectedPatient(
                                                patients.find(
                                                  (patient) => patient.id === id
                                                )
                                              )
                                            );
                                            dispatch(
                                              openAppointmentModal({
                                                checkIn: true,
                                              })
                                            );
                                          }}
                                          // icon={<BsFillPersonCheckFill />}
                                        >
                                          Check In
                                        </Button>

                                        {/* <IconButton
                                      colorScheme="primary"
                                      variant="ghost"
                                      boxSize={4}
                                      size="sm"
                                      onClick={(event) => {
                                        event.preventDefault();
                                        event.stopPropagation();
                                        dispatch(
                                          setSelectedPatient(
                                            patients.find(
                                              (patient) => patient.id === id
                                            )
                                          )
                                        );
                                        dispatch(openAppointmentModal({}));
                                      }}
                                      icon={<BiCalendarPlus />}
                                    /> */}
                                      </HStack>
                                    )}
                                  </HStack>
                                </Td>
                              </Tr>
                            )
                          )
                        )
                      }
                    />
                  </Tbody>
                </Table>
              </Stack>
            </Stack>
          </Stack>
        </Stack>

        <Divider
          border="1px solid"
          borderColor="gray.400"
          h={40}
          orientation="vertical"
        />
        <AddPatient searchValue={searchValue} />
      </HStack>
    </Stack>
  );
}
