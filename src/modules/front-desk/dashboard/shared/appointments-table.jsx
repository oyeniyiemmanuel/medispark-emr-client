import { CalendarIcon, Icon } from "@chakra-ui/icons";
import {
  Badge,
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuList,
  Stack,
  Text,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/table";
import check from "check-types";
import React, { useEffect, useState } from "react";
import { AiFillCaretDown } from "react-icons/ai";
import { BiCalendarEvent } from "react-icons/bi";
import { BsFillPersonCheckFill } from "react-icons/bs";
import { RiEditLine } from "react-icons/ri";
import { useDispatch, useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";
import {
  clearStates,
  fetchAppointments,
} from "../../../../redux/slices/appointmentSlice";
import { setSelectedPatient } from "../../../../redux/slices/patientSlice";
import theme from "../../../../theme";
import {
  formatDateToDDMMYY,
  toastError,
  wordTruncate,
} from "../../../../utils";
import SendToDoctor from "../../../patients/each-patient/send-to-doctor";
import Moment from "react-moment";
import "moment-timezone";
import DayPicker from "react-day-picker";
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from "react-day-picker/moment";
import "react-day-picker/lib/style.css";
import Router from "next/router";
import { openAppointmentModal } from "../../../../redux/slices/commonSlice";
import ReactTooltip from "react-tooltip";

const AppointmentRow = ({ appointment }) => {
  const [showOptions, setShowOptions] = useState(false);
  const { patients, selectedPatient } = useSelector((state) => state.patients);
  const { retainers } = useSelector((state) => state.retainers);
  const { services } = useSelector((state) => state.services);
  const { staff } = useSelector((state) => state.staff);
  const dispatch = useDispatch();

  const {
    id,
    user_id,
    retainer_id,
    appointment_date,
    appointment_start_time,
    appointment_end_time,
    appointee_id,
    service_id,
    status,
    service_user_id,
  } = appointment;

  return (
    <Tr
      onMouseEnter={() => setShowOptions(true)}
      onMouseLeave={() => setShowOptions(false)}
      _hover={{ bg: "gray.30" }}
    >
      <Td>
        <Stack spacing={0}>
          <Text fontWeight="bold">
            <Moment format="hh:mm a">
              {new Date(
                `${appointment_date} ${appointment_start_time}`
              ).getTime()}
            </Moment>
          </Text>
          {/* <Text fontSize="xs">9:30am</Text> */}
        </Stack>
      </Td>
      <Td>Today</Td>
      <Td>{patients.find(({ id }) => id === user_id)?.full_name}</Td>
      <Td>{staff.find(({ id }) => id === appointee_id)?.full_name}</Td>
      <Td>
        <ReactTooltip
          multiline
          place="right"
          type="dark"
          effect="solid"
          id="main"
        />
        <a
          data-tip={services.find(({ id }) => id === service_id)?.name}
          data-for="main"
        >
          {wordTruncate(services.find(({ id }) => id === service_id)?.name, 10)}
        </a>
      </Td>
      <Td isNumeric>{patients.find(({ id }) => id === user_id)?.phone}</Td>
      <Td>
        <HStack
          pos="relative"
          // d={showOptions ? "flex" : "none"}
          zIndex={showOptions ? 1 : -10}
        >
          <Button
            variant="outline"
            size="sm"
            onClick={() => {
              dispatch(
                setSelectedPatient(patients.find(({ id }) => id === user_id))
              );
              dispatch(
                openAppointmentModal({
                  checkIn: true,
                  existingAppointmentDetails: appointment,
                })
              );
            }}
            // icon={<BsFillPersonCheckFill />}
          >
            Check In
          </Button>
        </HStack>
      </Td>
    </Tr>
  );
};

export default function AppointmentsTable({ ...rest }) {
  const { userDetails } = useSelector((state) => state.user);
  const { appointments, loading, success, error } = useSelector(
    (state) => state.appointments
  );
  const dispatch = useDispatch();
  const toast = useToast();
  const [selectedDay, setSelectedDay] = useState(new Date());

  useEffect(() => {
    //Fetch Appointments

    var initialDate = new Date();
    var twoMonthsTime = new Date(
      initialDate.setMonth(initialDate.getMonth() + 2)
    );

    dispatch(
      fetchAppointments({
        branch_id: userDetails.branch_id,
        start_date: formatDateToDDMMYY("yyyy-mm-dd", selectedDay),
        end_date: formatDateToDDMMYY("yyyy-mm-dd", twoMonthsTime),
      })
    );
  }, [selectedDay]);

  return (
    <Box
      rounded={8}
      w="full"
      pb={8}
      h="398px"
      border="1px solid lightgrey"
      pos="relative"
      bg="white"
      {...rest}
    >
      <Stack
        pos="sticky"
        top={0}
        bg="white"
        color="gray.500"
        spacing={4}
        justify="space-between"
        py={6}
        direction={["column", "row"]}
        px={8}
      >
        <Heading fontSize="xl">Upcoming Appointments</Heading>
        <HStack fontSize="sm" spacing={0}>
          <Button
            onClick={() => Router.push("/schedules")}
            colorScheme="primary"
          >
            View All
          </Button>
          {/* <Menu>
            <MenuButton
              as={Button}
              variant="ghost"
              color="gray.500"
              fontWeight="normal"
              leftIcon={<CalendarIcon />}
            >
              <Moment format="MMMM D, YYYY ">{selectedDay}</Moment>
            </MenuButton>
            <MenuList p={4}>
              <DayPicker
                onDayClick={(day) => setSelectedDay(day)}
                selectedDays={selectedDay}
                formatDate={formatDate}
                // dayPickerProps={{ disabledDays: { before: new Date() } }}
                parseDate={parseDate}
                format="MMMM DD, YYYY"
              />
            </MenuList>
          </Menu> */}
        </HStack>
      </Stack>

      <Table variant="unstyled" size="sm">
        <Thead>
          <Tr color="gray.600" textTransform="initial" bg="gray.70">
            <Th>Time</Th>
            <Th>Date</Th>
            <Th>Patient</Th>
            <Th>Appointee</Th>
            <Th>Service</Th>
            <Th isNumeric>Phone No</Th>
            <Th></Th>
          </Tr>
        </Thead>

        <Tbody color="gray.500">
          {[...appointments]
            .filter(({ status }) => status === "open")
            .slice(0, 5)
            .map((appointment) => (
              <AppointmentRow appointment={appointment} key={appointment.id} />
            ))}
        </Tbody>
      </Table>
      {loading === "FETCH_APPOINTMENTS" && ( // While loading appointments, show spinner
        <Flex w="full" justify="center">
          <BeatLoader size={8} color={theme.colors.primary[500]} />
        </Flex>
      )}
      {!loading &&
        check.emptyArray(appointments) && ( // While loading appointments, show spinner
          <Flex w="full" pt={4} justify="center">
            <Text fontSize="sm" color="gray.500">
              There are no upcoming appointments to display
            </Text>
          </Flex>
        )}
    </Box>
  );
}
