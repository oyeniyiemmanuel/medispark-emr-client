import { Flex, Heading, HStack, Text } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

export default function FrontDeskDashboardNavbar({ ...rest }) {
  return (
    <Flex
      {...rest}
      align="center"
      px="5%"
      justify="center"
      pt={4}
      w="full"
      bg="gray.30"
    >
      <HStack spacing={[8, 8, 20]} w={["full", "full", "2xl", "5xl"]}>
        <Heading
          cursor="pointer"
          borderBottom="4px solid"
          borderBottomColor="primary.500"
          pb={4}
          fontSize={["2xl", "3xl"]}
          color="primary.500"
        >
          Clinics
        </Heading>
        <HStack d={["none", "flex"]} fontSize="xl" spacing={8} pb={2}>
          <Link href="#">
            <Text cursor="pointer" color="gray.500">
              Clinics
            </Text>
          </Link>
          <Link href="#">
            <Text cursor="pointer" color="gray.500">
              Pharmacy
            </Text>
          </Link>
          <Link href="#">
            <Text cursor="pointer" color="gray.400">
              Laboratory
            </Text>
          </Link>
          <Link href="#">
            <Text cursor="pointer" color="gray.400">
              Other
            </Text>
          </Link>
        </HStack>
      </HStack>
    </Flex>
  );
}
