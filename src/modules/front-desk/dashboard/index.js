import { Box, Button, Heading, Stack } from "@chakra-ui/react";
import Router from "next/router";
import React from "react";
import DashboardContainer from "../../../components/layouts/dashboard";
import SendToDoctor from "../../patients/each-patient/send-to-doctor";
import AppointmentsTable from "./shared/appointments-table";
import SearchBox from "./shared/search-box";

export default function FrontDeskDashboard() {
  return (
    <Stack
      spacing={[7, 7, 7, 10]}
      mt={[8, 8, 8, 12]}
      pb={24}
      px={["5%", 6, 6, 8, 16]}
    >
      <SearchBox />
      <Stack
        align="start"
        direction={["column", "column", "column", "column", "row"]}
        spacing={4}
      >
        <AppointmentsTable />
        <Box
          rounded={8}
          w={["full", "full", "full", "full", 274]}
          h={398}
          bgColor="primary.500"
          bgImage="url(/images/dashboard-queue-bg.svg)"
          bgSize="cover"
          bgPos="center"
          py={6}
          px={3}
        >
          <Stack justify="space-between" h="full" w="full">
            <Heading fontSize="3xl" color="white">
              Manage the order in which patients see the doctor.
            </Heading>
            <Button
              bg="white"
              color="gray.500"
              fontWeight="normal"
              shadow="xl"
              onClick={() => Router.push("/queues")}
            >
              Manage Queue
            </Button>
          </Stack>
        </Box>
      </Stack>
      <SendToDoctor />
    </Stack>
  );
}
