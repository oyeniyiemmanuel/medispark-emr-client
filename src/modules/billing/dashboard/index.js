import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  Avatar,
  Text,
  IconButton,
  Stack,
  Wrap,
  WrapItem,
  Divider,
  Center,
  Icon,
} from "@chakra-ui/react";
import Router from "next/router";
import React from "react";
import { AiFillEye } from "react-icons/ai";
import { BiDotsVerticalRounded } from "react-icons/bi";
import { IoMdBusiness } from "react-icons/io";
import { MdMovie, MdShare } from "react-icons/md";
import FakeIcon from "../../../components/fake-icon";
import AppointmentsTable from "../../front-desk/dashboard/shared/appointments-table";
import SearchBox from "../../front-desk/dashboard/shared/search-box";
import HMOlist from "./shared/hmos";
import TransactionsList from "./shared/transactions";

export default function BillingDashboard() {
  return (
    <>
      <Flex
        py={5}
        pr={4}
        pl={16}
        bg="white"
        w="full"
        justify="space-between"
        align="center"
        borderBottom="1px solid lightgrey"
        color="gray.600"
      >
        <HStack>
          <Heading fontSize="3xl">Billing Dashboard</Heading>
        </HStack>
      </Flex>
      <Stack spacing={7} mt={[8, 8, 8, 12]} pb={24} px={["5%", 6, 6, 8, 16]}>
        <Stack>
          <Heading fontSize="xl" color="gray.500">
            Quick Actions
          </Heading>
          <Wrap spacing={4} w="full">
            <WrapItem>
              <Button color="gray.600" leftIcon={<MdMovie />} variant="ghost">
                New Invoice
              </Button>
            </WrapItem>
            <WrapItem>
              <Button color="gray.600" leftIcon={<MdMovie />} variant="ghost">
                New Invoice
              </Button>
            </WrapItem>
            <WrapItem>
              <Button color="gray.600" leftIcon={<MdMovie />} variant="ghost">
                New Invoice
              </Button>
            </WrapItem>
            <WrapItem>
              <Button color="gray.600" leftIcon={<MdMovie />} variant="ghost">
                New Invoice
              </Button>
            </WrapItem>
          </Wrap>
        </Stack>
        <Stack
          bg="white"
          pt={[6, 8]}
          border="1px solid lightgray"
          pb={8}
          justify="space-between"
          direction="row"
          pl={[6, 8, 8, 10]}
          pr={[6, 8, 8, 10]}
          spacing={6}
          rounded={8}
        >
          <HStack align="start" color="gray.500" spacing={7}>
            <FakeIcon boxSize={32} bg="green.600" rounded={4} />
            <Stack spacing={10}>
              <Heading fontSize="lg">Hospital Name</Heading>
              <Text>Balance</Text>
              <HStack>
                <Button w={40} colorScheme="primary">
                  View Profile
                </Button>
                <Button w={40} colorScheme="primary">
                  View Profile
                </Button>
              </HStack>
            </Stack>
          </HStack>
          <Stack direction="row" spacing={10}>
            <Divider
              height={56}
              orientation="vertical"
              borderWidth="1px"
              borderColor="gray.400"
            />
            <Stack color="gray.500" spacing={4}>
              <Text>Tracklist</Text>
              <HStack align="start">
                <Center
                  boxSize={10}
                  rounded={4}
                  bg="green.100"
                  color="green.500"
                >
                  <Text fontSize="sm">Rcpt.</Text>
                </Center>
                <Box>
                  <Text fontSize="sm">Receipt</Text>
                  <HStack>
                    <Text fontSize="2xl" fontWeight="bold" color="green.500">
                      #54,250
                    </Text>
                    <IconButton
                      variant="ghost"
                      size="sm"
                      fontSize="xl"
                      colorScheme="green"
                      icon={<AiFillEye />}
                    />
                  </HStack>
                </Box>
              </HStack>
              <HStack align="start">
                <Center
                  boxSize={10}
                  rounded={4}
                  bg="orange.100"
                  color="orange.500"
                >
                  <Text fontSize="sm">Inv.</Text>
                </Center>
                <Box>
                  <Text fontSize="sm">Invoice</Text>
                  <HStack>
                    <Text fontSize="2xl" fontWeight="bold" color="blue.500">
                      #54,250
                    </Text>
                    <IconButton
                      variant="ghost"
                      size="sm"
                      fontSize="xl"
                      colorScheme="blue"
                      icon={<AiFillEye />}
                    />
                  </HStack>
                </Box>
              </HStack>
              <Button size="lg" variant="outline">
                Track New list
              </Button>
            </Stack>
          </Stack>
        </Stack>
        <Stack
          align="start"
          direction={["column", "column", "column", "column", "row"]}
          spacing={4}
        >
          <TransactionsList />
          <HMOlist />
        </Stack>
      </Stack>
    </>
  );
}
