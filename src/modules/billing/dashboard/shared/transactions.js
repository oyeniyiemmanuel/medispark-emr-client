import { Box, Button, Divider, HStack, Stack, Text } from "@chakra-ui/react";
import Router from "next/router";
import React from "react";
import FakeIcon from "../../../../components/fake-icon";

export default function TransactionsList({ ...rest }) {
  return (
    <Box
      rounded={8}
      w="full"
      py={6}
      px={6}
      border="1px solid lightgray"
      // overflowY="hidden"
      // overflowX="scroll"
      // className="container-body"
      h="398px"
      pos="relative"
      bg="white"
      color="gray.600"
      {...rest}
    >
      <Stack direction="row" justify="space-between" align="center">
        <Box>
          <Text fontWeight="bold">Transactions</Text>
          <Text fontSize="sm">Today 24th January, 2021</Text>
        </Box>
        <Button
          onClick={() => Router.push("/billing/transactions")}
          colorScheme="primary"
        >
          View All Transactions
        </Button>
      </Stack>
      <Divider py={2} />
      <Stack pt={4} spacing={6}>
        {[0, 1, 2, 3, 4].map((index) => (
          <Stack
            align="center"
            direction="row"
            justify="space-between"
            w="full"
            key={index}
          >
            <HStack spacing={3}>
              <FakeIcon boxSize={8} rounded={3} />
              <Text>Richard Mofe Damijo</Text>
            </HStack>
            <Text color="green.500" fontSize="xl" fontWeight="bold">
              #34,000
            </Text>
          </Stack>
        ))}
      </Stack>
    </Box>
  );
}
