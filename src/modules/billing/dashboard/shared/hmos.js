import {
  Box,
  Center,
  HStack,
  Icon,
  IconButton,
  Stack,
  Text,
} from "@chakra-ui/react";
import React from "react";
import { BiDotsVerticalRounded } from "react-icons/bi";
import { IoMdBusiness } from "react-icons/io";

export default function HMOlist({ ...rest }) {
  return (
    <Box
      rounded={8}
      w={["full", "full", "full", "full", 440]}
      h={398}
      bg="white"
      border="1px solid lightgrey"
      color="gray.600"
      p={4}
      {...rest}
    >
      <HStack w="full" justify="space-between">
        <Text fontSize="lg" fontWeight="bold">
          HMOs
        </Text>
        <IconButton icon={<BiDotsVerticalRounded />} variant="ghost" />
      </HStack>
      <Stack spacing={4} pt={4}>
        {[0, 1, 2, 3, 4].map((index) => (
          <Stack key={index} direction="row" justify="space-between" w="full">
            <HStack>
              <Center boxSize={8} bg="primary.500" borderRadius={4}>
                <Icon color="white" as={IoMdBusiness} />
              </Center>
              <Box fontSize="sm">
                <Text>Mansard</Text>
                <Text fontSize={12}>
                  <Box as="span" color="primary.500">
                    BAL
                  </Box>
                  #1,200,000
                </Text>
              </Box>
            </HStack>
            <Text fontSize="sm">2dy 20h</Text>
          </Stack>
        ))}
      </Stack>
    </Box>
  );
}
