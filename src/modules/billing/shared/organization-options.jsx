import {
  Avatar,
  Box,
  HStack,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import React from "react";
import { useSelector } from "react-redux";
import FakeIcon from "../../../components/fake-icon";

export default function OrganizationOptions({ ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const organizationOptions = [
    { id: 1, icon: "", href: "biodata", name: "Biodata" },
    { id: 2, icon: "", href: "profile", name: "Profile Settings" },
    { id: 3, icon: "", href: "tarriffs", name: "Tarriffs" },
    { id: 4, icon: "", href: "accounts", name: "Accounts" },
    { id: 5, icon: "", href: "reports", name: "Reports & Analytics" },
    { id: 6, icon: "", href: "patients", name: "Patients" },
    { id: 7, icon: "", href: "staffing", name: "Staffing" },
    { id: 8, icon: "", href: "access", name: "Access Rights" },
    { id: 9, icon: "", href: "stock", name: "Stock" },
    { id: 10, icon: "", href: "products", name: "Products & Services" },
  ];

  return (
    <>
      <Box
        border="1px solid lightgrey"
        position="sticky"
        top="80px"
        bg="gray.50"
        w="250px"
        {...rest}
      >
        <Stack w="full" bg="white">
          <HStack
            py={4}
            bg="primary.500"
            color="white"
            w="full"
            spacing={4}
            px={4}
          >
            <Avatar size="md" name={``} src="https://bit.ly/prosper-baba" />
            <Box>
              <Text fontWeight="bold" fontSize="lg">
                Hosp Name
              </Text>
              <Text fontSize="sm">Organization</Text>
            </Box>
          </HStack>
          <Stack spacing={0}>
            {organizationOptions.map(({ id, name }) => (
              <HStack
                _hover={{
                  borderLeft: "3px solid",
                  borderLeftColor: "primary.500",
                  transform: "scale(1.08)",
                  fontWeight: "bold",
                  shadow: "md",
                }}
                transition="0.2s all"
                cursor="pointer"
                key={id}
                py={2}
                spacing={4}
                px={6}
              >
                <FakeIcon />
                <Text color="gray.500">{name}</Text>
              </HStack>
            ))}
          </Stack>
        </Stack>
        <Box bg="white" color="gray.500" m={4}></Box>
      </Box>
    </>
  );
}
