import { AddIcon, CalendarIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  Avatar,
  Text,
  IconButton,
  Stack,
  Wrap,
  WrapItem,
  Divider,
  Center,
  Icon,
  Menu,
  MenuButton,
  MenuList,
  Table,
  TableCaption,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
  Tfoot,
  Badge,
  CloseButton,
} from "@chakra-ui/react";
import Router from "next/router";
import React, { useState } from "react";
import { AiFillEye } from "react-icons/ai";
import { BiDotsVerticalRounded } from "react-icons/bi";
import { IoMdBusiness } from "react-icons/io";
import { MdMovie, MdShare } from "react-icons/md";
import FakeIcon from "../../../components/fake-icon";
import AppointmentsTable from "../../front-desk/dashboard/shared/appointments-table";
import SearchBox from "../../front-desk/dashboard/shared/search-box";
import DayPicker from "react-day-picker";
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from "react-day-picker/moment";
import "react-day-picker/lib/style.css";
import Moment from "react-moment";
import "moment-timezone";
import { BsThreeDots } from "react-icons/bs";
import OrganizationOptions from "../shared/organization-options";

export default function BillingTransactionsView() {
  const [selectedDay, setSelectedDay] = useState(new Date());

  return (
    <>
      <Flex
        py={5}
        pr={4}
        pl={16}
        bg="white"
        w="full"
        justify="space-between"
        align="center"
        borderBottom="1px solid lightgrey"
        color="gray.600"
      >
        <HStack>
          <Box h={12} w="200px" />
          <Heading fontSize="3xl">Transactions</Heading>
        </HStack>
        <CloseButton onClick={() => Router.push("/billing")} />
      </Flex>
      <HStack spacing={8} align="start" w="full" px={["5%", 6, 6, 8, 8]}>
        <OrganizationOptions />
        <Stack maxW="calc(100% - 250px)" pt={6} spacing={7} pb={24}>
          <Wrap
            justify="space-between"
            w="full"
            border="1px solid lightgray"
            bg="white"
            px={6}
            spacing={4}
            flexWrap="wrap"
            rounded={8}
            align="center"
            py={3}
          >
            <WrapItem>
              <Wrap spacing={4}>
                <WrapItem>
                  <Button color="gray.600" variant="ghost">
                    All
                  </Button>
                </WrapItem>
                <WrapItem>
                  <Button color="gray.600" variant="ghost">
                    Paid
                  </Button>
                </WrapItem>
                <WrapItem>
                  <Button color="gray.600" variant="ghost">
                    Pending
                  </Button>
                </WrapItem>
                <WrapItem>
                  <Button color="gray.600" variant="ghost">
                    Canceled
                  </Button>
                </WrapItem>
              </Wrap>
            </WrapItem>

            <WrapItem>
              <HStack>
                <Menu>
                  <MenuButton
                    as={Button}
                    variant="ghost"
                    color="gray.500"
                    fontWeight="normal"
                    leftIcon={<CalendarIcon />}
                  >
                    <Moment format="MMMM D, YYYY ">{selectedDay}</Moment>
                  </MenuButton>
                  <MenuList p={4}>
                    <DayPicker
                      onDayClick={(day) => setSelectedDay(day)}
                      selectedDays={selectedDay}
                      formatDate={formatDate}
                      dayPickerProps={{ disabledDays: { before: new Date() } }}
                      parseDate={parseDate}
                      format="MMMM DD, YYYY"
                      // onDayChange={(day) => setDate(day)}
                    />
                  </MenuList>
                </Menu>
                <Button w={40} colorScheme="primary" leftIcon={<AddIcon />}>
                  New Invoice
                </Button>
              </HStack>
            </WrapItem>
          </Wrap>

          <Box
            rounded={8}
            w="full"
            border="1px solid lightgray"
            bg="white"
            p={2}
            color="gray.600"
          >
            <Table variant="unstyled">
              <Thead>
                <Tr borderBottom="1px solid lightgrey">
                  <Th>TYPE</Th>
                  <Th>INVOICE No.</Th>
                  <Th>NAME</Th>
                  <Th isNumeric>AMOUNT</Th>
                  <Th>STATUS</Th>
                  <Th>DUE DATE</Th>
                  <Th color="primary.500">ADD FIELD</Th>
                </Tr>
              </Thead>
              <Tbody>
                {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map((index) => (
                  <Tr
                    cursor="pointer"
                    _hover={{ bg: "gray.50" }}
                    fontSize="sm"
                    key={index}
                  >
                    <Td>
                      <Center
                        boxSize={10}
                        rounded={4}
                        bg="orange.100"
                        color="orange.500"
                      >
                        <Text fontSize="sm">Inv.</Text>
                      </Center>
                    </Td>
                    <Td>INV00122</Td>
                    <Td>Richard Mofe Damijo</Td>
                    <Td isNumeric>#50,000</Td>
                    <Td>
                      <Badge colorScheme="primary" color="primary.500">
                        New
                      </Badge>
                    </Td>
                    <Td>24th Jan, 2021</Td>
                    <Td>
                      <IconButton variant="ghost" icon={<BsThreeDots />} />
                    </Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </Box>
        </Stack>
      </HStack>
    </>
  );
}
