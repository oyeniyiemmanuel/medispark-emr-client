import { SearchIcon } from "@chakra-ui/icons";
import {
  Avatar,
  Badge,
  Box,
  Button,
  Checkbox,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Select,
  Stack,
  Text,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import Router from "next/router";
import React, { useState } from "react";
import { MdAddCircle, MdPrint, MdShare } from "react-icons/md";
import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/table";
import { RiEditLine } from "react-icons/ri";
import DayPicker from "react-day-picker";
import DayPickerInput from "react-day-picker/DayPickerInput";
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from "react-day-picker/moment";
import "react-day-picker/lib/style.css";
import FakeIcon from "../../components/fake-icon";
import { BsCircle } from "react-icons/bs";

export default function TimeSlots({ ...rest }) {
  return (
    <Stack
      rounded={4}
      border="1px solid"
      borderColor="gray.200"
      p={6}
      bg="white"
      {...rest}
    >
      <Stack spacing={3}>
        <Stack direction="row" justify="space-between" w="full" align="center">
          <HStack color="gray.500" fontSize="sm">
            <FakeIcon boxSize={8} rounded={4} bg="red.100" />
            <Box>
              <Text fontWeight="bold">Hospital Shift</Text>
              <Text>Ward 316, 07:00am - 3:30pm</Text>
            </Box>
          </HStack>
          <Button
            variant="link"
            size="sm"
            colorScheme="primary"
            rightIcon={<MdAddCircle />}
          >
            Add New
          </Button>
        </Stack>
        <Divider borderBottomColor="gray.300" borderBottomWidth="2px" />
        <Wrap px={3}>
          {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((index) => (
            <WrapItem>
              <Button
                variant="outline"
                leftIcon={<BsCircle />}
                colorScheme="primary"
              >
                7:00 - 7:30
              </Button>
            </WrapItem>
          ))}
        </Wrap>
        <HStack align="start" px={3} pt={3} fontSize="sm">
          <Avatar
            size="sm"
            name="Kent Dodds"
            src="https://bit.ly/kent-c-dodds"
          />
          <Box maxW="2xs">
            <Text fontWeight="bold" color="primary.500">
              Richard Mofe Damijo
            </Text>
            <Text fontSize="sm" color="gray.500">
              7:00 - 7:30
            </Text>
            <Text pt={1} fontSize="sm" color="gray.500">
              The reason Mr Mofe Damijo is coming to this appoint...
            </Text>
          </Box>
        </HStack>
      </Stack>
    </Stack>
  );
}
