import { CheckIcon, DeleteIcon } from "@chakra-ui/icons";
import {
  Avatar,
  Badge,
  Box,
  Button,
  ButtonGroup,
  Checkbox,
  Flex,
  HStack,
  IconButton,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/table";
import check from "check-types";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import FilterResults from "react-filter-search";
import { MdDelete, MdMail, MdNotificationsActive } from "react-icons/md";
import { VscCheck } from "react-icons/vsc";
import { useDispatch, useSelector } from "react-redux";
import Moment from "react-moment";
import "moment-timezone";
import SendToDoctor from "../patients/each-patient/send-to-doctor";
import { setSelectedPatient } from "../../redux/slices/patientSlice";
import { BeatLoader } from "react-spinners";
import theme from "../../theme";
import { openAppointmentModal } from "../../redux/slices/commonSlice";
import { BiCalendarPlus } from "react-icons/bi";
import { RiChatNewFill } from "react-icons/ri";
import { wordTruncate } from "../../utils";
import ReactTooltip from "react-tooltip";

export default function AllAppointments({
  appointments,
  selectedDay,
  loading,
  ...rest
}) {
  const [appointmentToCheckIn, setAppointmentToCheckIn] = useState(null);
  const { patients, selectedPatient } = useSelector((state) => state.patients);
  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [selectedAppointments, setSelectedAppointments] = useState([]);
  const [selectAllAppointments, setSelectAllAppointments] = useState(false);
  const { staff } = useSelector((state) => state.staff);
  const { services } = useSelector((state) => state.services);

  const addOrRemoveFromSelectedAppointments = (id) => {
    if (selectedAppointments.find((appointmentId) => appointmentId == id)) {
      setSelectAllAppointments(false);
      setSelectedAppointments(
        [...selectedAppointments].filter((appointmentId) => appointmentId != id)
      );
    } else {
      setSelectedAppointments([...selectedAppointments, id]);
    }
  };
  useEffect(() => {
    if (selectAllAppointments) {
      setSelectedAppointments([...appointments].map(({ id }) => id));
    } else {
      setSelectedAppointments([]);
    }
  }, [selectAllAppointments]);

  return (
    <Stack pt={12}>
      {check.nonEmptyArray(selectedAppointments) && (
        <HStack fontSize="sm" spacing={8} color="gray.500" pl={4}>
          <Checkbox
            isChecked={selectAllAppointments}
            onChange={() => setSelectAllAppointments(!selectAllAppointments)}
          >
            Select All
          </Checkbox>
          <HStack spacing={0}>
            <Button
              variant="link"
              size="sm"
              leftIcon={<MdNotificationsActive />}
            >
              Send Reminder
            </Button>
            <IconButton variant="default" icon={<MdDelete />} />
            <IconButton variant="default" icon={<VscCheck />} />
          </HStack>
        </HStack>
      )}
      <Table variant="unstyled" size="sm" {...rest}>
        {check.emptyArray(selectedAppointments) && (
          <Thead>
            <Tr color="gray.600" textTransform="initial" bg="gray.70">
              <Th pr={0} pb={2}>
                <Checkbox
                  colorScheme="primary"
                  borderColor="primary.300"
                  isChecked={selectAllAppointments}
                  onChange={() =>
                    setSelectAllAppointments(!selectAllAppointments)
                  }
                ></Checkbox>
              </Th>
              <Th pb={2}>DATE</Th>
              <Th pb={2}>PATIENT</Th>
              <Th pb={2}>SERVICE</Th>
              <Th pb={2}>PERSONNEL</Th>
              <Th pb={2}>STATUS</Th>
              <Th pb={2}>COMMENT</Th>
              <Th pb={2}>ACTIONS</Th>
            </Tr>
          </Thead>
        )}

        <Tbody color="gray.500">
          {[...appointments].map((eachAppointment) => {
            const {
              id,
              user_id,
              retainer_id,
              appointment_date,
              appointment_start_time,
              appointment_end_time,
              appointee_id,
              service_id,
              comment,
              status,
              service_user_id,
            } = eachAppointment;
            const isAvailable = status == "open";
            return (
              <Tr
                onMouseEnter={() => setAppointmentToCheckIn(eachAppointment)}
                onMouseLeave={() => setAppointmentToCheckIn(null)}
                // bg={isAvailable ? "transparent" : "gray.100"}
                opacity={isAvailable ? 1 : 0.6}
                cursor={isAvailable ? "pointer" : "initial"}
                mb={3}
                key={id}
              >
                <Td p={0}>
                  <Stack
                    onClick={(event) => event.stopPropagation()}
                    w="full"
                    align="center"
                    mb={2}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={6}
                    px={3}
                    spacing={0}
                  >
                    <Checkbox
                      isChecked={Boolean(
                        [...selectedAppointments].find(
                          (appointmentId) => appointmentId == id
                        )
                      )}
                      onChange={() => addOrRemoveFromSelectedAppointments(id)}
                    />
                  </Stack>
                </Td>
                <Td p={0} isTruncated>
                  <HStack
                    mb={2}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={6}
                    px={3}
                  >
                    <Text fontSize="xl">
                      {new Date(
                        `${appointment_date} ${appointment_start_time}`
                      ).getDate()}
                    </Text>
                    <Text textTransform="uppercase">
                      <Moment format="MMM">
                        {new Date(
                          `${appointment_date} ${appointment_start_time}`
                        ).toDateString()}
                      </Moment>
                    </Text>
                  </HStack>
                </Td>
                <Td isTruncated p={0}>
                  <HStack
                    mb={2}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={4}
                    px={3}
                    spacing={3}
                  >
                    <Avatar
                      size="sm"
                      name={
                        patients.find(({ id }) => id === user_id)?.full_name
                      }
                      src={
                        patients.find(({ id }) => id === user_id)?.avatar
                          ?.file_path
                      }
                    />
                    <Box>
                      <Text>
                        {patients.find(({ id }) => id === user_id)?.full_name}
                      </Text>
                      <Text>
                        <Moment format="hh:mm a">
                          {new Date(
                            `${appointment_date} ${appointment_start_time}`
                          ).getTime()}
                        </Moment>{" "}
                      </Text>
                    </Box>
                  </HStack>
                </Td>
                <Td isTruncated p={0}>
                  <Text
                    mb={2}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={6}
                    px={3}
                  >
                    <ReactTooltip
                      multiline
                      place="bottom"
                      type="dark"
                      effect="solid"
                      id="main"
                    />
                    <a
                      data-tip={
                        services.find(({ id }) => id === service_id)?.name
                      }
                      data-for="main"
                    >
                      {wordTruncate(
                        services.find(({ id }) => id === service_id)?.name,
                        20
                      )}
                    </a>
                  </Text>
                </Td>
                <Td isTruncated p={0}>
                  <Text
                    textTransform="capitalize"
                    mb={2}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={6}
                    px={3}
                  >
                    {staff.find(({ id }) => id === appointee_id)?.full_name}
                  </Text>
                </Td>
                <Td p={0}>
                  <HStack
                    mb={2}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={4}
                    px={3}
                  >
                    <Badge
                      textTransform="initial"
                      colorScheme="primary"
                      py={2}
                      rounded={8}
                      px={3}
                    >
                      Upcoming
                    </Badge>
                  </HStack>
                </Td>
                <Td p={0}>
                  <HStack
                    bg={isAvailable ? "white" : "gray.100"}
                    mb={2}
                    py={4}
                    px={3}
                    minH={16}
                  >
                    <Text noOfLines={2} maxW="13rem" isTruncated>
                      {comment}
                    </Text>
                  </HStack>
                </Td>

                <Td p={0} minW={20} isNumeric>
                  <HStack
                    onClick={(event) => event.stopPropagation()}
                    mb={2}
                    h={16}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={6}
                    spacing={0}
                  >
                    {isAvailable && appointmentToCheckIn?.id == id && (
                      <>
                        <IconButton
                          colorScheme="primary"
                          variant="ghost"
                          //   size="sm"
                          icon={<BiCalendarPlus />}
                          onClick={(event) => {
                            dispatch(
                              setSelectedPatient(
                                patients.find(
                                  (patient) => patient.id === user_id
                                )
                              )
                            );
                            dispatch(
                              openAppointmentModal({
                                checkIn: true,
                                existingAppointmentDetails: eachAppointment,
                              })
                            );
                          }}
                        />
                        <IconButton
                          colorScheme="primary"
                          variant="ghost"
                          //   size="sm"
                          //   onClick={() =>
                          //     Router.push(
                          //       `/messages/new?selectedPatients=${encodeURIComponent(
                          //         JSON.stringify([id])
                          //       )}`
                          //     )
                          //   }
                          icon={<RiChatNewFill />}
                        />

                        {/* <IconButton
                              colorScheme="primary"
                              variant="ghost"
                              size="sm"
                              icon={<RiEditLine />}
                            />
                            <IconButton
                              colorScheme="primary"
                              variant="ghost"
                              size="sm"
                              icon={<RiEditLine />}
                            /> */}
                      </>
                    )}
                  </HStack>
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
      {loading === "FETCH_APPOINTMENTS" && ( // While loading appointments, show spinner
        <Flex w="full" justify="center">
          <BeatLoader size={8} color={theme.colors.primary[500]} />
        </Flex>
      )}
      {!loading &&
        check.emptyArray(appointments) && ( // While loading appointments, show spinner
          <Flex w="full" justify="center">
            <Text fontSize="sm" color="gray.500">
              There are no upcoming appointments to display
            </Text>
          </Flex>
        )}
      <SendToDoctor />
    </Stack>
  );
}
