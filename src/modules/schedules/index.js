import { SearchIcon } from "@chakra-ui/icons";
import {
  Avatar,
  Badge,
  Box,
  Button,
  Checkbox,
  CloseButton,
  Flex,
  Heading,
  HStack,
  IconButton,
  Select,
  Stack,
  Text,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import { MdPrint, MdShare } from "react-icons/md";
import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/table";
import { RiEditLine } from "react-icons/ri";
import DayPicker from "react-day-picker";
import DayPickerInput from "react-day-picker/DayPickerInput";
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from "react-day-picker/moment";
import "react-day-picker/lib/style.css";
import AllAppointments from "./all-appointments";
import TimeSlots from "./time-slots";
import { useDispatch, useSelector } from "react-redux";
import { fetchAppointments } from "../../redux/slices/appointmentSlice";
import { formatDateToDDMMYY } from "../../utils";
import { fetchPatients } from "../../redux/slices/patientSlice";
import { fetchRetainers } from "../../redux/slices/retainerSlice";
import check from "check-types";
import { FaPlus } from "react-icons/fa";
import { openAppointmentModal } from "../../redux/slices/commonSlice";

export default function SchedulesView() {
  const [selectedDay, setSelectedDay] = useState(new Date());
  const [scheduleType, setScheduleType] = useState("all");
  const dispatch = useDispatch();
  const { userDetails } = useSelector((state) => state.user);
  const { retainers } = useSelector((state) => state.retainers);
  const { patients } = useSelector((state) => state.patients);
  const { appointments, loading, success, error } = useSelector(
    (state) => state.appointments
  );
  const toast = useToast();

  useEffect(() => {
    //Fetch Patients
    check.emptyArray(patients) &&
      dispatch(
        fetchPatients({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );

    //Fetch Patient retainers
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
  }, []);

  useEffect(() => {
    //Fetch Appointments

    dispatch(
      fetchAppointments({
        branch_id: userDetails.branch_id,
        start_date: formatDateToDDMMYY("yyyy-mm-dd", selectedDay),
        end_date: formatDateToDDMMYY("yyyy-mm-dd", selectedDay),
      })
    );
  }, [selectedDay]);

  return (
    <Box>
      <Flex
        py={5}
        pr={4}
        pl={12}
        bg="white"
        w="full"
        justify="space-between"
        align="center"
        borderBottom="1px solid lightgrey"
        color="gray.600"
      >
        <HStack>
          <Heading fontSize="3xl">Schedule</Heading>
        </HStack>
      </Flex>
      <Stack color="gray.500" pt={6} px={10}>
        <HStack justify="space-between">
          <Heading fontSize="xl">All Appointments</Heading>
          <Button
            onClick={() =>
              dispatch(
                openAppointmentModal({ checkIn: false, choosePatient: true })
              )
            }
            colorScheme="primary"
            leftIcon={<FaPlus />}
          >
            New Appointment
          </Button>
        </HStack>
        <AllAppointments
          appointments={appointments}
          loading={loading}
          selectedDay={selectedDay}
        />
      </Stack>
    </Box>
  );
}
