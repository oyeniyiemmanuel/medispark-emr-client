import { SearchIcon } from "@chakra-ui/icons";
import {
  Avatar,
  Badge,
  Box,
  Button,
  Checkbox,
  CloseButton,
  Flex,
  Heading,
  HStack,
  IconButton,
  Select,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import Router from "next/router";
import React, { useState } from "react";
import { MdPrint, MdShare } from "react-icons/md";
import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/table";
import { RiEditLine } from "react-icons/ri";
import DayPicker from "react-day-picker";
import DayPickerInput from "react-day-picker/DayPickerInput";
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from "react-day-picker/moment";
import "react-day-picker/lib/style.css";
import check from "check-types";
import { BeatLoader } from "react-spinners";
import theme from "../../theme";
import { useDispatch, useSelector } from "react-redux";
import Moment from "react-moment";
import "moment-timezone";
import { setSelectedPatient } from "../../redux/slices/patientSlice";
import { BsFillPersonCheckFill } from "react-icons/bs";
import SendToDoctor from "../patients/each-patient/send-to-doctor";
import { openAppointmentModal } from "../../redux/slices/commonSlice";

export default function AllAppointments({
  appointments,
  selectedDay,
  loading,
  ...rest
}) {
  const [appointmentToCheckIn, setAppointmentToCheckIn] = useState(null);
  const { patients, selectedPatient } = useSelector((state) => state.patients);
  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Stack
      rounded={4}
      border="1px solid"
      borderColor="gray.200"
      p={4}
      bg="white"
      {...rest}
    >
      <Table variant="unstyled" size="sm">
        <Tbody color="gray.500">
          {[...appointments].map((eachAppointment) => {
            const {
              id,
              user_id,
              retainer_id,
              appointment_date,
              appointment_start_time,
              appointment_end_time,
              appointee_id,
              service_id,
              comment,
              status,
              service_user_id,
            } = eachAppointment;
            const isAvailable = status == "open";
            return (
              <Tr
                key={id}
                cursor="pointer"
                my={3}
                bg="white"
                border="1px solid"
                borderColor="gray.200"
                shadow="md"
                onMouseEnter={() => setAppointmentToCheckIn(eachAppointment)}
                onMouseLeave={() => setAppointmentToCheckIn(null)}
                bg={isAvailable ? "transparent" : "gray.100"}
                opacity={isAvailable ? 1 : 0.6}
              >
                <Td p={0} isTruncated>
                  <HStack
                    mb={2}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={4}
                    px={3}
                    color="gray.600"
                  >
                    <Text fontSize="xl">
                      {new Date(
                        `${appointment_date} ${appointment_start_time}`
                      ).getDate()}
                    </Text>
                    <Text textTransform="uppercase">
                      <Moment format="MMM">
                        {new Date(
                          `${appointment_date} ${appointment_start_time}`
                        ).toDateString()}
                      </Moment>
                    </Text>
                  </HStack>
                </Td>
                <Td p={0}>
                  <HStack
                    mb={2}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={2}
                    px={3}
                    fontSize="sm"
                  >
                    <Avatar
                      size="sm"
                      name={
                        patients.find(({ id }) => id === user_id)?.full_name
                      }
                      src={patients.find(({ id }) => id === user_id)?.avatar?.file_path}
                    />
                    <Box>
                      <Text>
                        {patients.find(({ id }) => id === user_id)?.full_name}
                      </Text>
                      <Text>
                        <Moment format="hh:mm">
                          {new Date(
                            `${appointment_date} ${appointment_start_time}`
                          ).getTime()}
                        </Moment>{" "}
                        {/* - 7:30 */}
                      </Text>
                    </Box>
                  </HStack>
                </Td>
                {/* <Td p={0}>
                  <Stack mb={2} bg={isAvailable ? "white": 'gray.100'} py={4} px={3} spacing={0}>
                    <Badge
                      w="fit-content"
                      textTransform="initial"
                      rounded={4}
                      fontWeight="normal"
                      colorScheme="red"
                    >
                      Urgent
                    </Badge>
                  </Stack>
                </Td> */}
                <Td p={0}>
                  <Text
                    maxW="3xs"
                    noOfLines={2}
                    isTruncated
                    mb={2}
                    bg={isAvailable ? "white" : "gray.100"}
                    py={2}
                    px={3}
                    h={10}
                  >
                    {comment}
                  </Text>
                </Td>

                <Td p={0}>
                  <HStack
                    pos="relative"
                    w={24}
                    zIndex={appointmentToCheckIn?.id == id ? 1 : -10}
                  >
                    {isAvailable && (
                      <Button
                        // colorScheme="green"
                        variant="outline"
                        size="sm"
                        onClick={() => {
                          dispatch(
                            setSelectedPatient(
                              patients.find(({ id }) => id === user_id)
                            )
                          );
                          dispatch(
                            openAppointmentModal({
                              checkIn: true,
                              existingAppointmentDetails: eachAppointment,
                            })
                          );
                        }}
                        // icon={<BsFillPersonCheckFill />}
                      >
                        Check In
                      </Button>
                    )}
                  </HStack>
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
      {loading === "FETCH_APPOINTMENTS" && ( // While loading appointments, show spinner
        <Flex w="full" justify="center">
          <BeatLoader size={8} color={theme.colors.primary[500]} />
        </Flex>
      )}
      {!loading &&
        check.emptyArray(appointments) && ( // While loading appointments, show spinner
          <Flex w="full" justify="center">
            <Text fontSize="sm" color="gray.500">
              There are no upcoming appointments to display
            </Text>
          </Flex>
        )}
      <SendToDoctor
        existingAppointmentDetails={appointmentToCheckIn}
        onClose={onClose}
        onOpen={onOpen}
        isOpen={isOpen}
        checkIn
      />
    </Stack>
  );
}
