import { AddIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Flex,
  HStack,
  IconButton,
  Select,
  Stack,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import { debounce } from "lodash";
import Router from "next/router";
import React, { useCallback, useEffect, useState } from "react";
import { ImLab } from "react-icons/im";
import { MdCancel } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import RXIcon from "../../../../components/icons";
import TextEditor from "../../../../components/text-editor";
import { closePopup } from "../../../../redux/slices/commonSlice";
import {
  createEncounter,
  editEncounter,
  encounterActions,
} from "../../../../redux/slices/encounterSlice";
import { patientActions } from "../../../../redux/slices/patientSlice";
import {
  createPrescription,
  prescriptionActions,
} from "../../../../redux/slices/prescriptionSlice";
import { fetchServices } from "../../../../redux/slices/serviceSlice";
import { toastError, toastSuccess } from "../../../../utils";
import PrescribeDrug from "../prescriptions/prescribe-drug";
import UploadFileForPatient from "../uploads/new-upload";
import AddAssessments from "./add-assessments";

export default function EditEncounterView({ encounter }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const toast = useToast();
  const { services } = useSelector((state) => state.services);
  const { retainers } = useSelector((state) => state.retainers);
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);
  const encounters = useSelector((state) => state.encounters);
  const prescriptions = useSelector((state) => state.prescriptions);
  const common = useSelector((state) => state.common);
  const [toShowAssessments, setToShowAssessments] = useState(
    encounter?.diagnoses
  );

  const [diagnoses, setDiagnoses] = useState(
    encounter?.diagnoses?.map(({ text, code }) => ({
      label: text,
      code,
      value: text,
    }))
  );

  const [toUploadDocument, setToUploadDocument] = useState(false);
  const [body, setBody] = useState(encounter?.body || "");
  const [document, setDocument] = useState(null);
  const [prescriptionList, setPrescriptionList] = useState([]);

  const headerOptions = [
    "Complaints",
    "History",
    "Clinical Exam",
    "Assessment",
    "Plan",
  ];

  useEffect(() => {
    check.emptyArray(services) &&
      check.nonEmptyObject(userDetails) && // If services haven't been loaded, then load it
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );
  }, [userDetails]);

  const onEditEncounter = (event) => {
    event.preventDefault();
    if (
      !body
        .replace(/class="[^"]*"/, "")
        .replace(/(<([^>]+)>)/gi, "")
        .replace(/ /g, "")
    ) {
      toast({
        title: "You haven't written anything yet...",
        status: "error",
        duration: 4000,
        position: "bottom-left",
        isClosable: true,
      });
    } else {
      dispatch(
        editEncounter({
          encounter_id: encounter?.id,
          saved_by_id: userDetails.id,
          diagnoses: diagnoses.map(({ code, value }) => ({
            text: value,
            code: code || null,
          })),
          body,
        })
      );
    }
  };

  useEffect(() => {
    if (encounters.error && encounters.error.errorType === "EDIT_ENCOUNTER") {
      // If an encounters.error occurs while creating patient,
      toast(toastError(encounters.error)); // Display Error Message
      dispatch(encounterActions.clearStates()); // Then clear stale encounters.error messages
    }
  }, [encounters.error]);

  useEffect(() => {
    if (encounters.success === "EDIT_ENCOUNTER") {
      // If patient is created successfully,
      toast(
        toastSuccess(
          `Encounter Saved Successfully...${
            check.nonEmptyArray(prescriptionList) && !document
              ? " Saving Prescriptions..."
              : check.nonEmptyArray(prescriptionList)
              ? " Saving Prescriptions"
              : ""
          }${
            document && check.emptyArray(prescriptionList)
              ? " Saving Attachment..."
              : document
              ? " and attachment"
              : ""
          }`
        )
      ); // Display Success message
      dispatch(
        patientActions.editPatientTimeline({
          activity: encounters.editedEncounter,
          type: "encounters",
        })
      );
      dispatch(encounterActions.clearStates()); // Then clear stale error messages

      if (check.emptyArray(prescriptionList) && !document) {
        dispatch(closePopup());
      } else {
        if (document) {
          setToUploadDocument(true);
        }
        if (check.nonEmptyArray(prescriptionList)) {
          dispatch(
            createPrescription({
              user_id: selectedPatient?.id,
              organization_id: selectedPatient.organization_id,
              branch_id: selectedPatient.branch_id,
              retainer_id: selectedPatient.retainer_id,
              encounter_id: encounters.editedEncounter?.id,
              retainer_category: retainers.find(
                ({ id }) => id === selectedPatient.retainer_id
              )?.category,
              visit_status: "new",
              override_add_to_queue: true,
              visit_type: "see-physician",
              care_type: "outpatient",
              services: prescriptionList.map(
                ({ prescriptionId, quantity, comment }) => {
                  return {
                    id: prescriptionId,
                    category: "pharmacy",
                    override_add_to_queue: true,
                    strength_value: services.find(
                      ({ id }) => id == prescriptionId
                    )?.strength_value,
                    strength_unit_type: services.find(
                      ({ id }) => id == prescriptionId
                    )?.strength_unit_type,
                    quantity: parseInt(quantity),
                    instruction: comment,
                    stock_unit_type: services.find(
                      ({ id }) => id == prescriptionId
                    )?.stock_unit_type,
                  };
                }
              ),
            })
          );
        }
      }
    }
  }, [encounters.success]);

  useEffect(() => {
    if (
      prescriptions.error &&
      prescriptions.error.errorType === "CREATE_PRESCRIPTION"
    ) {
      // If an prescriptions.error occurs while creating patient,
      toast(toastError(prescriptions.error)); // Display Error Message
      dispatch(prescriptionActions.clearStates()); // Then clear stale prescriptions.error messages
    }
  }, [prescriptions.error]);

  useEffect(() => {
    if (prescriptions.success === "CREATE_PRESCRIPTION") {
      // If patient is created successfully,
      dispatch(
        patientActions.addEntryToTimelineActivity({
          entry: prescriptions.newestPrescription,
          timelineType: "encounters",
          entryType: "rendered_services",
        })
      );

      !document && dispatch(closePopup());
    }
  }, [prescriptions.success]);

  return (
    <Stack w="full" rounded={8} p={3} border="1px solid lightgrey" bg="white">
      <HStack align="start">
        <Select variant="filled" size="sm" width={40}>
          <option value="SOAP Note">SOAP Note</option>
          <option value="SOAP Note">ANOTHER Note</option>
          <option value="SOAP Note">SOAP Note</option>
        </Select>
        <HStack
          d="inline-block"
          whiteSpace="nowrap"
          overflowX="scroll"
          overflowY="hidden"
          className="container-body"
          w="full"
        >
          <Button
            variant={toShowAssessments ? "solid" : "ghost"}
            colorScheme="primary"
            rounded="full"
            bg={toShowAssessments ? "primary.500" : "primary.50"}
            onClick={() => {
              setToShowAssessments(!toShowAssessments);
            }}
            size="sm"
            rightIcon={toShowAssessments ? <MdCancel /> : <AddIcon />}
          >
            Assessment
          </Button>
          {headerOptions.map((header, index) => (
            <Button
              key={index}
              variant="ghost"
              colorScheme="primary"
              rounded="full"
              bg="primary.50"
              onClick={() =>
                setBody(
                  body +
                    `<p><strong><u>${header}:</u></strong></p><p><br><br></p>`
                )
              }
              size="sm"
              rightIcon={<AddIcon />}
            >
              {header}
            </Button>
          ))}
        </HStack>
      </HStack>
      <AddAssessments
        diagnoses={diagnoses}
        setDiagnoses={setDiagnoses}
        toShowAssessments={toShowAssessments}
      />
      <Box
        as="form"
        mt="0 !important"
        action="submit"
        onSubmit={onEditEncounter}
      >
        <Stack spacing={4} w="full">
          <Stack spacing={5} bg="white">
            <TextEditor
              onChange={(html) => {
                setBody(html);
              }}
              value={body}
              placeholder="Start typing here..."
            />
            <PrescribeDrug
              externalPrescriptionList={prescriptionList}
              setExternalPrescriptionList={setPrescriptionList}
              onOpen={onOpen}
              onClose={onClose}
              isOpen={isOpen}
            />

            <HStack align="center" justify="space-between">
              <HStack>
                <IconButton
                  colorScheme="pink"
                  bg="pink.50"
                  variant="ghost"
                  color="purple.500"
                  icon={<ImLab />}
                />
                <IconButton
                  colorScheme="green"
                  bg={
                    check.emptyArray(prescriptionList)
                      ? "green.100"
                      : "green.400"
                  }
                  _hover={{
                    bg: check.emptyArray(prescriptionList)
                      ? "green.100"
                      : "green.400",
                  }}
                  _active={{
                    bg: check.emptyArray(prescriptionList)
                      ? "green.100"
                      : "green.400",
                  }}
                  onClick={onOpen}
                  color="green.500"
                  icon={<RXIcon />}
                />
              </HStack>
              <HStack>
                <UploadFileForPatient
                  document={document}
                  setDocument={setDocument}
                  toUploadDocument={toUploadDocument}
                  setToUploadDocument={setToUploadDocument}
                  activityType="encounters"
                  activityCategory="user_encounter"
                  activityId={encounters.editedEncounter?.id}
                />
                <Button
                  isLoading={
                    encounters.loading === "EDIT_ENCOUNTER" ||
                    prescriptions.loading === "CREATE_PRESCRIPTION" ||
                    common.loading === "UPLOAD_FILE"
                  }
                  loadingText={`Saving ${
                    encounters.loading === "EDIT_ENCOUNTER"
                      ? "Encounter"
                      : prescriptions.loading === "CREATE_PRESCRIPTION"
                      ? "Prescription"
                      : common.loading === "UPLOAD_FILE"
                      ? "File"
                      : ""
                  }...`}
                  // isDisabled={body == encounter?.body}
                  type="submit"
                  minW={32}
                  colorScheme="primary"
                >
                  Save
                </Button>
              </HStack>
            </HStack>
          </Stack>
        </Stack>
      </Box>
    </Stack>
  );
}
