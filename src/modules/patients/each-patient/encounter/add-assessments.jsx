import { Badge, Box, Divider, HStack, Text } from "@chakra-ui/layout";
import React from "react";
import CustomSelect from "../../../../components/multi-select";
import icd10List from "../../../../utils/icd10List.json";

export default function AddAssessments({
  diagnoses,
  setDiagnoses,
  toShowAssessments,
}) {
  const optionComponent = ({ diagnosis, code, label, value }) => {
    if (diagnosis) {
      return {
        value: diagnosis,
        code,
        diagnosis,
        label: (
          <HStack>
            <Badge colorScheme="green">{code}</Badge>
            <Text fontSize="sm">{diagnosis}</Text>
          </HStack>
        ),
      };
    } else {
      return {
        value,
        diagnosis: value,
        code: null,
        label,
      };
    }
  };

  const filterIllnesses = (inputValue) => {
    return [...icd10List]
      .filter((i) =>
        i.diagnosis.toLowerCase().includes(inputValue.toLowerCase())
      )
      .slice(0, 100)
      .map((illness) => {
        return optionComponent(illness);
      });
  };

  const promiseOptions = (inputValue) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve(filterIllnesses(inputValue));
      }, 500);
    });

  const filterOption = (option, inputValue) => {
    return (
      option.value.includes(inputValue) || option.data.code.includes(inputValue)
    );
  };

  return (
    toShowAssessments && (
      <Box>
        <HStack>
          <Text pl={3}>Assessments:</Text>
          <Box w="full">
            <CustomSelect
              isAsyncCreatable
              value={[...diagnoses].map((illness) => {
                return optionComponent(illness);
              })}
              placeholder="Select Assessments..."
              onChange={(data) => {
                setDiagnoses(data.map((illness) => illness));
              }}
              cacheOptions
              isMulti
              defaultOptions
              loadOptions={promiseOptions}
              filterOption={filterOption}
              formatCreateLabel={(inputValue) => `Add "${inputValue}"`}
            />
          </Box>
        </HStack>
        <Divider />
      </Box>
    )
  );
}
