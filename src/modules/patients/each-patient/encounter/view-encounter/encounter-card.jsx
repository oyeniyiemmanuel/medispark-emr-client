import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Select,
  Input,
  Icon,
  AvatarGroup,
  Image,
  Badge,
  Wrap,
  WrapItem,
  Checkbox,
} from "@chakra-ui/react";
import React, { useRef, useState } from "react";
import { RiEditLine, RiFileList2Fill } from "react-icons/ri";
import { BsFillCaretRightFill } from "react-icons/bs";
import DashboardContainer from "../../../../../components/layouts/dashboard";
import {
  BiCalendarEvent,
  BiChevronRight,
  BiDotsVerticalRounded,
  BiExpand,
  BiMicrophone,
} from "react-icons/bi";
import Head from "next/head";
import { AiFillCaretDown } from "react-icons/ai";
import { CgNotes } from "react-icons/cg";
import Router from "next/router";
import { wordTruncate } from "../../../../../utils";
import {
  MdAddCircle,
  MdCameraAlt,
  MdClose,
  MdDelete,
  MdEdit,
  MdFileDownload,
  MdImage,
  MdMonetizationOn,
} from "react-icons/md";
import EncounterModal from "./encounter-modal";
import { VscFilePdf } from "react-icons/vsc";
import { ImLab } from "react-icons/im";
import RXIcon from "../../../../../components/icons";
import { Rnd } from "react-rnd";
import { AddIcon } from "@chakra-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import Moment from "react-moment";
import "moment-timezone";
import ViewImages from "../../uploads/view-uploads/view-images";
import { openPopup } from "../../../../../redux/slices/commonSlice";
import ViewDocuments from "../../uploads/view-uploads/view-documents";
import ViewUploadInActivities from "../../uploads/view-uploads";
import AuditTrailModal from "../../audit-trail/audit-trail-modal";

export default function EncounterCard({
  encounter,
  isChecked,
  addOrRemoveFromSelectedActivities,
}) {
  const [showMenu, setShowMenu] = useState(false);
  const {
    id,
    organization_id,
    branch_id,
    user_id,
    saved_by_id,
    user_visit_id,
    diagnoses,
    timeline_entry_type,
    body,
    created_at,
    rendered_services,
    files,
    updated_at,
    audits,
  } = encounter;

  const { staff } = useSelector((state) => state.staff);
  const dispatch = useDispatch();

  return (
    <HStack align="start">
      <Checkbox
        mt={4}
        size="lg"
        pos="initial"
        borderColor="gray.400"
        isChecked={isChecked}
        value={id}
        onChange={addOrRemoveFromSelectedActivities}
      />
      <Stack
        onMouseEnter={() => setShowMenu(true)}
        onMouseLeave={() => setShowMenu(false)}
        cursor="pointer"
        // onClick={() => setExpandModal(!expandModal)}
        w="full"
        bg="white"
        border="2px solid"
        borderColor="green.200"
        p={4}
        fontSize="sm"
        // h={250}
        pt={2}
        color="gray.500"
        spacing={2}
        rounded={8}
      >
        <Flex
          minH={12}
          borderBottom="1px solid lightgrey"
          w="full"
          justify="space-between"
          align="center"
        >
          <HStack>
            <HStack>
              <Center boxSize={7} bg="green.100" rounded="full">
                <Icon color="green.500" boxSize={4} as={RiFileList2Fill} />
              </Center>
              <Heading color="green.500" fontSize="xl">
                Encounter
              </Heading>
            </HStack>
            <HStack>
              <Text fontWeight="bold" color="gray.500">
                By{" "}
                {staff?.find(({ id }) => id == audits[0]?.user_id)?.full_name}
              </Text>{" "}
              <Text> | </Text>
              <Text color="gray.500">
                <Moment format="MMMM D, YYYY,">
                  {new Date(created_at).toDateString()}
                </Moment>
              </Text>
              <Text>
                <Moment format="hh:mm a">{new Date(created_at)}</Moment>
              </Text>
            </HStack>
          </HStack>
          {showMenu && (
            <HStack spacing={0}>
              <IconButton
                onClick={() =>
                  dispatch(
                    openPopup({
                      popupType: "Edit Encounter",
                      popupId: id,
                    })
                  )
                }
                variant="ghost"
                icon={<MdEdit />}
              />
              {/* <IconButton variant="ghost" icon={<MdDelete />} /> */}
              {/* <IconButton variant="ghost" icon={<BiExpand />} /> */}
            </HStack>
          )}
        </Flex>

        <Stack>
          <Stack spacing={3} pl={12} w="full">
            <Wrap spacing={1}>
              {diagnoses.map(({ code, text }, index) => (
                <WrapItem key={index}>
                  <HStack
                    rounded={16}
                    bg="primary.50"
                    borderColor="primary.100"
                    borderWidth="1px"
                    py={1}
                    px={2}
                  >
                    {code && <Badge colorScheme="green">{code}</Badge>}
                    <Text fontSize="sm">{text}</Text>
                  </HStack>
                </WrapItem>
              ))}
            </Wrap>
            <Text maxW="xl">
              {wordTruncate(body.replace(/(<([^>]+)>)/gi, " "), 200)}
            </Text>
          </Stack>
        </Stack>

        {/* <HStack pt={2} px={10}>
        <AvatarGroup size="md" max={2}>
          <Avatar name="Ryan Florence" src="https://bit.ly/ryan-florence" />
          <Avatar name="Segun Adebayo" src="https://bit.ly/sage-adebayo" />
          <Avatar name="Kent Dodds" src="https://bit.ly/kent-c-dodds" />
          <Avatar name="Prosper Otemuyiwa" src="https://bit.ly/prosper-baba" />
          <Avatar name="Christian Nwamba" src="https://bit.ly/code-beast" />
        </AvatarGroup>
      </HStack> */}

        <HStack
          minH={10}
          borderTop="1px solid lightgrey"
          justify="space-between"
        >
          <HStack>
            <ViewUploadInActivities
              files={files}
              timeline_entry_type={timeline_entry_type}
              created_at={created_at}
            />
            <AuditTrailModal
              created_at={created_at}
              timeline_entry_type={timeline_entry_type}
              audits={audits}
            />
          </HStack>
          <HStack spacing={3}>
            {/* {showMenu && (
              <Button variant="link" colorScheme="green">
                ₦ See Bill
              </Button>
            )} */}
            {rendered_services?.find(
              ({ service_category }) => service_category == "laboratory"
            ) && (
              <IconButton
                colorScheme="pink"
                bg="pink.50"
                variant="ghost"
                color="purple.500"
                size="sm"
                icon={<ImLab />}
              />
            )}
            {rendered_services?.find(
              ({ service_category }) => service_category == "pharmacy"
            ) && (
              <IconButton
                colorScheme="green"
                bg="green.100"
                _hover={{ bg: "green.100" }}
                _active={{ bg: "green.100" }}
                color="green.500"
                size="sm"
                icon={<RXIcon />}
              />
            )}
          </HStack>
        </HStack>

        {/* <EncounterModal isOpen={isOpen} onOpen={onOpen} onClose={onClose} /> */}
      </Stack>
    </HStack>
  );
}
