import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Select,
  Input,
  Icon,
  useMediaQuery,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { RiEditLine, RiFileList2Fill } from "react-icons/ri";
import { BsFillCaretRightFill } from "react-icons/bs";
import DashboardContainer from "../../../../../components/layouts/dashboard";
import {
  BiCalendarEvent,
  BiChevronRight,
  BiDotsVerticalRounded,
} from "react-icons/bi";
import Head from "next/head";
import { AiFillCaretDown } from "react-icons/ai";
import { CgNotes } from "react-icons/cg";
import Router from "next/router";
import { formatDateToDDMMYY } from "../../../../../utils";
import { MdAddCircle } from "react-icons/md";
import FakeIcon from "../../../../../components/fake-icon";
import { CloseIcon } from "@chakra-ui/icons";

export default function EncounterModal({ isOpen, onOpen, onClose }) {
  const [isPhone] = useMediaQuery("(max-width: 575px)");

  return (
    <Modal
      closeOnOverlayClick={false}
      scrollBehavior={isPhone ? "inside" : "inside"}
      isCentered
      size="lg"
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalOverlay bgColor="rgba(0, 0, 0, 0.2)" />
      <ModalContent
        color="gray.500"
        px={3}
        rounded={0}
        maxW="4xl"
        overflow="scroll"
      >
        <ModalHeader>
          <HStack spacing={4}>
            <Box as="span">Encounter</Box>
            <FakeIcon />
            <FakeIcon />
          </HStack>
          <Divider mt={2} borderBottomWidth="2px" />
        </ModalHeader>
        <ModalCloseButton />
        <ModalBody pt={0} mt={0}>
          <Stack spacing={3}>
            <Flex justify="space-between">
              <Text>Encounter Summary</Text>
              <Button
                variant="link"
                // size="sm"
                colorScheme="primary"
                rightIcon={<MdAddCircle />}
              >
                Edit
              </Button>
            </Flex>
            <Stack
              direction={["column", "column", "row"]}
              spacing={[6, 6, 20]}
              px={6}
            >
              <Stack spacing={3}>
                <Text fontWeight="bold" color="gray.600">
                  Prepared By:
                </Text>
                <HStack fontSize="sm">
                  <Avatar
                    size="sm"
                    name="Kent Dodds"
                    src="https://bit.ly/kent-c-dodds"
                  />
                  <Box>
                    <Text>Dr SID</Text>
                    <Text>Ward 316, 9:00 - 9:30am</Text>
                  </Box>
                </HStack>
              </Stack>

              <Stack spacing={3}>
                <Text fontWeight="bold" color="gray.600">
                  Diagnosis
                </Text>
                <Text>Resistant Malaria</Text>
              </Stack>
              <Stack spacing={3}>
                <Text fontWeight="bold" color="gray.600">
                  Treatment
                </Text>
                <Text>Paracetamol</Text>
              </Stack>
            </Stack>
            <Divider
              pt={5}
              borderBottomColor="gray.300"
              borderBottomWidth="2px"
            />
            <Flex justify="space-between">
              <Text>Consultation</Text>
            </Flex>
            <Stack
              justify="space-between"
              direction={["column", "column", "row"]}
              pl={6}
              spacing={[8, 8, 2]}
            >
              <Stack spacing={4}>
                <HStack spacing={4} align="start">
                  <Text
                    fontWeight="bold"
                    w={24}
                    textAlign="right"
                    color="gray.600"
                  >
                    Diagnosis
                  </Text>
                  <Text>Resistant Malaria</Text>
                </HStack>
                <HStack spacing={4} align="start">
                  <Text
                    fontWeight="bold"
                    w={24}
                    textAlign="right"
                    color="gray.600"
                  >
                    Symptom I
                  </Text>
                  <Text>
                    Running temperature with warmth that goes highin the
                    mornings and cools down towards midday
                  </Text>
                </HStack>
                <HStack spacing={4} align="start">
                  <Text
                    fontWeight="bold"
                    w={24}
                    textAlign="right"
                    color="gray.600"
                  >
                    Symptom I
                  </Text>
                  <Text>
                    Running temperature with warmth that goes highin the
                    mornings and cools down towards midday
                  </Text>
                </HStack>
                <HStack spacing={4} align="start">
                  <Text
                    fontWeight="bold"
                    w={24}
                    textAlign="right"
                    color="gray.600"
                  >
                    Test
                  </Text>
                  <Text>X-Ray and Bronchial Swab</Text>
                </HStack>
              </Stack>
              <Stack spacing={4}>
                <HStack align="start" spacing={4}>
                  <Text
                    fontWeight="bold"
                    w={24}
                    textAlign="right"
                    color="gray.600"
                  >
                    Presc.
                  </Text>
                  <Box>
                    <Text>Paracetamol</Text>
                    <Text>Resistant Malaria</Text>
                    <Text>Resistant Malaria</Text>
                  </Box>
                </HStack>
              </Stack>
              <Stack
                mt={["30px !important", "30px !important", "-5px !important"]}
                w={["full", "full", "500px"]}
                bg="gray.70"
                spacing={0}
              >
                <Flex
                  px={3}
                  py={2}
                  w="full"
                  align="center"
                  height={12}
                  bg="gray.200"
                >
                  <Text fontWeight="bold" color="gray.600">
                    Billing
                  </Text>
                </Flex>
                <Flex px={3} py={2} w="full" height={20}>
                  <Text fontWeight="bold" color="green.500">
                    #12,000
                  </Text>
                </Flex>
                <Flex px={3} py={2} w="full" height={20}>
                  <Text fontWeight="bold" color="green.500">
                    #12,000
                  </Text>
                </Flex>
                <Flex px={3} py={2} w="full" height={20}>
                  <Text fontWeight="bold" color="green.500">
                    #12,000
                  </Text>
                </Flex>
                <Flex px={3} py={2} w="full" height={20}>
                  <HStack>
                    <Text fontWeight="bold" color="gray.600">
                      Total
                    </Text>
                    <Text fontWeight="bold" color="green.500">
                      #12,000
                    </Text>
                  </HStack>
                </Flex>
              </Stack>
            </Stack>
            <Divider
              pt={5}
              borderBottomColor="gray.300"
              borderBottomWidth="2px"
            />
            <Flex justify="space-between">
              <Text>Case Notes</Text>
            </Flex>
          </Stack>
          <Box>
            <Text fontSize="sm">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo
              corporis quis porro suscipit eius laborum assumenda labore cum
              quasi, quos, rerum voluptate perferendis hic tenetur, odit magni.
              Ipsa, necessitatibus nulla.
            </Text>
          </Box>
        </ModalBody>

        <ModalFooter pt={8}>
          <Button w={32} colorScheme="gray" mr={3}>
            Print
          </Button>
          <Button
            w={32}
            rightIcon={<CloseIcon boxSize={2} />}
            variant="outline"
            onClick={onClose}
            colorScheme="gray"
          >
            Close
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
