import { Button, IconButton } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Input } from "@chakra-ui/input";
import { Divider, Flex, HStack, Stack, Text } from "@chakra-ui/layout";
import check from "check-types";
import React, { useEffect, useState } from "react";
import { AiFillCaretDown } from "react-icons/ai";
import { BiCalendarEvent, BiDotsVerticalRounded } from "react-icons/bi";
import { BsGrid1X2Fill } from "react-icons/bs";
import { MdFormatListBulleted } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { fetchPatientTimeline } from "../../../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../../../redux/slices/retainerSlice";
import { fetchServices } from "../../../../../redux/slices/serviceSlice";
import { fetchStaff } from "../../../../../redux/slices/staffSlice";
import EncounterCard from "./encounter-card";
import FilterResults from "react-filter-search";
import { Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/menu";

export default function ViewEncounters() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient, patientTimeline, loading, success, error } =
    useSelector((state) => state.patients);
  const dispatch = useDispatch();

  const { retainers } = useSelector((state) => state.retainers);
  const { services } = useSelector((state) => state.services);
  const { staff } = useSelector((state) => state.staff);
  const [searchValue, setSearchValue] = useState("");
  const [selectedDocumenterId, setSelectedDocumenterId] = useState(null);

  const allEncounters = check.nonEmptyObject(patientTimeline)
    ? [...patientTimeline.encounters].sort(
        (a, b) => new Date(b.created_at) - new Date(a.created_at)
      )
    : [];

  useEffect(() => {
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
    check.emptyArray(services) &&
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );

    check.emptyArray(staff) &&
      dispatch(
        fetchStaff({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );
  }, [userDetails]);

  const encounterDocumenters = [
    ...new Set(allEncounters.map(({ audits }) => audits[0]?.user_id)),
  ];

  const findStaff = (staffId) => staff?.find(({ id }) => id == staffId);

  const filterByStaff = ({ audits }) => {
    if (selectedDocumenterId) {
      return audits[0]?.user_id == selectedDocumenterId;
    } else {
      return true;
    }
  };

  return (
    <Stack spacing={4}>
      <Flex justify="flex-end">
        <HStack color="gray.500">
          <Input
            size="sm"
            pos="initial"
            w={40}
            value={searchValue}
            onChange={(event) => {
              setSearchValue(event.target.value);
            }}
            placeholder="Search Encounters"
            rounded="full"
          />
          <Menu placement="bottom-end">
            <MenuButton
              variant={selectedDocumenterId ? "solid" : "ghost"}
              size="sm"
              as={Button}
              rightIcon={<AiFillCaretDown />}
            >
              {selectedDocumenterId
                ? findStaff(selectedDocumenterId)?.full_name
                : "All Users"}
            </MenuButton>
            <MenuList>
              <MenuItem onClick={() => setSelectedDocumenterId(null)}>
                All Staff
              </MenuItem>
              {encounterDocumenters.map((documenterId) => (
                <MenuItem
                  onClick={() => setSelectedDocumenterId(documenterId)}
                  key={documenterId}
                >
                  {findStaff(documenterId)?.full_name}
                </MenuItem>
              ))}
            </MenuList>
          </Menu>

          <Button variant="ghost" size="sm" rightIcon={<BiCalendarEvent />}>
            Filter by date
          </Button>
        </HStack>
      </Flex>
      <Divider opacity="initial" borderColor="gray.400" />
      <FilterResults
        value={searchValue}
        data={allEncounters.filter(filterByStaff)}
        renderResults={(results) =>
          check.emptyArray(results) ? (
            <Text
              color="gray.500"
              w="full"
              wordBreak="break-word"
              textAlign="center"
            >
              When you add a new encounter, it will show here
            </Text>
          ) : (
            results.map((encounter) => (
              <EncounterCard key={encounter.id} encounter={encounter} />
            ))
          )
        }
      />
    </Stack>
  );
}
