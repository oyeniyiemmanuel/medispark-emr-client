import { AddIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  HStack,
  IconButton,
  Select,
  Stack,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import { debounce } from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import { ImLab } from "react-icons/im";
import { MdCancel } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import RXIcon from "../../../../components/icons";
import TextEditor from "../../../../components/text-editor";
import { closePopup } from "../../../../redux/slices/commonSlice";
import {
  createEncounter,
  encounterActions,
} from "../../../../redux/slices/encounterSlice";
import { patientActions } from "../../../../redux/slices/patientSlice";
import {
  createPrescription,
  prescriptionActions,
} from "../../../../redux/slices/prescriptionSlice";
import { fetchServices } from "../../../../redux/slices/serviceSlice";
import { toastError, toastSuccess } from "../../../../utils";
import PrescribeDrug from "../prescriptions/prescribe-drug";
import UploadFileForPatient from "../uploads/new-upload";
import AddAssessments from "./add-assessments";

export default function CreateEncounterView() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const toast = useToast();
  const { services } = useSelector((state) => state.services);
  const { retainers } = useSelector((state) => state.retainers);
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);
  const encounters = useSelector((state) => state.encounters);
  const prescriptions = useSelector((state) => state.prescriptions);
  const [toShowAssessments, setToShowAssessments] = useState(false);
  const [diagnoses, setDiagnoses] = useState([]);
  const [body, setBody] = useState(() => {
    return localStorage.getItem("encounter-draft") || "";
  });
  const [document, setDocument] = useState(null);
  const [toUploadDocument, setToUploadDocument] = useState(false);
  const common = useSelector((state) => state.common);

  const [prescriptionList, setPrescriptionList] = useState([]);
  const headerOptions = ["Complaints", "History", "Clinical Exam", "Plan"];

  const storeEncounterDraft = useCallback(
    debounce(() => {
      localStorage.setItem("encounter-draft", body);
    }, 1000),
    []
  );

  useEffect(() => {
    check.emptyArray(services) &&
      check.nonEmptyObject(userDetails) && // If services haven't been loaded, then load it
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );
  }, [userDetails]);

  const onCreateNewEncounter = (event) => {
    event.preventDefault();
    if (
      !body
        .replace(/class="[^"]*"/, "")
        .replace(/(<([^>]+)>)/gi, "")
        .replace(/ /g, "")
    ) {
      toast({
        title: "You haven't written anything yet...",
        status: "error",
        duration: 4000,
        position: "bottom-left",
        isClosable: true,
      });
    } else {
      dispatch(
        createEncounter({
          organization_id: userDetails.organization_id,
          branch_id: userDetails.branch_id,
          user_id: selectedPatient.id,
          saved_by_id: userDetails.id,
          user_visit_id: 1,
          diagnoses: diagnoses.map(({ code, value }) => ({
            text: value,
            code: code || null,
          })),
          body,
        })
      );
    }
  };

  useEffect(() => {
    if (encounters.error && encounters.error.errorType === "CREATE_ENCOUNTER") {
      // If an encounters.error occurs while creating patient,
      toast(toastError(encounters.error)); // Display Error Message
      dispatch(encounterActions.clearStates()); // Then clear stale encounters.error messages
    }
  }, [encounters.error]);

  useEffect(() => {
    if (encounters.success === "CREATE_ENCOUNTER") {
      // If patient is created successfully,
      toast(
        toastSuccess(
          `Encounter Created Successfully...${
            check.nonEmptyArray(prescriptionList) && !document
              ? " Saving Prescriptions..."
              : check.nonEmptyArray(prescriptionList)
              ? " Saving Prescriptions"
              : ""
          }${
            document && check.emptyArray(prescriptionList)
              ? " Saving Attachment..."
              : document
              ? " and attachment"
              : ""
          }`
        )
      ); // Display Success message
      localStorage.removeItem("encounter-draft");

      if (check.emptyArray(prescriptionList) && !document) {
        dispatch(closePopup());
      } else {
        if (document) {
          setToUploadDocument(true);
        }
        if (check.nonEmptyArray(prescriptionList)) {
          dispatch(
            createPrescription({
              user_id: selectedPatient.id,
              organization_id: selectedPatient.organization_id,
              branch_id: selectedPatient.branch_id,
              retainer_id: selectedPatient.retainer_id,
              encounter_id: encounters.newestEncounter?.id,
              retainer_category: retainers.find(
                ({ id }) => id === selectedPatient.retainer_id
              )?.category,
              visit_status: "new",
              override_add_to_queue: true,
              visit_type: "see-physician",
              care_type: "outpatient",
              services: prescriptionList.map(
                ({ prescriptionId, quantity, comment }) => {
                  return {
                    id: prescriptionId,
                    category: "pharmacy",
                    override_add_to_queue: true,
                    strength_value: services.find(
                      ({ id }) => id == prescriptionId
                    )?.strength_value,
                    strength_unit_type: services.find(
                      ({ id }) => id == prescriptionId
                    )?.strength_unit_type,
                    quantity,
                    instruction: comment,
                    stock_unit_type: services.find(
                      ({ id }) => id == prescriptionId
                    )?.stock_unit_type,
                  };
                }
              ),
            })
          );
        }
      }

      dispatch(
        patientActions.addToPatientTimeline({
          activity: encounters.newestEncounter,
          type: "encounters",
        })
      );
      dispatch(encounterActions.clearStates()); // Then clear stale error messages
    }
  }, [encounters.success]);

  useEffect(() => {
    if (prescriptions.success === "CREATE_PRESCRIPTION") {
      // If prescription is created successfully,
      dispatch(
        patientActions.addEntryToTimelineActivity({
          // add the prescription under services_rendered in the newly created encounter
          entry: prescriptions.newestPrescription,
          timelineType: "encounters",
          entryType: "rendered_services",
        })
      );

      !document && dispatch(closePopup());
    }
  }, [prescriptions.success]);

  return (
    <Stack w="full" rounded={8} p={3} border="1px solid lightgrey" bg="white">
      <HStack align="start">
        <Select variant="filled" size="sm" width={40}>
          <option value="SOAP Note">SOAP Note</option>
          <option value="SOAP Note">ANOTHER Note</option>
          <option value="SOAP Note">SOAP Note</option>
        </Select>
        <HStack
          d="inline-block"
          whiteSpace="nowrap"
          overflowX="scroll"
          overflowY="hidden"
          className="container-body"
          w="full"
        >
          <Button
            variant={toShowAssessments ? "solid" : "ghost"}
            colorScheme="primary"
            rounded="full"
            bg={toShowAssessments ? "primary.500" : "primary.50"}
            onClick={() => {
              setToShowAssessments(!toShowAssessments);
            }}
            size="sm"
            rightIcon={toShowAssessments ? <MdCancel /> : <AddIcon />}
          >
            Assessment
          </Button>
          {headerOptions.map((header, index) => (
            <Button
              key={index}
              variant="ghost"
              colorScheme="primary"
              rounded="full"
              bg="primary.50"
              onClick={() =>
                setBody(
                  body +
                    `<p><strong><u>${header}:</u></strong></p><p><br><br></p>`
                )
              }
              size="sm"
              rightIcon={<AddIcon />}
            >
              {header}
            </Button>
          ))}
        </HStack>
      </HStack>
      <AddAssessments
        diagnoses={diagnoses}
        setDiagnoses={setDiagnoses}
        toShowAssessments={toShowAssessments}
      />
      <Box
        as="form"
        mt="0 !important"
        action="submit"
        onSubmit={onCreateNewEncounter}
      >
        <Stack spacing={4} w="full">
          <Stack spacing={5} bg="white">
            <TextEditor
              onChange={(html) => {
                setBody(html);
                storeEncounterDraft();
              }}
              value={body}
              placeholder="Start typing here..."
            />
            <PrescribeDrug
              externalPrescriptionList={prescriptionList}
              setExternalPrescriptionList={setPrescriptionList}
              onOpen={onOpen}
              onClose={onClose}
              isOpen={isOpen}
            />

            <HStack align="center" justify="space-between">
              <HStack>
                <IconButton
                  colorScheme="pink"
                  bg="pink.50"
                  variant="ghost"
                  color="purple.500"
                  icon={<ImLab />}
                />
                <IconButton
                  colorScheme="green"
                  bg={
                    check.emptyArray(prescriptionList)
                      ? "green.100"
                      : "green.400"
                  }
                  _hover={{
                    bg: check.emptyArray(prescriptionList)
                      ? "green.100"
                      : "green.400",
                  }}
                  _active={{
                    bg: check.emptyArray(prescriptionList)
                      ? "green.100"
                      : "green.400",
                  }}
                  onClick={onOpen}
                  color="green.500"
                  icon={<RXIcon />}
                />
              </HStack>
              <HStack>
                <UploadFileForPatient
                  document={document}
                  setDocument={setDocument}
                  toUploadDocument={toUploadDocument}
                  setToUploadDocument={setToUploadDocument}
                  activityType="encounters"
                  activityCategory="user_encounter"
                  activityId={encounters.newestEncounter?.id}
                />
                <Button
                  isLoading={
                    encounters.loading === "CREATE_ENCOUNTER" ||
                    prescriptions.loading === "CREATE_PRESCRIPTION" ||
                    common.loading === "UPLOAD_FILE"
                  }
                  loadingText={`Saving ${
                    encounters.loading === "CREATE_ENCOUNTER"
                      ? "Encounter"
                      : prescriptions.loading === "CREATE_PRESCRIPTION"
                      ? "Prescription"
                      : common.loading === "UPLOAD_FILE"
                      ? "File"
                      : ""
                  }...`}
                  type="submit"
                  minW={32}
                  colorScheme="primary"
                >
                  Save
                </Button>
              </HStack>
            </HStack>
          </Stack>
        </Stack>
      </Box>
    </Stack>
  );
}
