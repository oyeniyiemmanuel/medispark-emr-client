import {
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
  Avatar,
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  Icon,
  Input,
  Select,
  Stack,
  Text,
  Textarea,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import { IoIosArrowDropdownCircle } from "react-icons/io";
import { MdAddCircle, MdRemoveCircle } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import FakeIcon from "../../../../components/fake-icon";
import {
  createEncounter,
  encounterActions,
} from "../../../../redux/slices/encounterSlice";
import {
  createPrescription,
  prescriptionActions,
} from "../../../../redux/slices/prescriptionSlice";
import { fetchServices } from "../../../../redux/slices/serviceSlice";
import { toastError, toastSuccess } from "../../../../utils";
import PrescribeDrug from "../prescriptions/prescribe-drug";
import PatientContainer from "../shared/patient-container";
import VitalsSummary from "../shared/vitals-summary";

export default function OldCreateEncounterView() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const toast = useToast();
  const { services } = useSelector((state) => state.services);
  const { retainers } = useSelector((state) => state.retainers);
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);
  const encounters = useSelector((state) => state.encounters);
  const prescriptions = useSelector((state) => state.prescriptions);
  const [diagnosis, setDiagnosis] = useState("");
  const [symptoms, setSymptoms] = useState("");
  const [prescriptionList, setPrescriptionList] = useState([]);
  const [selectedPrescription, setSelectedPrescription] = useState(null);

  useEffect(() => {
    check.emptyArray(services) &&
      check.nonEmptyObject(userDetails) && // If services haven't been loaded, then load it
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );
  }, [userDetails]);

  const onCreateNewEncounter = (event) => {
    event.preventDefault();
    dispatch(
      createEncounter({
        organization_id: userDetails.organization_id,
        branch_id: userDetails.branch_id,
        user_id: selectedPatient.id,
        saved_by_id: userDetails.id,
        user_visit_id: 1,
        diagnosis,
        symptoms,
      })
    );
  };

  useEffect(() => {
    if (encounters.error && encounters.error.errorType === "CREATE_ENCOUNTER") {
      // If an encounters.error occurs while creating patient,
      toast(toastError(encounters.error)); // Display Error Message
      dispatch(encounterActions.clearStates()); // Then clear stale encounters.error messages
    }
  }, [encounters.error]);

  useEffect(() => {
    if (encounters.success === "CREATE_ENCOUNTER") {
      // If patient is created successfully,
      toast(
        toastSuccess(
          `Encounter Created Successfully...${
            check.emptyArray(prescriptionList) && " Saving Prescriptions..."
          }`
        )
      ); // Display Success message
      if (check.emptyArray(prescriptionList)) {
        Router.push(`/patients/${selectedPatient.id}`);
      } else {
        dispatch(
          createPrescription({
            user_id: selectedPatient.id,
            organization_id: selectedPatient.organization_id,
            branch_id: selectedPatient.branch_id,
            retainer_id: selectedPatient.retainer_id,
            encounter_id: 1,
            retainer_category: retainers.find(
              ({ id }) => id === selectedPatient.retainer_id
            )?.category,
            visit_status: "new",
            visit_type: "see-physician",
            care_type: "outpatient",
            services: prescriptionList.map(
              ({
                prescriptionId,
                dosageValue,
                periodValue,
                periodType,
                durationValue,
                durationType,
                comment,
              }) => {
                return {
                  id: prescriptionId,
                  category: "pharmacy",
                  inventory_unit_value: dosageValue,
                  inventory_unit_type: services.find(
                    ({ id }) => id == prescriptionId
                  )?.inventory_unit_type,
                  medication_period_value: periodValue,
                  medication_period_type: periodType,
                  medication_duration_value: durationValue,
                  medication_duration_type: durationType,
                  comment,
                };
              }
            ),
          })
        );
      }

      dispatch(encounterActions.clearStates()); // Then clear stale error messages
    }
  }, [encounters.success]);

  useEffect(() => {
    if (
      prescriptions.error &&
      prescriptions.error.errorType === "CREATE_PRESCRIPTION"
    ) {
      // If an prescriptions.error occurs while creating patient,
      toast(toastError(prescriptions.error)); // Display Error Message
      dispatch(prescriptionActions.clearStates()); // Then clear stale prescriptions.error messages
    }
  }, [prescriptions.error]);

  useEffect(() => {
    if (prescriptions.success === "CREATE_PRESCRIPTION") {
      // If patient is created successfully,
      Router.push(`/patients/${selectedPatient.id}`);
    }
  }, [prescriptions.success]);

  return (
    <PatientContainer title="Encounter Patient">
      <form action="submit" onSubmit={onCreateNewEncounter}>
        <Stack spacing={4} w="full">
          <Select
            size="lg"
            bg="white"
            border="none"
            fontSize="xl"
            color="gray.500"
            isFullWidth
          >
            <option value="New Encounter">New Encounter</option>
            {/* <option value="Immunization">Immunization</option>
          <option value="Procedure">Procedure</option> */}
          </Select>
          {/* <Accordion allowMultiple>
            <AccordionItem border={0}>
              <AccordionButton px={8} border={0}>
                <Text w="60%" textAlign="left" fontSize="sm" color="gray.600">
                  Last seen on{" "}
                  <Box as="span" fontWeight="bold">
                    01 January, 2021
                  </Box>
                </Text>
                <Divider borderBottomColor="gray.500" w="full" />
                <Icon as={IoIosArrowDropdownCircle} colo />
              </AccordionButton>
              <AccordionPanel border={0} pb={4}>
                <Stack fontSize="sm" px={4} color="gray.500">
                  <Stack direction="row" align="center" justify="space-between">
                    <Text>Encounter with Dr Sid on 01, January 2021</Text>
                    <HStack>
                      <FakeIcon />
                      <FakeIcon />
                      <FakeIcon />
                      <FakeIcon />
                    </HStack>
                  </Stack>
                </Stack>
              </AccordionPanel>
            </AccordionItem>
          </Accordion>
         */}
          <Stack
            spacing={5}
            bg="white"
            p={6}
            pb={12}
            // {...rest}
          >
            <VitalsSummary />
            <Stack py={3} spacing={12}>
              <Stack align="center" spacing={4} direction="row">
                <Text fontWeight="bold" color="gray.600" w={32}>
                  Diagnosis
                </Text>
                <Input
                  isRequired
                  value={diagnosis}
                  onChange={(event) => setDiagnosis(event.target.value)}
                  variant="filled"
                />
              </Stack>
              <Stack spacing={4} direction="row">
                <Text fontWeight="bold" color="gray.600" w={32}>
                  Symptom
                </Text>
                <Textarea
                  isRequired
                  value={symptoms}
                  onChange={(event) => setSymptoms(event.target.value)}
                  h={32}
                  variant="filled"
                />
              </Stack>
            </Stack>
            <Divider borderBottomColor="gray.400" />
            <Stack direction="row">
              <PrescribeDrug
                prescriptionList={prescriptionList}
                setPrescriptionList={setPrescriptionList}
                selectedPrescription={selectedPrescription}
                setSelectedPrescription={setSelectedPrescription}
                onOpen={onOpen}
                onClose={onClose}
                isOpen={isOpen}
              />
              <Text fontWeight="bold" color="gray.600" w={32}>
                Prescription
              </Text>
              <Stack>
                {prescriptionList.map((prescription) => (
                  <Stack key={prescription.id} spacing={4}>
                    <Text fontWeight="bold" color="gray.600">
                      {
                        services.find(
                          ({ id }) => id == prescription.prescriptionId
                        )?.name
                      }
                    </Text>
                    <Stack color="gray.600" direction="row" spacing={12}>
                      <Text>
                        {prescription.dosageValue}{" "}
                        {
                          services.find(
                            ({ id }) => id == prescription.prescriptionId
                          )?.inventory_unit_type
                        }
                        s
                      </Text>
                      <Text>
                        {prescription.periodValue} {prescription.periodType} for{" "}
                      </Text>
                      <Text>
                        {prescription.durationValue} {prescription.durationType}
                      </Text>
                    </Stack>
                    <Text color="gray.400" fontSize="sm">
                      {prescription.comment}
                    </Text>
                    <HStack py={3} spacing={8}>
                      <Button
                        variant="link"
                        size="sm"
                        onClick={() => {
                          setSelectedPrescription(prescription);
                          onOpen();
                        }}
                        colorScheme="primary"
                        rightIcon={<MdAddCircle />}
                      >
                        Edit
                      </Button>
                      <Button
                        variant="link"
                        size="sm"
                        onClick={() => {
                          setPrescriptionList([
                            ...prescriptionList,
                            {
                              ...prescription,
                              id: prescriptionList.length + 1,
                            },
                          ]);
                        }}
                        colorScheme="primary"
                        rightIcon={<MdAddCircle />}
                      >
                        Duplicate
                      </Button>
                      <Button
                        variant="link"
                        size="sm"
                        onClick={() => {
                          setPrescriptionList(
                            [...prescriptionList].filter(
                              ({ id }) => id !== prescription.id
                            )
                          );
                        }}
                        colorScheme="red"
                        rightIcon={<MdRemoveCircle />}
                      >
                        Remove
                      </Button>
                    </HStack>
                  </Stack>
                ))}
              </Stack>
            </Stack>
            <Divider borderBottomColor="gray.400" />
            <Stack pb={8} direction="row">
              <Text fontWeight="bold" color="gray.600" w={32}>
                Case Note
              </Text>
              <Stack w="full" spacing={4}>
                <Textarea variant="filled" h={40} />
              </Stack>
            </Stack>
            <HStack justify="flex-end">
              {services.map(({ id, name }) => (
                <FakeIcon
                  cursor="pointer"
                  onClick={onOpen}
                  key={id}
                  boxSize={10}
                  rounded={4}
                />
              ))}
            </HStack>
            <Flex pt={12} justify="center">
              <Button
                isLoading={
                  encounters.loading === "CREATE_ENCOUNTER" ||
                  prescriptions.loading === "CREATE_PRESCRIPTION"
                }
                loadingText="Saving..."
                type="submit"
                w="xs"
                size="lg"
                colorScheme="primary"
              >
                Done
              </Button>
            </Flex>
          </Stack>
        </Stack>
      </form>
    </PatientContainer>
  );
}
