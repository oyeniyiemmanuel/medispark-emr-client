import { Icon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  Input,
  Stack,
  Text,
  Textarea,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import Head from "next/head";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import { MdAddCircle } from "react-icons/md";
import { IoDocumentText } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import UploadFile from "../../../../components/upload-file";
import {
  documentationActions,
  recordNote,
} from "../../../../redux/slices/documentationSlice";
import { commonActions } from "../../../../redux/slices/commonSlice";
import { getFileIconByType, toastError, toastSuccess } from "../../../../utils";
import VitalsSummary from "../shared/vitals-summary";
import { uploadFile } from "../../../../redux/slices/commonSlice";
import TextEditor from "../../../../components/text-editor";

export default function RecordNotesOld({ ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const initialNoteDetailState = {
    // Set Initial Values of input fields
    title: "",
    body: "",
  };
  const [newNote, setNewNote] = useState(
    initialNoteDetailState // Store values in state
  );
  const [document, setDocument] = useState(null);
  const handleChange = (event) => {
    const { name, value } = event.target;
    setNewNote({ ...newNote, [name]: value }); // onChange handler
  };
  const dispatch = useDispatch();
  const toast = useToast();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);
  const common = useSelector((state) => state.common);
  const documentations = useSelector((state) => state.documentations);
  const { title, body } = newNote;

  const onRecordNewNote = (event) => {
    event.preventDefault();
    if (
      !body
        .replace(/class="[^"]*"/, "")
        .replace(/(<([^>]+)>)/gi, "")
        .replace(/ /g, "")
    ) {
      toast({
        title: "You haven't written anything yet...",
        status: "error",
        duration: 4000,
        position: "bottom-left",
        isClosable: true,
      });
    } else {
      dispatch(
        recordNote({
          organization_id: userDetails.organization_id,
          branch_id: userDetails.branch_id,
          user_id: selectedPatient.id,
          // title,
          body,
        })
      );
    }
  };

  useEffect(() => {
    if (
      documentations.error &&
      documentations.error.errorType === "RECORD_NOTE"
    ) {
      // If an documentations.error occurs while creating patient,
      toast(toastError(documentations.error)); // Display Error Message
      dispatch(documentationActions.clearStates()); // Then clear stale documentations.error messages
    }
  }, [documentations.error]);

  useEffect(() => {
    if (documentations.success === "RECORD_NOTE") {
      // If patient is created successfully,
      toast(
        toastSuccess(
          `Note Recorded Successfully${document && ". Uploading file..."}`
        )
      ); // Display Success message
      dispatch(documentationActions.clearStates()); // Then clear stale error messages
      if (document) {
        const formData = new FormData();
        formData.append("organization_id", userDetails.organization_id);
        formData.append("branch_id", userDetails.branch_id);
        formData.append("user_id", selectedPatient?.id);
        formData.append("note_id", 1);
        formData.append("upload_category", "user_note");
        formData.append("file", document);

        dispatch(uploadFile({ formData, service: "clinic" }));
      } else {
        Router.push(`/patients/${selectedPatient.id}`);
      }
    }
  }, [documentations.success]);

  useEffect(() => {
    if (common.error && common.error.errorType === "UPLOAD_FILE") {
      // If an common.error occurs while creating patient,
      toast(toastError(common.error)); // Display Error Message
      dispatch(commonActions.clearStates()); // Then clear stale common.error messages
    }
  }, [common.error]);

  useEffect(() => {
    if (common.success === "UPLOAD_FILE") {
      toast(toastSuccess("File uploaded successfully!")); // Display Success message

      dispatch(commonActions.clearStates()); // Then clear stale error messages

      Router.push(`/patients/${selectedPatient.id}`);
    }
  }, [common.success]);
  return (
    <>
      <form onSubmit={onRecordNewNote} action="submit">
        <Stack spacing={5} bg="white" py={2} px={4} pb={12} {...rest}>
          {/* <Textarea
          border="none"
          h={250}
          isRequired
          name="body"
          value={body}
          onChange={handleChange}
          placeholder="Body text: Type in something here"
        /> */}

          <TextEditor
            onChange={(html) => setNewNote({ ...newNote, body: html })}
            value={body}
            placeholder="Body text: Type in something here"
          />

          <Divider borderBottomColor="gray.400" />
          <Flex justifyContent="flex-end">
            {document ? (
              <HStack
                _hover={{ bg: "gray.100" }}
                p={2}
                rounded={4}
                cursor="pointer"
                spacing={8}
              >
                <HStack>
                  {getFileIconByType(document.type)}
                  <Text color="gray.600" fontWeight="bold">
                    {document.name}
                  </Text>
                </HStack>
                <CloseButton
                  onClick={() => setDocument(null)}
                  color="gray.600"
                />
              </HStack>
            ) : (
              <Button
                variant="link"
                size="sm"
                colorScheme="primary"
                rightIcon={<MdAddCircle />}
                onClick={onOpen}
              >
                Upload
              </Button>
            )}
          </Flex>
          <Divider borderBottomColor="gray.400" />

          <Stack spacing={4}>
            <Flex pt={4} justify="flex-end">
              <Button
                type="submit"
                isLoading={
                  documentations.loading === "RECORD_NOTE" ||
                  common.loading === "UPLOAD_FILE"
                }
                loadingText="Saving..."
                size="lg"
                w="2xs"
                colorScheme="primary"
              >
                Save
              </Button>
            </Flex>
          </Stack>
          <UploadFile
            isOpen={isOpen}
            fileToUpload={document}
            setFileToUpload={setDocument}
            onOpen={onOpen}
            onClose={onClose}
          />
        </Stack>
      </form>
    </>
  );
}
