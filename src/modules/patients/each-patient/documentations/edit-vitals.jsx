import {
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  Input,
  Stack,
  Text,
  useToast,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import { MdAddCircle } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { closePopup } from "../../../../redux/slices/commonSlice";
import {
  clearStates,
  editVital,
  recordVitals,
} from "../../../../redux/slices/documentationSlice";
import { patientActions } from "../../../../redux/slices/patientSlice";
import { toastError, toastSuccess } from "../../../../utils";

export default function EditVitals({ vital, ...rest }) {
  const initialVitalDetailState = {
    // Set Initial Values of input fields
    temperature: vital.temperature || "",
    pulse: vital.pulse || "",
    weight: vital.weight || "",
    bmi: vital.body_mass_index || "",
    respiration: vital.respiration || "",
    pain: vital.pain_score || "",
    height: vital.height || "",
    sp02: vital.oxygen_saturation || "",
    systolicBp: vital.systolic_bp || "",
    diastolicBp: vital.diastolic_bp || "",
  };
  const [existingVitals, setExistingVitals] = useState(
    initialVitalDetailState // Store values in state
  );
  const handleChange = (event) => {
    const { name, value } = event.target;
    setExistingVitals({ ...existingVitals, [name]: value }); // onChange handler
  };
  const dispatch = useDispatch();
  const toast = useToast();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);
  const { vitals, editedVital, loading, success, error } = useSelector(
    (state) => state.documentations
  );

  const onEditVitals = (event) => {
    event.preventDefault();
    if (
      JSON.stringify(existingVitals) == JSON.stringify(initialVitalDetailState)
    ) {
      toast({
        title: "You haven't filled in any vital",
        status: "error",
        duration: 4000,
        position: "bottom-left",
        isClosable: true,
      });
    } else {
      dispatch(
        editVital({
          vitals_id: vital.id,
          saved_by_id: userDetails.id,
          temperature: existingVitals.temperature,
          pulse: existingVitals.pulse,
          respiration: existingVitals.respiration,
          oxygen_saturation: existingVitals.sp02,
          weight: existingVitals.weight,
          height: existingVitals.height,
          systolic_bp: existingVitals.systolicBp,
          diastolic_bp: existingVitals.diastolicBp,
          body_mass_index: existingVitals.bmi,
          pain_score: existingVitals.pain,
        })
      );
    }
  };

  useEffect(() => {
    if (error && error.errorType === "EDIT_VITAL") {
      // If an error occurs while creating patient,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  useEffect(() => {
    if (success === "EDIT_VITAL") {
      // If patient is created successfully,
      toast(toastSuccess("Vitals Saved Successfully!")); // Display Success message

      dispatch(
        patientActions.editPatientTimeline({
          activity: editedVital,
          type: "vitals",
        })
      );

      dispatch(clearStates()); // Then clear stale error messages
      dispatch(closePopup());
    }
  }, [success]);

  return (
    <Stack spacing={5} border="1px solid lightgrey" bg="white" p={6} {...rest}>
      <form onSubmit={onEditVitals} action="submit">
        <Stack spacing={2}>
          <Wrap align="center" justify="space-between" spacing={2}>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">
                  Temperature
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(°C)
                  </Box>
                </Text>
                <Input
                  value={existingVitals.temperature}
                  onChange={handleChange}
                  name="temperature"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">
                  Weight
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(kg)
                  </Box>
                </Text>
                <Input
                  value={existingVitals.weight}
                  onChange={handleChange}
                  name="weight"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">
                  Height
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(cm)
                  </Box>
                </Text>
                <Input
                  value={existingVitals.height}
                  onChange={handleChange}
                  name="height"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>

            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">
                  Pulse
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(BPM)
                  </Box>
                </Text>
                <Input
                  value={existingVitals.pulse}
                  onChange={handleChange}
                  name="pulse"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">BMI</Text>
                <Input
                  value={existingVitals.bmi}
                  onChange={handleChange}
                  name="bmi"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>

            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">
                  Respiratiory rate
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(CPM)
                  </Box>
                </Text>
                <Input
                  value={existingVitals.respiration}
                  onChange={handleChange}
                  name="respiration"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">
                  Pain
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(0-10)
                  </Box>
                </Text>
                <Input
                  value={existingVitals.pain}
                  onChange={handleChange}
                  name="pain"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>

            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">
                  Stystolic BP
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(mmHg)
                  </Box>
                </Text>
                <Input
                  value={existingVitals.systolicBp}
                  onChange={handleChange}
                  name="systolicBp"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>

            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">
                  Diastolic BP
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(mmHg)
                  </Box>
                </Text>
                <Input
                  value={existingVitals.diastolicBp}
                  onChange={handleChange}
                  name="diastolicBp"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text fontSize="sm">Sp02</Text>
                <Input
                  value={existingVitals.sp02}
                  onChange={handleChange}
                  name="sp02"
                  variant="filled"
                  pos="initial"
                  size="sm"
                  type="number"
                  w={32}
                />
              </Stack>
            </WrapItem>
          </Wrap>
          <Flex justify="flex-end">
            <Button
              type="submit"
              isLoading={loading === "EDIT_VITAL"}
              loadingText="Saving..."
              w={32}
              colorScheme="primary"
            >
              Save
            </Button>
          </Flex>
        </Stack>
      </form>
    </Stack>
  );
}
