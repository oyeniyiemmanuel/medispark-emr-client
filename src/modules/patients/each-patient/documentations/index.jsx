import {
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
  Box,
  Divider,
  HStack,
  Icon,
  Select,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { IoIosArrowDropdownCircle } from "react-icons/io";
import FakeIcon from "../../../../components/fake-icon";
import PatientContainer from "../shared/patient-container";
import RecordNotes from "./record-notes";
import RecordVitals from "./record-vitals";

export default function NewClerkingView() {
  const documentationOptions = [
    {
      id: 1,
      category: "Documentation",
      name: "Notes",
      value: "notes",
    },
    // {
    //   id: 2,
    //   category: "Documentation",
    //   name: "Referral notes",
    //   value: "referral",
    // },
    // {
    //   id: 3,
    //   category: "Documentation",
    //   name: "Mortality",
    //   value: "mortality",
    // },
    {
      id: 4,
      category: "Clerk patient",
      name: "Vitals",
      value: "vitals",
    },
    // {
    //   id: 5,
    //   category: "Clerk patient",
    //   name: "Clerking",
    //   value: "clerking",
    // },
    // {
    //   id: 6,
    //   category: "Documentation",
    //   name: "Contact Forms",
    //   value: "contact",
    // },
  ];
  const { query } = useRouter();
  const [selectedDocumentation, setSelectedDocumentation] = useState("vitals");
  useEffect(() => {
    query?.type && setSelectedDocumentation(query.type);
  }, [query]);

  return (
    <PatientContainer
      title={
        documentationOptions.find(
          ({ value }) => value === selectedDocumentation
        )?.category
      }
    >
      <Stack spacing={4} w="full">
        <Select
          size="lg"
          cursor="pointer"
          bg="white"
          value={selectedDocumentation}
          onChange={(event) => setSelectedDocumentation(event.target.value)}
          border="none"
          fontSize="xl"
          color="gray.500"
          isFullWidth
        >
          {documentationOptions.map(({ id, name, value }) => (
            <option key={id} value={value}>
              {name}
            </option>
          ))}
        </Select>
        {/* <Accordion allowMultiple>
          <AccordionItem border={0}>
            <AccordionButton px={8} border={0}>
              <Text w="60%" textAlign="left" fontSize="sm" color="gray.600">
                Last seen on{" "}
                <Box as="span" fontWeight="bold">
                  01 January, 2021
                </Box>
              </Text>
              <Divider borderBottomColor="gray.500" w="full" />
              <Icon as={IoIosArrowDropdownCircle} colo />
            </AccordionButton>
            <AccordionPanel border={0} pb={4}>
              <Stack fontSize="sm" px={4} color="gray.500">
                <Stack direction="row" align="center" justify="space-between">
                  <Text>Encounter with Dr Sid on 01, January 2021</Text>
                  <HStack>
                    <FakeIcon />
                    <FakeIcon />
                    <FakeIcon />
                    <FakeIcon />
                  </HStack>
                </Stack>
              </Stack>
            </AccordionPanel>
          </AccordionItem>
        </Accordion>
     */}
        {selectedDocumentation === "vitals" && <RecordVitals />}
        {selectedDocumentation === "notes" && <RecordNotes />}
      </Stack>
    </PatientContainer>
  );
}
