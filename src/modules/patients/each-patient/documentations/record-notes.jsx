import { Button, Divider, HStack, Stack, useToast } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import TextEditor from "../../../../components/text-editor";
import { closePopup } from "../../../../redux/slices/commonSlice";
import {
  documentationActions,
  recordNote,
} from "../../../../redux/slices/documentationSlice";
import { patientActions } from "../../../../redux/slices/patientSlice";
import { toastError, toastSuccess } from "../../../../utils";
import UploadFileForPatient from "../uploads/new-upload";

export default function RecordNotes({ ...rest }) {
  const initialNoteDetailState = {
    // Set Initial Values of input fields
    title: "",
    body: "",
  };
  const [newNote, setNewNote] = useState(
    initialNoteDetailState // Store values in state
  );
  const [document, setDocument] = useState(null);
  const [toUploadDocument, setToUploadDocument] = useState(false);
  const handleChange = (event) => {
    const { name, value } = event.target;
    setNewNote({ ...newNote, [name]: value }); // onChange handler
  };
  const dispatch = useDispatch();
  const toast = useToast();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);
  const common = useSelector((state) => state.common);
  const documentations = useSelector((state) => state.documentations);
  const { title, body } = newNote;

  const onRecordNewNote = (event) => {
    event.preventDefault();
    if (
      !body
        .replace(/class="[^"]*"/, "")
        .replace(/(<([^>]+)>)/gi, "")
        .replace(/ /g, "")
    ) {
      toast({
        title: "You haven't written anything yet...",
        status: "error",
        duration: 4000,
        position: "bottom-left",
        isClosable: true,
      });
    } else {
      dispatch(
        recordNote({
          organization_id: userDetails.organization_id,
          branch_id: userDetails.branch_id,
          user_id: selectedPatient.id,
          saved_by_id: userDetails.id,
          body,
        })
      );
    }
  };

  useEffect(() => {
    if (
      documentations.error &&
      documentations.error.errorType === "RECORD_NOTE"
    ) {
      // If an documentations.error occurs while creating patient,
      toast(toastError(documentations.error)); // Display Error Message
      dispatch(documentationActions.clearStates()); // Then clear stale documentations.error messages
    }
  }, [documentations.error]);

  useEffect(() => {
    if (documentations.success === "RECORD_NOTE") {
      // If patient is created successfully,
      toast(
        toastSuccess(
          `Note Recorded Successfully${document ? ". Uploading file..." : ""}`
        )
      ); // Display Success message
      dispatch(documentationActions.clearStates()); // Then clear stale error messages
      if (document) {
        setToUploadDocument(true);
      } else {
        dispatch(closePopup());
      }

      dispatch(
        patientActions.addToPatientTimeline({
          activity: documentations.newestNote,
          type: "notes",
        })
      );
    }
  }, [documentations.success]);

  return (
    <>
      <form onSubmit={onRecordNewNote} action="submit">
        <Stack
          border="1px solid lightgrey"
          spacing={3}
          bg="white"
          rounded={8}
          p={3}
          {...rest}
        >
          {/* <Textarea
          border="none"
          h={250}
          isRequired
          name="body"
          value={body}
          onChange={handleChange}
          placeholder="Body text: Type in something here"
        /> */}

          <TextEditor
            onChange={(html) => setNewNote({ ...newNote, body: html })}
            value={body}
            placeholder="Body text: Type in something here"
          />

          <Divider borderBottomColor="gray.400" />

          <Stack spacing={4}>
            <HStack align="center" justify="flex-end">
              <UploadFileForPatient
                document={document}
                setDocument={setDocument}
                toUploadDocument={toUploadDocument}
                setToUploadDocument={setToUploadDocument}
                activityType="notes"
                activityCategory="user_note"
                activityId={documentations.newestNote?.id}
              />
              <Button
                type="submit"
                isLoading={
                  documentations.loading === "RECORD_NOTE" ||
                  common.loading === "UPLOAD_FILE"
                }
                loadingText={`Saving ${
                  documentations.loading === "RECORD_NOTE"
                    ? "Note"
                    : common.loading === "UPLOAD_FILE"
                    ? "File"
                    : ""
                }...`}
                minW={32}
                colorScheme="primary"
              >
                Save
              </Button>
            </HStack>
          </Stack>
        </Stack>
      </form>
    </>
  );
}
