import {
  Center,
  Checkbox,
  Divider,
  Flex,
  Heading,
  HStack,
  Icon,
  IconButton,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import "moment-timezone";
import React, { useState } from "react";
import { MdEdit } from "react-icons/md";
import { RiFileList2Fill } from "react-icons/ri";
import Moment from "react-moment";
import { useDispatch, useSelector } from "react-redux";
import { openPopup } from "../../../../../redux/slices/commonSlice";
import { wordTruncate } from "../../../../../utils";
import AuditTrailModal from "../../audit-trail/audit-trail-modal";
import ViewUploadInActivities from "../../uploads/view-uploads";
import NoteModal from "./note-modal";

export default function NoteCard({
  note,
  isChecked,
  addOrRemoveFromSelectedActivities,
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [expandModal, setExpandModal] = useState(false);
  const {
    id,
    saved_by_id,
    body,
    created_at,
    audits,
    files,
    timeline_entry_type,
  } = note;

  const { staff } = useSelector((state) => state.staff);
  const [showMenu, setShowMenu] = useState(false);
  const dispatch = useDispatch();

  return (
    <HStack align="start">
      <Checkbox
        mt={4}
        size="lg"
        pos="initial"
        borderColor="gray.400"
        isChecked={isChecked}
        value={id}
        onChange={addOrRemoveFromSelectedActivities}
      />
      <Stack
        cursor="pointer"
        onMouseEnter={() => setShowMenu(true)}
        onMouseLeave={() => setShowMenu(false)}
        w="full"
        bg="white"
        border="2px solid"
        borderColor="blue.200"
        p={4}
        pt={2}
        fontSize="sm"
        // h={250}
        color="gray.500"
        spacing={2}
        rounded={8}
      >
        <Flex
          minH={12}
          borderBottom="1px solid lightgrey"
          w="full"
          justify="space-between"
          align="center"
        >
          <HStack>
            <HStack>
              <Center boxSize={7} bg="blue.100" rounded="full">
                <Icon color="blue.500" boxSize={4} as={RiFileList2Fill} />
              </Center>
              <Heading color="blue.500" fontSize="xl">
                Note
              </Heading>
            </HStack>
            <HStack>
              <Text fontWeight="bold" color="gray.500">
                By{" "}
                {staff?.find(({ id }) => id == audits[0]?.user_id)?.full_name}
              </Text>{" "}
              <Text> | </Text>
              <Text color="gray.500">
                <Moment format="MMMM D, YYYY,">
                  {new Date(created_at).toDateString()}
                </Moment>
              </Text>
              <Text>
                <Moment format="hh:mm a">{new Date(created_at)}</Moment>
              </Text>
            </HStack>
          </HStack>
          {showMenu && (
            <HStack spacing={0}>
              <IconButton
                onClick={() =>
                  dispatch(
                    openPopup({
                      popupType: "Edit Note",
                      popupId: id,
                    })
                  )
                }
                variant="ghost"
                icon={<MdEdit />}
              />
              {/* <IconButton variant="ghost" icon={<MdDelete />} /> */}
              {/* <IconButton variant="ghost" icon={<BiExpand />} /> */}
            </HStack>
          )}
        </Flex>

        <Stack>
          <Stack pl={8} w="full">
            <Text maxW="xl">
              {body && wordTruncate(body?.replace(/(<([^>]+)>)/gi, " "), 200)}
            </Text>
          </Stack>
        </Stack>

        <Divider pt={2} />
        <HStack justify="space-between">
          <HStack>
            <ViewUploadInActivities
              files={files}
              timeline_entry_type={timeline_entry_type}
              created_at={created_at}
            />
            <AuditTrailModal
              created_at={created_at}
              timeline_entry_type={timeline_entry_type}
              audits={audits}
            />
          </HStack>
        </HStack>
        <NoteModal isOpen={isOpen} onOpen={onOpen} onClose={onClose} />
      </Stack>
    </HStack>
  );
}
