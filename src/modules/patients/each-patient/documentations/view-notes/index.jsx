import { Button, IconButton } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Input } from "@chakra-ui/input";
import { Divider, Flex, HStack, Stack, Text } from "@chakra-ui/layout";
import check from "check-types";
import React, { useEffect, useState } from "react";
import { AiFillCaretDown } from "react-icons/ai";
import { BiCalendarEvent, BiDotsVerticalRounded } from "react-icons/bi";
import { BsGrid1X2Fill } from "react-icons/bs";
import { MdFormatListBulleted } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { fetchPatientTimeline } from "../../../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../../../redux/slices/retainerSlice";
import { fetchServices } from "../../../../../redux/slices/serviceSlice";
import { fetchStaff } from "../../../../../redux/slices/staffSlice";
import NoteCard from "./note-card";
import FilterResults from "react-filter-search";

export default function ViewNotes() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient, patientTimeline, loading, success, error } =
    useSelector((state) => state.patients);
  const dispatch = useDispatch();
  const [searchValue, setSearchValue] = useState("");

  const { retainers } = useSelector((state) => state.retainers);
  const { services } = useSelector((state) => state.services);
  const { staff } = useSelector((state) => state.staff);

  useEffect(() => {
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
    check.emptyArray(services) &&
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );

    check.emptyArray(staff) &&
      dispatch(
        fetchStaff({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );
  }, [userDetails]);

  return (
    <Stack spacing={4}>
      <Flex justify="flex-end">
        <HStack color="gray.500">
          <Input
            size="sm"
            pos="initial"
            w={40}
            value={searchValue}
            onChange={(event) => {
              setSearchValue(event.target.value);
            }}
            placeholder="Search Notes"
            rounded="full"
          />
          <Button variant="ghost" size="sm" rightIcon={<AiFillCaretDown />}>
            All Users
          </Button>
          <Button variant="ghost" size="sm" rightIcon={<BiCalendarEvent />}>
            Filter by date
          </Button>
        </HStack>
      </Flex>
      <Divider opacity="initial" borderColor="gray.400" />
      <FilterResults
        value={searchValue}
        data={
          check.nonEmptyObject(patientTimeline)
            ? [...patientTimeline.notes].sort(
                (a, b) => new Date(b.created_at) - new Date(a.created_at)
              )
            : []
        }
        renderResults={(results) =>
          check.emptyArray(results) ? (
            <Text
              w="full"
              color="gray.500"
              wordBreak="break-word"
              textAlign="center"
            >
              When you add a new note, it will show here
            </Text>
          ) : (
            results.map((note) => <NoteCard key={note.id} note={note} />)
          )
        }
      />
    </Stack>
  );
}
