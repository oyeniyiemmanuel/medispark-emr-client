import {
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  Input,
  Stack,
  Text,
  useToast,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import { MdAddCircle } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import {
  clearStates,
  recordVitals,
} from "../../../../redux/slices/documentationSlice";
import { patientActions } from "../../../../redux/slices/patientSlice";
import { toastError, toastSuccess } from "../../../../utils";

export default function RecordVitals({ ...rest }) {
  const initialVitalDetailState = {
    // Set Initial Values of input fields
    temperature: "",
    pulse: "",
    weight: "",
    bmi: "",
    respiration: "",
    pain: "",
    height: "",
    sp02: "",
    systolicBp: "",
    diastolicBp: "",
  };
  const [newVitals, setNewVitals] = useState(
    initialVitalDetailState // Store values in state
  );
  const handleChange = (event) => {
    const { name, value } = event.target;
    setNewVitals({ ...newVitals, [name]: value }); // onChange handler
  };
  const dispatch = useDispatch();
  const toast = useToast();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);
  const { vitals, loading, success, error } = useSelector(
    (state) => state.documentations
  );

  const onRecordNewVitals = (event) => {
    event.preventDefault();
    if (JSON.stringify(newVitals) == JSON.stringify(initialVitalDetailState)) {
      toast({
        title: "You haven't filled in any vital",
        status: "error",
        duration: 4000,
        position: "bottom-left",
        isClosable: true,
      });
    } else {
      dispatch(
        recordVitals({
          organization_id: userDetails.organization_id,
          branch_id: userDetails.branch_id,
          user_id: selectedPatient.id,
          saved_by_id: userDetails.id,
          // user_visit_id: 1,
          temperature: newVitals.temperature,
          pulse: newVitals.pulse,
          respiration: newVitals.respiration,
          oxygen_saturation: newVitals.sp02,
          weight: newVitals.weight,
          height: newVitals.height,
          systolic_bp: newVitals.systolicBp,
          diastolic_bp: newVitals.diastolicBp,
          body_mass_index: newVitals.bmi,
          pain_score: newVitals.pain,
        })
      );
    }
  };

  useEffect(() => {
    if (error && error.errorType === "RECORD_VITAL") {
      // If an error occurs while creating patient,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  useEffect(() => {
    if (success === "RECORD_VITAL") {
      // If patient is created successfully,
      toast(toastSuccess("Vitals Recorded Successfully!")); // Display Success message
      dispatch(
        patientActions.addToPatientTimeline({
          activity: vitals.newestVital,
          type: "vitals",
        })
      );

      dispatch(clearStates()); // Then clear stale error messages
      Router.push(`/patients/${selectedPatient.id}`);
    }
  }, [success]);

  return (
    <Stack spacing={5} bg="white" p={6} pb={12} {...rest}>
      <HStack justify="space-between">
        <Heading color="gray.600" fontSize="xl">
          Routine Vitals
        </Heading>
        <Button
          variant="link"
          size="sm"
          colorScheme="primary"
          rightIcon={<MdAddCircle />}
        >
          Add New
        </Button>
      </HStack>
      <Text color="gray.400" fontSize="sm" maxW="450px">
        Routine vitals are mandated tests to be taken before a patient can
        consult a doctor
      </Text>
      <Divider />
      <form onSubmit={onRecordNewVitals} action="submit">
        <Stack spacing={4}>
          <Wrap align="center" justify="space-between" spacing={4}>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>
                  Temperature
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(°C)
                  </Box>
                </Text>
                <Input
                  value={newVitals.temperature}
                  onChange={handleChange}
                  name="temperature"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>
                  Weight
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(kg)
                  </Box>
                </Text>
                <Input
                  value={newVitals.weight}
                  onChange={handleChange}
                  name="weight"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>

            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>
                  Pulse
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(BPM)
                  </Box>
                </Text>
                <Input
                  value={newVitals.pulse}
                  onChange={handleChange}
                  name="pulse"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>BMI</Text>
                <Input
                  value={newVitals.bmi}
                  onChange={handleChange}
                  name="bmi"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>

            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>
                  Respiratiory rate
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(CPM)
                  </Box>
                </Text>
                <Input
                  value={newVitals.respiration}
                  onChange={handleChange}
                  name="respiration"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>
                  Pain
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(0-10)
                  </Box>
                </Text>
                <Input
                  value={newVitals.pain}
                  onChange={handleChange}
                  name="pain"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>

            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>
                  Height
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(cm)
                  </Box>
                </Text>
                <Input
                  value={newVitals.height}
                  onChange={handleChange}
                  name="height"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>
                  Stystolic BP
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(mmHg)
                  </Box>
                </Text>
                <Input
                  value={newVitals.systolicBp}
                  onChange={handleChange}
                  name="systolicBp"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>

            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>
                  Diastolic BP
                  <Box as="span" color="gray.500" fontStyle="italic">
                    &nbsp;(mmHg)
                  </Box>
                </Text>
                <Input
                  value={newVitals.diastolicBp}
                  onChange={handleChange}
                  name="diastolicBp"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>
            <WrapItem>
              <Stack align="center" spacing={2} mb={4}>
                <Text>Sp02</Text>
                <Input
                  value={newVitals.sp02}
                  onChange={handleChange}
                  name="diastolicBp"
                  variant="filled"
                  pos="initial"
                  type="number"
                  w={40}
                />
              </Stack>
            </WrapItem>
          </Wrap>
          <Flex pt={4} justify="center">
            <Button
              type="submit"
              isLoading={loading === "RECORD_VITAL"}
              loadingText="Saving..."
              size="lg"
              w="2xs"
              colorScheme="primary"
            >
              Done
            </Button>
          </Flex>
        </Stack>
      </form>
    </Stack>
  );
}
