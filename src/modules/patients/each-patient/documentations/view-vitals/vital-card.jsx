import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Select,
  Input,
  Icon,
  AvatarGroup,
  Wrap,
  WrapItem,
  Checkbox,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { RiEditLine, RiFileList2Fill } from "react-icons/ri";
import { BsFillCaretRightFill } from "react-icons/bs";
import DashboardContainer from "../../../../../components/layouts/dashboard";
import {
  BiCalendarEvent,
  BiChevronRight,
  BiDotsVerticalRounded,
  BiExpand,
  BiMicrophone,
} from "react-icons/bi";
import Head from "next/head";
import { AiFillCaretDown } from "react-icons/ai";
import { CgNotes } from "react-icons/cg";
import Router from "next/router";
import { wordTruncate } from "../../../../../utils";
import {
  MdAddCircle,
  MdCameraAlt,
  MdClose,
  MdDelete,
  MdEdit,
  MdImage,
  MdMonetizationOn,
} from "react-icons/md";
import VitalModal from "./vital-modal";
import { VscFilePdf } from "react-icons/vsc";
import { ImLab } from "react-icons/im";
import RXIcon from "../../../../../components/icons";
import { Rnd } from "react-rnd";
import { AddIcon } from "@chakra-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import Moment from "react-moment";
import "moment-timezone";
import { FaHeartbeat, FaRegFileAudio } from "react-icons/fa";
import { openPopup } from "../../../../../redux/slices/commonSlice";
import VitalDetails from "./vital-details";
import AuditTrailModal from "../../audit-trail/audit-trail-modal";

export default function VitalCard({
  vital,
  isChecked,
  addOrRemoveFromSelectedActivities,
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { id, saved_by_id, created_at, audits, timeline_entry_type } = vital;

  const { staff } = useSelector((state) => state.staff);
  const [showMenu, setShowMenu] = useState(false);
  const dispatch = useDispatch();

  return (
    <HStack align="start">
      <Checkbox
        mt={4}
        size="lg"
        pos="initial"
        borderColor="gray.400"
        isChecked={isChecked}
        value={id}
        onChange={addOrRemoveFromSelectedActivities}
      />
      <Stack
        cursor="pointer"
        onMouseEnter={() => setShowMenu(true)}
        onMouseLeave={() => setShowMenu(false)}
        w="full"
        bg="white"
        border="2px solid"
        borderColor="red.200"
        p={4}
        fontSize="sm"
        pt={2}
        // h={250}
        color="gray.500"
        spacing={1}
        rounded={8}
      >
        <Flex
          minH={12}
          borderBottom="1px solid lightgrey"
          w="full"
          justify="space-between"
          align="center"
        >
          <HStack>
            <HStack>
              <Center boxSize={7} bg="red.100" rounded="full">
                <Icon color="red.500" boxSize={4} as={FaHeartbeat} />
              </Center>
              <Heading color="red.500" fontSize="xl">
                Vitals
              </Heading>
            </HStack>
            <HStack>
              <Text fontWeight="bold" color="gray.500">
                By{" "}
                {staff?.find(({ id }) => id == audits[0]?.user_id)?.full_name}
              </Text>{" "}
              <Text> | </Text>
              <Text color="gray.500">
                <Moment format="MMMM D, YYYY,">
                  {new Date(created_at).toDateString()}
                </Moment>
              </Text>
              <Text>
                <Moment format="hh:mm a">{new Date(created_at)}</Moment>
              </Text>
            </HStack>
          </HStack>
          {showMenu && (
            <HStack spacing={0}>
              <IconButton
                onClick={() =>
                  dispatch(
                    openPopup({
                      popupType: "Edit Vitals",
                      popupId: id,
                    })
                  )
                }
                variant="ghost"
                icon={<MdEdit />}
              />
              {/* <IconButton variant="ghost" icon={<MdDelete />} /> */}
              {/* <IconButton variant="ghost" icon={<BiExpand />} /> */}
            </HStack>
          )}
        </Flex>

        <Stack>
          <VitalDetails vital={vital} />
        </Stack>
        <Divider pt={2} />
        <HStack justify="space-between">
          <HStack>
            <AuditTrailModal
              created_at={created_at}
              timeline_entry_type={timeline_entry_type}
              audits={audits}
            />
          </HStack>
        </HStack>
        <VitalModal isOpen={isOpen} onOpen={onOpen} onClose={onClose} />
      </Stack>
    </HStack>
  );
}
