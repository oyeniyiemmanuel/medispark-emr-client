import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Select,
  Input,
  Icon,
  AvatarGroup,
  Wrap,
  WrapItem,
  Checkbox,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { RiEditLine, RiFileList2Fill } from "react-icons/ri";
import { BsFillCaretRightFill } from "react-icons/bs";
import DashboardContainer from "../../../../../components/layouts/dashboard";
import {
  BiCalendarEvent,
  BiChevronRight,
  BiDotsVerticalRounded,
  BiExpand,
  BiMicrophone,
} from "react-icons/bi";
import Head from "next/head";
import { AiFillCaretDown } from "react-icons/ai";
import { CgNotes } from "react-icons/cg";
import Router from "next/router";
import { wordTruncate } from "../../../../../utils";
import {
  MdAddCircle,
  MdCameraAlt,
  MdClose,
  MdDelete,
  MdEdit,
  MdImage,
  MdMonetizationOn,
} from "react-icons/md";
import VitalModal from "./vital-modal";
import { VscFilePdf } from "react-icons/vsc";
import { ImLab } from "react-icons/im";
import RXIcon from "../../../../../components/icons";
import { Rnd } from "react-rnd";
import { AddIcon } from "@chakra-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import Moment from "react-moment";
import "moment-timezone";
import { FaHeartbeat, FaRegFileAudio } from "react-icons/fa";
import { openPopup } from "../../../../../redux/slices/commonSlice";

export default function VitalDetails({ vital }) {
  const {
    temperature,
    pulse,
    respiration,
    oxygen_saturation,
    weight,
    height,
    systolic_bp,
    diastolic_bp,
    body_mass_index,
    pain_score,
  } = vital;

  return (
    <Wrap spacing={8} pl={8} w="full">
      {temperature && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">Temperature</Text>
            <Text
              fontWeight="bold"
              color={
                temperature >= 36.5 && temperature <= 37.3
                  ? "gray.600"
                  : "red.500"
              }
            >
              {temperature} °C
            </Text>
          </Stack>
        </WrapItem>
      )}
      {height && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">Height</Text>
            <Text fontWeight="bold" color="gray.600">
              {height} cm
            </Text>
          </Stack>
        </WrapItem>
      )}
      {weight && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">Weight</Text>
            <Text fontWeight="bold" color="gray.600">
              {weight} kg
            </Text>
          </Stack>
        </WrapItem>
      )}
      {pulse && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">Pulse</Text>
            <Text
              fontWeight="bold"
              color={pulse >= 60 && pulse <= 100 ? "gray.600" : "red.500"}
            >
              {pulse} BPM
            </Text>
          </Stack>
        </WrapItem>
      )}
      {body_mass_index && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">BMI</Text>
            <Text
              fontWeight="bold"
              color={
                body_mass_index >= 18.5 && body_mass_index <= 24.9
                  ? "gray.600"
                  : "red.500"
              }
            >
              {body_mass_index}
            </Text>
          </Stack>
        </WrapItem>
      )}
      {respiration && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">Respiratory rate</Text>
            <Text
              fontWeight="bold"
              color={
                respiration >= 12 && respiration <= 18 ? "gray.600" : "red.500"
              }
            >
              {respiration} CPM
            </Text>
          </Stack>
        </WrapItem>
      )}
      {pain_score && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">Pain</Text>
            <Text fontWeight="bold" color="gray.600">
              {pain_score}
            </Text>
          </Stack>
        </WrapItem>
      )}

      {systolic_bp && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">Stystolic BP</Text>
            <Text
              fontWeight="bold"
              color={
                systolic_bp >= 90 && systolic_bp <= 120 ? "gray.600" : "red.500"
              }
            >
              {systolic_bp} mmHg
            </Text>
          </Stack>
        </WrapItem>
      )}
      {diastolic_bp && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">Diastolic BP</Text>
            <Text
              fontWeight="bold"
              color={
                diastolic_bp >= 60 && diastolic_bp <= 80
                  ? "gray.600"
                  : "red.500"
              }
            >
              {diastolic_bp} mmHg
            </Text>
          </Stack>
        </WrapItem>
      )}
      {oxygen_saturation && (
        <WrapItem>
          <Stack spacing={0}>
            <Text color="gray.500">Sp02</Text>
            <Text
              fontWeight="bold"
              color={
                oxygen_saturation >= 95 && oxygen_saturation <= 100
                  ? "gray.600"
                  : "red.500"
              }
            >
              {oxygen_saturation}%
            </Text>
          </Stack>
        </WrapItem>
      )}
    </Wrap>
  );
}
