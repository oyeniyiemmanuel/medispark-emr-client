import { Heading, Input, Stack, Text } from "@chakra-ui/react";
import React from "react";

export default function EditClinicalInfo({ patientDetails, handleChange }) {
  return (
    <Stack spacing={6} w="full">
      <Heading color="gray.500" fontSize="2xl">
        Clinical Info
      </Heading>
      <Stack w="full" spacing={8} direction="row">
        <Stack w="full">
          <Stack>
            <Text pl={4} color="gray.500">
              Height
            </Text>
            <Input
              value={patientDetails.height}
              onChange={handleChange}
              name="height"
              variant="filled"
              placeholder="Enter Height"
            />
          </Stack>
          <Stack>
            <Text pl={4} color="gray.500">
              Weight
            </Text>
            <Input
              value={patientDetails.weight}
              onChange={handleChange}
              name="weight"
              variant="filled"
              placeholder="Enter Weight"
            />
          </Stack>
          {/* <Stack w="full">
            <Text pl={4} color="gray.500">
              Medical Condition
            </Text>
            <Input
              value={patientDetails.medicalCondition}
              onChange={handleChange}
              name="medicalCondition"
              variant="filled"
              placeholder="Enter Medical Condition"
            />
          </Stack>
          <Stack w="full">
            <Text pl={4} color="gray.500">
              Recent Medication
            </Text>
            <Textarea
              value={patientDetails.recentMedication}
              onChange={handleChange}
              name="recentMedication"
              variant="filled"
              
              placeholder="Enter Recent Medication"
            />
          </Stack> */}
          <Stack>
            <Text pl={4} color="gray.500">
              Genotype
            </Text>
            <Input
              value={patientDetails.genotype}
              onChange={handleChange}
              name="genotype"
              variant="filled"
              placeholder="Enter Genotype"
            />
          </Stack>
        </Stack>
        <Stack w="full">
          <Stack w="full">
            <Text pl={4} color="gray.500">
              Blood Type
            </Text>
            <Input
              value={patientDetails.bloodType}
              onChange={handleChange}
              name="bloodType"
              variant="filled"
              placeholder="Enter Blood Type "
            />
          </Stack>
          <Stack>
            <Text pl={4} color="gray.500">
              Blood Group
            </Text>
            <Input
              value={patientDetails.bloodGroup}
              onChange={handleChange}
              name="bloodGroup"
              variant="filled"
              placeholder="Enter Blood Group"
            />
          </Stack>

          {/* <Stack w="full">
            <Text pl={4} color="gray.500">
              Allergies
            </Text>
            <Input
              value={patientDetails.allergies}
              onChange={handleChange}
              name="allergies"
              variant="filled"
              
              placeholder="Enter Allergies"
            />
          </Stack> */}
        </Stack>
      </Stack>
    </Stack>
  );
}
