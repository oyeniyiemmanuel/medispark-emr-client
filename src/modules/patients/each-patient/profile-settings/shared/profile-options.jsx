import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  Divider,
  Heading,
  HStack,
  Icon,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import Router from "next/router";
import React from "react";
import { BsFillCaretRightFill } from "react-icons/bs";
import { RiEditLine, RiFolderUserLine } from "react-icons/ri";
import { useSelector } from "react-redux";
import FakeIcon from "../../../../../components/fake-icon";
import SendToDoctor from "../../send-to-doctor";

export default function ProfileOptions({ ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const profileOptions = [
    { id: 1, icon: RiFolderUserLine, name: "Biodata" },
    // { id: 2, icon: RiFolderUserLine, name: "Profile Settings" },
    // { id: 3, icon: RiFolderUserLine, name: "Tarriffs" },
    // { id: 4, icon: RiFolderUserLine, name: "Accounts  " },
    // { id: 5, icon: RiFolderUserLine, name: "Reports" },
    // { id: 6, icon: RiFolderUserLine, name: "Profile Settings" },
  ];

  return (
    <>
      <Box position="sticky" top="80px" bg="gray.50" w="300px" {...rest}>
        <Stack w="full" bg="white">
          <HStack py={4} bg="primary.50" w="full" spacing={4} px={6}>
            <Avatar
              size="md"
              name={`${selectedPatient.first_name} ${selectedPatient.last_name}`}
              src={selectedPatient.avatar?.file_path}
            />
            <Box>
              <Text fontWeight="bold" color="gray.500" fontSize="lg">
                {selectedPatient.first_name} {selectedPatient.last_name}
              </Text>
              <Text color="primary.500">Patient</Text>
            </Box>
          </HStack>
          <Stack spacing={0}>
            {profileOptions.map(({ id, name, icon }) => (
              <HStack
                // _hover={{
                //   borderLeft: "3px solid",
                //   borderLeftColor: "primary.500",
                //   transform: "scale(1.08)",
                //   fontWeight: "bold",
                //   shadow: "md",
                // }}
                transition="0.2s all"
                cursor="pointer"
                color="gray.500"
                key={id}
                py={3}
                spacing={4}
                px={6}
              >
                <Icon as={icon} />
                <Text>{name}</Text>
              </HStack>
            ))}
          </Stack>
        </Stack>
        <Box bg="white" color="gray.500" m={4}></Box>
      </Box>
    </>
  );
}
