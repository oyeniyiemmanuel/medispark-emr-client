import {
  Box,
  Button,
  HStack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  useToast,
} from "@chakra-ui/react";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  clearStates,
  editPatient,
} from "../../../../redux/slices/patientSlice";
import { toastError, toastSuccess } from "../../../../utils";
import EditClinicalInfo from "./edit-clinical-info";
import EditGrowthChart from "./edit-growth-chart";
import EditPersonalInfo from "./edit-personal-info";
import EditSocialInfo from "./edit-social-info";

export default function PatientProfileSettings() {
  const dispatch = useDispatch();
  const toast = useToast();
  const patientTabs = [
    "Personal Info",
    "Social Info",
    "Clinical Info",
    // "Growth Chart",
  ];
  const { selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );

  const initialPatientDetails = {
    lastName: selectedPatient.last_name,
    firstName: selectedPatient.first_name,
    middleName: selectedPatient.middle_name,
    email: selectedPatient.email,
    phone: selectedPatient.phone,
    dob: selectedPatient.dob,
    gender: selectedPatient.gender,
    maritalStatus: selectedPatient.marital_status,
    street: selectedPatient.street,
    city: selectedPatient.city,
    state: selectedPatient.state,
    country: selectedPatient.country,
    religion: selectedPatient.religion,
    occupation: selectedPatient.occupation,
    placeOfWork: selectedPatient.place_of_work,
    height: selectedPatient.height,
    weight: selectedPatient.weight,
    bloodType: selectedPatient.blood_type,
    bloodGroup: selectedPatient.blood_group,
    genotype: selectedPatient.genotype,
  };

  const [patientDetails, setPatientDetails] = useState(initialPatientDetails);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setPatientDetails({ ...patientDetails, [name]: value }); // onChange handler
  };

  const onSubmitEditPatient = (event) => {
    event?.preventDefault();
    dispatch(
      editPatient({
        //Create Patient Body
        user_id: selectedPatient.id,
        first_name:
          patientDetails.firstName.charAt(0).toUpperCase() +
          patientDetails.firstName.slice(1).toLowerCase(),
        last_name:
          patientDetails.lastName.charAt(0).toUpperCase() +
          patientDetails.lastName.slice(1).toLowerCase(),
        middle_name: patientDetails.middleName,
        phone: patientDetails.phone,
        dob: patientDetails.dob,
        gender: patientDetails.gender,
        email: patientDetails.email,
        street: patientDetails.street,
        city: patientDetails.city,
        state: patientDetails.state,
        country: patientDetails.country,
        marital_status: patientDetails.maritalStatus,
        religion: patientDetails.religion,
        occupation: patientDetails.occupation,
        place_of_work: patientDetails.placeOfWork,
        height: patientDetails.height,
        weight: patientDetails.weight,
        blood_type: patientDetails.bloodType,
        blood_group: patientDetails.bloodGroup,
        genotype: patientDetails.genotype,
      })
    );
  };

  useEffect(() => {
    if (error && error.errorType === "EDIT_PATIENT") {
      // If an error occurs while creating patient,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  useEffect(() => {
    if (success === "EDIT_PATIENT") {
      // If patient is created successfully,
      toast(toastSuccess("Saved Successfully!")); // Display Success message
      setPatientDetails(initialPatientDetails);
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [success]);

  return (
    <Box as="form" onSubmit={onSubmitEditPatient}>
      <Tabs variant="enclosed" isFitted isLazy>
        <TabList borderBottom="none">
          {patientTabs.map((tab, index) => (
            <Tab
              key={index}
              color="gray.500"
              backgroundColor="gray.300"
              fontSize="sm"
              marginRight={2}
              _selected={{
                fontWeight: "bold",
                backgroundColor: "white",
                color: "primary.500",
                borderLeft: "3px solid",
                borderLeftColor: "primary.500",
                borderBottomWidth: 4,
              }}
            >
              {tab}
            </Tab>
          ))}
        </TabList>

        <TabPanels bg="white">
          <TabPanel px={8} py={10}>
            <EditPersonalInfo
              handleChange={handleChange}
              patientDetails={patientDetails}
            />
          </TabPanel>
          <TabPanel px={8} py={10}>
            <EditSocialInfo
              handleChange={handleChange}
              patientDetails={patientDetails}
            />
          </TabPanel>
          <TabPanel px={8} py={10}>
            <EditClinicalInfo
              handleChange={handleChange}
              patientDetails={patientDetails}
            />
          </TabPanel>
          {/* <TabPanel px={8} py={10}>
            <EditGrowthChart
              handleChange={handleChange}
              patientDetails={patientDetails}
            />
          </TabPanel> */}
          {/* <TabPanel>
            <p>Lab tests</p>
          </TabPanel>
          <TabPanel>
            <p>Pharmacy</p>
          </TabPanel> */}
        </TabPanels>
      </Tabs>
      <HStack py={8} justify="flex-end" align="center">
        <Button
          onClick={() => Router.push(`/patients/${selectedPatient?.id}`)}
          variant="outline"
          w={40}
        >
          Discard Changes
        </Button>
        <Button
          isLoading={loading === "EDIT_PATIENT"}
          loadingText="Saving..."
          type="submit"
          colorScheme="primary"
          w={40}
        >
          Save
        </Button>
      </HStack>
    </Box>
  );
}
