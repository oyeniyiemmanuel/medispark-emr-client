import {
  Heading,
  Input,
  Radio,
  RadioGroup,
  Stack,
  Text,
} from "@chakra-ui/react";
import React from "react";
import { formatDateToDDMMYY } from "../../../../utils";
import EditProfilePicture from "./edit-profile-picture";

export default function EditPersonalInfo({ patientDetails, handleChange }) {
  return (
    <Stack>
      <Stack direction="row" spacing={8}>
        <Stack w="full" spacing={4}>
          <Heading color="gray.500" fontSize="2xl">
            Biodata
          </Heading>
          <Stack>
            <Text pl={4} color="gray.500">
              First Name
            </Text>
            <Input
              value={patientDetails.firstName}
              onChange={handleChange}
              name="firstName"
              variant="filled"
              isRequired
              placeholder="Enter First Name"
            />
          </Stack>
          <Stack>
            <Text pl={4} color="gray.500">
              Middle Name
            </Text>
            <Input
              value={patientDetails.middleName}
              onChange={handleChange}
              name="middleName"
              variant="filled"
              placeholder="Enter Middle name"
            />
          </Stack>
          <Stack>
            <Text pl={4} color="gray.500">
              Last Name
            </Text>
            <Input
              value={patientDetails.lastName}
              onChange={handleChange}
              name="lastName"
              variant="filled"
              isRequired
              placeholder="Enter Last name"
            />
          </Stack>
        </Stack>
        <EditProfilePicture />
      </Stack>

      <Stack pt={4}>
        <Stack w="full" spacing={8} direction="row">
          <Stack w="full">
            <Text pl={4} color="gray.500">
              Gender
            </Text>
            <RadioGroup
              name="gender"
              onChange={(value) =>
                handleChange({ target: { name: "gender", value } })
              }
              value={patientDetails.gender}
              isRequired
            >
              <Stack spacing={8} direction="row">
                <Radio name="gender" value="male">
                  Male
                </Radio>
                <Radio name="gender" value="female">
                  Female
                </Radio>
              </Stack>
            </RadioGroup>
          </Stack>

          <Stack w="full">
            <Text pl={4} color="gray.500">
              Date of Birth
            </Text>
            <Input
              value={patientDetails.dob}
              onChange={handleChange}
              name="dob"
              variant="filled"
              isRequired
              max={formatDateToDDMMYY("yyyy-mm-dd")}
              type="date"
              placeholder="dd-mm-yyyy"
              format="dd-mm-yyyy"
            />
          </Stack>
        </Stack>
        <Stack w="full">
          <Text pl={4} color="gray.500">
            Phone Number
          </Text>
          <Input
            value={patientDetails.phone}
            onChange={handleChange}
            name="phone"
            variant="filled"
            isRequired
            type="number"
            placeholder="Enter Phone Number"
          />
        </Stack>
        <Stack w="full">
          <Text pl={4} color="gray.500">
            Email Address
          </Text>
          <Input
            value={patientDetails.email}
            onChange={handleChange}
            name="email"
            variant="filled"
            isRequired
            type="email"
            placeholder="Enter Email Address"
          />
        </Stack>
      </Stack>
    </Stack>
  );
}
