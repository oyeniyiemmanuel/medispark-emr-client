import {
  Box,
  Button,
  Center,
  Flex,
  Spinner,
  Text,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { BsImage } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import UploadFile from "../../../../components/upload-file";
import {
  commonActions,
  uploadFile,
} from "../../../../redux/slices/commonSlice";
import { patientActions } from "../../../../redux/slices/patientSlice";
import { toastError, toastSuccess } from "../../../../utils";

export default function EditProfilePicture({ ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const toast = useToast();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);
  const common = useSelector((state) => state.common);
  const [imageToUpload, setImageToUpload] = useState(null);

  useEffect(() => {
    if (imageToUpload) {
      const formData = new FormData();
      formData.append("organization_id", userDetails.organization_id);
      formData.append("branch_id", userDetails.branch_id);
      formData.append("user_id", selectedPatient?.id);
      formData.append("upload_category", "user_avatar");
      formData.append("file", imageToUpload);

      dispatch(uploadFile({ formData, service: "gateway" }));
    }
  }, [imageToUpload]);

  const onRemoveProfilePicture = () => {
    const formData = new FormData();
    formData.append("organization_id", userDetails.organization_id);
    formData.append("branch_id", userDetails.branch_id);
    formData.append("user_id", selectedPatient?.id);
    formData.append("upload_category", "user_avatar");
    formData.append("file", null);

    dispatch(uploadFile({ formData, service: "gateway" }));
  };

  useEffect(() => {
    if (common.error && common.error.errorType === "UPLOAD_FILE") {
      // If an error occurs while uploading file,
      toast(toastError(common.error)); // Display Error Message
      setImageToUpload(null);
      dispatch(commonActions.clearStates()); // Then clear stale error messages
    }
  }, [common.error]);

  useEffect(() => {
    if (common.success === "UPLOAD_FILE") {
      toast(toastSuccess("File uploaded successfully!")); // Display Success message
      dispatch(
        patientActions.changePatientProfilePicture({
          avatar: common.uploadedFile,
        })
      );
      setImageToUpload(null);
      dispatch(commonActions.clearStates()); // Then clear stale error messages
    }
  }, [common.success]);

  return (
    <Box {...rest}>
      <Flex
        onClick={() => {
          common.loading !== "UPLOAD_FILE" && onOpen();
        }}
        w={["full", "2xs", "2xs"]}
        h={["200px", "250px"]}
        mt={4}
        rounded="full"
        pos="relative"
        cursor="pointer"
      >
        {selectedPatient.avatar ? (
          <>
            <Flex
              w="full"
              bgPos="center"
              bgSize="cover"
              backgroundRepeat="no-repeat"
              bgImage={`url(${selectedPatient.avatar?.file_path})`}
              h="full"
              rounded="full"
              px="6%"
            />
            <Center
              pos="absolute"
              flexDirection="column"
              w="full"
              rounded="full"
              h="full"
              opacity={0}
              bg="black"
              transition="0.5s all"
              _hover={{
                opacity: 0.5,
              }}
            >
              {common.loading === "UPLOAD_FILE" ? (
                <Spinner size="sm" color="primary.500" />
              ) : (
                <>
                  <Box as={BsImage} color="white" boxSize={10} />
                  <Text color="white" mt={1}>
                    Change Image
                  </Text>
                </>
              )}
            </Center>
          </>
        ) : (
          <Flex
            w="full"
            h="full"
            rounded="full"
            px="6%"
            align="center"
            justify="center"
            border="2px dotted"
            borderColor="primary.500"
            direction="column"
          >
            {common.loading === "UPLOAD_FILE" ? (
              <Spinner size="sm" color="primary.500" />
            ) : (
              <>
                <Box as={BsImage} color="primary.600" boxSize={10} />
                <Text color="primary.600" mt={4}>
                  Upload an Image
                </Text>
              </>
            )}
          </Flex>
        )}
      </Flex>
      {selectedPatient.avatar && common.loading !== "UPLOAD_FILE" && (
        <Button
          mb={8}
          mt={2}
          onClick={onRemoveProfilePicture}
          colorScheme="red"
          variant="link"
        >
          Remove
        </Button>
      )}
      <UploadFile
        fileToUpload={imageToUpload}
        setFileToUpload={setImageToUpload}
        accept="image/x-png,image/jpg,image/jpeg"
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
      />
    </Box>
  );
}
