import { Heading, Input, Stack, Text } from "@chakra-ui/react";
import React from "react";

export default function EditSocialInfo({ patientDetails, handleChange }) {
  return (
    <Stack spacing={6} w="full">
      <Heading color="gray.500" fontSize="2xl">
        Social Info
      </Heading>
      <Stack w="full" spacing={8} direction="row">
        <Stack w="full">
          <Stack>
            <Text pl={4} color="gray.500">
              Marital Status
            </Text>
            <Input
              value={patientDetails.maritalStatus}
              onChange={handleChange}
              name="maritalStatus"
              variant="filled"
              placeholder="Enter Marital Status"
            />
          </Stack>
          <Stack>
            <Text pl={4} color="gray.500">
              Religion
            </Text>
            <Input
              value={patientDetails.religion}
              onChange={handleChange}
              name="religion"
              variant="filled"
              placeholder="Enter Religion"
            />
          </Stack>
          <Stack w="full">
            <Text pl={4} color="gray.500">
              Street
            </Text>
            <Input
              value={patientDetails.street}
              onChange={handleChange}
              name="street"
              variant="filled"
              placeholder="Enter Street"
            />
          </Stack>
          <Stack w="full">
            <Text pl={4} color="gray.500">
              City
            </Text>
            <Input
              value={patientDetails.city}
              onChange={handleChange}
              name="city"
              variant="filled"
              placeholder="Enter City"
            />
          </Stack>
          <Stack w="full">
            <Text pl={4} color="gray.500">
              State
            </Text>
            <Input
              value={patientDetails.state}
              onChange={handleChange}
              name="state"
              variant="filled"
              placeholder="Enter State"
            />
          </Stack>
        </Stack>
        <Stack w="full">
          <Stack>
            <Text pl={4} color="gray.500">
              Country
            </Text>
            <Input
              value={patientDetails.country}
              onChange={handleChange}
              name="country"
              variant="filled"
              placeholder="Enter Country"
            />
          </Stack>
          {/* <Stack>
            <Text pl={4} color="gray.500">
              Tribe/Race
            </Text>
            <Input
              value={patientDetails.tribe}
              onChange={handleChange}
              name="tribe"
              variant="filled"
              
              placeholder="Enter Tribe/Race"
            />
          </Stack> */}
          <Stack w="full">
            <Text pl={4} color="gray.500">
              Occupation
            </Text>
            <Input
              value={patientDetails.occupation}
              onChange={handleChange}
              name="occupation"
              variant="filled"
              placeholder="Enter Occupation"
            />
          </Stack>
          <Stack w="full">
            <Text pl={4} color="gray.500">
              Place of Work
            </Text>
            <Input
              value={patientDetails.placeOfWork}
              onChange={handleChange}
              name="placeOfWork"
              variant="filled"
              placeholder="Enter Place of Work"
            />
          </Stack>
          {/* <Stack w="full">
            <Text pl={4} color="gray.500">
              Abuse History
            </Text>
            <Input
              value={patientDetails.abuseHistory}
              onChange={handleChange}
              name="abuseHistory"
              variant="filled"
              
              placeholder="Enter Abuse History"
            />
          </Stack> */}
        </Stack>
      </Stack>
    </Stack>
  );
}
