import {
  Box,
  Center,
  Checkbox,
  Flex,
  Heading,
  HStack,
  Stack,
  Table,
  Tbody,
  Td,
  Text,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import "moment-timezone";
import React, { useState } from "react";
import Moment from "react-moment";
import { useDispatch, useSelector } from "react-redux";
import RXIcon from "../../../../../components/icons";
import { separateWithComma } from "../../../../../utils";
import AuditTrailModal from "../../audit-trail/audit-trail-modal";

export default function TreatmentCard({
  treatment,
  isChecked,
  addOrRemoveFromSelectedActivities,
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [showMenu, setShowMenu] = useState(false);
  const {
    id,
    encounter_id,
    created_at,
    prescription,
    audits,
    timeline_entry_type,
  } = treatment;

  const { staff } = useSelector((state) => state.staff);
  const { services } = useSelector((state) => state.services);
  const { newestPrescription } = useSelector((state) => state.prescriptions);
  const { retainers } = useSelector((state) => state.retainers);
  const { patients, selectedPatient } = useSelector((state) => state.patients);
  const dispatch = useDispatch();

  const selectedMedication = services.find(
    ({ id }) => id == treatment.service_id
  );
  const prescriptionAmount =
    selectedMedication?.prices?.find(
      ({ retainer_id }) => retainer_id == selectedPatient.retainer_id
    )?.amount || 0;

  return (
    <HStack align="start">
      <Checkbox
        mt={4}
        size="lg"
        pos="initial"
        borderColor="gray.400"
        isChecked={isChecked}
        value={id}
        onChange={addOrRemoveFromSelectedActivities}
      />
      <Stack
        onMouseEnter={() => setShowMenu(true)}
        onMouseLeave={() => setShowMenu(false)}
        cursor="pointer"
        // onClick={() => setExpandModal(!expandModal)}
        w="full"
        bg="white"
        border="2px solid"
        borderColor="teal.200"
        p={4}
        fontSize="sm"
        // h={250}
        pt={2}
        color="gray.500"
        spacing={2}
        rounded={8}
      >
        <Flex
          minH={12}
          borderBottom="1px solid lightgrey"
          w="full"
          justify="space-between"
          align="center"
        >
          <HStack>
            <HStack>
              <Center boxSize={8} bg="teal.100" rounded="full">
                <RXIcon />
              </Center>
              <Heading color="teal.500" fontSize="xl">
                Prescription
              </Heading>
            </HStack>
            <HStack>
              <Text fontWeight="bold" color="gray.500">
                By{" "}
                {staff?.find(({ id }) => id == audits[0]?.user_id)?.full_name}
              </Text>{" "}
              <Text> | </Text>
              <Text color="gray.500">
                <Moment format="MMMM D, YYYY,">
                  {new Date(created_at).toDateString()}
                </Moment>
              </Text>
              <Text>
                <Moment format="hh:mm a">{new Date(created_at)}</Moment>
              </Text>
              {encounter_id && (
                <>
                  &nbsp;in{" "}
                  <Box fontWeight="bold" as="span" color="green.500">
                    Encounter
                  </Box>
                </>
              )}
            </HStack>
          </HStack>

          {showMenu && (
            <HStack spacing={0}>
              {/* <IconButton
              // onClick={() =>
              //   dispatch(
              //     openPopup({
              //       popupType: "Edit Encounter",
              //       popupId: id,
              //     })
              //   )
              // }
              variant="ghost"
              icon={<MdEdit />}
            /> */}
            </HStack>
          )}
        </Flex>

        <Stack>
          <Stack px={4} spacing={3} w="full">
            <Stack spacing={3}>
              <Stack>
                <Table variant="unstyled" size="sm">
                  <Tbody color="gray.500">
                    <Tr>
                      <Td>
                        {selectedMedication?.name}{" "}
                        {selectedMedication?.strength_value}{" "}
                        {selectedMedication?.strength_unit_type}
                      </Td>
                      {/* <Td>6pcs</Td> */}
                      <Td isNumeric>
                        {separateWithComma(prescription?.quantity)}{" "}
                        {prescription?.stock_unit_type}
                      </Td>
                      <Td>{prescription?.instruction}</Td>
                      <Td isNumeric>
                        ₦
                        {separateWithComma(
                          prescriptionAmount * parseInt(prescription?.quantity)
                        )}
                      </Td>
                    </Tr>
                  </Tbody>
                </Table>
              </Stack>
              <HStack px={8} justify="flex-end">
                <Text fontWeight="bold">
                  Total &nbsp; ₦
                  {separateWithComma(
                    prescriptionAmount * parseInt(prescription?.quantity)
                  )}
                </Text>
              </HStack>
            </Stack>
          </Stack>
        </Stack>

        <HStack pt={2} borderTop="1px solid lightgrey" justify="space-between">
          <Flex>
            <AuditTrailModal
              created_at={created_at}
              timeline_entry_type={timeline_entry_type}
              audits={audits}
            />
          </Flex>
          {/* <HStack spacing={6}>
            <Button size="sm" variant="solid" colorScheme="red">
              ₦ See Bill
            </Button>
            <Button
              leftIcon={<FaPrint />}
              size="sm"
              variant="link"
              colorScheme="primary"
            >
              Print Prescription
            </Button>
          </HStack> */}
        </HStack>
      </Stack>
    </HStack>
  );
}
