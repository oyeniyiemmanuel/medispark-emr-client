import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Select,
  Input,
  useMediaQuery,
  useToast,
  Textarea,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { RiEditLine } from "react-icons/ri";
import { BsFillCaretRightFill } from "react-icons/bs";
import DashboardContainer from "../../../../components/layouts/dashboard";
import {
  BiCalendarEvent,
  BiChevronRight,
  BiDotsVerticalRounded,
} from "react-icons/bi";
import Head from "next/head";
import { AiFillCaretDown } from "react-icons/ai";
import Router from "next/router";
import {
  formatDateToDDMMYY,
  toastError,
  toastSuccess,
} from "../../../../utils";
import { useDispatch, useSelector } from "react-redux";
import check from "check-types";
import {
  clearStates,
  sendToDoctor,
} from "../../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../../redux/slices/retainerSlice";
import { fetchServices } from "../../../../redux/slices/serviceSlice";

export default function PrescribeDrug({
  isOpen,
  onOpen,
  onClose,
  prescriptionList,
  selectedPrescription,
  setSelectedPrescription,
  setPrescriptionList,
  ...rest
}) {
  const initialPrescriptionState = selectedPrescription || {
    // Set Initial Values of input fields
    prescriptionId: "",
    dosageValue: "",
    periodValue: "",
    periodType: "",
    durationValue: "",
    durationType: "",
    comment: "",
  };
  const [prescriptionDetails, setPrescriptionDetails] = useState(
    initialPrescriptionState // Store values in state
  );
  const dispatch = useDispatch();
  const [isPhone] = useMediaQuery("(max-width: 575px)");
  const toast = useToast();
  const { retainers } = useSelector((state) => state.retainers);
  const { services } = useSelector((state) => state.services);
  const { patients, selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const { userDetails } = useSelector((state) => state.user);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setPrescriptionDetails({ ...prescriptionDetails, [name]: value }); // onChange handler
  };
  useEffect(() => {
    check.emptyArray(retainers) &&
      check.nonEmptyObject(userDetails) && // If retainers haven't been loaded, then load it
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
    check.emptyArray(services) &&
      check.nonEmptyObject(userDetails) && // If services haven't been loaded, then load it
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );
  }, [userDetails]);

  useEffect(() => {
    !isOpen && setSelectedPrescription(null); //Anytime the modal is Closed, remove any selected prescription
  }, [isOpen]);

  useEffect(() => {
    selectedPrescription // If there's any selected prescription,
      ? setPrescriptionDetails(selectedPrescription) // then populate it in the modal
      : setPrescriptionDetails(initialPrescriptionState); //else, use the empty modal states
  }, [selectedPrescription]);

  const onSubmitPrescribeDrug = (event) => {
    event.stopPropagation();
    event.preventDefault();

    if (selectedPrescription) {
      setPrescriptionList(
        prescriptionList.map((prescription) =>
          prescription.id === selectedPrescription.id
            ? prescriptionDetails
            : prescription
        )
      );
    } else {
      setPrescriptionList([
        ...prescriptionList,
        { ...prescriptionDetails, id: prescriptionList.length + 1 },
      ]);
    }

    setPrescriptionDetails(initialPrescriptionState);
    onClose();
    // dispatch(
    //   sendToDoctor({
    //     user_id: selectedPatient?.id,
    //     organization_id: userDetails.organization_id,
    //     branch_id: userDetails.branch_id,
    //     retainer_id: selectedPatient.retainer_id,

    //     retainer_category: retainers.find(
    //       ({ id }) => id === selectedPatient.retainer_id
    //     )?.category,
    //     visit_status: "new",
    //     visit_type: "see-physician",
    //     services: [prescriptionDetails.specialty],
    //   })
    // );
  };

  useEffect(() => {
    if (error && error.errorType === "SEND_TO_DOCTOR") {
      // If an error occurs while sending to doctor,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  useEffect(() => {
    if (success === "SEND_TO_DOCTOR") {
      // If patient is sent successfully,
      toast(toastSuccess("Done!")); // Display Success message
      dispatch(clearStates()); // Then clear stale error messages
      onClose(); // Close form modal
    }
  }, [success]);

  return (
    <Modal
      scrollBehavior="inside"
      closeOnOverlayClick={false}
      isCentered
      size="md"
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalOverlay bgColor="rgba(0, 0, 0, 0.2)" />
      <ModalContent bg="gray.70" overflow="scroll" pb={4} rounded={8}>
        <form onSubmit={onSubmitPrescribeDrug} action="submit">
          <ModalHeader>
            <Box as="span" pl={4}>
              New Prescription
            </Box>
            <Divider mt={2} borderBottomWidth="2px" />
          </ModalHeader>
          <ModalCloseButton />

          <ModalBody pb={4}>
            <Stack spacing={4}>
              <Text px={4} color="gray.500" fontSize="sm">
                Prescribe Drug for{" "}
                <Box as="span" fontWeight="bold">
                  {selectedPatient.first_name} {selectedPatient.last_name}
                </Box>
              </Text>

              <Stack spacing={4} rounded={8} py={8} px={6} bg="white">
                <Stack>
                  <Text pl={4} color="gray.500">
                    Medicine Name
                  </Text>
                  <Select
                    value={prescriptionDetails.prescriptionId}
                    onChange={handleChange}
                    name="prescriptionId"
                    variant="filled"
                    isRequired
                    placeholder="Choose Medicine"
                  >
                    {services
                      .filter(({ category }) => category == "pharmacy")
                      .map(({ id, name }) => (
                        <option value={id}>{name}</option>
                      ))}
                  </Select>
                </Stack>

                <Stack direction={["column", "column", "row"]}>
                  <Stack>
                    <Text pl={4} color="gray.500">
                      Dosage
                    </Text>
                    <Input
                      value={prescriptionDetails.dosageValue}
                      onChange={handleChange}
                      name="dosageValue"
                      type="number"
                      variant="filled"
                      placeholder="2"
                    />
                  </Stack>
                  <HStack>
                    <Stack>
                      <Text fontSize="sm" pl={4} color="gray.500">
                        Period
                      </Text>
                      <HStack spacing={0}>
                        <Input
                          value={prescriptionDetails.periodValue}
                          onChange={handleChange}
                          name="periodValue"
                          variant="filled"
                          type="number"
                          placeholder="No. of times"
                        />
                      </HStack>
                    </Stack>
                    <Stack>
                      <Text pl={4} color="gray.500">
                        Per
                      </Text>

                      <Select
                        isFullWidth
                        value={prescriptionDetails.periodType}
                        onChange={handleChange}
                        name="periodType"
                        placeholder="Per"
                        pr={0}
                        w={24}
                        pl={0}
                        bg="transparent"
                        variant="filled"
                      >
                        <option value="hourly">Hour</option>
                        <option value="daily">Day</option>
                      </Select>
                    </Stack>
                  </HStack>
                </Stack>

                <Stack>
                  <Text pl={4} color="gray.500">
                    Duration
                  </Text>
                  <HStack>
                    <Input
                      value={prescriptionDetails.durationValue}
                      type="number"
                      onChange={handleChange}
                      name="durationValue"
                      variant="filled"
                      placeholder="Duration"
                    />
                    <Select
                      isFullWidth
                      value={prescriptionDetails.durationType}
                      onChange={handleChange}
                      name="durationType"
                      pr={0}
                      w={56}
                      placeholder="in"
                      pl={0}
                      bg="transparent"
                      variant="filled"
                    >
                      <option value="minutes">Minutes</option>
                      <option value="hours">Hours</option>
                      <option value="days">Days</option>
                      <option value="weeks">Weeks</option>
                      <option value="months">Months</option>
                      <option value="years">Years</option>
                    </Select>
                  </HStack>
                </Stack>
                <Flex pt={4} w="full">
                  <Textarea
                    placeholder="Notes: E.g. to be taken at least 2hrs After meal"
                    value={prescriptionDetails.comment}
                    onChange={handleChange}
                    h={24}
                    name="comment"
                    size="sm"
                    variant="filled"
                  />
                </Flex>
                <HStack pt={8} justify="flex-end" align="center">
                  <Button onClick={onClose} variant="outline" w={32}>
                    Cancel
                  </Button>
                  <Button
                    isLoading={loading === "SEND_TO_DOCTOR"}
                    loadingText="Sending..."
                    type="submit"
                    colorScheme="primary"
                    w={32}
                  >
                    Prescribe
                  </Button>
                </HStack>
              </Stack>
            </Stack>
          </ModalBody>
        </form>
      </ModalContent>
    </Modal>
  );
}
