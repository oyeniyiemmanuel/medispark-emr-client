import {
  Box,
  Button,
  CloseButton,
  Divider,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Flex,
  Heading,
  HStack,
  IconButton,
  Input,
  Stack,
  Table,
  Tbody,
  Td,
  Text,
  Textarea,
  Th,
  Thead,
  Tr,
  useMediaQuery,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import React, { useEffect, useState } from "react";
import { MdDelete, MdEdit } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import CustomSelect from "../../../../components/multi-select";
import SelectService from "../../../../components/select-service";
import { patientActions } from "../../../../redux/slices/patientSlice";
import {
  createPrescription,
  prescriptionActions,
} from "../../../../redux/slices/prescriptionSlice";
import { fetchRetainers } from "../../../../redux/slices/retainerSlice";
import { fetchServices } from "../../../../redux/slices/serviceSlice";
import { separateWithComma, toastError, toastSuccess } from "../../../../utils";

export default function PrescribeDrug({
  isOpen,
  onOpen,
  onClose,
  externalPrescriptionList,
  setExternalPrescriptionList,
  ...rest
}) {
  const initialPrescriptionState = {
    // Set Initial Values of input fields
    prescriptionId: null,
    quantity: "",
    comment: "",
  };

  const [prescriptionList, setPrescriptionList] = useState([]);
  const prescriptions = useSelector((state) => state.prescriptions);
  const [selectedPrescription, setSelectedPrescription] = useState(
    initialPrescriptionState
  );

  useEffect(() => {
    if (externalPrescriptionList) {
      setExternalPrescriptionList(prescriptionList);
    }
  }, [prescriptionList]);

  const dispatch = useDispatch();
  const [isPhone] = useMediaQuery("(max-width: 575px)");
  const toast = useToast();
  const [isEditingPrescription, setIsEditingPrescription] = useState(true);
  const { retainers } = useSelector((state) => state.retainers);
  const { services } = useSelector((state) => state.services);
  const { patients, selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const { userDetails } = useSelector((state) => state.user);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setSelectedPrescription({ ...selectedPrescription, [name]: value }); // onChange handler
  };
  useEffect(() => {
    check.emptyArray(retainers) &&
      check.nonEmptyObject(userDetails) && // If retainers haven't been loaded, then load it
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
    check.emptyArray(services) &&
      check.nonEmptyObject(userDetails) && // If services haven't been loaded, then load it
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );
  }, [userDetails]);

  const onUploadPrescription = (event) => {
    dispatch(
      createPrescription({
        user_id: selectedPatient.id,
        organization_id: selectedPatient.organization_id,
        branch_id: selectedPatient.branch_id,
        retainer_id: selectedPatient.retainer_id,
        retainer_category: retainers.find(
          ({ id }) => id === selectedPatient.retainer_id
        )?.category,
        visit_status: "new",
        visit_type: "see-physician",
        care_type: "outpatient",
        services: prescriptionList.map(
          ({ prescriptionId, quantity, comment }) => {
            return {
              id: prescriptionId,
              category: "pharmacy",
              strength_value: services.find(({ id }) => id == prescriptionId)
                ?.strength_value,
              strength_unit_type: services.find(
                ({ id }) => id == prescriptionId
              )?.strength_unit_type,
              quantity,
              override_add_to_queue: true,
              instruction: comment,
              stock_unit_type: services.find(({ id }) => id == prescriptionId)
                ?.stock_unit_type,
            };
          }
        ),
      })
    );
  };

  useEffect(() => {
    if (prescriptions.success === "CREATE_PRESCRIPTION") {
      // If patient is created successfully,
      setPrescriptionList([]);
      onClose();
    }
  }, [prescriptions.success]);

  const onSubmitAddDrugToList = (event) => {
    event.stopPropagation();
    event.preventDefault();

    if (selectedPrescription.id) {
      setPrescriptionList(
        prescriptionList.map((prescription) =>
          prescription.id === selectedPrescription.id
            ? selectedPrescription
            : prescription
        )
      );
    } else {
      setPrescriptionList([
        ...prescriptionList,
        { ...selectedPrescription, id: prescriptionList.length + 1 },
      ]);
    }

    setSelectedPrescription(initialPrescriptionState);
    setIsEditingPrescription(false);
  };

  const medicationOptions = [...services]
    .filter(({ category }) => category == "pharmacy")
    .map(({ id, name }) => ({ value: id, label: name }));

  const selectedMedication = [...services]
    .filter(({ category }) => category == "pharmacy")
    .find(({ id }) => id == selectedPrescription.prescriptionId);

  const selectedMedicationRemaining =
    selectedMedication?.stock_current_level -
    parseInt(selectedPrescription?.quantity || 0);

  console.log(selectedMedication, selectedPrescription);

  return (
    <Drawer onClose={onClose} isOpen={isOpen} size="md">
      <DrawerOverlay>
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader pb={0}>
            <Box as="span">Prescribe</Box>
            <Divider mb={2} border="2px solid grey" />
          </DrawerHeader>
          <DrawerBody>
            <Stack spacing={4}>
              <Text color="gray.500" fontSize="sm">
                Prescribe Medication for{" "}
                <Box as="span" fontWeight="bold">
                  {selectedPatient.first_name} {selectedPatient.last_name}
                </Box>
              </Text>

              <Stack spacing={5} rounded={8} bg="white">
                <SelectService
                  isClearable
                  valueChecker={({ id }) =>
                    id == selectedPrescription.prescriptionId
                  }
                  placeholder="Search medicine or medicine class"
                  serviceCategory="pharmacy"
                  onChange={(medication) => {
                    if (medication) {
                      setSelectedPrescription({
                        ...selectedPrescription,
                        prescriptionId: medication.id,
                      });
                      setIsEditingPrescription(true);
                    } else {
                      setSelectedPrescription(initialPrescriptionState);
                    }
                  }}
                />

                {isEditingPrescription && selectedPrescription.prescriptionId && (
                  <form onSubmit={onSubmitAddDrugToList} action="submit">
                    <Stack
                      border="1px solid lightgrey"
                      shadow="md"
                      px={5}
                      rounded={8}
                      color="gray.600"
                      py={3}
                      spacing={3}
                      w="full"
                    >
                      <HStack w="full" justify="space-between">
                        <Heading fontSize="md">
                          Prescribing {selectedMedication?.name}
                        </Heading>
                        <CloseButton
                          onClick={() => {
                            setIsEditingPrescription(false);
                            setSelectedPrescription(initialPrescriptionState);
                          }}
                        />
                      </HStack>

                      <Stack
                        w="full"
                        justify="space-between"
                        direction={["column", "column", "row"]}
                      >
                        <Stack>
                          <Text fontSize="sm" color="gray.500">
                            Dosage
                          </Text>
                          <Text fontSize="sm" color="gray.500">
                            {selectedMedication.strength_value}{" "}
                            {selectedMedication.strength_unit_type}
                          </Text>
                          {/* <Input
                            value={prescriptionDetails.dosageValue}
                            onChange={handleChange}
                            name="dosageValue"
                            isRequired
                            type="number"
                            variant="filled"
                            placeholder="2"
                          /> */}
                        </Stack>
                        <HStack>
                          <Stack>
                            <Text fontSize="sm" color="gray.500">
                              Quantity
                            </Text>
                            <HStack spacing={0}>
                              <Input
                                value={selectedPrescription.quantity}
                                onChange={handleChange}
                                name="quantity"
                                min={1}
                                variant="filled"
                                type="number"
                                isRequired
                                placeholder="Quantity"
                              />
                            </HStack>
                          </Stack>
                          <Stack w={24}>
                            <Text fontSize="sm" color="gray.500">
                              Quantity Left
                            </Text>
                            <Text
                              textAlign="center"
                              fontSize="sm"
                              color={
                                selectedMedicationRemaining < 0
                                  ? "red.500"
                                  : "gray.500"
                              }
                            >
                              {separateWithComma(selectedMedicationRemaining)}
                            </Text>
                          </Stack>
                        </HStack>
                      </Stack>
                      <Stack w="full">
                        <Text color="gray.500">Instruction</Text>
                        <Textarea
                          placeholder="eg. To be taken 2xce daily..."
                          value={selectedPrescription.comment}
                          onChange={handleChange}
                          h={16}
                          isRequired
                          name="comment"
                          size="sm"
                          variant="filled"
                        />
                      </Stack>
                      <HStack justify="flex-end" align="center">
                        <Button type="submit" colorScheme="primary" w={40}>
                          {isEditingPrescription ? "Save" : `Add Prescription`}
                        </Button>
                      </HStack>
                    </Stack>
                  </form>
                )}

                {prescriptionList.length !== 0 && (
                  <Stack
                    border="1px solid lightgrey"
                    px={1}
                    color="gray.600"
                    py={3}
                    spacing={3}
                    w="full"
                  >
                    <Heading pl={6} pb={3} fontSize="md">
                      Prescription (
                      <Box as="span" color="primary.500">
                        {prescriptionList.length} Item
                        {prescriptionList.length > 1 && "s"}
                      </Box>
                      )
                    </Heading>

                    <Table size="sm">
                      <Thead>
                        <Tr bg="gray.100">
                          <Th>Medicine</Th>
                          <Th isNumeric>Unit Price</Th>
                          <Th isNumeric>Qty</Th>
                          <Th isNumeric>Total</Th>
                          <Th isNumeric></Th>
                        </Tr>
                      </Thead>
                      <Tbody>
                        {prescriptionList.map((prescription, index) => {
                          const prescriptionAmount =
                            services
                              .find(
                                ({ id }) => id == prescription.prescriptionId
                              )
                              ?.prices?.find(
                                ({ retainer_id }) =>
                                  retainer_id == selectedPatient.retainer_id
                              )?.amount || 0;
                          return (
                            <Tr key={index}>
                              <Td>
                                {
                                  services.find(
                                    ({ id }) =>
                                      id == prescription.prescriptionId
                                  )?.name
                                }
                              </Td>
                              <Td isNumeric>
                                ₦{separateWithComma(prescriptionAmount)}
                              </Td>
                              <Td isNumeric>{prescription.quantity}</Td>
                              <Td isNumeric>
                                ₦
                                {separateWithComma(
                                  prescriptionAmount *
                                    parseInt(prescription.quantity)
                                )}
                              </Td>
                              <Td isNumeric>
                                <Flex>
                                  <IconButton
                                    pos="initial"
                                    size="sm"
                                    colorScheme="primary"
                                    variant="ghost"
                                    onClick={() => {
                                      setSelectedPrescription(prescription);
                                      setIsEditingPrescription(true);
                                    }}
                                    icon={<MdEdit />}
                                  />
                                  <IconButton
                                    pos="initial"
                                    size="sm"
                                    colorScheme="primary"
                                    onClick={() => {
                                      setPrescriptionList(
                                        [...prescriptionList].filter(
                                          ({ id }) => id !== prescription.id
                                        )
                                      );
                                    }}
                                    variant="ghost"
                                    icon={<MdDelete />}
                                  />
                                </Flex>
                              </Td>
                            </Tr>
                          );
                        })}
                      </Tbody>
                    </Table>
                    <Flex px={4} justify="flex-end" w="full">
                      <Text fontWeight="bold">
                        Total &nbsp; ₦
                        {separateWithComma(
                          prescriptionList.reduce(
                            (sum, { prescriptionId, quantity }) =>
                              sum +
                              (services
                                .find(({ id }) => id == prescriptionId)
                                ?.prices?.find(
                                  ({ retainer_id }) =>
                                    retainer_id == selectedPatient.retainer_id
                                )?.amount || 0) *
                                quantity,
                            0
                          )
                        )}
                      </Text>
                    </Flex>
                    <Divider />
                    <HStack px={4} justify="flex-end" align="center">
                      <Button
                        onClick={() => {
                          if (externalPrescriptionList) {
                            onClose();
                          } else {
                            onUploadPrescription();
                          }
                        }}
                        isLoading={
                          prescriptions.loading === "CREATE_PRESCRIPTION"
                        }
                        loadingText="Prescribing..."
                        type="submit"
                        colorScheme="primary"
                        w={40}
                      >
                        Done
                      </Button>
                    </HStack>
                  </Stack>
                )}
              </Stack>
            </Stack>
          </DrawerBody>
        </DrawerContent>
      </DrawerOverlay>
    </Drawer>
  );
}
