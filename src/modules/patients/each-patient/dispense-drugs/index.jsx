import { EditIcon } from "@chakra-ui/icons";
import {
  Avatar,
  Box,
  Button,
  Divider,
  Flex,
  HStack,
  Input,
  Stack,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { closePopup } from "../../../../redux/slices/commonSlice";
import { patientActions } from "../../../../redux/slices/patientSlice";
import { separateWithComma, toastError, toastSuccess } from "../../../../utils";
import PrescribeDrug from "../prescriptions/prescribe-drug";
import Moment from "react-moment";
import "moment-timezone";
import {
  dispensaryActions,
  dispenseMedication,
} from "../../../../redux/slices/dispensarySlice";
import check from "check-types";
import { BeatLoader } from "react-spinners";
import theme from "../../../../theme";

export default function DispenseDrugs({ prescription, ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const toast = useToast();
  const { userDetails } = useSelector((state) => state.user);
  const { patients, selectedPatient } = useSelector((state) => state.patients);
  const { services } = useSelector((state) => state.services);
  const { newestPrescription } = useSelector((state) => state.prescriptions);
  const { staff } = useSelector((state) => state.staff);
  const { retainers } = useSelector((state) => state.retainers);
  const common = useSelector((state) => state.common);
  const dispensaries = useSelector((state) => state.dispensaries);
  const [prescriptionList, setPrescriptionList] = useState([]);
  const [prescriptionQuantity, setPrescriptionQuantity] = useState(
    newestPrescription.quantity || ""
  );

  const selectedMedication = services.find(
    ({ id }) => id == prescription.service_id
  );

  useEffect(() => {
    setPrescriptionQuantity(newestPrescription.quantity);
  }, [newestPrescription.quantity]);

  const onDispenseMedication = (event) => {
    event.preventDefault();

    dispatch(
      dispenseMedication({
        stock_id: selectedMedication.stock_id,
        quantity: parseInt(prescriptionQuantity),
      })
    );
  };

  useEffect(() => {
    if (
      dispensaries.error &&
      dispensaries.error.errorType === "DISPENSE_MEDICATION"
    ) {
      // If an dispensaries.error occurs while creating patient,
      toast(toastError(dispensaries.error)); // Display Error Message
      dispatch(dispensaryActions.clearStates()); // Then clear stale dispensaries.error messages
    }
  }, [dispensaries.error]);

  useEffect(() => {
    if (dispensaries.success === "DISPENSE_MEDICATION") {
      // If patient is created successfully,
      toast(toastSuccess(`Dispensed Successfully`)); // Display Success message
      dispatch(dispensaryActions.clearStates()); // Then clear stale error messages
      dispatch(closePopup());
    }
  }, [dispensaries.success]);

  const prescriptionAmount =
    selectedMedication?.prices?.find(
      ({ retainer_id }) => retainer_id == selectedPatient.retainer_id
    )?.amount || 0;

  return (
    <>
      <form onSubmit={onDispenseMedication} action="submit">
        <Stack
          border="1px solid lightgrey"
          bg="white"
          rounded={8}
          color="gray.500"
          p={3}
          {...rest}
        >
          {check.nonEmptyObject(newestPrescription) ? (
            <Stack spacing={3}>
              <Stack>
                <HStack justify="space-between">
                  <HStack fontSize="sm">
                    <Avatar
                      size="sm"
                      name={
                        patients.find(
                          ({ id }) => id === prescription.patient_id
                        )?.full_name
                      }
                      src={
                        patients.find(
                          ({ id }) => id === prescription.patient_id
                        )?.avatar?.file_path
                      }
                    />
                    <Box>
                      <Text fontSize="md">
                        {
                          patients.find(
                            ({ id }) => id === prescription.patient_id
                          )?.full_name
                        }
                      </Text>
                      <Text
                        fontSize="xs"
                        textTransform="uppercase"
                        color="primary.600"
                      >
                        {
                          retainers.find(
                            ({ id }) => id === prescription.patient_retainer_id
                          )?.name
                        }
                      </Text>
                    </Box>
                  </HStack>
                  <Stack align="flex-end" justify="flex-end" spacing={0}>
                    <Text fontSize="sm">
                      By{" "}
                      {
                        staff.find(({ id }) => id === prescription.staff_id)
                          ?.full_name
                      }{" "}
                      |{" "}
                      <Moment format="MMMM D, YYYY,">
                        {new Date(prescription.created_at).toDateString()}
                      </Moment>{" "}
                      {prescription.encounter_id && (
                        <>
                          in{" "}
                          <Box fontWeight="bold" as="span" color="green.500">
                            Encounter
                          </Box>
                        </>
                      )}
                    </Text>
                    <Text>
                      <Moment format="hh:mm a">
                        {new Date(prescription.created_at)}
                      </Moment>
                    </Text>
                  </Stack>
                </HStack>
                <Divider borderBottomColor="gray.400" />
                <Table variant="unstyled" size="sm">
                  <Thead>
                    <Tr textTransform="capitalize">
                      <Th>Medicine</Th>
                      {/* <Th>Prsc</Th> */}
                      <Th>Instruction</Th>
                      <Th isNumeric>Qty</Th>
                      <Th isNumeric>Unit Price</Th>
                      <Th isNumeric>Total</Th>
                    </Tr>
                  </Thead>

                  <Tbody color="gray.500">
                    <Tr>
                      <Td>
                        {prescription.service_name}{" "}
                        {selectedMedication.strength_value}{" "}
                        {selectedMedication.strength_unit_type}
                      </Td>
                      {/* <Td>6pcs</Td> */}
                      <Td>{newestPrescription.instruction}</Td>
                      <Input
                        type="number"
                        size="sm"
                        variant="filled"
                        isRequired
                        w={24}
                        min={1}
                        value={prescriptionQuantity}
                        onChange={(event) =>
                          setPrescriptionQuantity(event.target.value)
                        }
                      />
                      <Td isNumeric>
                        ₦{separateWithComma(prescriptionAmount)}
                      </Td>
                      <Td isNumeric>
                        ₦
                        {separateWithComma(
                          prescriptionAmount * parseInt(prescriptionQuantity)
                        )}
                      </Td>
                    </Tr>
                  </Tbody>
                </Table>
              </Stack>
              <Divider borderBottomColor="gray.400" />
              <HStack px={8} justify="space-between">
                <Button
                  variant="link"
                  size="sm"
                  colorScheme="primary"
                  leftIcon={<EditIcon />}
                  onClick={onOpen}
                >
                  Adjust Medication
                </Button>
                <Text fontWeight="bold">
                  Total &nbsp; ₦
                  {separateWithComma(
                    prescriptionAmount * parseInt(prescriptionQuantity)
                  )}
                </Text>
              </HStack>
              <Divider borderBottomColor="gray.400" />

              <Stack spacing={4}>
                <HStack align="center" justify="space-between">
                  <Stack
                  // py={2} px={6} bg="primary.50" borderRadius={4}
                  >
                    {/* <Text
                      fontSize="sm"
                      color="gray.600"
                      textTransform="uppercase"
                    >
                      <Box as="span" color="green.500">
                        AMOUNT PAID
                      </Box>{" "}
                      #1,120,000
                    </Text>
                    <Text
                      fontSize="sm"
                      color="gray.600"
                      textTransform="uppercase"
                    >
                      <Box as="span" color="red.500">
                        OUTSTANDING
                      </Box>{" "}
                      #1,120,000
                    </Text> */}
                  </Stack>
                  <Button
                    type="submit"
                    isLoading={dispensaries.loading === "DISPENSE_MEDICATION"}
                    loadingText="Dispensing..."
                    w={32}
                    colorScheme="primary"
                  >
                    Dispense
                  </Button>
                  <PrescribeDrug
                    externalPrescriptionList={prescriptionList}
                    setExternalPrescriptionList={setPrescriptionList}
                    onOpen={onOpen}
                    onClose={onClose}
                    isOpen={isOpen}
                  />
                </HStack>
              </Stack>
            </Stack>
          ) : (
            <Flex justify="center" w="full">
              <BeatLoader size={6} color={theme.colors.primary[500]} />
            </Flex>
          )}
        </Stack>
      </form>
    </>
  );
}
