import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Select,
  Input,
  Icon,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { RiEditLine, RiFileList2Fill } from "react-icons/ri";
import { BsFillCaretRightFill } from "react-icons/bs";
import DashboardContainer from "../../../../components/layouts/dashboard";
import {
  BiCalendarEvent,
  BiChevronRight,
  BiDotsVerticalRounded,
} from "react-icons/bi";
import Head from "next/head";
import { AiFillCaretDown } from "react-icons/ai";
import { CgNotes } from "react-icons/cg";
import { formatDateToDDMMYY } from "../../../../utils";
import { MdAddCircle } from "react-icons/md";
import EncounterCard from "../encounter/view-encounter/encounter-card";
import InvoiceCard from "../billing/invoice-card";
import ViewEncounters from "../encounter/view-encounter";
import check from "check-types";
import { useDispatch, useSelector } from "react-redux";
import { fetchPatientTimeline } from "../../../../redux/slices/patientSlice";
import ViewNotes from "../documentations/view-notes";
import ViewAllTimeline from "./view-all-timeline";
import ViewVitals from "../documentations/view-vitals";
import ViewPrescriptions from "../prescriptions/view-treatments";
import ViewBills from "../billing/view-patient-bills";
import Router, { useRouter } from "next/router";

export default function ActivityTimeline() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [tabIndex, setTabIndex] = useState(0);
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient, patientTimeline, loading, success, error } =
    useSelector((state) => state.patients);
  const dispatch = useDispatch();
  const { query } = useRouter();

  useEffect(() => {
    if (query?.activity === "bills") {
      setTabIndex(5);
    } else if (query?.activity === "treatments") {
      setTabIndex(4);
    }
  }, [query]);

  useEffect(() => {
    check.emptyObject(patientTimeline) &&
      dispatch(
        fetchPatientTimeline({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
          user_id: selectedPatient.id,
        })
      );
  }, [selectedPatient, check.emptyObject(patientTimeline)]);

  const handleTabsChange = (index) => {
    setTabIndex(index);
  };

  const patientTabs = [
    "All Entries",
    "Encounters",
    "Notes",
    "Vitals",
    // "Investigations",
    "Treatments",
    "Bills",
  ];
  return (
    <Tabs index={tabIndex} onChange={handleTabsChange} isFitted isLazy>
      <TabList borderBottom="none">
        {patientTabs.map((tab, index) => (
          <Tab
            key={index}
            color="gray.500"
            fontSize="sm"
            _selected={{
              fontWeight: "bold",
              color: "primary.500",
              borderColor: "currentcolor",
              borderBottomWidth: 4,
            }}
            _hover={{
              backgroundColor: "#e3f2f5",
            }}
          >
            {tab}
          </Tab>
        ))}
      </TabList>

      <TabPanels>
        <TabPanel px={0}>
          <ViewAllTimeline />
        </TabPanel>
        <TabPanel px={0}>
          <ViewEncounters />
        </TabPanel>
        <TabPanel px={0}>
          <ViewNotes />
        </TabPanel>
        <TabPanel px={0}>
          <ViewVitals />
        </TabPanel>
        {/* <TabPanel px={0}>
          <InvoiceCard />
        </TabPanel> */}
        <TabPanel px={0}>
          <ViewPrescriptions />
        </TabPanel>
        <TabPanel px={0}>
          <ViewBills />
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
}
