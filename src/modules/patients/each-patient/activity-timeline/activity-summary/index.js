import { Button, IconButton } from "@chakra-ui/button";
import { Checkbox } from "@chakra-ui/checkbox";
import {
  Box,
  Center,
  Divider,
  Flex,
  Heading,
  HStack,
  Stack,
  Text,
} from "@chakra-ui/layout";
import { Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/menu";
import {
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
} from "@chakra-ui/modal";
import { Table, Tbody, Td, Tr } from "@chakra-ui/table";
import React, { useEffect, useState } from "react";
import { FaPrint, FaShare } from "react-icons/fa";
import { MdShare } from "react-icons/md";
import { useSelector } from "react-redux";
import Moment from "react-moment";
import "moment-timezone";
import check from "check-types";
import { BeatLoader } from "react-spinners";
import {
  BillItem,
  EncounterItem,
  InvestigationItem,
  NoteItem,
  TreatmentItem,
  VitalItem,
} from "./summary-list-items";

export default function ActivitySummary({
  activitiesToSummarize,
  setActivitiesToSummarize,
  isOpen,
  onOpen,
  onClose,
}) {
  const { selectedPatient } = useSelector((state) => state.patients);

  const hasEncounter = ({ timeline_entry_type }) =>
    timeline_entry_type == "encounter";

  const hasNote = ({ timeline_entry_type }) => timeline_entry_type == "note";

  const hasVitals = ({ timeline_entry_type }) =>
    timeline_entry_type == "vitals";

  const hasTreatment = ({ timeline_entry_type }) =>
    timeline_entry_type == "treatment";

  const hasInvestigation = ({ timeline_entry_type }) =>
    timeline_entry_type == "investigation";

  const hasOtherBillables = ({ timeline_entry_type }) =>
    timeline_entry_type == "other_billables";

  return (
    <Drawer onClose={onClose} isOpen={isOpen} size="md">
      <DrawerOverlay>
        <DrawerContent px={3}>
          <DrawerCloseButton top={5} />
          <DrawerHeader pr={12} pt={4} pb={0}>
            <HStack justify="space-between">
              <Box color="gray.600" as="span">
                Visit Summary
              </Box>
              <HStack spacing={0}>
                <IconButton
                  variant="default"
                  color="primary.500"
                  icon={<FaPrint />}
                />
                <IconButton
                  variant="default"
                  color="primary.500"
                  icon={<MdShare />}
                />
              </HStack>
            </HStack>
            <Divider mb={2} border="1px solid grey" />
          </DrawerHeader>
          <DrawerBody
            overflowY="scroll"
            className="container-body"
            pb={8}
            color="gray.500"
          >
            <Stack spacing={8} w="full">
              <HStack justify="space-between">
                <Box>
                  <Text fontSize="sm">Patient Name</Text>
                  <Text fontSize="lg">{selectedPatient.full_name}</Text>
                  <Text color="primary.500" fontSize="sm">
                    HOSP NUMBER
                  </Text>
                </Box>
                <Center bg="gray.300" boxSize={10} rounded={6} />
              </HStack>
              <Text fontSize="sm" fontWeight="bold">
                21st January, 2021
              </Text>
            </Stack>
            {activitiesToSummarize.find(hasEncounter) && (
              <Box>
                <Divider mt={4} border="1px solid grey" />
                <HStack my={2} justify="space-between">
                  <Text fontWeight="bold" fontSize="sm">
                    Encounter
                  </Text>
                </HStack>
                <Divider mb={4} border="1px solid grey" />
                <Stack px={5}>
                  {[...activitiesToSummarize]
                    .filter(hasEncounter)
                    .map((activity) => (
                      <EncounterItem encounter={activity} key={activity.id} />
                    ))}
                </Stack>
              </Box>
            )}
            {activitiesToSummarize.find(hasNote) && (
              <Box>
                <Divider mt={4} border="1px solid grey" />
                <HStack my={2} justify="space-between">
                  <Text fontWeight="bold" fontSize="sm">
                    Notes
                  </Text>
                </HStack>
                <Divider mb={4} border="1px solid grey" />
                <Stack px={5}>
                  {[...activitiesToSummarize]
                    .filter(hasNote)
                    .map((activity) => (
                      <NoteItem note={activity} key={activity.id} />
                    ))}
                </Stack>
              </Box>
            )}
            {activitiesToSummarize.find(hasVitals) && (
              <Box>
                <Divider mt={4} border="1px solid grey" />
                <HStack my={2} justify="space-between">
                  <Text fontWeight="bold" fontSize="sm">
                    Vitals
                  </Text>
                </HStack>
                <Divider mb={4} border="1px solid grey" />
                <Stack px={5}>
                  {[...activitiesToSummarize]
                    .filter(hasVitals)
                    .map((activity) => (
                      <VitalItem vital={activity} key={activity.id} />
                    ))}
                </Stack>
              </Box>
            )}
            {activitiesToSummarize.find(hasInvestigation) && (
              <Box>
                <Divider mt={4} border="1px solid grey" />
                <HStack my={2} justify="space-between">
                  <Text fontWeight="bold" fontSize="sm">
                    Investigation
                  </Text>
                </HStack>
                <Divider mb={4} border="1px solid grey" />
                <Stack px={5}>
                  {[...activitiesToSummarize]
                    .filter(hasInvestigation)
                    .map((activity) => (
                      <InvestigationItem
                        investigation={activity}
                        key={activity.id}
                      />
                    ))}
                </Stack>
              </Box>
            )}
            {activitiesToSummarize.find(hasTreatment) && (
              <Box>
                <Divider mt={4} border="1px solid grey" />
                <HStack my={2} justify="space-between">
                  <Text fontWeight="bold" fontSize="sm">
                    Prescription
                  </Text>
                </HStack>
                <Divider mb={4} border="1px solid grey" />
                <Stack spacing={5} px={5}>
                  {[...activitiesToSummarize]
                    .filter(hasTreatment)
                    .map((activity) => (
                      <TreatmentItem treatment={activity} key={activity.id} />
                    ))}
                </Stack>
              </Box>
            )}
            {activitiesToSummarize.find(hasOtherBillables) && (
              <Box>
                <Divider mt={4} border="1px solid grey" />
                <HStack my={2} justify="space-between">
                  <Text fontWeight="bold" fontSize="sm">
                    Bills
                  </Text>
                </HStack>
                <Divider mb={4} border="1px solid grey" />
                <Stack px={5}>
                  {[...activitiesToSummarize]
                    .filter(hasOtherBillables)
                    .map((activity) => (
                      <BillItem bill={activity} key={activity.id} />
                    ))}
                </Stack>
              </Box>
            )}
          </DrawerBody>
        </DrawerContent>
      </DrawerOverlay>
    </Drawer>
  );
}
