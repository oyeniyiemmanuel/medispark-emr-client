import {
  Badge,
  Box,
  HStack,
  Stack,
  Text,
  Wrap,
  WrapItem,
} from "@chakra-ui/layout";
import React from "react";
import { useSelector } from "react-redux";
import { separateWithComma } from "../../../../../utils";
import Moment from "react-moment";
import "moment-timezone";
import check from "check-types";
import VitalDetails from "../../documentations/view-vitals/vital-details";
import { Table, Tbody, Td, Tr } from "@chakra-ui/table";

export const EncounterItem = ({ encounter }) => {
  return (
    <Stack fontSize="sm" spacing={4} w="full">
      <Wrap spacing={1}>
        {encounter.diagnoses?.map(({ code, text }, index) => (
          <WrapItem key={index}>
            <HStack
              rounded={16}
              bg="primary.50"
              borderColor="primary.100"
              borderWidth="1px"
              py={1}
              px={2}
            >
              {code && <Badge colorScheme="green">{code}</Badge>}
              <Text fontSize="sm">{text}</Text>
            </HStack>
          </WrapItem>
        ))}
      </Wrap>
      <Box w="full">
        <Text>{encounter?.body.replace(/(<([^>]+)>)/gi, " ")}</Text>
        <Text color="gray.400" textAlign="right">
          <Moment format="MMMM D, YYYY,">
            {new Date(encounter.created_at).toDateString()}
          </Moment>{" "}
          <Moment format="hh:mm a">{new Date(encounter.created_at)}</Moment>
        </Text>
      </Box>
    </Stack>
  );
};

export const NoteItem = ({ note }) => {
  return (
    <Stack fontSize="sm" spacing={4} w="full">
      <Box w="full">
        <Text>{note.body?.replace(/(<([^>]+)>)/gi, " ")}</Text>
        <Text color="gray.400" textAlign="right">
          <Moment format="MMMM D, YYYY,">
            {new Date(note.created_at).toDateString()}
          </Moment>{" "}
          <Moment format="hh:mm a">{new Date(note.created_at)}</Moment>
        </Text>
      </Box>
    </Stack>
  );
};

export const VitalItem = ({ vital }) => {
  return (
    <Stack fontSize="sm" spacing={4} w="full">
      <Box w="full">
        <VitalDetails vital={vital} />
        <Text color="gray.400" textAlign="right">
          <Moment format="MMMM D, YYYY,">
            {new Date(vital.created_at).toDateString()}
          </Moment>{" "}
          <Moment format="hh:mm a">{new Date(vital.created_at)}</Moment>
        </Text>
      </Box>
    </Stack>
  );
};

export const InvestigationItem = ({ investigation }) => {
  return (
    <Stack fontSize="sm" spacing={4} w="full">
      <HStack justify="space-between">
        <Text>Aspirin 75mg</Text>
        <Text>#12,000</Text>
      </HStack>
      <Box w="full">
        <Text>
          14 tablets 3 times dailyTake 2 tablets 3 times dailyTake 2 tablets 3
          times daily DESCRIPTION*
        </Text>
        <Text color="gray.400" textAlign="right">
          <Moment format="MMMM D, YYYY,">
            {new Date(investigation.created_at).toDateString()}
          </Moment>{" "}
          <Moment format="hh:mm a">{new Date(investigation.created_at)}</Moment>
        </Text>
      </Box>
    </Stack>
  );
};

export const TreatmentItem = ({ treatment }) => {
  const { services } = useSelector((state) => state.services);
  const { selectedPatient } = useSelector((state) => state.patients);

  const selectedMedication = services.find(
    ({ id }) => id == treatment.service_id
  );
  const prescriptionAmount =
    selectedMedication?.prices?.find(
      ({ retainer_id }) => retainer_id == selectedPatient.retainer_id
    )?.amount || 0;

  return (
    <Stack fontSize="sm" spacing={3} w="full">
      <HStack justify="space-between">
        <Text>
          {selectedMedication?.name} {selectedMedication?.strength_value}{" "}
          {selectedMedication?.strength_unit_type}
        </Text>
        <Text>
          ₦
          {separateWithComma(
            prescriptionAmount * parseInt(treatment.prescription?.quantity)
          )}
        </Text>
      </HStack>
      <Box w="full">
        <Text>{treatment.prescription?.instruction}</Text>
        <Text color="gray.400" textAlign="right">
          <Moment format="MMMM D, YYYY,">
            {new Date(treatment.created_at).toDateString()}
          </Moment>{" "}
          <Moment format="hh:mm a">{new Date(treatment.created_at)}</Moment>
        </Text>
      </Box>
    </Stack>
  );
};

export const BillItem = ({ bill }) => {
  const { services } = useSelector((state) => state.services);
  const { selectedPatient } = useSelector((state) => state.patients);
  const selectedService = services.find(({ id }) => id == bill.service_id);
  const prescriptionAmount =
    selectedService?.prices?.find(
      ({ retainer_id }) => retainer_id == selectedPatient.retainer_id
    )?.amount || 0;

  const isDepositRequest = bill.payload_type == "deposit_request";

  return (
    <Stack fontSize="sm" spacing={4} w="full">
      {!isDepositRequest && (
        <HStack px={8} justify="flex-end">
          <Text fontWeight="bold">
            Total &nbsp; ₦
            {separateWithComma(
              prescriptionAmount * parseInt(bill.prescription?.quantity || 1)
            )}
          </Text>
        </HStack>
      )}
      <Box w="full">
        <Stack>
          {bill.prescription ? (
            <Table variant="unstyled" size="sm">
              <Tbody color="gray.500">
                <Tr>
                  <Td>
                    {selectedService?.name} {selectedService?.strength_value}{" "}
                    {selectedService?.strength_unit_type}
                  </Td>
                  <Td isNumeric>
                    {separateWithComma(bill.prescription?.quantity)}{" "}
                    {bill.prescription?.stock_unit_type}
                  </Td>
                  <Td>{bill.prescription?.instruction}</Td>
                  <Td isNumeric>
                    ₦
                    {separateWithComma(
                      prescriptionAmount * parseInt(bill.prescription?.quantity)
                    )}
                  </Td>
                </Tr>
              </Tbody>
            </Table>
          ) : isDepositRequest ? (
            <Text>
              {selectedPatient.full_name} requested for a deposit for{" "}
              <Box textTransform="capitalize" as="span" fontWeight="bold">
                {bill.reason}
              </Box>
            </Text>
          ) : (
            <Text>
              {selectedPatient.full_name} was checked in for service named{" "}
              <Box textTransform="capitalize" as="span" fontWeight="bold">
                {selectedService?.name}
              </Box>
            </Text>
          )}
        </Stack>
        <Text color="gray.400" textAlign="right">
          <Moment format="MMMM D, YYYY,">
            {new Date(bill.created_at).toDateString()}
          </Moment>{" "}
          <Moment format="hh:mm a">{new Date(bill.created_at)}</Moment>
        </Text>
      </Box>
    </Stack>
  );
};
