import { Button, IconButton } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Input } from "@chakra-ui/input";
import { Badge, Divider, Flex, HStack, Stack, Text } from "@chakra-ui/layout";
import check from "check-types";
import React, { useEffect, useState } from "react";
import { AiFillCaretDown } from "react-icons/ai";
import { BiCalendarEvent, BiDotsVerticalRounded } from "react-icons/bi";
import { BsGrid1X2Fill } from "react-icons/bs";
import { MdDelete, MdFormatListBulleted } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { fetchPatientTimeline } from "../../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../../redux/slices/retainerSlice";
import { fetchServices } from "../../../../redux/slices/serviceSlice";
import { fetchStaff } from "../../../../redux/slices/staffSlice";
import FilterResults from "react-filter-search";
import NoteCard from "../documentations/view-notes/note-card";
import EncounterCard from "../encounter/view-encounter/encounter-card";
import theme from "../../../../theme";
import { BeatLoader } from "react-spinners";
import VitalCard from "../documentations/view-vitals/vital-card";
import Moment from "react-moment";
import "moment-timezone";
import TreatmentCard from "../prescriptions/view-treatments/treatment-card";
import BillCard from "../billing/view-patient-bills/bill-card";
import { Checkbox } from "@chakra-ui/checkbox";
import { CgPlayListCheck } from "react-icons/cg";
import { VscCheck } from "react-icons/vsc";
import ActivitySummary from "./activity-summary";

export default function ViewAllTimeline() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient, patientTimeline, loading, success, error } =
    useSelector((state) => state.patients);
  const dispatch = useDispatch();
  const [searchValue, setSearchValue] = useState("");
  const [selectAllActivities, setSelectAllActivities] = useState(false);
  const [selectedActivities, setSelectedActivities] = useState([]);
  const { retainers } = useSelector((state) => state.retainers);
  const { services } = useSelector((state) => state.services);
  const { staff } = useSelector((state) => state.staff);
  const [activitiesToSummarize, setActivitiesToSummarize] = useState([]);

  const allActivities = check.nonEmptyObject(patientTimeline)
    ? [
        ...patientTimeline.encounters,
        ...patientTimeline.notes,
        ...patientTimeline.vitals,
        ...patientTimeline.treatments,
        ...patientTimeline.investigations,
        ...patientTimeline.bills,
      ].sort((a, b) => new Date(b.created_at) - new Date(a.created_at))
    : [];

  useEffect(() => {
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
    check.emptyArray(services) &&
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );

    check.emptyArray(staff) &&
      dispatch(
        fetchStaff({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );
  }, [userDetails]);

  useEffect(() => {
    if (selectAllActivities) {
      setSelectedActivities(allActivities.map(({ id }) => id));
    } else {
      setSelectedActivities([]);
    }
  }, [selectAllActivities]);

  const addOrRemoveFromSelectedActivities = (event) => {
    if (selectedActivities.find((entryId) => entryId == event.target.value)) {
      setSelectAllActivities(false);
      setSelectedActivities(
        [...selectedActivities].filter(
          (entryId) => entryId != event.target.value
        )
      );
    } else {
      setSelectedActivities([...selectedActivities, event.target.value]);
    }
  };

  const checkIfActivitySelected = (activityId) =>
    Boolean(selectedActivities.find((id) => id == activityId));

  const selectedActivitiesObjects = selectedActivities.map((activityId) =>
    allActivities?.find(({ id }) => id == activityId)
  );

  const processSelectedActivities = () => {
    setActivitiesToSummarize(selectedActivitiesObjects);
    onOpen();
  };

  // console.log(
  //   allActivities,
  //   allActivities.filter(({ id }) => id == 106),
  //   patientTimeline
  // );

  return (
    <Stack spacing={4}>
      <Flex justify="space-between">
        <HStack color="gray.500">
          <Checkbox
            colorScheme="primary"
            borderColor="primary.500"
            size="lg"
            pos="initial"
            mr={3}
            isChecked={selectAllActivities}
            onChange={() => setSelectAllActivities(!selectAllActivities)}
          ></Checkbox>

          {check.nonEmptyArray(selectedActivities) ? (
            <HStack fontSize="sm" color="gray.500">
              <HStack spacing={0}>
                <IconButton variant="default" icon={<MdDelete />} />
                <IconButton variant="default" icon={<VscCheck />} />
                <Button
                  leftIcon={<CgPlayListCheck />}
                  size="sm"
                  variant="solid"
                  colorScheme="green"
                  onClick={processSelectedActivities}
                >
                  Summarize
                </Button>
              </HStack>
            </HStack>
          ) : (
            <HStack>
              <Input
                size="sm"
                pos="initial"
                w={40}
                value={searchValue}
                onChange={(event) => {
                  setSearchValue(event.target.value);
                }}
                placeholder="Search Activities"
                rounded="full"
              />
              <Button variant="ghost" size="sm" rightIcon={<AiFillCaretDown />}>
                All Users
              </Button>
              <Button variant="ghost" size="sm" rightIcon={<BiCalendarEvent />}>
                Filter by date
              </Button>
            </HStack>
          )}
        </HStack>
      </Flex>
      <Divider opacity="initial" borderColor="gray.400" />
      {loading === "FETCH_PATIENT_TIMELINE" && (
        <Flex w="full" justify="center">
          <BeatLoader color={theme.colors.primary[500]} />
        </Flex>
      )}
      <FilterResults
        value={searchValue}
        data={allActivities}
        renderResults={(results) =>
          check.emptyArray(results) ? (
            <Text
              w="full"
              color="gray.500"
              wordBreak="break-word"
              textAlign="center"
            >
              {loading === "FETCH_PATIENT_TIMELINE"
                ? ""
                : "When you add a new activity, it will show here"}
            </Text>
          ) : (
            results.map((activity, index) => {
              if (activity.timeline_entry_type == "note") {
                return (
                  <NoteCard
                    isChecked={checkIfActivitySelected(activity.id)}
                    addOrRemoveFromSelectedActivities={
                      addOrRemoveFromSelectedActivities
                    }
                    key={index}
                    note={activity}
                  />
                );
              } else if (activity.timeline_entry_type == "encounter") {
                return (
                  <EncounterCard
                    isChecked={checkIfActivitySelected(activity.id)}
                    addOrRemoveFromSelectedActivities={
                      addOrRemoveFromSelectedActivities
                    }
                    key={index}
                    encounter={activity}
                  />
                );
              } else if (activity.timeline_entry_type == "vitals") {
                return (
                  <VitalCard
                    isChecked={checkIfActivitySelected(activity.id)}
                    addOrRemoveFromSelectedActivities={
                      addOrRemoveFromSelectedActivities
                    }
                    key={index}
                    vital={activity}
                  />
                );
              } else if (activity.timeline_entry_type == "treatment") {
                return (
                  <>
                    <TreatmentCard
                      isChecked={checkIfActivitySelected(activity.id)}
                      addOrRemoveFromSelectedActivities={
                        addOrRemoveFromSelectedActivities
                      }
                      key={index}
                      treatment={activity}
                    />
                    {/* <BillCard
                    isChecked={checkIfActivitySelected(activity.id)}
                    addOrRemoveFromSelectedActivities={
                      addOrRemoveFromSelectedActivities
                    }
                    key={index}
                    bill={activity}
                  /> */}
                  </>
                );
              } else if (activity.timeline_entry_type == "other_billables") {
                return (
                  <BillCard
                    isChecked={checkIfActivitySelected(activity.id)}
                    addOrRemoveFromSelectedActivities={
                      addOrRemoveFromSelectedActivities
                    }
                    key={index}
                    bill={activity}
                  />
                );
              }
            })
          )
        }
      />
      <Flex justify="center" w="full">
        <Badge
          py={2}
          px={4}
          rounded={8}
          colorScheme="blue"
          bg="primary.50"
          color="gray.500"
          fontWeight="normal"
          textTransform="initial"
        >
          <Flex color="gray.700" fontWeight="bold" w="full" justify="center">
            EVENT
          </Flex>
          {selectedPatient.full_name} was registered as a patient on{" "}
          <Moment format="MMMM D, YYYY">
            {new Date(selectedPatient.created_at).toDateString()}
          </Moment>
          &nbsp;at&nbsp;
          <Moment format="hh:mm a">
            {new Date(selectedPatient.created_at)}
          </Moment>
        </Badge>
      </Flex>
      <ActivitySummary
        activitiesToSummarize={activitiesToSummarize}
        setActivitiesToSummarize={setActivitiesToSummarize}
        isOpen={isOpen}
        onClose={onClose}
        onOpen={isOpen}
      />
    </Stack>
  );
}
