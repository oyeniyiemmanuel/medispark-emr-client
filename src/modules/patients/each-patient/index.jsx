import React from "react";
import ActivityTimeline from "./activity-timeline";
import PatientContainer from "./shared/patient-container";

export default function EachPatientView() {
  return (
    <PatientContainer title="Patient Card">
      <ActivityTimeline />
    </PatientContainer>
  );
}
