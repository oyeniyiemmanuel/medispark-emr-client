import { Button } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Box, Stack, Text } from "@chakra-ui/layout";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/table";
import React from "react";
import { useSelector } from "react-redux";
import Moment from "react-moment";
import "moment-timezone";

export default function AuditTrailModal({
  audits,
  created_at,
  timeline_entry_type,
  ...rest
}) {
  const { staff } = useSelector((state) => state.staff);
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Button
        onClick={onOpen}
        variant="link"
        colorScheme="primary"
        size="sm"
        {...rest}
      >
        Open Audit trail
      </Button>
      <Modal
        scrollBehavior={"inside"}
        isCentered
        size="xl"
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader pb={2} textTransform="uppercase" fontSize="md">
            Audit trail for <Box as="span">{timeline_entry_type}</Box> created
            on{" "}
            <Moment format="MMMM D, YYYY,">
              {new Date(created_at).toDateString()}
            </Moment>
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Stack>
              {[...audits]
                ?.reverse()
                .slice(0, -1)
                .map(({ user_id, new_values, old_values }, index) => {
                  const changedKeys = Object.keys(new_values);
                  return changedKeys?.map((eachKey, index) => {
                    return (
                      <Text as="ol" fontSize="sm" color="gray.500" key={index}>
                        &bull;{" "}
                        {staff?.find(({ id }) => id == user_id)?.full_name}{" "}
                        changed{" "}
                        <Box as="span" fontWeight="bold">
                          {eachKey}
                        </Box>{" "}
                        {old_values[eachKey] && (
                          <>
                            from{" "}
                            <Box as="span" fontWeight="bold">
                              {old_values[eachKey]
                                ?.toString()
                                .replace(/(<([^>]+)>)/gi, " ")}
                            </Box>
                          </>
                        )}{" "}
                        to{" "}
                        <Box as="span" fontWeight="bold">
                          {new_values[eachKey]
                            ?.toString()
                            .replace(/(<([^>]+)>)/gi, " ")}
                        </Box>{" "}
                        {/* on{" "}
                    <Box as="span" fontWeight="bold">
                      24th April 2021
                    </Box>{" "}
                    at{" "}
                    <Box as="span" fontWeight="bold">
                      8:33pm
                    </Box> */}
                      </Text>
                    );
                  });
                })}

              <Text as="ol" fontSize="sm" color="gray.500">
                &bull;{" "}
                {staff?.find(({ id }) => id == audits[0]?.user_id)?.full_name}{" "}
                created this{" "}
                <Box as="span" textTransform="capitalize">
                  {timeline_entry_type}
                </Box>
              </Text>
            </Stack>
          </ModalBody>

          <ModalFooter>
            <Button onClick={onClose} variant="outline">
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
