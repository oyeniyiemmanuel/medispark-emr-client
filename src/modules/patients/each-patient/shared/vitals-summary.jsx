import {
  Avatar,
  Box,
  Divider,
  Flex,
  Heading,
  HStack,
  Stack,
  Text,
} from "@chakra-ui/react";
import React from "react";

export default function VitalsSummary({ ...rest }) {
  return (
    <Stack spacing={5} {...rest}>
      <Flex>
        <Heading color="gray.500" fontSize="lg">
          Vitals Summary
        </Heading>
      </Flex>
      <Stack direction="row" spacing={12}>
        <HStack fontSize="sm">
          <Avatar
            size="md"
            name="Kent Dodds"
            src="https://bit.ly/kent-c-dodds"
          />
          <Box color="gray.500">
            <Text>Rita Dominic</Text>
            <Text>Age 47</Text>
          </Box>
        </HStack>
        <Box color="gray.500">
          <Text fontWeight="bold" color="gray.600">
            B/P
          </Text>
          <Text>180/97</Text>
        </Box>
        <Box color="gray.500">
          <Text fontWeight="bold" color="gray.600">
            Weight
          </Text>
          <Text>80kg</Text>
        </Box>
      </Stack>
      <Divider borderBottomColor="gray.400" />
    </Stack>
  );
}
