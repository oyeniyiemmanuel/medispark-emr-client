import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  HStack,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import React from "react";
import { useSelector } from "react-redux";

export default function PatientCardInformation({ ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  return (
    <Box bg="white" color="gray.500" m={4} {...rest}>
      <Accordion border="none" allowMultiple>
        <AccordionItem border="none">
          <AccordionButton>
            <Box flex="1" textAlign="left">
              Social Info
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel bg="gray.50" border="none" pb={4}>
            <Stack>
              <HStack fontSize="sm">
                <Text fontWeight="bold">Age</Text> <Text> 47 yrs</Text>
              </HStack>
            </Stack>
          </AccordionPanel>
        </AccordionItem>

        <AccordionItem border="none">
          <AccordionButton>
            <Box flex="1" textAlign="left">
              Medical
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel bg="gray.50" border="none" pb={4}>
            <Stack>
              <HStack fontSize="sm">
                <Text fontWeight="bold">Blood Group</Text> <Text> N/A</Text>
              </HStack>
            </Stack>
          </AccordionPanel>
        </AccordionItem>

        <AccordionItem border="none">
          <AccordionButton>
            <Box flex="1" textAlign="left">
              Program
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel bg="gray.50" border="none" pb={4}>
            <Stack>
              <HStack fontSize="sm">
                <Text fontWeight="bold">Blood Group</Text> <Text> N/A</Text>
              </HStack>
            </Stack>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem border="none">
          <AccordionButton>
            <Box flex="1" textAlign="left">
              Contact
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel bg="gray.50" border="none" pb={4}>
            <Stack>
              <HStack fontSize="sm">
                <Text fontWeight="bold">Blood Group</Text> <Text> N/A</Text>
              </HStack>
            </Stack>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem border="none">
          <AccordionButton>
            <Box flex="1" textAlign="left">
              Other
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel bg="gray.50" border="none" pb={4}>
            <Stack>
              <HStack fontSize="sm">
                <Text fontWeight="bold">Blood Group</Text> <Text> N/A</Text>
              </HStack>
            </Stack>
          </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </Box>
  );
}
