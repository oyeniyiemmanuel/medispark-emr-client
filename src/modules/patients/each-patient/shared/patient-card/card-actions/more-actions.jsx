import {
  Box,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import Router from "next/router";
import React from "react";
import { BiBed } from "react-icons/bi";
import { FaClipboardList, FaHeartbeat } from "react-icons/fa";
import { IoFolderOpen } from "react-icons/io5";
import { MdClose } from "react-icons/md";
import { RiChatNewFill, RiSearchFill } from "react-icons/ri";
import { useDispatch, useSelector } from "react-redux";

export default function MoreActions({ ...rest }) {
  const treatmentOptions = [
    {
      id: 1,
      name: "Prescribe",
      href: "prescribe",
    },
    {
      id: 2,
      name: "Program",
      href: "program",
    },
    {
      id: 3,
      name: "Procedures",
      href: "procedures",
    },
  ];
  return (
    <Stack justify="center" align="center" spacing={1} {...rest}>
      <Menu placement="right-start">
        <MenuButton
          as={IconButton}
          colorScheme="primary"
          variant="ghost"
          fontSize="xl"
          border="1px solid"
          borderColor="primary.300"
          bg="primary.50"
          rounded="full"
          icon={<RiChatNewFill />}
        ></MenuButton>
        <MenuList zIndex={30}>
          {treatmentOptions.map(({ id, name, href }) => (
            <MenuItem
              key={id}
              pl={6}
              onClick={() =>
                Router.push(
                  `/patients/${selectedPatient.id}/new-documentation?type=${href}`
                )
              }
            >
              {name}
            </MenuItem>
          ))}
        </MenuList>
      </Menu>

      <Text color="gray.600" fontWeight="bold" fontSize={12}>
        More
      </Text>
    </Stack>
  );
}
