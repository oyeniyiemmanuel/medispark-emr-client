import {
  Box,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import Router from "next/router";
import React from "react";
import { BiBed } from "react-icons/bi";
import { FaClipboardList, FaHeartbeat } from "react-icons/fa";
import { IoFolderOpen } from "react-icons/io5";
import { MdClose } from "react-icons/md";
import { RiChatNewFill, RiSearchFill } from "react-icons/ri";
import { useDispatch, useSelector } from "react-redux";

export default function InvestigateActions({ ...rest }) {
  return (
    <Stack justify="center" align="center" spacing={1} {...rest}>
      <Menu placement="top-start">
        <MenuButton
          as={IconButton}
          colorScheme="primary"
          variant="ghost"
          fontSize="xl"
          border="1px solid"
          borderColor="primary.300"
          bg="primary.50"
          rounded="full"
          icon={<RiSearchFill />}
        ></MenuButton>
        <MenuList transform="translate(-15px, 75px) !important" zIndex={10}>
          <MenuItem _focus={{ bg: "white" }} _hover={{ bg: "white" }}>
            <Box w="full" p={1}>
              <HStack spacing={8} align="flex-end">
                <IconButton
                  colorScheme="primary"
                  variant="ghost"
                  fontSize="lg"
                  border="1px solid"
                  borderColor="primary.300"
                  bg="primary.50"
                  size="sm"
                  rounded="full"
                  icon={<MdClose />}
                ></IconButton>
                <Stack spacing={4}>
                  <HStack spacing={6}>
                    <Stack justify="center" align="center" spacing={1}>
                      <IconButton
                        colorScheme="primary"
                        variant="ghost"
                        fontSize="xl"
                        border="1px solid"
                        borderColor="primary.300"
                        bg="primary.50"
                        rounded="full"
                        icon={<RiChatNewFill />}
                      ></IconButton>
                      <Text color="gray.600" fontWeight="bold" fontSize={12}>
                        Lab
                      </Text>
                    </Stack>

                    <Stack justify="center" align="center" spacing={1}>
                      <IconButton
                        colorScheme="primary"
                        variant="ghost"
                        fontSize="xl"
                        border="1px solid"
                        borderColor="primary.300"
                        bg="primary.50"
                        rounded="full"
                        icon={<RiChatNewFill />}
                      ></IconButton>
                      <Text color="gray.600" fontWeight="bold" fontSize={12}>
                        Imaging
                      </Text>
                    </Stack>
                  </HStack>
                </Stack>
              </HStack>
            </Box>
          </MenuItem>
        </MenuList>
      </Menu>

      <Text color="gray.600" fontWeight="bold" fontSize={12}>
        Investigate
      </Text>
    </Stack>
  );
}
