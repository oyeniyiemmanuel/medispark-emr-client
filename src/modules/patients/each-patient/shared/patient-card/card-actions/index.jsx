import {
  Box,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import Router from "next/router";
import React from "react";
import { BiBed } from "react-icons/bi";
import { FaClipboardList, FaHeartbeat } from "react-icons/fa";
import { IoFolderOpen } from "react-icons/io5";
import { MdClose } from "react-icons/md";
import { RiChatNewFill, RiSearchFill } from "react-icons/ri";
import { useDispatch, useSelector } from "react-redux";
import {
  closePopup,
  openPopup,
} from "../../../../../../redux/slices/commonSlice";
import PrescribeDrug from "../../../prescriptions/prescribe-drug";
import InvestigateActions from "./investigate-actions";
import MoreActions from "./more-actions";
import TreatActions from "./treat-actions";

export default function PatientCardActions({ ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();

  const { selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const documentationOptions = [
    {
      id: 1,
      name: "New Encounter",
      popupType: "Encounter",
      icon: <IoFolderOpen />,
      colorScheme: "primary",
    },
    {
      id: 2,
      name: "Add Note",
      popupType: "Add New Note",
      icon: <FaClipboardList />,
      colorScheme: "green",
    },
    {
      id: 3,
      name: "Add Vitals",
      popupType: "Routine Vitals",
      icon: <FaHeartbeat />,
      colorScheme: "red",
    },
  ];

  return (
    <Stack
      align="center"
      justifyContent="center"
      spacing={4}
      w="full"
      {...rest}
    >
      <HStack spacing={7} pt={6}>
        {documentationOptions.map(
          ({ id, icon, name, colorScheme, popupType }) => (
            <Stack key={id} justify="center" align="center" spacing={1}>
              <IconButton
                colorScheme={colorScheme}
                variant="ghost"
                bg={`${colorScheme}.50`}
                fontSize="2xl"
                onClick={() =>
                  dispatch(
                    openPopup({
                      popupType,
                    })
                  )
                }
                icon={icon}
              ></IconButton>

              <Text fontWeight="bold" color="primary.500" fontSize={12}>
                {name}
              </Text>
            </Stack>
          )
        )}
      </HStack>

      <HStack spacing={10} pt={2}>
        <InvestigateActions />
        <TreatActions />
        <MoreActions />
      </HStack>
    </Stack>
  );
}
