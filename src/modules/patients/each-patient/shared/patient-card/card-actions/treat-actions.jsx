import {
  Box,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Text,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import Router from "next/router";
import React, { useEffect } from "react";
import { BiBed } from "react-icons/bi";
import { FaClipboardList, FaHeartbeat } from "react-icons/fa";
import { IoFolderOpen } from "react-icons/io5";
import { MdClose } from "react-icons/md";
import { RiChatNewFill, RiSearchFill } from "react-icons/ri";
import { useDispatch, useSelector } from "react-redux";
import { closePopup } from "../../../../../../redux/slices/commonSlice";
import { patientActions } from "../../../../../../redux/slices/patientSlice";
import { prescriptionActions } from "../../../../../../redux/slices/prescriptionSlice";
import { toastError, toastSuccess } from "../../../../../../utils";
import PrescribeDrug from "../../../prescriptions/prescribe-drug";

export default function TreatActions({ ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const prescriptions = useSelector((state) => state.prescriptions);
  const dispatch = useDispatch();
  const toast = useToast();

  useEffect(() => {
    if (
      prescriptions.error &&
      prescriptions.error.errorType === "CREATE_PRESCRIPTION"
    ) {
      // If an prescriptions.error occurs while creating patient,
      toast(toastError(prescriptions.error)); // Display Error Message
      dispatch(prescriptionActions.clearStates()); // Then clear stale prescriptions.error messages
    }
  }, [prescriptions.error]);

  useEffect(() => {
    if (prescriptions.success === "CREATE_PRESCRIPTION") {
      // If patient is created successfully,
      toast(toastSuccess("Prescription sent Successfully!")); // Display Success message
      dispatch(
        patientActions.addToPatientTimeline({
          // add the prescription to the treatment timeline
          activity: prescriptions.newestPrescription,
          type: "treatments",
        })
      );
      dispatch(
        patientActions.addToPatientTimeline({
          // add the prescription to the bills timeline
          activity: prescriptions.newestPrescription,
          type: "bills",
        })
      );
      dispatch(prescriptionActions.clearStates()); // Then clear stale error messages
    }
  }, [prescriptions.success]);

  return (
    <Stack justify="center" align="center" spacing={1} {...rest}>
      <PrescribeDrug isOpen={isOpen} onOpen={onOpen} onClose={onClose} />
      <Menu placement="top-start">
        <MenuButton
          as={IconButton}
          colorScheme="primary"
          variant="ghost"
          fontSize="xl"
          border="1px solid"
          borderColor="primary.300"
          bg="primary.50"
          rounded="full"
          icon={<BiBed />}
        ></MenuButton>
        <MenuList transform="translate(-15px, 75px) !important" zIndex={10}>
          <MenuItem _focus={{ bg: "white" }} _hover={{ bg: "white" }}>
            <Box w="full" p={1}>
              <HStack spacing={8} align="flex-end">
                <IconButton
                  colorScheme="primary"
                  variant="ghost"
                  fontSize="lg"
                  border="1px solid"
                  borderColor="primary.300"
                  bg="primary.50"
                  size="sm"
                  rounded="full"
                  icon={<MdClose />}
                ></IconButton>
                <Stack spacing={4}>
                  <HStack spacing={6}>
                    <Stack
                      onClick={() => {
                        dispatch(closePopup());
                        onOpen();
                      }}
                      justify="center"
                      align="center"
                      spacing={1}
                    >
                      <IconButton
                        colorScheme="primary"
                        variant="ghost"
                        fontSize="xl"
                        border="1px solid"
                        borderColor="primary.300"
                        bg="primary.50"
                        rounded="full"
                        icon={<RiChatNewFill />}
                      ></IconButton>
                      <Text color="gray.600" fontWeight="bold" fontSize={12}>
                        Prescribe
                      </Text>
                    </Stack>

                    <Stack justify="center" align="center" spacing={1}>
                      <IconButton
                        colorScheme="primary"
                        variant="ghost"
                        fontSize="xl"
                        border="1px solid"
                        borderColor="primary.300"
                        bg="primary.50"
                        rounded="full"
                        icon={<RiChatNewFill />}
                      ></IconButton>
                      <Text color="gray.600" fontWeight="bold" fontSize={12}>
                        Procedure
                      </Text>
                    </Stack>
                  </HStack>
                  <HStack spacing={6}>
                    <Stack justify="center" align="center" spacing={1}>
                      <IconButton
                        colorScheme="primary"
                        variant="ghost"
                        fontSize="xl"
                        border="1px solid"
                        borderColor="primary.300"
                        bg="primary.50"
                        rounded="full"
                        icon={<RiChatNewFill />}
                      ></IconButton>
                      <Text color="gray.600" fontWeight="bold" fontSize={12}>
                        Program
                      </Text>
                    </Stack>
                    <Stack justify="center" align="center" spacing={1}>
                      <IconButton
                        colorScheme="primary"
                        variant="ghost"
                        fontSize="xl"
                        border="1px solid"
                        borderColor="primary.300"
                        bg="primary.50"
                        rounded="full"
                        icon={<RiChatNewFill />}
                      ></IconButton>
                      <Text color="gray.600" fontWeight="bold" fontSize={12}>
                        Immunize
                      </Text>
                    </Stack>
                  </HStack>
                </Stack>
              </HStack>
            </Box>
          </MenuItem>
        </MenuList>
      </Menu>

      <Text color="gray.600" fontWeight="bold" fontSize={12}>
        Treat
      </Text>
    </Stack>
  );
}
