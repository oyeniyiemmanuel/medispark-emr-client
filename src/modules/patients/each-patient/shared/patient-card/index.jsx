import {
  Avatar,
  Box,
  Button,
  Center,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Text,
} from "@chakra-ui/react";
import "moment-timezone";
import Router from "next/router";
import React, { useState } from "react";
import { BiCalendarCheck, BiCalendarEvent } from "react-icons/bi";
import { IoIosCalendar } from "react-icons/io";
import { MdEdit } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import ReactTooltip from "react-tooltip";
import { openAppointmentModal } from "../../../../../redux/slices/commonSlice";
import { calculateAge, separateWithComma } from "../../../../../utils";
import SendToDoctor from "../../send-to-doctor";
import PatientCardActions from "./card-actions";

export default function PatientCard({ ...rest }) {
  const { selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const { userDetails } = useSelector((state) => state.user);
  const { retainers } = useSelector((state) => state.retainers);
  const [showMenu, setShowMenu] = useState(false);
  const dispatch = useDispatch();
  // const { isOpen, onOpen, onClose } = useDisclosure();

  const showMainButtonTextByRole = () => {
    if (userDetails?.roles) {
      switch (userDetails?.roles[0]) {
        case "billing":
          return { text: "Receive Payment", action: () => {} };

        case "front-desk":
          return {
            text: "Check-In",
            action: () => dispatch(openAppointmentModal({ checkIn: true })),
          };

        case "doctor":
          return {
            text: "New Encounter",
            action: () =>
              Router.push(`/patients/${selectedPatient.id}/new-encounter`),
          };

        default:
          return {
            text: "Check-In",
            action: () =>
              dispatch(
                openAppointmentModal({
                  checkIn: true,
                })
              ),
          };
      }
    }
  };

  return (
    <>
      <Box
        border="1px solid lightgrey"
        position="sticky"
        top="80px"
        onMouseEnter={() => setShowMenu(true)}
        onMouseLeave={() => setShowMenu(false)}
        // onFocus={() => setShowMenu(true)}
        // onBlur={() => setShowMenu(false)}
        bg="white"
        w="300px"
        {...rest}
      >
        {/* <PrescribeDrug isOpen={isOpen} onOpen={onOpen} onClose={onClose} /> */}
        <Flex minH={12} p={4} pos="relative" w="full" justify="space-between">
          <Flex>
            {showMenu && (
              <>
                <ReactTooltip
                  place="bottom"
                  type="dark"
                  effect="solid"
                  id="main"
                />
                <a data-tip="Edit Profile" data-for="main">
                  <IconButton
                    fontSize="xl"
                    colorScheme="primary"
                    variant="ghost"
                    icon={<MdEdit />}
                    onClick={() =>
                      Router.push(`/patients/${selectedPatient?.id}/profile`)
                    }
                  />
                </a>
              </>
            )}
          </Flex>

          <Menu placement="bottom-start">
            <MenuButton
              fontSize="xl"
              as={IconButton}
              colorScheme="primary"
              variant="ghost"
              icon={<IoIosCalendar />}
            ></MenuButton>
            <MenuList zIndex={20}>
              <MenuItem
                as={Button}
                variant="ghost"
                color="gray.500"
                leftIcon={<BiCalendarCheck />}
                onClick={() =>
                  dispatch(openAppointmentModal({ checkIn: true }))
                }
              >
                Check In &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              </MenuItem>

              <MenuItem
                as={Button}
                variant="ghost"
                color="gray.500"
                leftIcon={<BiCalendarEvent />}
                onClick={() =>
                  dispatch(openAppointmentModal({ checkIn: false }))
                }
              >
                Appointment
              </MenuItem>
            </MenuList>
          </Menu>
        </Flex>

        <HStack w="full">
          <Center w="full" borderBottomRadius={16} pb={8} pt={2}>
            <Stack align="center" justify="center">
              {/* <Text fontSize="sm"  color="gray.600">
                Last seen&nbsp;
                <Box as="span" fontWeight="bold">
                  24th Jan, 2021
                </Box>
              </Text> */}
              <Avatar
                size="2xl"
                name={`${selectedPatient?.first_name} ${selectedPatient?.last_name}`}
                src={selectedPatient?.avatar?.file_path}
                // onClick={onOpen}
              >
                {/* <AvatarBadge
                  boxSize={5}
                  top={4}
                  borderWidth={1}
                  bg="green.500"
                /> */}
              </Avatar>
              <HStack>
                <Heading fontSize="xl" color="gray.600">
                  {`${selectedPatient?.first_name} ${selectedPatient?.last_name}`}
                </Heading>
              </HStack>
              <Stack align="center" justify="center" spacing={0}>
                <Text fontSize="sm" color="gray.600">
                  {selectedPatient?.dob && (
                    <>{calculateAge(selectedPatient?.dob)} years old,&nbsp;</>
                  )}
                  <Box as="span" textTransform="capitalize">
                    {selectedPatient?.gender}
                  </Box>
                </Text>
                <Text fontSize="sm" color="primary.500">
                  {
                    retainers.find(
                      ({ id }) => id === selectedPatient.retainer_id
                    )?.name
                  }
                </Text>
              </Stack>
              <HStack pt={2} color="gray.600" align="center" spacing={4}>
                {/* <Stack textAlign="right" spacing={0}>
                  <Text fontWeight="bold" fontSize="lg">
                    50%
                  </Text>
                  <Text fontSize="sm">Progress</Text>
                </Stack>
                <Divider
                  h={50}
                  borderLeftColor="gray.600"
                  borderLeftWidth={2}
                  orientation="vertical"
                /> */}
                <HStack
                  onClick={() =>
                    Router.push(
                      `/patients/${selectedPatient.id}?activity=bills`
                    )
                  }
                  cursor="pointer"
                >
                  <Text fontWeight="bold" fontSize="xl">
                    ₦{separateWithComma(selectedPatient.total_balance)}
                  </Text>
                  <Text
                    color={
                      selectedPatient.total_balance < 0
                        ? "red.500"
                        : "green.500"
                    }
                    fontSize="sm"
                  >
                    {selectedPatient.total_balance < 0 ? "Debit" : "Credit"}
                  </Text>
                </HStack>

                {/* <Divider
                  h={50}
                  borderLeftColor="gray.600"
                  borderLeftWidth={2}
                  orientation="vertical"
                />
                <Stack textAlign="right" spacing={0}>
                  <Text fontWeight="bold" fontSize="xl">
                    45
                  </Text>
                  <Text fontSize="sm">Visits</Text>
                </Stack> */}
              </HStack>

              <PatientCardActions />
            </Stack>
          </Center>
        </HStack>
        {/* <PatientCardInformation /> */}
        {/* <Flex pt={4} justify="center">
          <Text fontSize="sm" color="gray.600">
            Last Edited by{" "}
            <Box as="span" fontWeight="bold">
              {" "}
              DR SID
            </Box>
          </Text>
        </Flex> */}
      </Box>

      <SendToDoctor />
    </>
  );
}
