import { IconButton } from "@chakra-ui/button";
import { HStack } from "@chakra-ui/layout";
import React from "react";
import { FaRegFileAudio } from "react-icons/fa";
import ViewDocuments from "./view-documents";
import ViewImages from "./view-images";

export default function ViewUploadInActivities({
  files,
  timeline_entry_type,
  created_at,
  ...rest
}) {
  return (
    <HStack spacing={0} {...rest}>
      {files?.find(
        ({ path, file_path }) =>
          (path || file_path).match(/\.(pdf|docx|.xlsx|txt)$/) != null
      ) && (
        <ViewDocuments
          documents={files.filter(
            ({ path, file_path }) =>
              (path || file_path).match(/\.(pdf|docx|.xlsx|txt)$/) != null
          )}
          timeline_entry_type={timeline_entry_type}
          created_at={created_at}
        />
      )}
      {files?.find(
        ({ path, file_path }) =>
          (path || file_path).match(/\.(jpeg|jpg|gif|png)$/) != null
      ) && (
        <ViewImages
          images={files.filter(
            ({ path, file_path }) =>
              (path || file_path).match(/\.(jpeg|jpg|gif|png)$/) != null
          )}
          timeline_entry_type={timeline_entry_type}
          created_at={created_at}
        />
      )}
      {files?.find(
        ({ path, file_path }) =>
          (path || file_path).match(/\.(mp3|mp4|aac|wma|flac)$/) != null
      ) && (
        <IconButton
          pos="initial"
          size="sm"
          variant="ghost"
          icon={<FaRegFileAudio />}
        />
      )}
    </HStack>
  );
}
