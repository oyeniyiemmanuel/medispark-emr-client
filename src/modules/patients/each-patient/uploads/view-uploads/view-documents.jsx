import { Button, IconButton } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Box } from "@chakra-ui/layout";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import "moment-timezone";
import React, { useRef } from "react";
import { MdFileDownload } from "react-icons/md";
import { VscFilePdf } from "react-icons/vsc";
import Moment from "react-moment";
import ReactTooltip from "react-tooltip";
import { wordTruncate } from "../../../../../utils";

export default function ViewDocuments({
  documents,
  timeline_entry_type,
  created_at,
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const galleryRef = useRef(null);

  return (
    <>
      <ReactTooltip place="bottom" type="dark" effect="solid" id="main" />
      <a data-tip="Document" data-for="main">
        <IconButton
          pos="initial"
          onClick={onOpen}
          size="sm"
          variant="ghost"
          icon={<VscFilePdf />}
        />
      </a>

      <Modal isCentered size="lg" isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            <Box as="span" textTransform="capitalize">
              {timeline_entry_type}{" "}
            </Box>
            on{" "}
            <Moment format="MMMM D, YYYY ">
              {new Date(created_at).toDateString()}
            </Moment>{" "}
            at <Moment format="hh:mm a">{new Date(created_at)}</Moment>
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            {documents.map(({ path, file_path }) => (
              <Button
                // as="a"
                // download
                // href={(path || file_path)?.replace(
                //   "/upload/",
                //   "/upload/fl_attachment/"
                // )}
                onClick={() => {
                  window.open(
                    (path || file_path)?.replace(
                      "/upload/",
                      "/upload/fl_attachment/"
                    ),
                    "_blank"
                  );
                }}
                rightIcon={<MdFileDownload />}
                color="gray.600"
                variant="outline"
                size="lg"
                leftIcon={<VscFilePdf />}
              >
                {wordTruncate(path || file_path, 38)}
              </Button>
            ))}
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
