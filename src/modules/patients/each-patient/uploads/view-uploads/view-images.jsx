import { Button, IconButton } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Box } from "@chakra-ui/layout";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import "moment-timezone";
import React, { useRef } from "react";
import { MdFileDownload, MdImage } from "react-icons/md";
import ImageGallery from "react-image-gallery";
import Moment from "react-moment";
import ReactTooltip from "react-tooltip";

export default function ViewImages({
  images,
  timeline_entry_type,
  created_at,
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const galleryRef = useRef(null);

  return (
    <>
      <ReactTooltip place="bottom" type="dark" effect="solid" id="main" />
      <a data-tip="Image" data-for="main">
        <IconButton
          pos="initial"
          size="sm"
          variant="ghost"
          onClick={onOpen}
          icon={<MdImage />}
        />
      </a>

      <Modal isCentered size="lg" isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            <Box as="span" textTransform="capitalize">
              {timeline_entry_type}{" "}
            </Box>
            on{" "}
            <Moment format="MMMM D, YYYY ">
              {new Date(created_at).toDateString()}
            </Moment>{" "}
            at <Moment format="hh:mm a">{new Date(created_at)}</Moment>
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <ImageGallery
              useBrowserFullscreen={false}
              showThumbnails={false}
              showIndex
              ref={galleryRef}
              showBullets
              items={images.map(({ path, file_path }) => ({
                original: path || file_path,
                // description: path || file_path,
                thumbnail: path || file_path,
              }))}
            />
          </ModalBody>

          <ModalFooter>
            <Button
              onClick={() => {
                const imageToDownload =
                  images[galleryRef?.current?.getCurrentIndex()];

                window.open(
                  (
                    imageToDownload?.path || imageToDownload?.file_path
                  )?.replace("/upload/", "/upload/fl_attachment/"),
                  "_blank"
                );
              }}
              leftIcon={<MdFileDownload />}
              variant="outline"
            >
              Download
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
