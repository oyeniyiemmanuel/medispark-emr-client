import { IconButton } from "@chakra-ui/button";
import { CloseButton } from "@chakra-ui/close-button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Flex, HStack, Text } from "@chakra-ui/layout";
import { useToast } from "@chakra-ui/toast";
import React, { useEffect } from "react";
import { CgAttachment } from "react-icons/cg";
import { useDispatch, useSelector } from "react-redux";
import UploadFile from "../../../../components/upload-file";
import {
  commonActions,
  closePopup,
  uploadFile,
} from "../../../../redux/slices/commonSlice";
import { patientActions } from "../../../../redux/slices/patientSlice";
import { getFileIconByType, toastError, toastSuccess } from "../../../../utils";

export default function UploadFileForPatient({
  document,
  setDocument,
  toUploadDocument,
  setToUploadDocument,
  activityType,
  activityCategory,
  activityId,
}) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const common = useSelector((state) => state.common);
  const dispatch = useDispatch();
  const toast = useToast();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);

  useEffect(() => {
    if (toUploadDocument) {
      const formData = new FormData();
      formData.append("organization_id", userDetails.organization_id);
      formData.append("branch_id", userDetails.branch_id);
      formData.append("user_id", selectedPatient?.id);
      if (activityType == "notes") {
        formData.append("note_id", activityId);
      }
      if (activityType == "encounters") {
        formData.append("encounter_id", activityId);
      }
      formData.append("upload_category", activityCategory);
      formData.append("file", document);

      dispatch(uploadFile({ formData, service: "clinic" }));
    }
  }, [toUploadDocument]);

  useEffect(() => {
    if (common.error && common.error.errorType === "UPLOAD_FILE") {
      // If an error occurs while uploading file,
      toast(toastError(common.error)); // Display Error Message
      setToUploadDocument(false);
      dispatch(commonActions.clearStates()); // Then clear stale error messages
    }
  }, [common.error]);

  useEffect(() => {
    if (common.success === "UPLOAD_FILE") {
      toast(toastSuccess("File uploaded successfully!")); // Display Success message
      if (activityType) {
        dispatch(
          patientActions.addEntryToTimelineActivity({
            entry: common.uploadedFile,
            timelineType: activityType,
            entryType: "files",
          })
        );
      }
      setDocument(null);
      dispatch(commonActions.clearStates()); // Then clear stale error messages
      dispatch(closePopup());
    }
  }, [common.success]);

  return (
    <Flex justifyContent="flex-end">
      {document ? (
        <HStack
          _hover={{ bg: "gray.100" }}
          px={2}
          py={1}
          rounded={4}
          cursor="pointer"
          spacing={2}
        >
          <HStack w="full">
            {getFileIconByType(document.type)}
            <Text
              fontSize="sm"
              wordBreak="break-word"
              color="gray.600"
              fontWeight="bold"
            >
              {document.name}
            </Text>
          </HStack>
          <CloseButton onClick={() => setDocument(null)} color="gray.600" />
        </HStack>
      ) : (
        <IconButton
          size="sm"
          fontSize="xl"
          variant="ghost"
          colorScheme="primary"
          rightIcon={<CgAttachment />}
          onClick={onOpen}
        />
      )}
      <UploadFile
        fileToUpload={document}
        setFileToUpload={setDocument}
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
        accept="image/x-png,image/jpg,image/jpeg,image/gif,image/svg,application/pdf,.doc,.docx"
      />
    </Flex>
  );
}
