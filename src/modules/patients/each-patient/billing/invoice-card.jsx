import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Select,
  Input,
  Icon,
  Checkbox,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { RiEditLine, RiFileList2Fill } from "react-icons/ri";
import { BsFillCaretRightFill } from "react-icons/bs";
import DashboardContainer from "../../../../components/layouts/dashboard";
import {
  BiCalendarEvent,
  BiChevronRight,
  BiDotsVerticalRounded,
} from "react-icons/bi";
import Head from "next/head";
import { AiFillCaretDown } from "react-icons/ai";
import { CgNotes } from "react-icons/cg";
import Router from "next/router";
import { formatDateToDDMMYY } from "../../../../utils";
import { MdAddCircle } from "react-icons/md";
import InvoiceModal from "./invoice-modal";

export default function InvoiceCard() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [expandModal, setExpandModal] = useState(false);
  return (
    <Stack
      cursor="pointer"
      onClick={() => setExpandModal(!expandModal)}
      border="1px solid lightgrey"
      w="full"
      bg="white"
      p={4}
      fontSize="sm"
      // h={250}
      color="gray.500"
      spacing={4}
      rounded={8}
    >
      <Flex w="full" justify="space-between" align="center">
        <HStack>
          <Center boxSize={7} bg="green.100" rounded="full">
            <Icon color="green.500" boxSize={4} as={RiFileList2Fill} />
          </Center>
          <Heading color="green.500" fontSize="xl">
            Invoice
          </Heading>
        </HStack>
        <Text color="gray.500">24th January, 2021.</Text>
      </Flex>
      <Stack px={10} justify="space-between" align="center" direction="row">
        <HStack>
          <Center boxSize={10} rounded={4} bg="orange.100" color="orange.500">
            <Text fontSize="sm">Inv.</Text>
          </Center>
          <Text>Invoice 124</Text>
        </HStack>
        <HStack>
          <Avatar
            size="sm"
            name="Kent Dodds"
            src="https://bit.ly/kent-c-dodds"
          />
          <Box>
            <Text>Richard Mofe Damijo</Text>
            <Text fontSize={12}>SELF - SPONSORED</Text>
          </Box>
        </HStack>
      </Stack>
      <Stack>
        <HStack
          w="full"
          py={2}
          px={10}
          borderBottom="1px solid"
          borderTop="1px solid"
          borderColor="gray.300"
          justify="space-between"
        >
          <HStack spacing={6}>
            <Text fontWeight="bold">Total</Text>
            <Text fontWeight="bold" fontSize="lg">
              #130,500
            </Text>
          </HStack>
          <Button
            variant="link"
            size="sm"
            onClick={(event) => {
              event.stopPropagation();
              onOpen();
            }}
            colorScheme="primary"
            rightIcon={<MdAddCircle />}
          >
            View Full Invoice
          </Button>
        </HStack>
        <Stack pl={12} pt={3} w="full">
          <Stack
            // h={expandModal ? 16 : 0}
            // opacity={expandModal ? 1 : 0}
            // transition="0.2s all"
            spacing={3}
            w="full"
            justify="space-between"
          >
            {[0, 1, 2].map((index) => (
              <HStack spacing={4}>
                <Text>1</Text>
                <Checkbox>
                  <Text color="gray.600" fontWeight="bold">
                    Paracetamol
                  </Text>
                  <Text fontSize="sm">2 tablets Once a day</Text>
                </Checkbox>
              </HStack>
            ))}
          </Stack>
          <Flex pt={2} justify="flex-end">
            <Checkbox colorScheme="green" color="green.500" fontSize="sm">
              Payment Received
            </Checkbox>
          </Flex>
        </Stack>
      </Stack>
      <InvoiceModal
        isInvoiceModalOpen={isOpen}
        onInvoiceModalOpen={onOpen}
        onInvoiceModalClose={onClose}
      />
    </Stack>
  );
}
