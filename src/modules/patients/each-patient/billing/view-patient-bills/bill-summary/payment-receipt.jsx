import { Divider, Flex, Heading, HStack, Stack, Text } from "@chakra-ui/layout";
import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/table";
import React from "react";
import { useSelector } from "react-redux";
import { separateWithComma } from "../../../../../../utils";
import Moment from "react-moment";
import "moment-timezone";

const Receipt = React.forwardRef(
  (
    {
      billsToPayFor,
      totalAmountOfBillsToPayFor,
      paymentMethod,
      areDepositRequests,
    },
    ref
  ) => {
    const { organizationDetails } = useSelector((state) => state.organization);
    const { selectedPatient } = useSelector((state) => state.patients);
    const { userDetails } = useSelector((state) => state.user);
    const { bills, newestPaidBill } = useSelector((state) => state.bills);
    const { services } = useSelector((state) => state.services);
    const { staff } = useSelector((state) => state.staff);
    const [newestInvoice] = newestPaidBill.invoices.slice(-1);

    return (
      <div ref={ref} className="print-source">
        <Stack color="gray.600" w="full" align="center" justify="center">
          <Stack w="md" spacing={4} py={12}>
            <Stack spacing={10} align="center" justify="center">
              <HStack>
                <Heading
                  textTransform="uppercase"
                  fontSize="lg"
                  textAlign="center"
                >
                  {organizationDetails.name}
                </Heading>
              </HStack>
              <Stack align="center" justify="center">
                <Text textAlign="center" fontSize="sm">
                  {organizationDetails.street}, {organizationDetails.city},{" "}
                  {organizationDetails.state}
                </Text>
                <Text textAlign="center">{organizationDetails.phone}</Text>
                <Text fontSize={12} textAlign="center">
                  {organizationDetails.website}
                </Text>
              </Stack>
            </Stack>
            <Divider w="full" borderColor="gray.500" />
            <Stack w="full" align="center" justify="center">
              <Text fontSize="sm" textAlign="center">
                {areDepositRequests ? "Deposit" : "Sales"} Receipt
              </Text>
              <Text fontSize="sm" textAlign="center">
                <Moment format="DD/MM/YYYY,">
                  {new Date(newestPaidBill.created_at).toDateString()}
                </Moment>{" "}
                <Moment format="hh:mm a">
                  {new Date(newestPaidBill.created_at)}
                </Moment>
              </Text>
              <Text fontSize="sm" textAlign="center">
                Prepared by:{" "}
                {
                  staff?.find(
                    ({ id }) => id == newestPaidBill.audits[0]?.user_id
                  )?.full_name
                }
              </Text>
            </Stack>
            <Divider w="full" borderColor="gray.500" />
            <Stack w="full" px={8}>
              <Text fontSize="sm" fontWeight="bold">
                Invoiced to:
              </Text>
              <Text fontSize="sm">{selectedPatient.full_name}</Text>
              <Text fontSize="sm">{selectedPatient.phone}</Text>
            </Stack>
            <Divider w="full" borderColor="gray.500" />
            <Stack px={4} align="center">
              {areDepositRequests ? (
                billsToPayFor.map((bill, index) => (
                  <Text w="full" fontSize="sm">
                    {index + 1}. Deposit request for {bill.reason} at{" "}
                    <Moment format="MMMM D, YYYY">
                      {new Date(bill.created_at).toDateString()}
                    </Moment>
                  </Text>
                ))
              ) : (
                <Table variant="unstyled" size="sm">
                  <Thead>
                    <Tr>
                      <Th textTransform="capitalize">Item List</Th>
                      <Th textTransform="capitalize" isNumeric>
                        Qty
                      </Th>
                      <Th textTransform="capitalize" isNumeric>
                        Price
                      </Th>
                    </Tr>
                  </Thead>

                  <Tbody>
                    {billsToPayFor.map((bill) => {
                      const selectedService = services.find(
                        ({ id }) => id == bill.service_id
                      );
                      const prescriptionAmount =
                        selectedService?.prices?.find(
                          ({ retainer_id }) =>
                            retainer_id == selectedPatient.retainer_id
                        )?.amount || 0;
                      return (
                        <Tr key={bill?.id}>
                          <Td>{selectedService.name} </Td>
                          <Td isNumeric>{bill.prescription?.quantity || 1}</Td>
                          <Td isNumeric>
                            ₦
                            {separateWithComma(
                              prescriptionAmount *
                                parseInt(bill.prescription?.quantity || 1)
                            )}
                          </Td>
                        </Tr>
                      );
                    })}
                  </Tbody>
                </Table>
              )}
            </Stack>
            <Divider w="full" borderColor="gray.500" />
            <Stack align="flex-end" justify="flex-end" w="full">
              <HStack align="baseline" spacing={6}>
                <Text fontSize="sm" textAlign="right">
                  Amount Paid
                </Text>
                <Text fontWeight="bold" w="7rem">
                  ₦{separateWithComma(newestPaidBill?.amount)}
                </Text>
              </HStack>
              <HStack align="baseline" spacing={6}>
                <Text fontSize="sm" textAlign="right">
                  Payment Method
                </Text>
                <Text fontWeight="bold" textTransform="capitalize" w="7rem">
                  {newestPaidBill.method?.replace("_", " ")}
                </Text>
              </HStack>
              656
            </Stack>
          </Stack>
        </Stack>
        <style jsx>{`
          .print-source {
            display: none;
          }
          @media print {
            .print-source {
              display: block;
            }
          }
        `}</style>
      </div>
    );
  }
);

export default Receipt;
