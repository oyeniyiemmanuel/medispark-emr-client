import { Button, IconButton } from "@chakra-ui/button";
import { Checkbox } from "@chakra-ui/checkbox";
import {
  Box,
  Divider,
  Flex,
  Heading,
  HStack,
  Stack,
  Text,
} from "@chakra-ui/layout";
import { Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/menu";
import {
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
} from "@chakra-ui/modal";
import { Table, Tbody, Td, Tr } from "@chakra-ui/table";
import React, { useEffect, useState } from "react";
import { FaPrint, FaShare } from "react-icons/fa";
import { MdShare } from "react-icons/md";
import { useSelector } from "react-redux";
import Moment from "react-moment";
import "moment-timezone";
import check from "check-types";
import { BeatLoader } from "react-spinners";
import { separateWithComma } from "../../../../../../utils";
import DisplayPaymentMethodComponent from "./display-payment-option";
import { Input, InputGroup, InputLeftElement } from "@chakra-ui/input";
import PayBill from "./pay-bill";
import theme from "../../../../../../theme";

export default function BillSummary({
  billsToSummarize,
  setBillsToSummarize,
  isOpen,
  onOpen,
  onClose,
}) {
  const availablePaymentOptions = [
    "wallet",
    "cash",
    "card",
    "bank_transfer",
    "other",
  ];
  const [paymentMethod, setPaymentMethod] = useState("bank_transfer");
  const { selectedPatient, patientTimeline } = useSelector(
    (state) => state.patients
  );
  const { services } = useSelector((state) => state.services);
  const [billsToPayFor, setBillsToPayFor] = useState([]);
  const { selectedInvoice, loading, success, error } = useSelector(
    (state) => state.bills
  );

  useEffect(() => {
    if (!isOpen) {
      setBillsToSummarize([]);
    }
  }, [isOpen]);

  useEffect(() => {
    if (check.nonEmptyArray(billsToSummarize)) {
      setBillsToPayFor(billsToSummarize.map(({ id }) => id));
    }
  }, [billsToSummarize]);

  const addOrRemoveFromBillsToPayFor = (id) => {
    if (billsToPayFor.find((billId) => billId == id)) {
      setBillsToPayFor([...billsToPayFor].filter((billId) => billId != id));
    } else {
      setBillsToPayFor([...billsToPayFor, id]);
    }
  };

  const totalAmountOfBillsToPayFor = () => {
    const totalAmount = billsToPayFor
      .map((billId) => patientTimeline.bills?.find(({ id }) => id == billId))
      .reduce((sum, bill) => {
        const selectedService = services.find(
          ({ id }) => id == bill?.service_id
        );
        const amount =
          selectedService?.prices?.find(
            ({ retainer_id }) => retainer_id == selectedPatient.retainer_id
          )?.amount || 0;

        return sum + amount * parseInt(bill?.prescription?.quantity || 1);
      }, 0);

    return totalAmount;
  };

  useEffect(() => {
    if (success === "FETCH_SELECTED_INVOICE") {
      setBillsToSummarize(
        selectedInvoice.bills.map((invoicedBill) =>
          patientTimeline.bills?.find(({ id }) => id == invoicedBill.id)
        )
      );
    }
  }, [success]);

  const isAlreadyInvoiced = billsToSummarize.find(
    ({ invoice_id }) => !!invoice_id
  );
  const amountToBalanceFromInvoice =
    selectedInvoice.expected_payment - selectedInvoice.payment_made;

  const areDepositRequests = billsToSummarize.every(
    (bill) => bill?.payload_type == "deposit_request"
  );

  return (
    <Drawer onClose={onClose} isOpen={isOpen} size="md">
      <DrawerOverlay>
        <DrawerContent px={3}>
          <DrawerCloseButton top={5} />
          <DrawerHeader pr={12} pt={4} pb={0}>
            <HStack justify="space-between">
              <Box color="gray.600" as="span">
                {areDepositRequests ? "Deposit" : "Bill"} Summary
              </Box>
              <HStack spacing={0}>
                <IconButton
                  variant="default"
                  color="primary.500"
                  icon={<FaPrint />}
                />
                <IconButton
                  variant="default"
                  color="primary.500"
                  icon={<MdShare />}
                />
              </HStack>
            </HStack>
            <Divider mb={2} border="2px solid grey" />
          </DrawerHeader>
          {loading === "FETCH_SELECTED_INVOICE" ? (
            <Flex w="full" justify="center">
              <BeatLoader size={8} color={theme.colors.primary[500]} />
            </Flex>
          ) : (
            <DrawerBody color="gray.500">
              <Stack spacing={6} w="full">
                <DisplayPaymentMethodComponent method={paymentMethod} />
                <Flex>
                  <Menu>
                    <MenuButton
                      size="sm"
                      as={Button}
                      colorScheme="primary"
                      variant="link"
                    >
                      CHANGE PAYMENT METHOD
                    </MenuButton>
                    <MenuList>
                      {availablePaymentOptions.map((option) => (
                        <MenuItem
                          textTransform="capitalize"
                          onClick={() => setPaymentMethod(option)}
                        >
                          {option.replace("_", " ")}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Menu>
                </Flex>
              </Stack>
              <Divider my={4} border="2px solid grey" />
              <Stack spacing={5}>
                <Flex>
                  <Heading fontSize="lg">Service List</Heading>
                </Flex>
                <Table variant="unstyled" size="sm">
                  <Tbody color="gray.500">
                    {billsToSummarize.map((bill) => {
                      const selectedService = services.find(
                        ({ id }) => id == bill.service_id
                      );
                      const prescriptionAmount =
                        selectedService?.prices?.find(
                          ({ retainer_id }) =>
                            retainer_id == selectedPatient.retainer_id
                        )?.amount || 0;

                      const isChecked = Boolean(
                        [...billsToPayFor].find((billId) => billId == bill.id)
                      );

                      return (
                        <Tr
                          textDecoration={
                            isChecked ? "initial" : "line-through"
                          }
                          key={bill.id}
                        >
                          <Td>
                            <Checkbox
                              isDisabled={isAlreadyInvoiced}
                              isChecked={isChecked}
                              onChange={() =>
                                addOrRemoveFromBillsToPayFor(bill.id)
                              }
                            />
                          </Td>
                          <Td>{selectedService?.name || bill.reason}</Td>
                          <Td isNumeric>
                            <Moment format="MMMM D, YYYY">
                              {new Date(bill.created_at).toDateString()}
                            </Moment>
                          </Td>
                          {!areDepositRequests && (
                            <Td fontWeight="bold" isNumeric>
                              ₦
                              {separateWithComma(
                                prescriptionAmount *
                                  parseInt(bill.prescription?.quantity || 1)
                              )}
                            </Td>
                          )}
                        </Tr>
                      );
                    })}
                  </Tbody>
                </Table>
              </Stack>
              <Divider my={4} border="2px solid grey" />
              {!areDepositRequests && (
                <HStack spacing={8} fontWeight="bold" justify="flex-end">
                  <Text>Total</Text>
                  <Text color={isAlreadyInvoiced ? "gray.500" : "green.500"}>
                    ₦{separateWithComma(totalAmountOfBillsToPayFor())}
                  </Text>
                </HStack>
              )}
              {isAlreadyInvoiced && (
                <HStack spacing={8} fontWeight="bold" justify="flex-end">
                  <Text>Outstanding</Text>
                  <Text color="green.500">
                    ₦{separateWithComma(amountToBalanceFromInvoice)}
                  </Text>
                </HStack>
              )}
              <PayBill
                billsToPayFor={billsToPayFor.map((billId) =>
                  [...patientTimeline.bills].find(({ id }) => id == billId)
                )}
                totalAmountOfBillsToPayFor={
                  isAlreadyInvoiced
                    ? amountToBalanceFromInvoice
                    : totalAmountOfBillsToPayFor()
                }
                onCloseBillSummary={onClose}
                paymentMethod={paymentMethod}
                areDepositRequests={areDepositRequests}
              />
            </DrawerBody>
          )}
        </DrawerContent>
      </DrawerOverlay>
    </Drawer>
  );
}
