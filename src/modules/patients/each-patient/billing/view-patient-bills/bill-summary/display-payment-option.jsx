import { HStack, Stack, Text } from "@chakra-ui/layout";
import React from "react";
import { useSelector } from "react-redux";
import { separateWithComma } from "../../../../../../utils";

export const PayWithWallet = () => {
  const { selectedPatient } = useSelector((state) => state.patients);

  return (
    <HStack pt={3} justify="space-between">
      <Stack spacing={0}>
        <Text fontSize="sm">Payment Method</Text>
        <Text color="gray.600">{selectedPatient.first_name}'s Wallet</Text>
      </Stack>
      <Stack spacing={0}>
        <Text fontSize="xl" fontWeight="bold">
          ₦{separateWithComma(selectedPatient.total_balance)}
        </Text>
      </Stack>
    </HStack>
  );
};
export const PayWithCash = () => {
  return (
    <HStack pt={3} justify="space-between">
      <Stack spacing={0}>
        <Text fontSize="sm">Payment Method</Text>
        <Text color="gray.600">Cash</Text>
      </Stack>
    </HStack>
  );
};

export const PayWithCard = () => {
  return (
    <HStack pt={3} justify="space-between">
      <Stack spacing={0}>
        <Text fontSize="sm">Payment Method</Text>
        <Text color="gray.600">Card</Text>
      </Stack>
      <Stack spacing={0}>
        <Text fontSize="lg" fontWeight="bold">
          HOSP NAME
        </Text>
        <Text fontSize="sm" color="gray.600">
          0000000000 - Zenith Bank
        </Text>
      </Stack>
    </HStack>
  );
};

export const PayWithBankTransfer = () => {
  const { organizationDetails } = useSelector((state) => state.organization);

  return (
    <HStack pt={3} justify="space-between">
      <Stack spacing={0}>
        <Text fontSize="sm">Payment Method</Text>
        <Text color="gray.600">Bank Transfer</Text>
      </Stack>
      <Stack spacing={0}>
        <Text textTransform="uppercase" fontSize="lg" fontWeight="bold">
          {organizationDetails.name}
        </Text>
        {organizationDetails.accounts?.map(({ id, name, number }) => (
          <Stack key={id} spacing={0}>
            <Text fontSize="sm" color="gray.600">
              {number} - {name}
            </Text>
          </Stack>
        ))}
      </Stack>
    </HStack>
  );
};
export const PayWithOtherMethod = () => {
  return (
    <HStack pt={3} justify="space-between">
      <Stack spacing={0}>
        <Text fontSize="sm">Payment Method</Text>
        <Text color="gray.600">Other</Text>
      </Stack>
    </HStack>
  );
};

export default function DisplayPaymentOptionComponent({ method }) {
  switch (method) {
    case "wallet":
      return <PayWithWallet />;
    case "cash":
      return <PayWithCash />;
    case "card":
      return <PayWithCard />;
    case "bank_transfer":
      return <PayWithBankTransfer />;
    case "other":
      return <PayWithOtherMethod />;

    default:
      break;
  }
}
