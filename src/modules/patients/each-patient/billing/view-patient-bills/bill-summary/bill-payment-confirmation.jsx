import React, { useEffect, useRef, useState } from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Divider,
  Stack,
  Avatar,
  Heading,
  HStack,
  Text,
  AvatarBadge,
  Flex,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { toastSuccess } from "../../../../../../utils";
import { clearStates } from "../../../../../../redux/slices/billSlice";
import { CheckIcon } from "@chakra-ui/icons";
import ReactToPrint from "react-to-print";
import Receipt from "./payment-receipt";
import { patientActions } from "../../../../../../redux/slices/patientSlice";

export default function BillPaymentConfirmation({
  onClose,
  isOpen,
  onSubmitPayBill,
  onCloseBillSummary,
  billsToPayFor,
  totalAmountOfBillsToPayFor,
  paymentMethod,
  areDepositRequests,
  onOpen,
}) {
  const componentRef = useRef();
  const { selectedPatient } = useSelector((state) => state.patients);
  const { bills, newestPaidBill, loading, success, error } = useSelector(
    (state) => state.bills
  );
  const [paymentConfirmed, setPaymentConfirmed] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (success === "PAY_BILL" || success === "ADD_PAYMENT_TO_INVOICE") {
      // If patient is created successfully,
      setPaymentConfirmed(true);
      if (newestPaidBill.method == "wallet") {
        dispatch(
          patientActions.updatePatientBalance({
            newBalance:
              selectedPatient.total_balance - parseInt(newestPaidBill.amount),
          })
        );
      }
      if (areDepositRequests) {
        dispatch(
          patientActions.updatePatientBalance({
            newBalance:
              selectedPatient.total_balance + parseInt(newestPaidBill.amount),
          })
        );
      }
      const [newestInvoice] = newestPaidBill.invoices.slice(-1);
      dispatch(
        patientActions.updatePatientBillsStatus({
          billsPaidFor: billsToPayFor,
          invoice_id: newestInvoice?.id,
          invoice_details: {
            id: newestInvoice?.id,
            payment_status: newestInvoice?.payment_status || "fully-paid",
            bills_size: billsToPayFor.length,
          },
        })
      );
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [success]);

  useEffect(() => {
    if (!isOpen) {
      setPaymentConfirmed(false);
    }
  }, [isOpen]);

  return (
    <Modal size="xl" onClose={onClose} isOpen={isOpen} isCentered>
      <ModalOverlay />
      {!paymentConfirmed && (
        <ModalContent>
          <ModalHeader>
            Confirm Payment
            <Divider mt={2} border="2px solid grey" />
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody color="gray.600">
            You are about to record a{" "}
            {areDepositRequests ? "deposit" : "payment"} for{" "}
            {selectedPatient.full_name}. Or some other copy that we will refine
            and fix here
          </ModalBody>
          <ModalFooter justifyContent="flex-end">
            <Button mr={3} onClick={onClose}>
              Cancel
            </Button>
            <Button
              isLoading={
                loading == "PAY_BILL" || loading == "ADD_PAYMENT_TO_INVOICE"
              }
              loadingText="Paying"
              colorScheme="primary"
              onClick={onSubmitPayBill}
            >
              Record Payment
            </Button>
          </ModalFooter>
        </ModalContent>
      )}

      {paymentConfirmed && (
        <ModalContent>
          <ModalBody>
            <Stack pt={8} pb={3} spacing={4} justify="center" align="center">
              <Avatar
                size="xl"
                name={selectedPatient.full_name}
                src={selectedPatient.avatar?.file_path}
              >
                <AvatarBadge
                  top={-3}
                  boxSize="1.0em"
                  borderWidth={0}
                  bg="green.500"
                >
                  <CheckIcon fontSize={12} boxSize={4} color="white" />
                </AvatarBadge>
              </Avatar>
              <Heading
                maxW="sm"
                color="gray.600"
                fontSize="xl"
                textAlign="center"
              >
                Payment successfully made for {selectedPatient.full_name}
              </Heading>
            </Stack>
          </ModalBody>
          <ModalFooter
            bg="gray.100"
            flexDir="column"
            justifyContent="center"
            alignItems="center"
          >
            <Text color="gray.500" maxW="md" textAlign="center">
              You have successfully made{" "}
              {areDepositRequests ? "a deposit" : "payment"} for{" "}
              {selectedPatient.full_name}
            </Text>
            <HStack mt={6}>
              <ReactToPrint
                trigger={() => (
                  <Button
                    colorScheme="primary"
                    variant="link"
                    color="primary.600"
                    // onClick={onPrintReciept}
                  >
                    Print Receipt
                  </Button>
                )}
                content={() => componentRef.current}
              />

              <Button
                mr={3}
                color="gray.600"
                onClick={() => {
                  onClose();
                  onCloseBillSummary();
                }}
              >
                Close
              </Button>
            </HStack>
            <Receipt
              ref={componentRef}
              billsToPayFor={billsToPayFor}
              totalAmountOfBillsToPayFor={totalAmountOfBillsToPayFor}
              paymentMethod={paymentMethod}
              areDepositRequests={areDepositRequests}
            />
          </ModalFooter>
        </ModalContent>
      )}
    </Modal>
  );
}
