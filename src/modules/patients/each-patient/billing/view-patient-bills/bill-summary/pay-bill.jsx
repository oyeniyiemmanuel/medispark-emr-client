import { Button } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Input, InputGroup, InputLeftElement } from "@chakra-ui/input";
import { Box, HStack, Stack, Text } from "@chakra-ui/layout";
import { useToast } from "@chakra-ui/toast";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addPaymentToInvoice,
  clearStates,
  payBill,
} from "../../../../../../redux/slices/billSlice";
import { separateWithComma, toastError } from "../../../../../../utils";
import BillPaymentConfirmation from "./bill-payment-confirmation";

export default function PayBill({
  billsToPayFor,
  totalAmountOfBillsToPayFor,
  onCloseBillSummary,
  paymentMethod,
  areDepositRequests,
}) {
  const [amountPaid, setAmountPaid] = useState("");
  const [comment, setComment] = useState("");
  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();
  const { selectedInvoice, error } = useSelector((state) => state.bills);
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient } = useSelector((state) => state.patients);

  const amountOutstanding = areDepositRequests
    ? 0
    : totalAmountOfBillsToPayFor - parseInt(amountPaid);
  const isAlreadyInvoiced = billsToPayFor.find(
    ({ invoice_id }) => !!invoice_id
  );

  const onSubmitPayBill = (event) => {
    event.preventDefault();
    const defaultPaymentPayload = {
      organization_id: userDetails.organization_id,
      branch_id: userDetails.branch_id,
      stakeholder: "user",
      payment_category: areDepositRequests ? "deposit" : "invoice",
      payment_method: paymentMethod,
      amount_paid: parseInt(amountPaid),
      user: {
        id: selectedPatient.id,
        retainer_id: selectedPatient.retainer_id,
      },
      comment,
    };

    if (isAlreadyInvoiced) {
      dispatch(
        addPaymentToInvoice({
          ...defaultPaymentPayload,
          invoice_id: selectedInvoice.id,
        })
      );
    } else if (areDepositRequests) {
      dispatch(
        payBill({
          ...defaultPaymentPayload,
          deposit_request_id: billsToPayFor[0]?.id,
        })
      );
    } else {
      dispatch(
        payBill({
          ...defaultPaymentPayload,
          services_rendered: billsToPayFor.map(
            ({ id, price, prescription }) => ({
              service_user_id: id,
              price: price * parseInt(prescription?.quantity || 1),
            })
          ),
        })
      );
    }
  };

  useEffect(() => {
    if (
      error &&
      (error.errorType === "PAY_BILL" ||
        error.errorType === "ADD_PAYMENT_TO_INVOICE")
    ) {
      // If an error occurs while creating patient,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  const insufficientWalletBalance =
    paymentMethod == "wallet" &&
    amountPaid &&
    amountPaid > parseInt(selectedPatient?.total_balance) &&
    !(amountOutstanding < 0);

  return (
    <Stack
      onSubmit={(event) => {
        event.preventDefault();
        onOpen();
      }}
      as="form"
    >
      <Stack>
        <Text>Amount Paid</Text>
        <InputGroup size="lg" w="full">
          <InputLeftElement
            pointerEvents="none"
            color="gray.300"
            fontSize="1.2em"
            children="₦"
          />
          <Input
            size="lg"
            isRequired
            value={amountPaid}
            onChange={(event) => setAmountPaid(event.target.value)}
            variant="filled"
            min={1}
            max={!areDepositRequests ? totalAmountOfBillsToPayFor : undefined}
            type="number"
            placeholder="Enter amount"
          />
        </InputGroup>
        <Box>
          {!insufficientWalletBalance && amountOutstanding > 0 && (
            <Text fontSize="sm" textAlign="right">
              <Box as="span" color="red.600">
                Outstanding:
              </Box>{" "}
              ₦{separateWithComma(amountOutstanding)}
            </Text>
          )}
          {amountOutstanding < 0 && (
            <Text fontSize="sm" textAlign="right">
              <Box as="span" color="orange.600">
                You can not pay more than ₦
                {separateWithComma(totalAmountOfBillsToPayFor)}
              </Box>
            </Text>
          )}
          {insufficientWalletBalance && (
            <Text fontSize="sm" textAlign="right">
              <Box as="span" color="orange.600">
                The patient does not have up to ₦{separateWithComma(amountPaid)}{" "}
                in the wallet.
              </Box>
            </Text>
          )}
        </Box>
      </Stack>

      <Stack>
        <Text>Comment</Text>
        <Input
          value={comment}
          onChange={(event) => setComment(event.target.value)}
          size="lg"
          variant="filled"
          placeholder="Add Comment"
        />
      </Stack>

      <HStack spacing={8} pt={4} pb={8} fontWeight="bold" justify="flex-end">
        <Button
          isDisabled={insufficientWalletBalance || amountOutstanding < 0}
          w={40}
          type="submit"
          colorScheme="primary"
        >
          Take Payment
        </Button>
      </HStack>
      <BillPaymentConfirmation
        onSubmitPayBill={onSubmitPayBill}
        onCloseBillSummary={onCloseBillSummary}
        billsToPayFor={billsToPayFor}
        totalAmountOfBillsToPayFor={totalAmountOfBillsToPayFor}
        paymentMethod={paymentMethod}
        areDepositRequests={areDepositRequests}
        isOpen={isOpen}
        onClose={onClose}
        onOpen={onOpen}
      />
    </Stack>
  );
}
