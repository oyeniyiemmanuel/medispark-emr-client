import {
  Badge,
  Box,
  Button,
  Center,
  Checkbox,
  Flex,
  Heading,
  HStack,
  Icon,
  Stack,
  Table,
  Tbody,
  Td,
  Text,
  Tr,
} from "@chakra-ui/react";
import "moment-timezone";
import React, { useState } from "react";
import { CgPlayListCheck } from "react-icons/cg";
import { RiFileList2Fill } from "react-icons/ri";
import Moment from "react-moment";
import { useDispatch, useSelector } from "react-redux";
import { fetchSelectedInvoice } from "../../../../../redux/slices/billSlice";
import { separateWithComma } from "../../../../../utils";

export default function BillCard({
  setBillsToSummarize,
  addOrRemoveFromSelectedActivities,
  anySelectedBills,
  isChecked,
  bill,
  onOpen,
}) {
  const [showMenu, setShowMenu] = useState(false);
  const {
    id,
    service_id,
    encounter_id,
    created_at,
    invoice_details,
    payload_type,
    reason,
    prescription,
    audits,
  } = bill;

  const { staff } = useSelector((state) => state.staff);
  const { services } = useSelector((state) => state.services);
  const { selectedPatient } = useSelector((state) => state.patients);
  const dispatch = useDispatch();

  const selectedService = services.find(({ id }) => id == service_id);
  const prescriptionAmount =
    selectedService?.prices?.find(
      ({ retainer_id }) => retainer_id == selectedPatient.retainer_id
    )?.amount || 0;

  const processBill = () => {
    if (bill?.invoice_id) {
      dispatch(
        fetchSelectedInvoice({
          invoice_id: bill?.invoice_id,
        })
      );
    } else {
      setBillsToSummarize([bill]);
    }
    onOpen();
  };

  const isDepositRequest = payload_type == "deposit_request";

  return (
    <HStack align="start">
      <Checkbox
        mt={4}
        size="lg"
        pos="initial"
        borderColor="gray.400"
        isChecked={isChecked}
        value={id}
        onChange={addOrRemoveFromSelectedActivities}
      />
      <Stack
        onMouseEnter={() => setShowMenu(true)}
        onMouseLeave={() => setShowMenu(false)}
        cursor="pointer"
        w="full"
        bg="white"
        border="2px solid"
        borderColor="green.200"
        p={4}
        fontSize="sm"
        // h={250}
        pt={2}
        color="gray.500"
        spacing={2}
        rounded={8}
      >
        <Flex
          minH={12}
          borderBottom="1px solid lightgrey"
          w="full"
          justify="space-between"
          align="center"
        >
          <HStack>
            <HStack>
              <Center boxSize={7} bg="green.100" rounded="full">
                <Icon color="green.500" boxSize={4} as={RiFileList2Fill} />
              </Center>
              <Heading color="green.500" fontSize="xl">
                {isDepositRequest ? "Deposit Request" : "Bill"}
              </Heading>
            </HStack>
            <HStack>
              <Text fontWeight="bold" color="gray.500">
                By{" "}
                {staff?.find(({ id }) => id == audits[0]?.user_id)?.full_name}
              </Text>{" "}
              <Text> | </Text>
              <Text color="gray.500">
                <Moment format="MMMM D, YYYY,">
                  {new Date(created_at).toDateString()}
                </Moment>
              </Text>
              <Text>
                <Moment format="hh:mm a">{new Date(created_at)}</Moment>
              </Text>
              {encounter_id && (
                <>
                  &nbsp;in{" "}
                  <Box fontWeight="bold" as="span" color="green.500">
                    Encounter
                  </Box>
                </>
              )}
            </HStack>
          </HStack>
        </Flex>

        <Stack>
          <Stack px={4} spacing={3} w="full">
            <Stack spacing={3}>
              <Stack>
                {prescription ? (
                  <Table variant="unstyled" size="sm">
                    <Tbody color="gray.500">
                      <Tr>
                        <Td>
                          {selectedService?.name}{" "}
                          {selectedService?.strength_value}{" "}
                          {selectedService?.strength_unit_type}
                        </Td>
                        <Td isNumeric>
                          {separateWithComma(prescription?.quantity)}{" "}
                          {prescription?.stock_unit_type}
                        </Td>
                        <Td>{prescription?.instruction}</Td>
                        <Td isNumeric>
                          ₦
                          {separateWithComma(
                            prescriptionAmount *
                              parseInt(prescription?.quantity)
                          )}
                        </Td>
                      </Tr>
                    </Tbody>
                  </Table>
                ) : isDepositRequest ? (
                  <Text>
                    {selectedPatient.full_name} requested for a deposit for{" "}
                    <Box textTransform="capitalize" as="span" fontWeight="bold">
                      {reason}
                    </Box>
                  </Text>
                ) : (
                  <Text>
                    {selectedPatient.full_name} was checked in for service named{" "}
                    <Box textTransform="capitalize" as="span" fontWeight="bold">
                      {selectedService?.name}
                    </Box>
                  </Text>
                )}
              </Stack>
              {!isDepositRequest && (
                <HStack px={8} justify="flex-end">
                  <Text fontWeight="bold">
                    Total &nbsp; ₦
                    {separateWithComma(
                      prescriptionAmount * parseInt(prescription?.quantity || 1)
                    )}
                  </Text>
                </HStack>
              )}
            </Stack>
          </Stack>
        </Stack>

        <HStack pt={2} borderTop="1px solid lightgrey" justify="space-between">
          <Flex></Flex>
          <HStack spacing={6}>
            {(invoice_details?.payment_status === "fully-paid" ||
              Boolean(bill.paid_status)) && (
              <Badge
                px={2}
                py={1}
                textTransform="initial"
                rounded="full"
                size="xs"
                colorScheme="green"
              >
                Paid
              </Badge>
            )}
            {invoice_details?.payment_status === "batch" && (
              <Badge
                px={2}
                py={1}
                textTransform="initial"
                rounded="full"
                size="xs"
                colorScheme="primary"
              >
                Batched
              </Badge>
            )}
            {invoice_details?.payment_status === "partly-paid" && (
              <Badge
                px={2}
                py={1}
                textTransform="initial"
                rounded="full"
                size="xs"
                colorScheme="yellow"
              >
                {invoice_details?.bills_size > 1 ? "Batched" : "Partly Paid"}
              </Badge>
            )}
            {onOpen &&
              !anySelectedBills &&
              invoice_details?.payment_status !== "fully-paid" &&
              !Boolean(bill.paid_status) && (
                // invoice + !paid_status = normal bill + deposit - show
                // paid_status + !invoice = deposit + invoice - show
                // !invoice + !paid_status - hide

                <Button
                  leftIcon={<CgPlayListCheck />}
                  size="sm"
                  variant="solid"
                  colorScheme="green"
                  onClick={processBill}
                >
                  {isDepositRequest
                    ? "Take Deposit"
                    : `Record ${
                        invoice_details?.bills_size > 1 ? "Batched" : ""
                      } Payment`}
                </Button>
              )}
          </HStack>
        </HStack>
      </Stack>
    </HStack>
  );
}
