import { Button, IconButton } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Input } from "@chakra-ui/input";
import { Divider, Flex, HStack, Stack, Text } from "@chakra-ui/layout";
import check from "check-types";
import React, { useEffect, useState } from "react";
import { AiFillCaretDown } from "react-icons/ai";
import { BiCalendarEvent, BiDotsVerticalRounded } from "react-icons/bi";
import { BsGrid1X2Fill } from "react-icons/bs";
import { MdDelete, MdFormatListBulleted } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { fetchPatientTimeline } from "../../../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../../../redux/slices/retainerSlice";
import { fetchServices } from "../../../../../redux/slices/serviceSlice";
import { fetchStaff } from "../../../../../redux/slices/staffSlice";
import BillCard from "./bill-card";
import FilterResults from "react-filter-search";
import { Checkbox } from "@chakra-ui/checkbox";
import { CgPlayListCheck } from "react-icons/cg";
import { VscCheck } from "react-icons/vsc";
import BillSummary from "./bill-summary";

export default function ViewBills() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { userDetails } = useSelector((state) => state.user);
  const { selectedPatient, patientTimeline, loading, success, error } =
    useSelector((state) => state.patients);
  const dispatch = useDispatch();
  const [selectAllBills, setSelectAllBills] = useState(false);
  const [selectedBills, setSelectedBills] = useState([]);
  const [billsToSummarize, setBillsToSummarize] = useState([]);
  const { retainers } = useSelector((state) => state.retainers);
  const { services } = useSelector((state) => state.services);
  const { staff } = useSelector((state) => state.staff);
  const [searchValue, setSearchValue] = useState("");

  const addOrRemoveFromSelectedBills = (event) => {
    if (selectedBills.find((billId) => billId == event.target.value)) {
      setSelectAllBills(false);
      setSelectedBills(
        [...selectedBills].filter((billId) => billId != event.target.value)
      );
    } else {
      setSelectedBills([...selectedBills, event.target.value]);
    }
  };

  const checkIfBillSelected = (billId) =>
    Boolean(selectedBills.find((id) => id == billId));

  useEffect(() => {
    if (selectAllBills) {
      setSelectedBills([...patientTimeline.bills].map(({ id }) => id));
    } else {
      setSelectedBills([]);
    }
  }, [selectAllBills]);

  useEffect(() => {
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
    check.emptyArray(services) &&
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );

    check.emptyArray(staff) &&
      dispatch(
        fetchStaff({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );
  }, [userDetails]);

  const selectedBillsObjects = selectedBills.map((billId) =>
    patientTimeline.bills?.find(({ id }) => id == billId)
  );

  const processSelectedBills = () => {
    setBillsToSummarize(selectedBillsObjects);
    onOpen();
  };

  const canSelectProcessedBills = () => {
    if (
      selectedBillsObjects.find(
        (bill) =>
          bill?.invoice_details?.payment_status == "fully-paid" ||
          bill?.invoice_details?.payment_status == "partly-paid"
      )
    ) {
      return false;
    } else if (
      selectedBillsObjects.find(
        (bill) => bill?.payload_type == "deposit_request"
      )
    ) {
      return false;
    } else {
      return true;
    }
  };

  return (
    <Stack spacing={4}>
      <Flex justify="space-between">
        <HStack color="gray.500">
          <Checkbox
            colorScheme="primary"
            borderColor="primary.500"
            size="lg"
            pos="initial"
            mr={3}
            isChecked={selectAllBills}
            onChange={() => setSelectAllBills(!selectAllBills)}
          ></Checkbox>

          {check.nonEmptyArray(selectedBills) ? (
            <HStack fontSize="sm" color="gray.500">
              <HStack spacing={0}>
                <IconButton variant="default" icon={<MdDelete />} />
                <IconButton variant="default" icon={<VscCheck />} />
                {canSelectProcessedBills() && (
                  <Button
                    leftIcon={<CgPlayListCheck />}
                    size="sm"
                    variant="solid"
                    colorScheme="green"
                    onClick={processSelectedBills}
                  >
                    Process Selected Bills
                  </Button>
                )}
              </HStack>
            </HStack>
          ) : (
            <HStack>
              <Input
                size="sm"
                pos="initial"
                w={40}
                value={searchValue}
                onChange={(event) => {
                  setSearchValue(event.target.value);
                }}
                placeholder="Search Bills"
                rounded="full"
              />
              <Button variant="ghost" size="sm" rightIcon={<AiFillCaretDown />}>
                All Users
              </Button>
              <Button variant="ghost" size="sm" rightIcon={<BiCalendarEvent />}>
                Filter by date
              </Button>
            </HStack>
          )}
        </HStack>
      </Flex>
      <Divider opacity="initial" borderColor="gray.400" />
      <FilterResults
        value={searchValue}
        data={
          check.nonEmptyObject(patientTimeline)
            ? [...patientTimeline.bills].sort(
                (a, b) => new Date(b.created_at) - new Date(a.created_at)
              )
            : []
        }
        renderResults={(results) =>
          check.emptyArray(results) ? (
            <Text
              color="gray.500"
              w="full"
              wordBreak="break-word"
              textAlign="center"
            >
              When you render a service, its bill will show here
            </Text>
          ) : (
            results.map((bill) => (
              <BillCard
                isChecked={checkIfBillSelected(bill.id)}
                anySelectedBills={check.nonEmptyArray(selectedBills)}
                addOrRemoveFromSelectedActivities={addOrRemoveFromSelectedBills}
                setBillsToSummarize={setBillsToSummarize}
                key={bill.id}
                bill={bill}
                isOpen={isOpen}
                onOpen={onOpen}
                onClose={onClose}
              />
            ))
          )
        }
      />
      <BillSummary
        billsToSummarize={billsToSummarize}
        setBillsToSummarize={setBillsToSummarize}
        isOpen={isOpen}
        onClose={onClose}
        onOpen={isOpen}
      />
    </Stack>
  );
}
