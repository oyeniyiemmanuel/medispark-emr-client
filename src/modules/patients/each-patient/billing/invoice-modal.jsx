import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Select,
  Input,
  Icon,
  useMediaQuery,
  Table,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
  Checkbox,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { RiEditLine, RiFileList2Fill } from "react-icons/ri";
import { BsFillCaretRightFill } from "react-icons/bs";
import DashboardContainer from "../../../../components/layouts/dashboard";
import {
  BiCalendarEvent,
  BiChevronRight,
  BiDotsVerticalRounded,
} from "react-icons/bi";
import Head from "next/head";
import { AiFillCaretDown } from "react-icons/ai";
import { CgNotes } from "react-icons/cg";
import Router from "next/router";
import { formatDateToDDMMYY } from "../../../../utils";
import { MdAddCircle, MdPrint } from "react-icons/md";
import FakeIcon from "../../../../components/fake-icon";
import { CloseIcon } from "@chakra-ui/icons";
import { FaShare } from "react-icons/fa";
import { IoMdShare } from "react-icons/io";
import RecordPayment from "./record-payment";

export default function InvoiceModal({
  isInvoiceModalOpen,
  onInvoiceModalOpen,
  onInvoiceModalClose,
}) {
  const [isPhone] = useMediaQuery("(max-width: 575px)");
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Modal
      closeOnOverlayClick={false}
      scrollBehavior={isPhone ? "inside" : "inside"}
      isCentered
      size="md"
      isOpen={isInvoiceModalOpen}
      onClose={onInvoiceModalClose}
    >
      <ModalOverlay bgColor="rgba(0, 0, 0, 0.2)" />
      <ModalContent
        color="gray.500"
        px={3}
        rounded={0}
        maxW="2xl"
        overflow="scroll"
      >
        <ModalHeader>
          <HStack justify="space-between" spacing={4}>
            <Box as="span">Invoice Details</Box>
            <HStack px={6}>
              <IconButton size="sm" variant="ghost" icon={<MdPrint />} />
              <IconButton size="sm" variant="ghost" icon={<IoMdShare />} />
            </HStack>
          </HStack>
          <Divider mt={2} borderBottomWidth="2px" />
        </ModalHeader>
        <ModalCloseButton top={4} />
        <ModalBody pt={0} mt={0}>
          <Stack spacing={3}>
            <Stack
              direction={["column", "column", "row"]}
              spacing={[6, 6, 20]}
              justify="space-between"
              px={6}
            >
              <Stack spacing={3}>
                <HStack fontSize="sm">
                  <Avatar
                    size="sm"
                    name="Kent Dodds"
                    src="https://bit.ly/kent-c-dodds"
                  />
                  <Box>
                    <Text fontSize="sm">Bill to:</Text>
                    <Text>Richard Mofe Damijo</Text>
                    <Text fontSize="sm" color="primary.500">
                      HOSP NUMBER
                    </Text>
                  </Box>
                </HStack>
              </Stack>

              <Stack align="flex-end" spacing={3}>
                <Text fontWeight="bold" color="gray.600">
                  Invoice 102100
                </Text>
                <Stack align="flex-end" spacing={1}>
                  <Text fontSize="sm">
                    Date Created: &nbsp;&nbsp; 24/01/2021
                  </Text>
                  <Text fontSize="sm">Due Date: &nbsp;&nbsp; 24/01/2021</Text>
                </Stack>
                <Button
                  variant="link"
                  size="sm"
                  // onClick={(event) => {
                  //   event.stopPropagation();
                  //   onOpen();
                  // }}
                  colorScheme="primary"
                  rightIcon={<MdAddCircle />}
                >
                  Edit Invoice
                </Button>
              </Stack>
            </Stack>
            <Divider
              pt={2}
              borderBottomColor="gray.300"
              borderBottomWidth="2px"
            />
            <Table fontSize="sm" variant="unstyled">
              <Thead>
                <Tr>
                  <Th>S/N</Th>
                  <Th>Item/Service</Th>
                  <Th isNumeric>Amount</Th>
                </Tr>
              </Thead>
              <Tbody>
                <Tr>
                  <Td>1</Td>
                  <Td>
                    <Box>
                      <Text fontWeight="bold">Paracetamol</Text>
                      <Text>8 packs of paracetamol ftom pharmacy</Text>
                    </Box>
                  </Td>
                  <Td color="green.500" isNumeric>
                    #12,000
                  </Td>
                </Tr>
              </Tbody>
            </Table>
            <Divider
              pt={5}
              borderBottomColor="gray.300"
              borderBottomWidth="2px"
            />
            <Flex w="full" justify="space-between">
              <HStack>
                <Avatar
                  size="sm"
                  name="Kent Dodds"
                  src="https://bit.ly/kent-c-dodds"
                />
                <Box>
                  <Text>Hospital Name</Text>
                  <Text fontSize={12}>
                    Hospital and Clinic services for inst
                  </Text>
                </Box>
              </HStack>
              <HStack fontWeight="bold" fontSize="lg">
                <Text>Total</Text>
                <Text color="green.500">#12,000</Text>
              </HStack>
            </Flex>
          </Stack>
        </ModalBody>

        <ModalFooter w="full" justifyContent="space-between" pt={8}>
          <Button
            onClick={() => {
              onOpen();
            }}
            w={40}
            colorScheme="primary"
            mr={3}
          >
            Receive Payment
          </Button>
          <Checkbox
            color="green.500"
            size="sm"
            colorScheme="green"
            defaultIsChecked
          >
            Payment Received
          </Checkbox>
        </ModalFooter>
      </ModalContent>
      <RecordPayment
        onPaymentModalClose={onClose}
        onPaymentModalOpen={onOpen}
        isPaymentModalOpen={isOpen}
      />
    </Modal>
  );
}
