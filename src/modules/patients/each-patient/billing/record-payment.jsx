import {
  Avatar,
  Box,
  Button,
  Divider,
  HStack,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Select,
  Stack,
  Switch,
  Text,
  useMediaQuery,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  clearStates,
  sendToDoctor,
} from "../../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../../redux/slices/retainerSlice";
import { fetchServices } from "../../../../redux/slices/serviceSlice";
import {
  formatDateToDDMMYY,
  toastError,
  toastSuccess,
} from "../../../../utils";

export default function RecordPayment({
  isPaymentModalOpen,
  onPaymentModalOpen,
  onPaymentModalClose,
  ...rest
}) {
  const initialAppointmentState = {
    // Set Initial Values of input fields
    date: null,
    time: "",
    specialty: "",
    comment: "",
    personnel: "",
  };
  const [appointmentDetails, setAppointmentDetails] = useState(
    initialAppointmentState // Store values in state
  );
  const dispatch = useDispatch();
  const [isPhone] = useMediaQuery("(max-width: 575px)");
  const toast = useToast();
  const { retainers } = useSelector((state) => state.retainers);
  const { services } = useSelector((state) => state.services);
  const { patients, selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const { userDetails } = useSelector((state) => state.user);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setAppointmentDetails({ ...appointmentDetails, [name]: value }); // onChange handler
  };

  // useEffect(() => {
  //   check.emptyArray(retainers) &&
  //     check.nonEmptyObject(userDetails) && // If retainers haven't been loaded, then load it
  //     dispatch(
  //       fetchRetainers({
  //         branch_id: userDetails.branch_id,
  //       })
  //     );
  //   check.emptyArray(services) &&
  //     check.nonEmptyObject(userDetails) && // If services haven't been loaded, then load it
  //     dispatch(
  //       fetchServices({
  //         branch_id: userDetails.branch_id,
  //         category: "consultation",
  //       })
  //     );
  // }, [userDetails]);

  // const onSubmitSendToDoctor = (event) => {
  //   event.preventDefault();
  //   dispatch(
  //     sendToDoctor({
  //       user_id: selectedPatient?.id,
  //       organization_id: userDetails.organization_id,
  //       branch_id: userDetails.branch_id,
  //       retainer_id: selectedPatient.retainer_id,

  //       retainer_category: retainers.find(
  //         ({ id }) => id === selectedPatient.retainer_id
  //       )?.category,
  //       visit_status: "new",
  //       visit_type: "see-physician",
  //       care_type: "outpatient",
  //       services: [
  //         services.find(({ id }) => id == appointmentDetails.specialty),
  //       ],
  //     })
  //   );
  // };

  // useEffect(() => {
  //   if (error && error.errorType === "SEND_TO_DOCTOR") {
  //     // If an error occurs while sending to doctor,
  //     toast(toastError(error)); // Display Error Message
  //     dispatch(clearStates()); // Then clear stale error messages
  //   }
  // }, [error]);

  // useEffect(() => {
  //   if (success === "SEND_TO_DOCTOR") {
  //     // If patient is sent successfully,
  //     toast(toastSuccess("Done!")); // Display Success message
  //     dispatch(clearStates()); // Then clear stale error messages
  //     onClose(); // Close form modal
  //   }
  // }, [success]);

  return (
    <Modal
      scrollBehavior="inside"
      closeOnOverlayClick={false}
      isCentered
      size={"lg"}
      isOpen={isPaymentModalOpen}
      onClose={onPaymentModalClose}
    >
      <ModalOverlay bgColor="rgba(0, 0, 0, 0.2)" />
      <ModalContent
        overflowY="scroll"
        className="container-body"
        pb={4}
        rounded={8}
      >
        <form
          //  onSubmit={onSubmitSendToDoctor}
          action="submit"
        >
          <ModalHeader>
            <Box as="span" pl={4}>
              Record Payment
            </Box>
            <Divider mt={2} borderBottomWidth="2px" />
          </ModalHeader>
          <ModalCloseButton />

          <ModalBody pb={4}>
            <Stack spacing={4}>
              <Text px={4} mb={4} color="gray.400" fontSize="sm">
                Record a payment you’ve already received, such as cash, cheque,
                or bank payment.
              </Text>

              <Stack spacing={4} rounded={8} px={4}>
                <Stack>
                  <Text pl={4} color="gray.500">
                    Payment date
                  </Text>
                  <Input
                    value={formatDateToDDMMYY(
                      "yyyy-mm-dd",
                      appointmentDetails.date
                    )}
                    onChange={handleChange}
                    name="date"
                    variant="filled"
                    isRequired
                    type="date"
                    placeholder="dd-mm-yyyy"
                    format="dd-mm-yyyy"
                  />
                </Stack>

                <Stack>
                  <Text pl={4} color="gray.500">
                    Amount
                  </Text>
                  <Input
                    // value={appointmentDetails.comment}
                    // onChange={handleChange}
                    // isRequired
                    name="amount"
                    variant="filled"
                    placeholder="Type an Amount"
                  />
                </Stack>

                <Stack w="full">
                  <Text pl={4} color="gray.500">
                    Credit Wallet
                  </Text>
                  <Select
                    isFullWidth
                    isRequired
                    // value={appointmentDetails.specialty}
                    // onChange={handleChange}
                    name="method"
                    variant="filled"
                    placeholder="Select Patient or Staff"
                  >
                    <option value="patient">Patient</option>
                  </Select>
                </Stack>

                <Stack w="full">
                  <Text pl={4} color="gray.500">
                    Payment Method
                  </Text>
                  <Select
                    isFullWidth
                    isRequired
                    // value={appointmentDetails.specialty}
                    // onChange={handleChange}
                    name="method"
                    variant="filled"
                    placeholder="Select Payment Method"
                  >
                    <option value="bank">Bank Payment</option>
                    <option value="cash">Cash</option>
                    <option value="cheque">Cheque</option>
                    <option value="cardPOS">Card/POS</option>
                    <option value="other">Other</option>
                  </Select>
                </Stack>

                <Stack>
                  <Text pl={4} color="gray.500">
                    Memo/Notes
                  </Text>
                  <Input
                    // value={appointmentDetails.comment}
                    // onChange={handleChange}
                    // isRequired
                    name="notes"
                    variant="filled"
                    placeholder="Add Memo"
                  />
                </Stack>

                <HStack pt={8} justify="flex-end" align="center">
                  <Button
                    onClick={onPaymentModalClose}
                    variant="outline"
                    w={32}
                  >
                    Discard
                  </Button>
                  <Button
                    // isLoading={loading === "SEND_TO_DOCTOR"}
                    loadingText="Sending..."
                    type="submit"
                    colorScheme="primary"
                    w={32}
                  >
                    Confirm
                  </Button>
                </HStack>
              </Stack>
            </Stack>
          </ModalBody>
        </form>
      </ModalContent>
    </Modal>
  );
}
