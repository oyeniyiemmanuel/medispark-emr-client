import React, { useEffect } from "react";
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  AvatarBadge,
  Box,
  Button,
  ButtonGroup,
  Center,
  CloseButton,
  Divider,
  Flex,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Select,
  Input,
  useMediaQuery,
  useToast,
  Switch,
  Icon,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import { BiCalendarAlt } from "react-icons/bi";
import { ST } from "next/dist/next-server/lib/utils";

export default function ChooseAppointmentSlot({
  staff,
  checkIn,
  handleChange,
  appointmentDetails,
  setAppointmentDetails,
  ...rest
}) {
  useEffect(() => {
    setAppointmentDetails({ ...appointmentDetails, time: "" });
  }, [appointmentDetails.personnel]);
  return (
    <Stack spacing={3} {...rest}>
      <Stack>
        <Text pl={4} color="gray.500">
          Pick Personnel
        </Text>
        <Select
          value={appointmentDetails.personnel}
          onChange={handleChange}
          isRequired={!checkIn}
          name="personnel"
          variant="filled"
          isDisabled={checkIn}
          placeholder="Select Doctor"
        >
          {staff.map(({ full_name, id }) => (
            <option value={id} key={id}>
              {full_name}
            </option>
          ))}
        </Select>
      </Stack>
      {appointmentDetails.personnel && (
        <Stack>
          <Flex align="center" color="gray.500">
            <Icon as={BiCalendarAlt} />
            <Text fontSize="sm" pl={4} color="gray.500">
              {
                staff.find(({ id }) => id == appointmentDetails.personnel)
                  ?.full_name
              }
              's Schedule
            </Text>
          </Flex>
          {/* 
          <Wrap w="full" p={4} bg="gray.100" rounded={4}>
            {[0, 1, 2, 3, 4, 5].map((index) => (
              <WrapItem>
                <Button
                  fontWeight="normal"
                  size="sm"
                  h={10}
                  variant="default"
                  bg="white"
                  _hover={{
                    bg: "gray.30",
                  }}
                  onClick={() =>
                    setAppointmentDetails({
                      ...appointmentDetails,
                      time: "03:12",
                    })
                  }
                >
                  10:00am - 10:30am
                </Button>
              </WrapItem>
            ))} 
          </Wrap>
            */}
          <HStack w="full" px={4} py={2} bg="gray.100" rounded={4}>
            <Stack spacing={0} w="full">
              <Text fontSize="sm" color="gray.500">
                From
              </Text>
              <Input
                value={appointmentDetails.time}
                onChange={handleChange}
                name="time"
                bg="white"
                variant="filled"
                isRequired
                type="time"
                placeholder="hh:mm"
                format="hh:mm"
              />
            </Stack>
            <Stack spacing={0} w="full">
              <Text fontSize="sm" color="gray.500">
                Till
              </Text>
              <Input
                value={appointmentDetails.endTime}
                onChange={handleChange}
                name="endTime"
                bg="white"
                variant="filled"
                isRequired
                min={appointmentDetails.time}
                type="time"
                placeholder="hh:mm"
                format="hh:mm"
              />
            </Stack>
          </HStack>
        </Stack>
      )}
    </Stack>
  );
}
