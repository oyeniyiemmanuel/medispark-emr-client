import {
  Box,
  Button,
  Divider,
  HStack,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Select,
  Stack,
  Switch,
  Text,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import CustomSelect from "../../../../components/multi-select";
import SelectService from "../../../../components/select-service";
import {
  checkInPatient,
  clearStates,
  createAppointment,
} from "../../../../redux/slices/appointmentSlice";
import { closeAppointmentModal } from "../../../../redux/slices/commonSlice";
import { patientActions } from "../../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../../redux/slices/retainerSlice";
import { fetchServices } from "../../../../redux/slices/serviceSlice";
import { fetchStaff } from "../../../../redux/slices/staffSlice";
import {
  formatDateToDDMMYY,
  separateWithComma,
  toastError,
  toastSuccess,
} from "../../../../utils";
import ChoosePatient from "./choose-patient";
import ChooseAppointmentSlot from "./choose-slot";

export default function SendToDoctor() {
  const dispatch = useDispatch();
  const {
    appointmentModal: {
      isOpen,
      existingAppointmentDetails,
      checkIn,
      choosePatient,
    },
  } = useSelector((state) => state.common);

  const toast = useToast();
  const { retainers } = useSelector((state) => state.retainers);
  const { services } = useSelector((state) => state.services);
  const { staff } = useSelector((state) => state.staff);
  const { patients, selectedPatient } = useSelector((state) => state.patients);
  const { appointments, newestCheckIn, loading, success, error } = useSelector(
    (state) => state.appointments
  );
  const { userDetails } = useSelector((state) => state.user);

  const initialAppointmentState = {
    // Set Initial Values of input fields
    userId: choosePatient ? null : selectedPatient?.id,
    date: existingAppointmentDetails?.appointment_date || null,
    time: existingAppointmentDetails?.appointment_time || "",
    endTime: existingAppointmentDetails?.appointment_end_time || "",
    specialty: existingAppointmentDetails?.service_id || null,
    comment: existingAppointmentDetails?.comment || "",
    personnel: existingAppointmentDetails?.appointee_id || "",
  };
  const [appointmentDetails, setAppointmentDetails] = useState(
    initialAppointmentState // Store values in state
  );
  const [toDeposit, setToDeposit] = useState(false);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setAppointmentDetails({ ...appointmentDetails, [name]: value }); // onChange handler
  };

  useEffect(() => {
    setAppointmentDetails(initialAppointmentState);
  }, [existingAppointmentDetails]);

  useEffect(() => {
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
    check.emptyArray(services) &&
      dispatch(
        fetchServices({
          branch_id: userDetails.branch_id,
        })
      );

    check.emptyArray(staff) &&
      dispatch(
        fetchStaff({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );
  }, [userDetails]);

  const onSubmitSendToDoctor = (event) => {
    event.preventDefault();
    if (!appointmentDetails.userId) {
      toast({
        title: "You haven't selected a patient yet",
        status: "error",
        duration: 4000,
        position: "bottom-left",
        isClosable: true,
      });
      return;
    }
    if (checkIn) {
      dispatch(
        checkInPatient({
          user_id: appointmentDetails.userId,
          organization_id: userDetails.organization_id,
          branch_id: userDetails.branch_id,
          comment: appointmentDetails.comment,
          override_add_to_queue: true,
          retainer_id: selectedPatient.retainer_id,
          retainer_category: retainers.find(
            ({ id }) => id === selectedPatient.retainer_id
          )?.category,
          visit_status: "new",
          visit_type: "see-physician",
          care_type: "outpatient",
          services: [
            {
              ...services.find(({ id }) => id == appointmentDetails.specialty),
              override_add_to_queue: true,
              appointment_id: existingAppointmentDetails?.id,
            },
          ],
        })
      );
    } else {
      dispatch(
        createAppointment({
          user_id: appointmentDetails.userId,
          organization_id: userDetails.organization_id,
          branch_id: userDetails.branch_id,
          retainer_id: selectedPatient.retainer_id,
          ask_for_deposit: toDeposit,
          services: [
            {
              ...services.find(({ id }) => id == appointmentDetails.specialty),
              override_add_to_queue: true,
            },
          ],
          appointment_date:
            appointmentDetails.date || formatDateToDDMMYY("yyyy-mm-dd"),
          appointment_start_time: appointmentDetails.time,
          appointment_end_time: appointmentDetails.endTime,
          appointee_id: appointmentDetails.personnel,
          comment: appointmentDetails.comment,
        })
      );
    }
  };

  useEffect(() => {
    if (
      (error && error.errorType === "CREATE_APPOINTMENT") ||
      (error && error.errorType === "CHECK_IN_PATIENT")
    ) {
      // If an error occurs while sending to doctor,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  useEffect(() => {
    if (success === "CREATE_APPOINTMENT" || success === "CHECK_IN_PATIENT") {
      // If patient is sent successfully,
      toast(toastSuccess("Done!")); // Display Success message
      if (success === "CHECK_IN_PATIENT") {
        dispatch(
          patientActions.addToPatientTimeline({
            activity: newestCheckIn,
            type: "bills",
          })
        );
      }
      dispatch(clearStates()); // Then clear stale error messages
      if (toDeposit) {
        Router.push(`/patients/${appointmentDetails.userId}?activity=bills`);
      }
      dispatch(closeAppointmentModal()); // Close form modal
    }
  }, [success]);

  return (
    <Modal
      scrollBehavior="inside"
      closeOnOverlayClick={false}
      isCentered
      size={"lg"}
      isOpen={isOpen}
      onClose={() => dispatch(closeAppointmentModal())}
    >
      <ModalOverlay bgColor="rgba(0, 0, 0, 0.2)" />
      <ModalContent
        bg="gray.70"
        overflow="scroll"
        className="container-body"
        pb={4}
        rounded={8}
      >
        <form onSubmit={onSubmitSendToDoctor} action="submit">
          <ModalHeader>
            <Box as="span" pl={4}>
              {checkIn ? "Check-In" : "Create New Appointment"}
            </Box>
            <Divider mt={2} borderBottomWidth="2px" />
          </ModalHeader>
          <ModalCloseButton />

          <ModalBody pb={4}>
            <Stack spacing={4}>
              <Stack spacing={4} rounded={8} py={8} px={6} bg="white">
                {/* <HStack justify="space-between">
                  <HStack align="start">
                    <Icon
                      color="gray.500"
                      boxSize={10}
                      as={MdPermContactCalendar}
                    />
                    <Stack maxW="4xs">
                      <Text color="gray.500" fontSize="sm">
                        <Box as="span" fontWeight="bold">
                          {selectedPatient?.full_name}
                        </Box>{" "}
                        has already been booked for consultation cons.. on the
                        4th January 2021
                      </Text>
                      <HStack spacing={4}>
                        <Button variant="link" size="sm" colorScheme="primary">
                          Prefill Appointment Details
                        </Button>
                        <Button variant="link" size="sm">
                          Ignore
                        </Button>
                      </HStack>
                    </Stack>
                  </HStack>
                </HStack>
                <Divider />
                 */}
                <ChoosePatient
                  appointmentDetails={appointmentDetails}
                  setAppointmentDetails={setAppointmentDetails}
                />
                <Stack>
                  <Text pl={4} color="gray.500">
                    Pick a date
                  </Text>
                  <Input
                    value={formatDateToDDMMYY(
                      "yyyy-mm-dd",
                      appointmentDetails.date
                    )}
                    onChange={handleChange}
                    name="date"
                    variant="filled"
                    isRequired
                    isDisabled={checkIn}
                    type="date"
                    min={formatDateToDDMMYY("yyyy-mm-dd")}
                    placeholder="dd-mm-yyyy"
                    format="dd-mm-yyyy"
                  />
                </Stack>

                <HStack align="start" spacing={4}>
                  <Stack w={checkIn ? "full" : "75%"}>
                    <Text pl={4} color="gray.500">
                      Choose a Service
                    </Text>
                    <SelectService
                      isDisabled={checkIn && existingAppointmentDetails}
                      valueChecker={({ id }) =>
                        id == appointmentDetails.specialty
                      }
                      onChange={(medication) => {
                        setAppointmentDetails({
                          ...appointmentDetails,
                          specialty: medication ? medication.id : null,
                        });
                      }}
                    />
                    {/* <Select
                      isFullWidth
                      isRequired
                      value={appointmentDetails.specialty}
                      onChange={handleChange}
                      name="specialty"
                      variant="filled"
                      isDisabled={checkIn && existingAppointmentDetails}
                      placeholder="Select Service"
                    >
                      {services
                        .filter(({ category }) => category !== "pharmacy")
                        .map(({ id, name, prices }) => (
                          <option
                            key={id}
                            value={id}
                          >{`${name} (₦${separateWithComma(
                            prices?.find(
                              ({ retainer_id }) =>
                                retainer_id == selectedPatient?.retainer_id
                            )?.amount || 0
                          )})`}</option>
                        ))}
                    </Select> */}
                  </Stack>

                  {!checkIn && (
                    <Stack align="center" justify="center" w="25%">
                      <Text color="gray.500">Pay deposit?</Text>
                      <Switch
                        isChecked={toDeposit}
                        onChange={() => setToDeposit(!toDeposit)}
                        size="lg"
                      />
                    </Stack>
                  )}
                </HStack>
                {!checkIn && !existingAppointmentDetails && (
                  <ChooseAppointmentSlot
                    staff={staff}
                    checkIn={checkIn}
                    handleChange={handleChange}
                    appointmentDetails={appointmentDetails}
                    setAppointmentDetails={setAppointmentDetails}
                  />
                )}
                <Stack>
                  <Text pl={4} color="gray.500">
                    Comment
                  </Text>
                  <Input
                    value={appointmentDetails.comment}
                    onChange={handleChange}
                    isDisabled={checkIn && existingAppointmentDetails}
                    name="comment"
                    variant="filled"
                    placeholder="General Description of ailment"
                  />
                </Stack>

                <HStack pt={8} justify="flex-end" align="center">
                  <Button
                    onClick={() => dispatch(closeAppointmentModal())}
                    variant="outline"
                    w={32}
                  >
                    Cancel
                  </Button>
                  <Button
                    isLoading={
                      loading === "CHECK_IN_PATIENT" ||
                      loading === "CREATE_APPOINTMENT"
                    }
                    loadingText="Sending..."
                    type="submit"
                    colorScheme="primary"
                    w={32}
                  >
                    Send
                  </Button>
                </HStack>
              </Stack>
            </Stack>
          </ModalBody>
        </form>
      </ModalContent>
    </Modal>
  );
}
