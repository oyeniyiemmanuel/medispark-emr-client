import React from "react";
import { useDispatch, useSelector } from "react-redux";
import CustomSelect from "../../../../components/multi-select";
import { setSelectedPatient } from "../../../../redux/slices/patientSlice";

export default function ChoosePatient({
  appointmentDetails,
  setAppointmentDetails,
  ...rest
}) {
  const dispatch = useDispatch();
  const { patients, selectedPatient } = useSelector((state) => state.patients);
  const {
    appointmentModal: { choosePatient },
  } = useSelector((state) => state.common);

  const patientOptions = [...patients].reverse().map(({ id, full_name }) => ({
    value: id,
    label: full_name,
  }));

  const filterPatients = (inputValue) => {
    return patientOptions
      .filter((i) => i.label.toLowerCase().includes(inputValue?.toLowerCase()))
      .slice(0, 100);
  };

  const promiseOptions = (inputValue) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve(filterPatients(inputValue));
      }, 500);
    });

  const onChangePatient = (data) => {
    setAppointmentDetails({
      ...appointmentDetails,
      userId: data.value,
    });
    dispatch(setSelectedPatient(patients.find(({ id }) => id === data.value)));
  };

  return (
    <CustomSelect
      isAsync
      isDisabled={!choosePatient}
      value={
        patientOptions.find(
          ({ value }) => value == appointmentDetails.userId
        ) || null
      }
      styles={false}
      placeholder="Select Patient"
      onChange={onChangePatient}
      cacheOptions
      defaultOptions
      loadOptions={promiseOptions}
    />
  );
}
