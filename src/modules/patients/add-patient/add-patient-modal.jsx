import { ChevronDownIcon } from "@chakra-ui/icons";
import {
  Avatar,
  AvatarGroup,
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Stack,
  Text,
  useDisclosure,
  useMediaQuery,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clearStates, createPatient } from "../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../redux/slices/retainerSlice";
import {
  toastSuccess,
  toastError,
  separateWithComma,
  formatDateToDDMMYY,
} from "../../../utils/index";

export default function AddPatient({ ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialPatientDetailState = {
    // Set Initial Values of input fields
    branchId: "",
    retainerId: "",
    retainerCategory: "",
    lastName: "",
    firstName: "",
    email: "",
    phone: "",
    dob: "",
    gender: "",
  };
  const [patientDetails, setPatientDetails] = useState(
    initialPatientDetailState // Store values in state
  );

  const handleChange = (event) => {
    const { name, value } = event.target;
    setPatientDetails({ ...patientDetails, [name]: value }); // onChange handler
  };

  const dispatch = useDispatch();
  const [isPhone] = useMediaQuery("(max-width: 575px)");
  const toast = useToast();
  const { retainers } = useSelector((state) => state.retainers);
  const { patients, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const { userDetails } = useSelector((state) => state.user);

  useEffect(() => {
    check.emptyArray(retainers) &&
      check.nonEmptyObject(userDetails) && // If retainers haven't been loaded, then load it
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
  }, [userDetails]);

  const onSubmitAddPatient = (event) => {
    event.preventDefault();
    dispatch(
      createPatient({
        //Create Patient Body
        branch_id: userDetails.branch_id,
        retainer_id:
          patientDetails.retainerId && parseInt(patientDetails.retainerId), // If a retainer is selected, parse the id to integer
        last_name:
          patientDetails.lastName.charAt(0).toUpperCase() +
          patientDetails.lastName.slice(1).toLowerCase(), // Capitalize Last Name
        first_name:
          patientDetails.firstName.charAt(0).toUpperCase() +
          patientDetails.firstName.slice(1).toLowerCase(), // Capitalize First Name
        phone: patientDetails.phone,
        dob: patientDetails.dob,
        gender: patientDetails.gender,
        email: patientDetails.email,
      })
    );
  };
  useEffect(() => {
    if (error && error.errorType === "CREATE_PATIENT") {
      // If an error occurs while creating patient,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  useEffect(() => {
    if (success === "CREATE_PATIENT") {
      // If patient is created successfully,
      toast(toastSuccess("Patient Created Successfully!")); // Display Success message
      dispatch(clearStates()); // Then clear stale error messages
      onClose(); // Close form modal
      Router.push(`/patients/${patients.slice(-1).pop()?.id}`); // Route to that newly-added patient
    }
  }, [success]);

  const retainersWithSelectedCategory = retainers.filter(
    ({ category }) => category == patientDetails.retainerCategory
  );

  useEffect(() => {
    if (patientDetails.retainerCategory == "fee-paying") {
      setPatientDetails({
        ...patientDetails,
        retainerId: retainersWithSelectedCategory[0].id,
      });
    }
  }, [patientDetails.retainerCategory]);

  return (
    <Stack {...rest} w="full" spacing={3}>
      <Heading fontSize="xl" color="gray.600">
        Register patient
      </Heading>
      <HStack spacing={3}>
        <AvatarGroup size="md" max={3}>
          {[...patients]
            .reverse()
            .slice(0, 3)
            .map(({ full_name, avatar }) => (
              <Avatar name={full_name} src={avatar?.file_path} />
            ))}
        </AvatarGroup>
        <Text color="gray.500">
          {separateWithComma(patients.length)}{" "}
          <Box as="span" fontWeight="bold" color="primary.500">
            {" "}
            Patients Registered
          </Box>
        </Text>
      </HStack>
      <Button
        onClick={onOpen}
        colorScheme="primary"
        size="md"
        w={["full", "3xs"]}
      >
        Register Patient
      </Button>
      <Modal
        scrollBehavior={"inside"}
        closeOnOverlayClick={false}
        isCentered
        size="lg"
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay bgColor="rgba(0, 0, 0, 0.2)" />
        <ModalContent
          className="container-body"
          overflow="scroll"
          px={[2, 6]}
          pb={4}
          rounded={8}
        >
          <form onSubmit={onSubmitAddPatient} action="submit">
            <ModalHeader>
              Register Patient
              <Divider mt={3} borderBottomWidth="2px" />
            </ModalHeader>
            <ModalCloseButton />

            <ModalBody pb={4}>
              <Stack spacing={4}>
                <Stack direction={["column", "column", "row"]}>
                  <Stack>
                    <Text pl={4} color="gray.500">
                      First Name
                    </Text>
                    <Input
                      value={patientDetails.firstName}
                      onChange={handleChange}
                      name="firstName"
                      variant="filled"
                      isRequired
                      placeholder="eg. John"
                    />
                  </Stack>

                  <Stack>
                    <Text pl={4} color="gray.500">
                      Surname
                    </Text>
                    <Input
                      value={patientDetails.lastName}
                      onChange={handleChange}
                      name="lastName"
                      variant="filled"
                      isRequired
                      placeholder="eg. Doe"
                    />
                  </Stack>
                </Stack>
                <Stack direction={["column", "column", "row"]}>
                  <Stack>
                    <Text pl={4} color="gray.500">
                      Date of Birth
                    </Text>
                    <Input
                      value={patientDetails.dob}
                      onChange={handleChange}
                      name="dob"
                      variant="filled"
                      isRequired
                      max={formatDateToDDMMYY("yyyy-mm-dd")}
                      type="date"
                      placeholder="dd-mm-yyyy"
                      format="dd-mm-yyyy"
                    />
                  </Stack>

                  <Stack w="full">
                    <Text pl={4} color="gray.500">
                      Gender
                    </Text>
                    <Select
                      isFullWidth
                      isRequired
                      value={patientDetails.gender}
                      onChange={handleChange}
                      name="gender"
                      variant="filled"
                      placeholder="Choose Gender"
                    >
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                    </Select>
                  </Stack>
                </Stack>
                <Stack>
                  <Text pl={4} color="gray.500">
                    Email Address
                  </Text>
                  <Input
                    value={patientDetails.email}
                    onChange={handleChange}
                    name="email"
                    variant="filled"
                    type="email"
                    isRequired
                    placeholder="eg. john@doe.com"
                  />
                </Stack>
                <Stack>
                  <Text pl={4} color="gray.500">
                    Phone Number
                  </Text>
                  <Input
                    value={patientDetails.phone}
                    onChange={handleChange}
                    name="phone"
                    variant="filled"
                    type="number"
                    isRequired
                    placeholder="eg. 08012345678"
                  />
                </Stack>

                <Stack>
                  <Text pl={4} color="gray.500">
                    Sponsor Type
                  </Text>
                  <Select
                    value={patientDetails.retainerCategory}
                    onChange={handleChange}
                    name="retainerCategory"
                    isRequired
                    variant="filled"
                    placeholder="Choose Sponsor Type"
                  >
                    <option value="fee-paying">Self Sponsored</option>
                    <option value="private-insurance">HMO</option>
                    <option value="company-insurance">Employer</option>
                  </Select>
                </Stack>
                {patientDetails.retainerCategory && (
                  <Stack>
                    <Text pl={4} color="gray.500">
                      Sponsor
                    </Text>
                    <Select
                      value={patientDetails.retainerId}
                      onChange={handleChange}
                      name="retainerId"
                      isRequired
                      variant="filled"
                      placeholder="Choose Sponsor"
                    >
                      {retainersWithSelectedCategory.map(({ name, id }) => (
                        <option value={id} key={id}>
                          {name}
                        </option>
                      ))}
                    </Select>
                  </Stack>
                )}
                {/* <Flex pt={4} w="full">
                  <Button
                    variant="link"
                    colorScheme="primary"
                    onClick={() =>
                      Router.push(
                        `/patients/new?firstName=${patientDetails.firstName}&lastName=${patientDetails.lastName}&gender=${patientDetails.gender}&phone=${patientDetails.phone}&dob=${patientDetails.dob}`
                      )
                    }
                    rightIcon={<ChevronDownIcon />}
                  >
                    Add More Details
                  </Button>
                </Flex> */}
              </Stack>
            </ModalBody>

            <ModalFooter>
              <Button
                isLoading={loading === "CREATE_PATIENT"}
                loadingText="Registering..."
                type="submit"
                colorScheme="primary"
                w={40}
              >
                Register
              </Button>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
    </Stack>
  );
}
