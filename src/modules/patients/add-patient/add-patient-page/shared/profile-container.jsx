import {
  Box,
  CloseButton,
  Flex,
  Heading,
  HStack,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import Head from "next/head";
import Router, { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";
import {
  clearStates,
  fetchPatients,
  setSelectedPatient,
} from "../../../../../redux/slices/patientSlice";
import {
  fetchRetainers,
  retainerActions,
} from "../../../../../redux/slices/retainerSlice";
import { serviceActions } from "../../../../../redux/slices/serviceSlice";
import theme from "../../../../../theme";
import { findInArray, toastError } from "../../../../../utils";
import PatientCard from "./profile-options";

export default function PatientProfileContainer({ children, title, ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const { userDetails } = useSelector((state) => state.user);
  const { services } = useSelector((state) => state.services);
  const dispatch = useDispatch();
  const { retainers } = useSelector((state) => state.retainers);
  const { patients, selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const { query } = useRouter();

  const toast = useToast();
  useEffect(() => {
    //Fetch Patients
    check.emptyArray(patients) &&
      dispatch(
        fetchPatients({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );

    //Fetch Patient retainers
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
  }, []);

  useEffect(() => {
    check.nonEmptyArray(patients) &&
      query.id &&
      dispatch(setSelectedPatient(findInArray(patients, "id", query.id)));
  }, [patients.length]);

  useEffect(() => {
    if (error && error.errorType === "FETCH_PATIENTS") {
      // If an error occurs while fetching patient,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  useEffect(() => {
    if (retainers.error && retainers.error.errorType === "FETCH_RETAINERS") {
      // If an error occurs while fetching retainers,
      toast(toastError(retainers.error)); // Display Error Message
      dispatch(retainerActions.clearStates()); // Then clear stale error messages
    }
  }, [retainers.error]);

  useEffect(() => {
    if (services.error && services.error.errorType === "FETCH_SERVICES") {
      // If an error occurs while fetching services,
      toast(toastError(services.error)); // Display Error Message
      dispatch(serviceActions.clearStates()); // Then clear stale error messages
    }
  }, [services.error]);

  return (
    <>
      <Head>
        <title>Create New Patient - MediSpark</title>
      </Head>
      <Flex
        py={2}
        pr={4}
        pl="3%"
        bg="white"
        w="full"
        justify="space-between"
        align="center"
        color="gray.600"
      >
        <HStack>
          <Box h={12} w="300px" />
          <Heading fontSize="2xl">Create Patient</Heading>
        </HStack>
        <CloseButton onClick={() => Router.push(`/`)} />
      </Flex>
      <HStack pb={4} spacing={4} align="start" w="full" pl="3%" pr={4}>
        <PatientCard />
        <Box pt={3} w="calc(100% - 300px)">
          {children}
        </Box>
      </HStack>
    </>
  );
}
