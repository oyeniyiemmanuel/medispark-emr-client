import {
  Avatar,
  Box,
  HStack,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import React from "react";
import { useSelector } from "react-redux";
import FakeIcon from "../../../../../components/fake-icon";

export default function ProfileOptions({ ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const profileOptions = [
    { id: 1, name: "Biodata" },
    { id: 2, name: "Profile Settings" },
    { id: 3, name: "Tarriffs" },
    { id: 4, name: "Accounts  " },
    { id: 5, name: "Reports" },
    { id: 6, name: "Profile Settings" },
  ];

  return (
    <>
      <Box position="sticky" top="80px" bg="gray.50" w="300px" {...rest}>
        <Stack w="full" bg="white">
          {/* <HStack py={4} bg="primary.50" w="full" spacing={4} px={6}>
            <Avatar
              size="md"
              name={`${selectedPatient.first_name} ${selectedPatient.last_name}`}
              src={selectedPatient.avatar?.file_path}
            />
            <Box>
              <Text fontWeight="bold" color="gray.500" fontSize="lg">
                {selectedPatient.first_name} {selectedPatient.last_name}
              </Text>
              <Text color="primary.500">Patient</Text>
            </Box>
          </HStack> */}
          <Stack spacing={0}>
            {profileOptions.map(({ id, name }) => (
              <HStack
                _hover={{
                  borderLeft: "3px solid",
                  borderLeftColor: "primary.500",
                  transform: "scale(1.08)",
                  fontWeight: "bold",
                  shadow: "md",
                }}
                transition="0.2s all"
                cursor="pointer"
                key={id}
                py={3}
                spacing={4}
                px={6}
              >
                <FakeIcon />
                <Text color="gray.500">{name}</Text>
              </HStack>
            ))}
          </Stack>
        </Stack>
        <Box bg="white" color="gray.500" m={4}></Box>
      </Box>
    </>
  );
}
