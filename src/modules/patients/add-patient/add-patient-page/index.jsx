import {
  Button,
  HStack,
  Input,
  Select,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import PatientProfileContainer from "./shared/profile-container";

export default function CreateNewPatientView() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [image, setImage] = useState("");
  const { selectedPatient, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const patientTabs = ["Personal Info", "Other Info", "State Of Origin"];
  return (
    <PatientProfileContainer>
      <Tabs variant="enclosed" isFitted isLazy>
        <TabList borderBottom="none">
          {patientTabs.map((tab, index) => (
            <Tab
              key={index}
              color="gray.500"
              backgroundColor="gray.300"
              fontSize="sm"
              marginRight={2}
              _selected={{
                fontWeight: "bold",
                backgroundColor: "white",
                color: "primary.500",
                borderLeft: "3px solid",
                borderLeftColor: "primary.500",
                borderBottomWidth: 4,
              }}
            >
              {tab}
            </Tab>
          ))}
        </TabList>

        <TabPanels bg="white">
          <TabPanel px={8} pt={10}>
            <Stack>
              <Stack direction="row" spacing={8}>
                <Stack w="full" spacing={4}>
                  <Stack>
                    <Text pl={4} color="gray.500">
                      First Name
                    </Text>
                    <Input
                      // value={patientDetails.phone}
                      // onChange={handleChange}
                      // name="phone"
                      variant="filled"
                      isRequired
                      placeholder="Enter First Name"
                    />
                  </Stack>
                  <Stack>
                    <Text pl={4} color="gray.500">
                      Last Name
                    </Text>
                    <Input
                      // value={patientDetails.phone}
                      // onChange={handleChange}
                      // name="phone"
                      variant="filled"
                      isRequired
                      placeholder="Enter Last name"
                    />
                  </Stack>
                  <Stack>
                    <Text pl={4} color="gray.500">
                      Gender
                    </Text>
                    <Select
                      // value={patientDetails.phone}
                      // onChange={handleChange}
                      // name="phone"
                      variant="filled"
                      isRequired
                      placeholder="Choose Gender"
                    >
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                    </Select>
                  </Stack>
                </Stack>
                {/* <ImageUpload image={image} setImage={setImage} /> */}
              </Stack>

              <Stack pt={4}>
                <Stack w="full">
                  <Text pl={4} color="gray.500">
                    Phone Number
                  </Text>
                  <Input
                    // value={patientDetails.phone}
                    // onChange={handleChange}
                    // name="phone"
                    variant="filled"
                    isRequired
                    placeholder="Enter Phone Number"
                  />
                </Stack>
                <Stack w="full">
                  <Text pl={4} color="gray.500">
                    Email Address
                  </Text>
                  <Input
                    // value={patientDetails.phone}
                    // onChange={handleChange}
                    // name="phone"
                    variant="filled"
                    isRequired
                    placeholder="Enter Email Address"
                  />
                </Stack>
              </Stack>
              <HStack pt={16} justify="flex-end" align="center">
                <Button onClick={onClose} variant="outline" w={40}>
                  Cancel
                </Button>
                <Button
                  // isLoading={loading === "SEND_TO_DOCTOR"}
                  loadingText="Saving..."
                  type="submit"
                  colorScheme="primary"
                  w={40}
                >
                  Save
                </Button>
              </HStack>
            </Stack>
          </TabPanel>
          <TabPanel>
            <p>two!</p>
          </TabPanel>
          <TabPanel>
            <p>three!</p>
          </TabPanel>
          <TabPanel>
            <p>Encounter</p>
          </TabPanel>
          <TabPanel>
            <p>Lab tests</p>
          </TabPanel>
          <TabPanel>
            <p>Pharmacy</p>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </PatientProfileContainer>
  );
}
