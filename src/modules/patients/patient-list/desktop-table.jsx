import { CheckIcon, DeleteIcon } from "@chakra-ui/icons";
import {
  Badge,
  Button,
  ButtonGroup,
  Checkbox,
  Flex,
  HStack,
  IconButton,
  Stack,
  Text,
} from "@chakra-ui/react";
import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/table";
import check from "check-types";
import Router from "next/router";
import React, { useEffect, useState } from "react";
import FilterResults from "react-filter-search";
import { BiCalendarPlus } from "react-icons/bi";
import { BsFillPersonCheckFill } from "react-icons/bs";
import { MdDelete, MdMail } from "react-icons/md";
import { RiChatNewFill, RiChatNewLine } from "react-icons/ri";
import { VscCheck } from "react-icons/vsc";
import { useDispatch } from "react-redux";
import { openAppointmentModal } from "../../../redux/slices/commonSlice";
import { setSelectedPatient } from "../../../redux/slices/patientSlice";
import { calculateAge, formatDateToDDMMYY } from "../../../utils";

export default function DesktopTable({
  searchValue,
  patients,
  retainers,
  ...rest
}) {
  const dispatch = useDispatch();
  const [hoveredOnPatient, setHoveredOnPatient] = useState(null);
  const [selectedPatients, setSelectedPatients] = useState([]);
  const [selectAllPatients, setSelectAllPatients] = useState(false);

  const addOrRemoveFromSelectedPatients = (id) => {
    if (selectedPatients.find((patientId) => patientId == id)) {
      setSelectAllPatients(false);
      setSelectedPatients(
        [...selectedPatients].filter((patientId) => patientId != id)
      );
    } else {
      setSelectedPatients([...selectedPatients, id]);
    }
  };

  useEffect(() => {
    if (selectAllPatients) {
      setSelectedPatients([...patients].map(({ id }) => id));
    } else {
      setSelectedPatients([]);
    }
  }, [selectAllPatients]);

  return (
    <>
      {/* Desktop Table */}

      {check.nonEmptyArray(selectedPatients) && (
        <HStack fontSize="sm" spacing={8} color="gray.500" pl={4} pb={3}>
          <Checkbox
            isChecked={selectAllPatients}
            onChange={() => setSelectAllPatients(!selectAllPatients)}
          >
            Select All
          </Checkbox>
          <HStack spacing={0}>
            <IconButton
              onClick={() =>
                Router.push(
                  `/messages/new?selectedPatients=${encodeURIComponent(
                    JSON.stringify(selectedPatients)
                  )}`
                )
              }
              variant="default"
              icon={<MdMail />}
            />
            <IconButton variant="default" icon={<MdDelete />} />
            <IconButton variant="default" icon={<VscCheck />} />
          </HStack>
        </HStack>
      )}
      <Table variant="unstyled" size="sm" {...rest}>
        {check.emptyArray(selectedPatients) && (
          <Thead>
            <Tr color="gray.600" textTransform="initial" bg="gray.70">
              <Th pb={2}>
                <Checkbox
                  colorScheme="primary"
                  borderColor="primary.300"
                  isChecked={selectAllPatients}
                  onChange={() => setSelectAllPatients(!selectAllPatients)}
                ></Checkbox>
              </Th>
              <Th pb={2}>NAME</Th>
              <Th pb={2}>AGE</Th>
              <Th pb={2}>GENDER</Th>
              <Th pb={2}>SPONSOR</Th>
              <Th pb={2}>PHONE</Th>
              <Th pb={2}>DATE REG.</Th>
              <Th pb={2}>ACTIONS</Th>
            </Tr>
          </Thead>
        )}

        <Tbody onMouseLeave={() => setHoveredOnPatient(null)} color="gray.500">
          <FilterResults
            value={searchValue}
            data={[...patients].reverse()}
            renderResults={(results) =>
              check.emptyArray(results) ? (
                <Flex justify="center" w="full">
                  <Text wordBreak="break-word" textAlign="center">
                    No patient matches the search value
                  </Text>
                </Flex>
              ) : (
                results.map(
                  ({
                    id,
                    first_name,
                    last_name,
                    retainer_id,
                    dob,
                    gender,
                    email,
                    phone,
                    created_at,
                  }) => (
                    <Tr
                      onMouseEnter={() => setHoveredOnPatient(id)}
                      onClick={() => Router.push(`/patients/${id}`)}
                      cursor="pointer"
                      mb={3}
                      key={id}
                    >
                      <Td p={0}>
                        <Stack
                          onClick={(event) => event.stopPropagation()}
                          w="full"
                          align="center"
                          mb={2}
                          bg="white"
                          py={4}
                          px={3}
                          spacing={0}
                        >
                          <Checkbox
                            isChecked={Boolean(
                              [...selectedPatients].find(
                                (patientId) => patientId == id
                              )
                            )}
                            onChange={() => addOrRemoveFromSelectedPatients(id)}
                          />
                        </Stack>
                      </Td>
                      <Td p={0} isTruncated>
                        <Text mb={2} bg="white" py={4} px={3}>
                          {first_name} {last_name}
                        </Text>
                      </Td>
                      <Td p={0}>
                        <Text isTruncated mb={2} bg="white" py={4} px={3}>
                          {dob && <>{calculateAge(dob)} yrs</>}
                        </Text>
                      </Td>
                      <Td p={0}>
                        <Text
                          textTransform="capitalize"
                          mb={2}
                          bg="white"
                          py={4}
                          px={3}
                        >
                          {gender}
                        </Text>
                      </Td>
                      <Td p={0}>
                        <Text isTruncated mb={2} bg="white" py={4} px={3}>
                          {retainers.find(({ id }) => id === retainer_id)?.name}
                        </Text>
                      </Td>
                      <Td p={0}>
                        <Text mb={2} bg="white" py={4} px={3}>
                          {phone}
                        </Text>
                      </Td>

                      <Td p={0}>
                        <Text isTruncated mb={2} bg="white" py={4} px={3}>
                          {formatDateToDDMMYY(null, created_at)}
                        </Text>
                      </Td>

                      <Td p={0} isNumeric>
                        <HStack
                          onClick={(event) => event.stopPropagation()}
                          mb={2}
                          h={12}
                          bg="white"
                          py={2}
                          px={3}
                          w={24}
                          spacing={0}
                        >
                          {hoveredOnPatient == id && (
                            <>
                              <IconButton
                                colorScheme="primary"
                                variant="ghost"
                                size="sm"
                                icon={<BiCalendarPlus />}
                                onClick={(event) => {
                                  dispatch(
                                    setSelectedPatient(
                                      patients.find(
                                        (patient) => patient.id === id
                                      )
                                    )
                                  );
                                  dispatch(
                                    openAppointmentModal({
                                      checkIn: true,
                                    })
                                  );
                                }}
                              />
                              <IconButton
                                colorScheme="primary"
                                variant="ghost"
                                size="sm"
                                onClick={() =>
                                  Router.push(
                                    `/messages/new?selectedPatients=${encodeURIComponent(
                                      JSON.stringify([id])
                                    )}`
                                  )
                                }
                                icon={<RiChatNewFill />}
                              />

                              {/* <IconButton
                              colorScheme="primary"
                              variant="ghost"
                              size="sm"
                              icon={<RiEditLine />}
                            />
                            <IconButton
                              colorScheme="primary"
                              variant="ghost"
                              size="sm"
                              icon={<RiEditLine />}
                            /> */}
                            </>
                          )}
                        </HStack>
                      </Td>
                    </Tr>
                  )
                )
              )
            }
          />
        </Tbody>
      </Table>
    </>
  );
}
