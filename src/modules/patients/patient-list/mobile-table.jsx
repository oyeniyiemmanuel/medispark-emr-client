import {
  Badge,
  Box,
  Checkbox,
  Flex,
  HStack,
  IconButton,
  Stack,
  Text,
} from "@chakra-ui/react";
import check from "check-types";
import Router from "next/router";
import React from "react";
import FilterResults from "react-filter-search";
import { RiEditLine } from "react-icons/ri";
import { formatDateToDDMMYY } from "../../../utils";

export default function MobileTable({
  searchValue,
  patients,
  retainers,
  ...rest
}) {
  return (
    // Mobile Table
    <Stack spacing={4} {...rest}>
      <FilterResults
        value={searchValue}
        data={[...patients]}
        renderResults={(results) =>
          check.emptyArray(results) ? (
            <Flex justify="center" w="full">
              <Text wordBreak="break-word" textAlign="center">
                No patient matches the search value
              </Text>
            </Flex>
          ) : (
            results.map(
              ({ id, first_name, last_name, retainer_id, created_at }) => (
                <Box
                  key={id}
                  bg="white"
                  onClick={() => Router.push(`/patients/${id}`)}
                  cursor="pointer"
                  mb={8}
                >
                  <Flex>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="45%"
                    >
                      <Text
                        wordBreak="break-word"
                        fontSize="sm"
                        color="gray.500"
                      >
                        STACK
                      </Text>
                    </Box>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="55%"
                    >
                      <Text wordBreak="break-word" fontSize="sm">
                        <Stack onClick={(event) => event.stopPropagation()}>
                          <Checkbox />
                        </Stack>
                      </Text>
                    </Box>
                  </Flex>

                  <Flex>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="45%"
                    >
                      <Text
                        wordBreak="break-word"
                        fontSize="sm"
                        color="gray.500"
                      >
                        NAME
                      </Text>
                    </Box>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="55%"
                    >
                      <Text wordBreak="break-word" fontSize="sm">
                        {first_name} {last_name}
                      </Text>
                    </Box>
                  </Flex>
                  <Flex>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="45%"
                    >
                      <Text
                        wordBreak="break-word"
                        fontSize="sm"
                        color="gray.500"
                      >
                        HOSP NO.
                      </Text>
                    </Box>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="55%"
                    >
                      <Text wordBreak="break-word" fontSize="sm">
                        Hosp. No
                      </Text>
                    </Box>
                  </Flex>
                  <Flex>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="45%"
                    >
                      <Text
                        wordBreak="break-word"
                        fontSize="sm"
                        color="gray.500"
                      >
                        RETAINER
                      </Text>
                    </Box>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="55%"
                    >
                      <Text wordBreak="break-word" fontSize="sm">
                        {retainers.find(({ id }) => id === retainer_id)?.name}
                      </Text>
                    </Box>
                  </Flex>

                  <Flex>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="45%"
                    >
                      <Text
                        wordBreak="break-word"
                        fontSize="sm"
                        color="gray.500"
                      >
                        STATUS
                      </Text>
                    </Box>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="55%"
                    >
                      <Badge
                        w="fit-content"
                        textTransform="initial"
                        rounded={4}
                        fontWeight="normal"
                        colorScheme="primary"
                      >
                        New
                      </Badge>
                    </Box>
                  </Flex>

                  <Flex>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="45%"
                    >
                      <Text
                        wordBreak="break-word"
                        fontSize="sm"
                        color="gray.500"
                      >
                        DATE
                      </Text>
                    </Box>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="55%"
                    >
                      <Text wordBreak="break-word" fontSize="sm">
                        {formatDateToDDMMYY(null, created_at)}
                      </Text>
                    </Box>
                  </Flex>

                  <Flex>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="45%"
                    >
                      <Text
                        wordBreak="break-word"
                        fontSize="sm"
                        color="gray.500"
                      >
                        ADD CELL
                      </Text>
                    </Box>
                    <Box
                      border="1px solid"
                      borderColor="gray.200"
                      py={2}
                      px={3}
                      w="55%"
                    >
                      <HStack
                        onClick={(event) => event.stopPropagation()}
                        mb={2}
                        bg="white"
                        py={2}
                        px={3}
                        spacing={0}
                      >
                        <IconButton
                          colorScheme="primary"
                          variant="ghost"
                          size="sm"
                          icon={<RiEditLine />}
                        />
                        <IconButton
                          colorScheme="primary"
                          variant="ghost"
                          size="sm"
                          icon={<RiEditLine />}
                        />
                        <IconButton
                          colorScheme="primary"
                          variant="ghost"
                          size="sm"
                          icon={<RiEditLine />}
                        />
                      </HStack>
                    </Box>
                  </Flex>
                </Box>
              )
            )
          )
        }
      />
    </Stack>
  );
}
