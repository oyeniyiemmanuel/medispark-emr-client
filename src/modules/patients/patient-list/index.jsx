import { SearchIcon } from "@chakra-ui/icons";
import {
  Box,
  CloseButton,
  Flex,
  Heading,
  HStack,
  Input,
  InputGroup,
  InputLeftElement,
  Stack,
  Text,
  useMediaQuery,
  useToast,
} from "@chakra-ui/react";
import check from "check-types";
import moment from "moment";
import Router, { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";
import DashboardContainer from "../../../components/layouts/dashboard";
import { clearStates, fetchPatients } from "../../../redux/slices/patientSlice";
import { fetchRetainers } from "../../../redux/slices/retainerSlice";
import theme from "../../../theme";
import { separateWithComma, toastError } from "../../../utils";
import SendToDoctor from "../each-patient/send-to-doctor";
import DesktopTable from "./desktop-table";
import MobileTable from "./mobile-table";
import FilterResults from "react-filter-search";

export default function ShowAllPatients() {
  const [searchValue, setSearchValue] = useState("");
  const { userDetails } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const { retainers } = useSelector((state) => state.retainers);
  const { patients, loading, success, error } = useSelector(
    (state) => state.patients
  );
  const { query } = useRouter();
  const [isMobile] = useMediaQuery("(max-width: 767px)");

  const toast = useToast();
  useEffect(() => {
    //Fetch Patients
    check.emptyArray(patients) &&
      dispatch(
        fetchPatients({
          branch_id: userDetails.branch_id,
          organization_id: userDetails.organization_id,
        })
      );

    //Fetch Patient retainers
    check.emptyArray(retainers) &&
      dispatch(
        fetchRetainers({
          branch_id: userDetails.branch_id,
        })
      );
    if (query.searchQuery) {
      setSearchValue(query.searchQuery);
    }
  }, []);

  useEffect(() => {
    if (error && error.errorType === "FETCH_PATIENTS") {
      // If an error occurs while fetching patient,
      toast(toastError(error)); // Display Error Message
      dispatch(clearStates()); // Then clear stale error messages
    }
  }, [error]);

  const patientsRegisteredThisWeek = () => {
    const now = moment();
    return [...patients].filter(({ created_at }) => {
      var input = moment(created_at);
      var isThisWeek = now.isoWeek() == input.isoWeek();
      return isThisWeek;
    }).length;
  };

  return (
    <DashboardContainer>
      <Flex py={6} px="5%" bg="white" w="full" justify="center">
        <Flex
          justify="space-between"
          w="full"
          maxW="4xl"
          align="center"
          color="gray.600"
        >
          <Heading color="gray.500" fontSize="3xl">
            Registered Patient List
          </Heading>
          <CloseButton onClick={() => Router.push("/")} />
        </Flex>
      </Flex>
      <Flex px="5%" pb={8} bg="white" w="full" justify="center">
        <Stack w="full" maxW="4xl" align="center">
          <InputGroup size="lg">
            <InputLeftElement
              pointerEvents="none"
              children={<SearchIcon color="gray.300" />}
            />
            <Input
              variant="filled"
              type="text"
              value={searchValue}
              onChange={(event) => setSearchValue(event.target.value)}
              placeholder="Search Patient"
            />
          </InputGroup>
        </Stack>
      </Flex>
      <Flex pt={6} pb={20} px="5%" w="full" justify="center">
        <Box w="full" maxW="4xl">
          {loading === "FETCH_PATIENTS" && ( // While loading patients, show spinner
            <Flex w="full" justify="center">
              <BeatLoader size={8} color={theme.colors.primary[500]} />
            </Flex>
          )}
          <Stack pb={6}>
            {!searchValue && (
              <HStack spacing={4}>
                <Box
                  border="1px solid lightgrey"
                  rounded={8}
                  bg="white"
                  py={3}
                  minW={40}
                  fontSize="sm"
                  fontWeight="bold"
                  color="gray.500"
                  px={4}
                >
                  <Text>
                    <Box as="span" color="primary.500">
                      {separateWithComma(patients?.length)}
                    </Box>
                    &nbsp;Total
                  </Text>
                  <Text>Patients</Text>
                </Box>
                <Box
                  border="1px solid lightgrey"
                  rounded={8}
                  bg="white"
                  py={3}
                  minW={40}
                  fontSize="sm"
                  fontWeight="bold"
                  color="gray.500"
                  px={4}
                >
                  <Text>
                    <Box as="span" color="primary.500">
                      {separateWithComma(patientsRegisteredThisWeek())}
                    </Box>
                    &nbsp;Patients
                  </Text>
                  <Text fontWeight="normal">Registered This week</Text>
                </Box>
              </HStack>
            )}

            {searchValue && (
              <FilterResults
                value={searchValue}
                data={[...patients]}
                renderResults={(results) => (
                  <Text fontSize="lg" color="gray.600">
                    <Box fontWeight="bold" as="span" color="primary.500">
                      {separateWithComma(results?.length)}
                    </Box>{" "}
                    Patients Found
                  </Text>
                )}
              />
            )}
          </Stack>

          {isMobile ? (
            <MobileTable
              searchValue={searchValue}
              patients={patients}
              retainers={retainers}
            />
          ) : (
            <DesktopTable
              searchValue={searchValue}
              patients={patients}
              retainers={retainers}
            />
          )}
        </Box>
      </Flex>
      <SendToDoctor />
    </DashboardContainer>
  );
}
