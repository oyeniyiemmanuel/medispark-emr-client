import { EmailIcon, LockIcon } from "@chakra-ui/icons";
import {
  Button,
  Flex,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  Link as ChakraLink,
  Stack,
  Text,
  useToast,
} from "@chakra-ui/react";
import Axios from "axios";
import Link from "next/link";
import Router from "next/router";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { ClipLoader } from "react-spinners";
import AuthLayout from "../../components/layouts/auth-layout";
import { setUserDetails } from "../../redux/slices/userSlice";
import { BASE_API_URL, toastError } from "../../utils";

export default function LogInModule() {
  const toast = useToast();
  const dispatch = useDispatch();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const onSubmitSignIn = (event) => {
    event.preventDefault();
    setIsLoading(true);
    Axios.post(BASE_API_URL + "/login", {
      organization_id: 1,
      branch_id: 2,
      identifier: email,
      password,
    })
      .then(({ data }) => {
        localStorage.setItem(
          "medispark_acccess_token",
          data?.data?.access_token
        );
        dispatch(setUserDetails(data?.data?.user));
        Router.push("/");
      })
      .catch(({ response }) => {
        toast(toastError(null, "An error occurred", response?.data?.errors[0]));
        setIsLoading(false);
        console.log(response);
      });
  };
  return (
    <AuthLayout active={1}>
      <Stack maxW="429px" spacing={5}>
        <Stack spacing={0}>
          <Heading
            fontSize={["2xl", "2xl", "2xl", "3xl", "3xl"]}
            color="primary.500"
          >
            Welcome Back,
          </Heading>
        </Stack>
        <Text fontSize={["md", "md", "md", "md", "lg"]} color="gray.500">
          Unlock the mysteries of the medical world, the power of digital
          medical records, at the tip of your fingers.
        </Text>
        <form onSubmit={onSubmitSignIn} action="submit">
          <Stack spacing={4}>
            <InputGroup size="lg">
              <InputLeftElement
                pointerEvents="none"
                children={<EmailIcon color="gray.300" />}
              />
              <Input
                size="lg"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                variant="filled"
                placeholder="Username or Email"
                isRequired
              />
            </InputGroup>
            <InputGroup size="lg">
              <InputLeftElement
                pointerEvents="none"
                children={<LockIcon color="gray.300" />}
              />
              <Input
                size="lg"
                variant="filled"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                type="password"
                placeholder="Password"
                isRequired
              />
            </InputGroup>
            <Flex justify="center">
              <Link href="/forgot-password">
                <ChakraLink color="gray.400" fontSize="sm">
                  Forgot Password?
                </ChakraLink>
              </Link>
            </Flex>
            <Button
              type="submit"
              rounded={4}
              isLoading={isLoading}
              spinner={<ClipLoader size={18} color="white" />}
              loadingText="Signing in...."
              h={12}
              fontWeight="normal"
              colorScheme="primary"
              px={[4, 8]}
            >
              Sign in
            </Button>
          </Stack>
        </form>
      </Stack>
    </AuthLayout>
  );
}
