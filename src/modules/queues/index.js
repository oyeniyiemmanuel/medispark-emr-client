import {
  Box,
  CloseButton,
  Flex,
  Heading,
  HStack,
  IconButton,
} from "@chakra-ui/react";
import Router from "next/router";
import React from "react";
import { MdPrint, MdShare } from "react-icons/md";

export default function QueuesView() {
  return (
    <Box>
      <Flex
        py={5}
        pr={4}
        pl={12}
        bg="white"
        w="full"
        justify="space-between"
        align="center"
        color="gray.600"
      >
        <HStack>
          <Heading fontSize="3xl">Queues</Heading>
        </HStack>
        <HStack>
          <IconButton icon={<MdShare />} variant="ghost" />
          <IconButton icon={<MdPrint />} variant="ghost" />
          <CloseButton onClick={() => Router.push("/")} />
        </HStack>
      </Flex>
    </Box>
  );
}
