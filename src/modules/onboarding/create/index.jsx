import React, { useState } from "react";
import ChooseOrganizationDetails from "./organization-details";
import ChooseOrganizationType from "./organization-type";

export default function CreateOrganization() {
  const [stage, setStage] = useState(0);

  switch (stage) {
    case 0: //Progress to different stages of the onboarding process
      //Once a step is completed, the stage increments by 1
      return <ChooseOrganizationType stage={stage} setStage={setStage} />;
    case 1:
      return <ChooseOrganizationDetails stage={stage} setStage={setStage} />;

    default:
      return <ChooseOrganizationType stage={stage} setStage={setStage} />;
  }
}
