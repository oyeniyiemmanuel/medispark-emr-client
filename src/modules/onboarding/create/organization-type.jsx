import {
  Box,
  Button,
  Flex,
  Heading,
  Stack,
  Text,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import React from "react";
import OnboardingLayout from "../../../components/layouts/onboarding-layout";
import EMRLogo from "../../../components/logo";

export default function ChooseOrganizationType({ setStage }) {
  return (
    <OnboardingLayout>
      <Stack maxW="592px" spacing={8}>
        <EMRLogo d={["block", "block", "none"]} mb={4} mt={[4, 8]} />

        <Heading color="primary.500">Choose the type of organization</Heading>
        <Text color="gray.500" fontSize="lg">
          Unlock the mysteries of the medical world, the power of digital
          medical records, at the tip of your fingers.
        </Text>
        <Wrap flexWrap="wrap" spacing={4}>
          <WrapItem>
            <Box
              w={["150px", "170px"]}
              h="200px"
              bg="gray.200"
              rounded={8}
            ></Box>
          </WrapItem>
          <WrapItem>
            <Box
              w={["150px", "170px"]}
              h="200px"
              bg="gray.200"
              rounded={8}
            ></Box>
          </WrapItem>
        </Wrap>

        <Flex justify="flex-end">
          <Button
            onClick={() => setStage(1)} //Move to the Next Stage
            w={40}
            fontWeight="normal"
            colorScheme="primary"
            size="lg"
          >
            Proceed
          </Button>
        </Flex>
      </Stack>
    </OnboardingLayout>
  );
}
