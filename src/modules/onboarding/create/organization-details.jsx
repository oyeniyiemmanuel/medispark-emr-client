import { InfoIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  useDisclosure,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import React from "react";
import OnboardingLayout from "../../../components/layouts/onboarding-layout";
import EMRLogo from "../../../components/logo";

export default function ChooseOrganizationDetails({ active, stage, setStage }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onSubmitOrganizationDetails = (event) => {
    event.preventDefault();

    // setStage(2); //Move forward
    onOpen();
  };
  return (
    <OnboardingLayout>
      <form onSubmit={onSubmitOrganizationDetails} action="submit">
        <Stack maxW="592px" spacing={8}>
          <EMRLogo d={["block", "block", "none"]} mb={4} mt={[4, 8]} />

          <Heading color="primary.500">Organization Details</Heading>
          <Text color="gray.500" fontSize="lg">
            Details about your organization and its incorporation/legitimization
          </Text>
          <Stack spacing={4}>
            <Stack>
              <Text color="gray.600" fontSize="lg">
                Company Name
              </Text>
              <Input
                variant="filled"
                size="lg"
                isRequired
                placeholder="Example Hospitals Limited"
              />
              <HStack pl={8} fontSize="sm" color="primary.500" align="center">
                <InfoIcon />
                <Text>
                  This is the name that will be registered on your Medispark
                  profile
                </Text>
              </HStack>
            </Stack>

            <Stack>
              <Text color="gray.600" fontSize="lg">
                RC Number
              </Text>
              <Input
                variant="filled"
                size="lg"
                isRequired
                placeholder="RC:000012"
              />
              <HStack pl={8} fontSize="sm" color="primary.500" align="center">
                <InfoIcon />
                <Text>
                  This is the registration number of your organization
                </Text>
              </HStack>
            </Stack>
          </Stack>

          <Flex pt={10} pb={10} justify="space-between">
            <Button
              onClick={() => setStage(0)} // Go back
              variant="link"
              colorScheme="primary"
              size="lg"
            >
              Go Back
            </Button>
            <Button
              w={40}
              type="submit"
              fontWeight="normal"
              colorScheme="primary"
              size="lg"
            >
              Proceed
            </Button>
          </Flex>
        </Stack>
      </form>
      <Modal
        closeOnOverlayClick={false}
        isCentered
        size="lg"
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay bgColor="rgba(255, 255, 255, 0.7)" />
        <ModalContent rounded={8}>
          <ModalHeader>Modal Title</ModalHeader>
          <ModalCloseButton variant="ghost" colorScheme="primary" />
          <ModalBody>Hello there</ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={onClose}>
              Close
            </Button>
            <Button variant="ghost">Secondary Action</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </OnboardingLayout>
  );
}
