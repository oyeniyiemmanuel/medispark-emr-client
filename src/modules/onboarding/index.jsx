import { AddIcon } from "@chakra-ui/icons";
import { Button, Heading, Stack, Text } from "@chakra-ui/react";
import Router from "next/router";
import React from "react";
import AuthLayout from "../../components/layouts/auth-layout";

export default function ChooseOnboardingOption({ active, ...rest }) {
  return (
    <AuthLayout>
      <Stack maxW="429px" spacing={5}>
        <Stack spacing={0}>
          <Heading
            fontSize={["2xl", "2xl", "2xl", "3xl", "3xl"]}
            color="gray.600"
          >
            Welcome to MediSpark EMR
          </Heading>
          <Heading
            fontSize={["2xl", "2xl", "2xl", "3xl", "3xl"]}
            color="primary.500"
          >
            Your own Organization
          </Heading>
        </Stack>
        <Text fontSize={["md", "md", "md", "md", "lg"]} color="gray.500">
          The welcome speech is what allures the attendees to either listen and
          take interest… or not want to participate at all. Given that, welcome
          speeches are really important and should be long enough to address
          everyone
        </Text>
        <Stack spacing={4}>
          <Button
            rounded={4}
            h={12}
            textAlign="left"
            rightIcon={<AddIcon />}
            colorScheme="primary"
            justifyContent="space-between"
            px={[4, 8]}
            shadow="0px 6px 6px rgba(27, 119, 186, 0.41)"
          >
            Join an existing Organization
          </Button>
          <Button
            rounded={4}
            h={12}
            textAlign="left"
            onClick={() => Router.push("/register/new")}
            rightIcon={<AddIcon />}
            colorScheme="primary"
            variant="outline"
            justifyContent="space-between"
            px={[4, 8]}
          >
            Create a new Organization
          </Button>
        </Stack>
      </Stack>
    </AuthLayout>
  );
}
