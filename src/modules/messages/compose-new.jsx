import { Button } from "@chakra-ui/button";
import { CloseButton } from "@chakra-ui/close-button";
import { Input } from "@chakra-ui/input";
import {
  Box,
  Divider,
  Flex,
  Heading,
  HStack,
  Stack,
  Text,
} from "@chakra-ui/layout";
import { useToast } from "@chakra-ui/toast";
import Router, { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import TextEditor from "../../components/text-editor";
import UploadFileForPatient from "../patients/each-patient/uploads/new-upload";
import appTheme from "../../theme";
import { Avatar } from "@chakra-ui/avatar";
import { createMessage, messageActions } from "../../redux/slices/messageSlice";
import check from "check-types";
import { toastError, toastSuccess } from "../../utils";
import CustomSelect from "../../components/multi-select";

export default function ComposeMessageView() {
  const { query } = useRouter();
  const initialMessageDetailState = {
    // Set Initial Values of input fields
    subject: "",
    body: "",
  };
  const [recipients, setRecipients] = useState([]);
  const { userDetails } = useSelector((state) => state.user);
  const [newMessage, setNewMessage] = useState(
    initialMessageDetailState // Store values in state
  );
  const { patients } = useSelector((state) => state.patients);
  const messages = useSelector((state) => state.messages);
  const handleChange = (event) => {
    const { name, value } = event.target;
    setNewMessage({ ...newMessage, [name]: value }); // onChange handler
  };
  const dispatch = useDispatch();
  const toast = useToast();
  const [document, setDocument] = useState(null);
  const [toUploadDocument, setToUploadDocument] = useState(false);
  const { subject, body } = newMessage;

  useEffect(() => {
    if (query.selectedPatients) {
      setRecipients(JSON.parse(query.selectedPatients));
    }
  }, [query]);

  const optionComponent = ({ full_name, avatar, email, id }) => ({
    value: id,
    label: (
      <Stack spacing={3} align="center" isInline>
        <Avatar size="sm" src={avatar?.file_path} name={full_name} boxSize={8} />
        <Box fontSize="sm" color="gray.600">
          <Text fontWeight="bold">{full_name}</Text>
          <Text>{email}</Text>
        </Box>
      </Stack>
    ),
  });

  const options = [...patients].reverse().map((patient) => {
    return optionComponent(patient);
  });

  const onSubmitSendMessage = (event) => {
    event.preventDefault();
    if (
      !body
        .replace(/class="[^"]*"/, "")
        .replace(/(<([^>]+)>)/gi, "")
        .replace(/ /g, "")
    ) {
      toast({
        title: "You haven't written anything yet...",
        status: "error",
        duration: 4000,
        position: "bottom-left",
        isClosable: true,
      });
    } else if (check.emptyArray(recipients)) {
      toast({
        title: "You need to add recipients...",
        status: "error",
        duration: 4000,
        position: "bottom-left",
        isClosable: true,
      });
    } else {
      dispatch(
        createMessage({
          organization_id: userDetails.organization_id,
          branch_id: userDetails.branch_id,
          mail_details: newMessage,
          users: recipients,
        })
      );
    }
  };

  useEffect(() => {
    if (messages.error && messages.error.errorType === "CREATE_MESSAGE") {
      // If an messages.error occurs while creating patient,
      toast(toastError(messages.error)); // Display Error Message
      dispatch(messageActions.clearStates()); // Then clear stale messages.error messages
    }
  }, [messages.error]);

  useEffect(() => {
    if (messages.success === "CREATE_MESSAGE") {
      // If patient is created successfully,
      toast(
        toastSuccess(
          `Message Sent Successfully${document ? ". Uploading file..." : ""}`
        )
      ); // Display Success message
      dispatch(messageActions.clearStates()); // Then clear stale error messages
      if (document) {
        setToUploadDocument(true);
      }
      Router.push("/");
    }
  }, [messages.success]);

  return (
    <Box as="form" onSubmit={onSubmitSendMessage}>
      <Flex py={4} px="5%" bg="white" w="full" justify="center">
        <Flex justify="space-between" w="full" align="center" color="gray.600">
          <Heading color="gray.500" fontSize="3xl">
            Messages
          </Heading>
        </Flex>
      </Flex>
      <Flex justify="center" my={8} w="full" px="5%">
        <Stack
          maxW="4xl"
          color="gray.500"
          w="full"
          bg="white"
          border="1px solid lightgrey"
          rounded={8}
          py={4}
          px={2}
        >
          <HStack justify="space-between" px={6}>
            <Heading fontSize="xl">Compose a Message</Heading>
            <CloseButton onClick={() => Router.push("/")} />
          </HStack>
          <Divider />
          <HStack spacing={6} px={6}>
            <Text fontWeight="bold">To:</Text>
            <Box w="full">
              <CustomSelect
                value={[...patients]
                  .filter(({ id }) => recipients.includes(id))
                  .map((patient) => {
                    return optionComponent(patient);
                  })}
                placeholder="Recipient Names..."
                isMulti
                options={options}
                onChange={(data) => {
                  setRecipients(data.map(({ value }) => value));
                }}
              />
            </Box>
          </HStack>
          <Divider />
          <HStack px={6}>
            <Text fontWeight="bold">Subject:</Text>
            <Input
              name="subject"
              value={subject}
              onChange={handleChange}
              size="sm"
              px={3}
              fontSize="md"
              isRequired
              variant="flushed"
              placeholder="A New Message From Your Esteemed Customer"
            />
          </HStack>
          <HStack align="start" minH={64} px={4}>
            <TextEditor
              onChange={(html) => setNewMessage({ ...newMessage, body: html })}
              value={body}
              placeholder="Write your message here"
            />
          </HStack>
          <Divider />
          <HStack justify="flex-end" px={6}>
            <UploadFileForPatient
              document={document}
              setDocument={setDocument}
              toUploadDocument={toUploadDocument}
              setToUploadDocument={setToUploadDocument}
            />
            <Button
              w={32}
              type="submit"
              isLoading={messages.loading === "CREATE_MESSAGE"}
              loadingText="Sending..."
              colorScheme="primary"
            >
              Send
            </Button>
          </HStack>
        </Stack>
      </Flex>
    </Box>
  );
}
