import { configureStore } from "@reduxjs/toolkit";

import retainerReducer from "./slices/retainerSlice.js";
import patientReducer from "./slices/patientSlice.js";
import queueReducer from "./slices/queueSlice.js";
import serviceReducer from "./slices/serviceSlice.js";
import billReducer from "./slices/billSlice.js";
import messageReducer from "./slices/messageSlice.js";
import dispensaryReducer from "./slices/dispensarySlice.js";
import prescriptionReducer from "./slices/prescriptionSlice.js";
import staffReducer from "./slices/staffSlice.js";
import appointmentReducer from "./slices/appointmentSlice.js";
import documentationReducer from "./slices/documentationSlice.js";
import encounterReducer from "./slices/encounterSlice.js";
import productReducer from "./slices/productSlice.js";
import userReducer from "./slices/userSlice.js";
import organizationReducer from "./slices/organizationSlice.js";
import commonReducer from "./slices/commonSlice.js";
//Import individual slices and configure store
export default configureStore({
  reducer: {
    products: productReducer,
    user: userReducer,
    organization: organizationReducer,
    common: commonReducer,
    patients: patientReducer,
    retainers: retainerReducer,
    services: serviceReducer,
    bills: billReducer,
    messages: messageReducer,
    dispensaries: dispensaryReducer,
    prescriptions: prescriptionReducer,
    queues: queueReducer,
    staff: staffReducer,
    appointments: appointmentReducer,
    documentations: documentationReducer,
    encounters: encounterReducer,
  },
  devTools: true,
});
