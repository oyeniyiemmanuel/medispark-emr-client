import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchVitals = createAsyncThunk(
  "vitals/fetchVitals",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/vital/get-vitals`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const recordVitals = createAsyncThunk(
  "vitals/recordVitals",
  async (recordPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/clinic/vitals/new`,
        recordPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const recordNote = createAsyncThunk(
  "notes/recordNote",
  async (recordPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/clinic/note/new`,
        recordPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editNote = createAsyncThunk(
  "notes/editNote",
  async (editPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/clinic/note/edit`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editVital = createAsyncThunk(
  "vitals/editVital",
  async (editPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/clinic/vitals/edit`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteVital = createAsyncThunk(
  "vitals/deleteVital",
  async (vitalId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(`${BASE_API_URL}/church/vitals/${vitalId}/`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return vitalId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const documentationSlice = createSlice({
  name: "documentation",
  initialState: {
    vitals: [],
    notes: [],
    newestNote: {},
    newestVital: {},
    editedNote: {},
    editedVital: {},
    loading: "FETCH_VITALS",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchVitals.pending]: (state) => {
      state.vitals = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_VITALS";
    },
    [fetchVitals.fulfilled]: (state, action) => {
      state.success = "FETCH_VITALS";
      state.vitals = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchVitals.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_VITALS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [recordVitals.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "RECORD_VITAL";
    },
    [recordVitals.fulfilled]: (state, action) => {
      state.success = "RECORD_VITAL";
      state.newestVital = action.payload.data;
      delete state.loading;
      delete state.error;
    },
    [recordVitals.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "RECORD_VITAL",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [recordNote.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "RECORD_NOTE";
    },
    [recordNote.fulfilled]: (state, action) => {
      state.success = "RECORD_NOTE";
      state.newestNote = action.payload.data;
      delete state.loading;
      delete state.error;
    },
    [recordNote.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "RECORD_NOTE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editNote.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_NOTE";
    },
    [editNote.fulfilled]: (state, action) => {
      state.success = "EDIT_NOTE";
      state.editedNote = action.payload.data;
      delete state.loading;
      delete state.error;
    },
    [editNote.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_NOTE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editVital.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_VITAL";
    },
    [editVital.fulfilled]: (state, action) => {
      state.success = "EDIT_VITAL";
      state.editedVital = action.payload.data;

      // const vital = state.vitals.find(
      //   (vital) => vital.id === action.payload.id
      // );
      // // delete state.tempNote;
      // Object.assign(vital, action.payload);
      // // state.vitals = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editVital.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_VITAL",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteVital.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_VITAL";
      const position = state.vitals.findIndex(
        (vital) => vital.id === action.meta.arg
      );
      state.backupVital = Object.assign({}, state.vitals[position]);
      state.backupPosition = position;
    },
    [deleteVital.fulfilled]: (state) => {
      state.success = "DELETE_VITAL";
      state.vitals.splice(state.backupPosition, 1);
      delete state.backupVital;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteVital.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_VITAL",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupVital;
      delete state.loading;
    },
  },
});
export const { clearStates } = documentationSlice.actions;
export const documentationActions = documentationSlice.actions;
export default documentationSlice.reducer;
