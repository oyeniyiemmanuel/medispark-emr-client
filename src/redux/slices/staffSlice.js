import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchStaff = createAsyncThunk(
  "staff/fetchStaff",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/gateway/staff/get-staffs`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const createStaff = createAsyncThunk(
  "staff/createStaff",
  async (createPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/billing/staff/new`,
        createPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editStaff = createAsyncThunk(
  "staff/editStaff",
  async (editPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.put(
        `${BASE_API_URL}/church/staff/${editPayload.id}/`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteStaff = createAsyncThunk(
  "staff/deleteStaff",
  async (staffId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(`${BASE_API_URL}/church/staff/${staffId}/`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return staffId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const staffSlice = createSlice({
  name: "staff",
  initialState: {
    staff: [],
    loading: "FETCH_STAFF",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchStaff.pending]: (state) => {
      state.staff = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_STAFF";
    },
    [fetchStaff.fulfilled]: (state, action) => {
      state.success = "FETCH_STAFF";
      state.staff = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchStaff.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_STAFF",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [createStaff.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CREATE_STAFF";
    },
    [createStaff.fulfilled]: (state, action) => {
      state.success = "CREATE_STAFF";
      state.staff.push(action.payload);
      delete state.loading;
      delete state.error;
    },
    [createStaff.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CREATE_STAFF",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editStaff.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_STAFF";
    },
    [editStaff.fulfilled]: (state, action) => {
      state.success = "EDIT_STAFF";
      const staff = state.staff.find((staff) => staff.id === action.payload.id);
      // delete state.tempNote;
      Object.assign(staff, action.payload);
      // state.staff = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editStaff.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_STAFF",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteStaff.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_STAFF";
      const position = state.staff.findIndex(
        (staff) => staff.id === action.meta.arg
      );
      state.backupStaff = Object.assign({}, state.staff[position]);
      state.backupPosition = position;
    },
    [deleteStaff.fulfilled]: (state) => {
      state.success = "DELETE_STAFF";
      state.staff.splice(state.backupPosition, 1);
      delete state.backupStaff;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteStaff.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_STAFF",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupStaff;
      delete state.loading;
    },
  },
});
export const { clearStates } = staffSlice.actions;
export const staffActions = staffSlice.actions;
export default staffSlice.reducer;
