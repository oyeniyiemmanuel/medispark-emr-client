import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const uploadFile = createAsyncThunk(
  "common/uploadFile",
  async ({ formData, service }, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.post(`${BASE_API_URL}/${service}/file/upload`, formData, {
        headers: {
          "content-type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const commonSlice = createSlice({
  name: "common",
  initialState: {
    countries: [],
    appointmentModal: {
      isOpen: false,
      existingAppointmentDetails: null,
      checkIn: false,
    },
    popup: {
      isOpen: false,
      popupId: null,
      isNew: false,
      popupType: null,
    },
    connectedChannels: {
      consultation: false,
      billing: false,
      procedure: false,
      dispensary: false,
    },
    uploadedFile: null,
    loading: "",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },

    setEcho: (state, { payload }) => {
      state.echo = payload;
    },

    openAppointmentModal: (state, { payload }) => {
      Object.assign(state.appointmentModal, {
        isOpen: true,
        checkIn: payload?.checkIn,
        existingAppointmentDetails: payload?.existingAppointmentDetails,
        choosePatient: payload?.choosePatient,
      });
    },

    closeAppointmentModal: (state, { payload }) => {
      Object.assign(state.appointmentModal, {
        isOpen: false,
        existingAppointmentDetails: null,
        checkIn: false,
        choosePatient: false,
      });
    },

    openPopup: (state, { payload }) => {
      Object.assign(state.popup, {
        isOpen: true,
        popupId: payload?.popupId,
        isNew: payload?.isNew,
        popupType: payload?.popupType,
      });
    },

    closePopup: (state, { payload }) => {
      Object.assign(state.popup, {
        isOpen: false,
        popupId: null,
        isNew: false,
        popupType: null,
      });
    },

    setConnectedChannels: (state, { payload }) => {
      Object.assign(state.connectedChannels, {
        ...state.connectedChannels,
        ...payload,
      });
    },
  },
  extraReducers: {
    [uploadFile.pending]: (state) => {
      state.uploadedFile = null;
      delete state.error;
      delete state.success;
      state.loading = "UPLOAD_FILE";
    },
    [uploadFile.fulfilled]: (state, action) => {
      state.success = "UPLOAD_FILE";
      state.uploadedFile = action.payload;
      delete state.loading;
      delete state.error;
    },
    [uploadFile.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "UPLOAD_FILE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },
  },
});
export const {
  clearStates,
  openAppointmentModal,
  closeAppointmentModal,
  openPopup,
  closePopup,
  setConnectedChannels,
  setEcho,
} = commonSlice.actions;

export default commonSlice.reducer;
export const commonActions = commonSlice.actions;
