import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchBills = createAsyncThunk(
  "bills/fetchBills",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/bill/get-bills`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const fetchSelectedInvoice = createAsyncThunk(
  "bills/fetchSelectedInvoice",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/service/get-invoice`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const payBill = createAsyncThunk(
  "bills/payBill",
  async (payPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/billing/payment/new-payment`,
        payPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const addPaymentToInvoice = createAsyncThunk(
  "bills/addPaymentToInvoice",
  async (paymentPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/billing/payment/extra-to-invoice`,
        paymentPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editBill = createAsyncThunk(
  "bills/editBill",
  async (editPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.put(
        `${BASE_API_URL}/church/bills/${editPayload.id}/`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteBill = createAsyncThunk(
  "bills/deleteBill",
  async (billId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(`${BASE_API_URL}/church/bills/${billId}/`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return billId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const billSlice = createSlice({
  name: "bills",
  initialState: {
    bills: [],
    newestPaidBill: {},
    selectedInvoice: {},
    loading: "FETCH_BILLS",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
    removeSelectedInvoice: (state, { payload }) => {
      state.selectedInvoice = {};
    },
  },
  extraReducers: {
    [fetchBills.pending]: (state) => {
      state.bills = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_BILLS";
    },
    [fetchBills.fulfilled]: (state, action) => {
      state.success = "FETCH_BILLS";
      state.bills = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchBills.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_BILLS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },
    [fetchSelectedInvoice.pending]: (state) => {
      state.selectedInvoice = {};
      delete state.error;
      delete state.success;
      state.loading = "FETCH_SELECTED_INVOICE";
    },
    [fetchSelectedInvoice.fulfilled]: (state, action) => {
      state.success = "FETCH_SELECTED_INVOICE";
      state.selectedInvoice = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchSelectedInvoice.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_SELECTED_INVOICE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },
    [payBill.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "PAY_BILL";
    },
    [payBill.fulfilled]: (state, action) => {
      state.success = "PAY_BILL";
      state.newestPaidBill = action.payload?.data;
      delete state.loading;
      delete state.error;
    },
    [payBill.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "PAY_BILL",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [addPaymentToInvoice.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "ADD_PAYMENT_TO_INVOICE";
    },
    [addPaymentToInvoice.fulfilled]: (state, action) => {
      state.success = "ADD_PAYMENT_TO_INVOICE";
      state.newestPaidBill = action.payload?.data;
      delete state.loading;
      delete state.error;
    },
    [addPaymentToInvoice.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "ADD_PAYMENT_TO_INVOICE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editBill.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_BILL";
    },
    [editBill.fulfilled]: (state, action) => {
      state.success = "EDIT_BILL";
      const bill = state.bills.find((bill) => bill.id === action.payload.id);
      // delete state.tempNote;
      Object.assign(bill, action.payload);
      // state.bills = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editBill.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_BILL",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteBill.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_BILL";
      const position = state.bills.findIndex(
        (bill) => bill.id === action.meta.arg
      );
      state.backupBill = Object.assign({}, state.bills[position]);
      state.backupPosition = position;
    },
    [deleteBill.fulfilled]: (state) => {
      state.success = "DELETE_BILL";
      state.bills.splice(state.backupPosition, 1);
      delete state.backupBill;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteBill.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_BILL",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupBill;
      delete state.loading;
    },
  },
});
export const { clearStates } = billSlice.actions;
export const billActions = billSlice.actions;
export default billSlice.reducer;
