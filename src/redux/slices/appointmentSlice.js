import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchAppointments = createAsyncThunk(
  "appointments/fetchAppointments",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(
        `${BASE_API_URL}/billing/appointment/get-appointments`,
        {
          params: fetchPayload,
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const createAppointment = createAsyncThunk(
  "appointments/createAppointment",
  async (createPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.post(
        `${BASE_API_URL}/billing/appointment/add-to-user`,
        createPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const checkInPatient = createAsyncThunk(
  "patients/checkInPatient",
  async (sendPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.post(
        `${BASE_API_URL}/billing/service/add-to-user`,
        sendPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editAppointment = createAsyncThunk(
  "appointments/editAppointment",
  async (editPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.put(
        `${BASE_API_URL}/church/appointments/${editPayload.id}/`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteAppointment = createAsyncThunk(
  "appointments/deleteAppointment",
  async (appointmentId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(
        `${BASE_API_URL}/church/appointments/${appointmentId}/`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return appointmentId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const appointmentSlice = createSlice({
  name: "appointments",
  initialState: {
    appointments: [],
    newestCheckIn: {},
    loading: "FETCH_APPOINTMENTS",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchAppointments.pending]: (state) => {
      state.appointments = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_APPOINTMENTS";
    },
    [fetchAppointments.fulfilled]: (state, action) => {
      state.success = "FETCH_APPOINTMENTS";
      state.appointments = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchAppointments.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_APPOINTMENTS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [createAppointment.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CREATE_APPOINTMENT";
    },
    [createAppointment.fulfilled]: (state, action) => {
      state.success = "CREATE_APPOINTMENT";
      state.appointments.push(action.payload);
      delete state.loading;
      delete state.error;
    },
    [createAppointment.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CREATE_APPOINTMENT",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [checkInPatient.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CHECK_IN_PATIENT";
    },
    [checkInPatient.fulfilled]: (state, action) => {
      state.success = "CHECK_IN_PATIENT";
      state.newestCheckIn = action.payload;
      delete state.loading;
      delete state.error;
    },
    [checkInPatient.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CHECK_IN_PATIENT",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editAppointment.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_APPOINTMENT";
    },
    [editAppointment.fulfilled]: (state, action) => {
      state.success = "EDIT_APPOINTMENT";
      const appointment = state.appointments.find(
        (appointment) => appointment.id === action.payload.id
      );
      // delete state.tempNote;
      Object.assign(appointment, action.payload);
      // state.appointments = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editAppointment.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_APPOINTMENT",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteAppointment.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_APPOINTMENT";
      const position = state.appointments.findIndex(
        (appointment) => appointment.id === action.meta.arg
      );
      state.backupAppointment = Object.assign({}, state.appointments[position]);
      state.backupPosition = position;
    },
    [deleteAppointment.fulfilled]: (state) => {
      state.success = "DELETE_APPOINTMENT";
      state.appointments.splice(state.backupPosition, 1);
      delete state.backupAppointment;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteAppointment.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_APPOINTMENT",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupAppointment;
      delete state.loading;
    },
  },
});
export const { clearStates } = appointmentSlice.actions;
export const appointmentActions = appointmentSlice.actions;
export default appointmentSlice.reducer;
