import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL, findInArray } from "../../utils";

export const fetchProducts = createAsyncThunk(
  "products/fetchProducts",
  async ({ searchValue, advancedSearchQuery }, thunkAPI) => {
    try {
      const { data } = await Axios.get(
        `${BASE_API_URL}/product/sub-products/?name=${searchValue}${advancedSearchQuery}`
      );
      return data;
    } catch ({ response }) {
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const fetchAllProducts = createAsyncThunk(
  "products/fetchAllProducts",
  async (query, thunkAPI) => {
    try {
      const { data } = await Axios.get(`${BASE_API_URL}/product/sub-products/`);
      return data;
    } catch ({ response }) {
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const fetchCategories = createAsyncThunk(
  "products/fetchCategories",
  async (query, thunkAPI) => {
    try {
      const { data } = await Axios.get(`${BASE_API_URL}/product/category/`);
      return data.results;
    } catch ({ response }) {
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const activateProduct = createAsyncThunk(
  "suppliers/activateProduct",
  async (id, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/product/sub-products/${id}/activate-single/`,
        null,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      console.log(data);
      return id;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const fetchUserProducts = createAsyncThunk(
  "products/fetchUserProducts",
  async (query, thunkAPI) => {
    try {
      const { data } = await Axios.get(
        `${BASE_API_URL}/product/sub-products/me/`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const createProduct = createAsyncThunk(
  "products/createProduct",
  async (createPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/product/sub-products/`,
        createPayload,
        {
          headers: {
            // "content-type": null,
            "content-type": "multipart/form-data",
            //; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",

            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editProduct = createAsyncThunk(
  "products/editProduct",
  async ({ id, editPayload }, thunkAPI) => {
    try {
      const { data } = await Axios.patch(
        `${BASE_API_URL}/product/sub-products/${id}/`,
        editPayload,
        {
          headers: {
            "content-type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteProduct = createAsyncThunk(
  "products/deleteProduct",
  async (productId, thunkAPI) => {
    try {
      const { data } = await Axios.delete(
        `${BASE_API_URL}/product/sub-products/${productId}/`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return productId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const orderProduct = createAsyncThunk(
  "products/orderProduct",
  async (orderPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(`${BASE_API_URL}/order/`, orderPayload);
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const productSlice = createSlice({
  name: "products",
  initialState: {
    products: {},
    allProducts: {},
    userProducts: [],
    productCategories: [],
    loading: "FETCH_USER_PRODUCTS",
    error: "",
    success: "",
  },
  reducers: {
    emptySearch: (state, { payload }) => {
      state.products = {};
    },
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchProducts.pending]: (state) => {
      state.products = {};
      delete state.error;
      delete state.success;
      state.loading = "FETCH_PRODUCTS";
    },
    [fetchProducts.fulfilled]: (state, action) => {
      state.success = "FETCH_PRODUCTS";
      state.products = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchProducts.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_PRODUCTS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [fetchAllProducts.pending]: (state) => {
      state.allProducts = {};
      delete state.error;
      delete state.success;
      state.loading = "FETCH_ALL_PRODUCTS";
    },
    [fetchAllProducts.fulfilled]: (state, action) => {
      state.success = "FETCH_ALL_PRODUCTS";
      state.allProducts = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchAllProducts.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_ALL_PRODUCTS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [activateProduct.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "ACTIVATE_PRODUCT";
    },
    [activateProduct.fulfilled]: (state, action) => {
      state.success = "ACTIVATE_PRODUCT";
      const product = state.userProducts.find(
        (product) => product.id === action.payload
      );
      product.is_active = !product.is_active;
      delete state.loading;
      delete state.error;
    },
    [activateProduct.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "ACTIVATE_PRODUCT",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [fetchCategories.pending]: (state) => {
      state.productCategories = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_CATEGORIES";
    },
    [fetchCategories.fulfilled]: (state, action) => {
      state.success = "FETCH_CATEGORIES";
      state.productCategories = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchCategories.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_CATEGORIES",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [fetchUserProducts.pending]: (state) => {
      state.products = {};
      delete state.error;
      delete state.success;
      state.loading = "FETCH_USER_PRODUCTS";
    },
    [fetchUserProducts.fulfilled]: (state, action) => {
      state.success = "FETCH_USER_PRODUCTS";
      state.userProducts = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchUserProducts.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_USER_PRODUCTS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [orderProduct.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "ORDER_PRODUCT";
    },
    [orderProduct.fulfilled]: (state, action) => {
      state.success = "ORDER_PRODUCT";
      // state.products = action.payload;
      delete state.loading;
      delete state.error;
    },
    [orderProduct.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "ORDER_PRODUCT",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [createProduct.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CREATE_PRODUCT";
    },
    [createProduct.fulfilled]: (state, action) => {
      state.success = "CREATE_PRODUCT";
      state.userProducts.push(action.payload);
      delete state.loading;
      delete state.error;
    },
    [createProduct.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CREATE_PRODUCT",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editProduct.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_PRODUCT";
    },
    [editProduct.fulfilled]: (state, action) => {
      state.success = "EDIT_PRODUCT";
      const product = state.userProducts.find(
        (product) => product.id === action.payload.id
      );
      // delete state.tempNote;
      Object.assign(product, action.payload);
      // state.products = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editProduct.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_PRODUCT",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteProduct.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_PRODUCT";
      const position = state.userProducts.findIndex(
        (userProduct) => userProduct.id === action.meta.arg
      );
      state.backupProduct = Object.assign({}, state.userProducts[position]);
      state.backupPosition = position;
    },
    [deleteProduct.fulfilled]: (state) => {
      state.success = "DELETE_PRODUCT";
      state.userProducts.splice(state.backupPosition, 1);
      delete state.backupProduct;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteProduct.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_PRODUCT",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupProduct;
      delete state.loading;
    },
  },
});
export const { emptySearch, clearStates } = productSlice.actions;

export default productSlice.reducer;
