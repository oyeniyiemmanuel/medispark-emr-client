import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchDispensaries = createAsyncThunk(
  "dispensaries/fetchDispensaries",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/service/get-dispensaries`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const dispenseMedication = createAsyncThunk(
  "dispensaries/dispenseMedication",
  async (createPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/billing/service/new-stock-dispensed`,
        createPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editDispensary = createAsyncThunk(
  "dispensaries/editDispensary",
  async (editPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.put(
        `${BASE_API_URL}/church/dispensaries/${editPayload.id}/`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteDispensary = createAsyncThunk(
  "dispensaries/deleteDispensary",
  async (serviceId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(
        `${BASE_API_URL}/church/dispensaries/${serviceId}/`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return serviceId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const dispensarySlice = createSlice({
  name: "dispensaries",
  initialState: {
    dispensaries: [],
    newestDispensed: {},
    loading: "FETCH_DISPENSARIES",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchDispensaries.pending]: (state) => {
      state.dispensaries = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_DISPENSARIES";
    },
    [fetchDispensaries.fulfilled]: (state, action) => {
      state.success = "FETCH_DISPENSARIES";
      state.dispensaries = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchDispensaries.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_DISPENSARIES",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [dispenseMedication.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "DISPENSE_MEDICATION";
    },
    [dispenseMedication.fulfilled]: (state, action) => {
      state.success = "DISPENSE_MEDICATION";
      state.newestDispensed = action.payload?.data;
      delete state.loading;
      delete state.error;
    },
    [dispenseMedication.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DISPENSE_MEDICATION",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editDispensary.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_DISPENSARY";
    },
    [editDispensary.fulfilled]: (state, action) => {
      state.success = "EDIT_DISPENSARY";
      const service = state.dispensaries.find(
        (service) => service.id === action.payload.id
      );
      // delete state.tempNote;
      Object.assign(service, action.payload);
      // state.dispensaries = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editDispensary.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_DISPENSARY",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteDispensary.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_DISPENSARY";
      const position = state.dispensaries.findIndex(
        (service) => service.id === action.meta.arg
      );
      state.backupService = Object.assign({}, state.dispensaries[position]);
      state.backupPosition = position;
    },
    [deleteDispensary.fulfilled]: (state) => {
      state.success = "DELETE_DISPENSARY";
      state.dispensaries.splice(state.backupPosition, 1);
      delete state.backupService;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteDispensary.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_DISPENSARY",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupService;
      delete state.loading;
    },
  },
});
export const { clearStates } = dispensarySlice.actions;
export const dispensaryActions = dispensarySlice.actions;
export default dispensarySlice.reducer;
