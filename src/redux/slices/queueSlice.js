import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchConsultationQueues = createAsyncThunk(
  "queues/fetchConsultationQueues",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/waitlist/get-waitlists`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);
export const fetchBillingQueues = createAsyncThunk(
  "queues/fetchBillingQueues",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/waitlist/get-waitlists`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);
export const fetchProcedureQueues = createAsyncThunk(
  "queues/fetchProcedureQueues",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/waitlist/get-waitlists`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);
export const fetchDispensaryQueues = createAsyncThunk(
  "queues/fetchDispensaryQueues",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/waitlist/get-waitlists`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const createQueue = createAsyncThunk(
  "queues/createQueue",
  async (createPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/billing/queue/new`,
        createPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editQueue = createAsyncThunk(
  "queues/editQueue",
  async (editPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.put(
        `${BASE_API_URL}/church/queues/${editPayload.id}/`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteQueue = createAsyncThunk(
  "queues/deleteQueue",
  async (queueId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(`${BASE_API_URL}/church/queues/${queueId}/`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return queueId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const queueSlice = createSlice({
  name: "queues",
  initialState: {
    queues: [],
    consultationQueue: [],
    billingQueue: [],
    procedureQueue: [],
    dispensaryQueue: [],
    loading: "FETCH_CONSULTATION_QUEUE",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
    addToQueue: (state, { payload }) => {
      state[`${payload.type}Queue`].push(payload.data);
    },
  },
  extraReducers: {
    [fetchConsultationQueues.pending]: (state) => {
      // state.consultationQueue = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_CONSULTATION_QUEUE";
    },
    [fetchConsultationQueues.fulfilled]: (state, action) => {
      state.success = "FETCH_CONSULTATION_QUEUE";
      state.consultationQueue = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchConsultationQueues.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_CONSULTATION_QUEUE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [fetchBillingQueues.pending]: (state) => {
      // state.billingQueue = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_BILLING_QUEUE";
    },
    [fetchBillingQueues.fulfilled]: (state, action) => {
      state.success = "FETCH_BILLING_QUEUE";
      state.billingQueue = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchBillingQueues.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_BILLING_QUEUE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [fetchProcedureQueues.pending]: (state) => {
      // state.procedureQueue = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_PROCEDURE_QUEUE";
    },
    [fetchProcedureQueues.fulfilled]: (state, action) => {
      state.success = "FETCH_PROCEDURE_QUEUE";
      state.procedureQueue = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchProcedureQueues.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_PROCEDURE_QUEUE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [fetchDispensaryQueues.pending]: (state) => {
      // state.dispensaryQueue = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_DISPENSARY_QUEUE";
    },
    [fetchDispensaryQueues.fulfilled]: (state, action) => {
      state.success = "FETCH_DISPENSARY_QUEUE";
      state.dispensaryQueue = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchDispensaryQueues.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_DISPENSARY_QUEUE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [createQueue.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CREATE_QUEUE";
    },
    [createQueue.fulfilled]: (state, action) => {
      state.success = "CREATE_QUEUE";
      state.queues.push(action.payload);
      delete state.loading;
      delete state.error;
    },
    [createQueue.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CREATE_QUEUE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editQueue.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_QUEUE";
    },
    [editQueue.fulfilled]: (state, action) => {
      state.success = "EDIT_QUEUE";
      const queue = state.queues.find(
        (queue) => queue.id === action.payload.id
      );
      // delete state.tempNote;
      Object.assign(queue, action.payload);
      // state.queues = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editQueue.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_QUEUE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteQueue.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_QUEUE";
      const position = state.queues.findIndex(
        (queue) => queue.id === action.meta.arg
      );
      state.backupQueue = Object.assign({}, state.queues[position]);
      state.backupPosition = position;
    },
    [deleteQueue.fulfilled]: (state) => {
      state.success = "DELETE_QUEUE";
      state.queues.splice(state.backupPosition, 1);
      delete state.backupQueue;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteQueue.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_QUEUE",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupQueue;
      delete state.loading;
    },
  },
});
export const { clearStates, addToQueue } = queueSlice.actions;
export const queueActions = queueSlice.actions;
export default queueSlice.reducer;
