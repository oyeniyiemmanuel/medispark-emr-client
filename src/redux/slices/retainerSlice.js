import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchRetainers = createAsyncThunk(
  "retainers/fetchRetainers",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.post(
        `${BASE_API_URL}/gateway/organization/branch/retainer/all`,
        fetchPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const createRetainer = createAsyncThunk(
  "retainers/createRetainer",
  async (createPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/church/retainers/`,
        createPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editRetainer = createAsyncThunk(
  "retainers/editRetainer",
  async (editPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.put(
        `${BASE_API_URL}/church/retainers/${editPayload.id}/`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteRetainer = createAsyncThunk(
  "retainers/deleteRetainer",
  async (retainerId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(
        `${BASE_API_URL}/church/retainers/${retainerId}/`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return retainerId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const retainerSlice = createSlice({
  name: "retainers",
  initialState: {
    retainers: [],
    loading: "FETCH_RETAINERS",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchRetainers.pending]: (state) => {
      state.retainers = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_RETAINERS";
    },
    [fetchRetainers.fulfilled]: (state, action) => {
      state.success = "FETCH_RETAINERS";
      state.retainers = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchRetainers.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_RETAINERS",
        errorMessage: payload?.error?.message,
      };
      delete state.loading;
    },

    [createRetainer.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CREATE_RETAINER";
    },
    [createRetainer.fulfilled]: (state, action) => {
      state.success = "CREATE_RETAINER";
      state.retainers.push(action.payload);
      delete state.loading;
      delete state.error;
    },
    [createRetainer.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CREATE_RETAINER",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editRetainer.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_RETAINER";
    },
    [editRetainer.fulfilled]: (state, action) => {
      state.success = "EDIT_RETAINER";
      const retainer = state.retainers.find(
        (retainer) => retainer.id === action.payload.id
      );
      Object.assign(retainer, action.payload);
      delete state.loading;
      delete state.error;
    },
    [editRetainer.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_RETAINER",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteRetainer.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_RETAINER";
      const position = state.retainers.findIndex(
        (retainer) => retainer.id === action.meta.arg
      );
      state.backupRetainer = Object.assign({}, state.retainers[position]);
      state.backupPosition = position;
    },
    [deleteRetainer.fulfilled]: (state) => {
      state.success = "DELETE_RETAINER";
      state.retainers.splice(state.backupPosition, 1);
      delete state.backupRetainer;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteRetainer.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_RETAINER",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupRetainer;
      delete state.loading;
    },
  },
});
export const { clearStates } = retainerSlice.actions;
export const retainerActions = retainerSlice.actions;
export default retainerSlice.reducer;
