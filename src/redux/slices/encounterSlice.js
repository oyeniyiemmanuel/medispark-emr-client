import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchEncounters = createAsyncThunk(
  "encounters/fetchEncounters",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/encounter/get-encounters`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const createEncounter = createAsyncThunk(
  "encounters/createEncounter",
  async (createPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/clinic/encounter/new`,
        createPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editEncounter = createAsyncThunk(
  "encounters/editEncounter",
  async (editPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/clinic/encounter/edit`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteEncounter = createAsyncThunk(
  "encounters/deleteEncounter",
  async (encounterId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(
        `${BASE_API_URL}/church/encounters/${encounterId}/`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return encounterId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const encounterSlice = createSlice({
  name: "encounters",
  initialState: {
    encounters: [],
    newestEncounter: {},
    editedEncounter: {},
    loading: "FETCH_ENCOUNTERS",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchEncounters.pending]: (state) => {
      state.encounters = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_ENCOUNTERS";
    },
    [fetchEncounters.fulfilled]: (state, action) => {
      state.success = "FETCH_ENCOUNTERS";
      state.encounters = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchEncounters.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_ENCOUNTERS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [createEncounter.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CREATE_ENCOUNTER";
    },
    [createEncounter.fulfilled]: (state, action) => {
      state.success = "CREATE_ENCOUNTER";
      state.newestEncounter = action.payload?.data;
      delete state.loading;
      delete state.error;
    },
    [createEncounter.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CREATE_ENCOUNTER",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editEncounter.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_ENCOUNTER";
    },
    [editEncounter.fulfilled]: (state, action) => {
      state.success = "EDIT_ENCOUNTER";
      state.editedEncounter = action.payload?.data;
      // const encounter = state.encounters.find(
      //   (encounter) => encounter.id === action.payload.id
      // );
      // // delete state.tempNote;
      // Object.assign(encounter, action.payload);
      // state.encounters = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editEncounter.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_ENCOUNTER",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteEncounter.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_ENCOUNTER";
      const position = state.encounters.findIndex(
        (encounter) => encounter.id === action.meta.arg
      );
      state.backupEncounter = Object.assign({}, state.encounters[position]);
      state.backupPosition = position;
    },
    [deleteEncounter.fulfilled]: (state) => {
      state.success = "DELETE_ENCOUNTER";
      state.encounters.splice(state.backupPosition, 1);
      delete state.backupEncounter;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteEncounter.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_ENCOUNTER",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupEncounter;
      delete state.loading;
    },
  },
});
export const { clearStates } = encounterSlice.actions;
export const encounterActions = encounterSlice.actions;
export default encounterSlice.reducer;
