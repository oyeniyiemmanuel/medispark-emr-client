import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchMessages = createAsyncThunk(
  "messages/fetchMessages",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/message/get-messages`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const createMessage = createAsyncThunk(
  "messages/createMessage",
  async (createPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/gateway/mail/organization/batch`,
        createPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editMessage = createAsyncThunk(
  "messages/editMessage",
  async (editPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.put(
        `${BASE_API_URL}/church/messages/${editPayload.id}/`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deleteMessage = createAsyncThunk(
  "messages/deleteMessage",
  async (messageId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(`${BASE_API_URL}/church/messages/${messageId}/`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return messageId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const messageSlice = createSlice({
  name: "messages",
  initialState: {
    messages: [],
    loading: "FETCH_MESSAGES",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchMessages.pending]: (state) => {
      state.messages = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_MESSAGES";
    },
    [fetchMessages.fulfilled]: (state, action) => {
      state.success = "FETCH_MESSAGES";
      state.messages = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchMessages.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_MESSAGES",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [createMessage.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CREATE_MESSAGE";
    },
    [createMessage.fulfilled]: (state, action) => {
      state.success = "CREATE_MESSAGE";
      state.messages.push(action.payload);
      delete state.loading;
      delete state.error;
    },
    [createMessage.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CREATE_MESSAGE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editMessage.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_MESSAGE";
    },
    [editMessage.fulfilled]: (state, action) => {
      state.success = "EDIT_MESSAGE";
      const message = state.messages.find(
        (message) => message.id === action.payload.id
      );
      // delete state.tempNote;
      Object.assign(message, action.payload);
      // state.messages = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editMessage.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_MESSAGE",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deleteMessage.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_MESSAGE";
      const position = state.messages.findIndex(
        (message) => message.id === action.meta.arg
      );
      state.backupMessage = Object.assign({}, state.messages[position]);
      state.backupPosition = position;
    },
    [deleteMessage.fulfilled]: (state) => {
      state.success = "DELETE_MESSAGE";
      state.messages.splice(state.backupPosition, 1);
      delete state.backupMessage;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deleteMessage.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_MESSAGE",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupMessage;
      delete state.loading;
    },
  },
});
export const { clearStates } = messageSlice.actions;
export const messageActions = messageSlice.actions;
export default messageSlice.reducer;
