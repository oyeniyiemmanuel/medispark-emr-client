import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchPatients = createAsyncThunk(
  "patients/fetchPatients",
  async (getPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/gateway/patient/get-patients`, {
        params: getPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const createPatient = createAsyncThunk(
  "patients/createPatient",
  async (createPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.post(
        `${BASE_API_URL}/gateway/organization/new-patient`,
        createPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editPatient = createAsyncThunk(
  "patients/editPatient",
  async (editPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.post(
        `${BASE_API_URL}/gateway/user/edit-profile`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deletePatient = createAsyncThunk(
  "patients/deletePatient",
  async (patientId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(
        `${BASE_API_URL}/users/admin/${patientId}/patient/`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return patientId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const fetchPatientTimeline = createAsyncThunk(
  "patients/fetchPatientTimeline",
  async (getPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(
        `${BASE_API_URL}/gateway/patient/get-timeline-entries`,
        {
          params: getPayload,
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const patientSlice = createSlice({
  name: "patients",
  initialState: {
    patients: [],
    selectedPatient: null,
    patientTimeline: {},
    patientProducts: [],
    loading: "FETCH_PATIENTS",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
    setSelectedPatient: (state, { payload }) => {
      state.patientTimeline = {};
      state.selectedPatient = payload;
    },
    updatePatientBalance: (state, { payload }) => {
      state.selectedPatient = {
        ...state.selectedPatient,
        total_balance: payload.newBalance,
      };
    },

    updatePatientBillsStatus: (state, { payload }) => {
      console.log(payload);
      for (const i in payload.billsPaidFor) {
        console.log(i);
        const billToUpdate = state.patientTimeline.bills?.find(
          (eachBill) => eachBill?.id == payload.billsPaidFor[i].id
        );
        Object.assign(billToUpdate, {
          ...billToUpdate,
          invoice_details: payload.invoice_details,
          invoice_id: payload.invoice_id,
        });
      }
    },

    addToPatientTimeline: (state, { payload }) => {
      state.patientTimeline[payload.type] = [
        ...state.patientTimeline[payload.type],
        payload.activity,
      ];
    },
    editPatientTimeline: (state, { payload }) => {
      const activityToEdit = state.patientTimeline[payload.type]?.find(
        (activity) => activity?.id == payload.activity?.id
      );
      Object.assign(activityToEdit, payload.activity);
    },
    addEntryToTimelineActivity: (state, { payload }) => {
      let activityToEdit;
      if (payload.entryType == "files") {
        activityToEdit = state.patientTimeline[payload.timelineType]?.find(
          (activity) => activity?.id == payload.entry?.fileable_id
        );
      } else if (payload.entryType == "rendered_services") {
        activityToEdit = state.patientTimeline[payload.timelineType]?.find(
          (activity) => activity?.id == payload.entry?.encounter_id
        );
      }

      Object.assign(activityToEdit, {
        ...activityToEdit,
        [payload.entryType]: activityToEdit[payload.entryType]
          ? [...activityToEdit[payload.entryType], payload?.entry]
          : [payload?.entry],
      });
    },
    changePatientProfilePicture: (state, { payload }) => {
      const patient = state.patients.find(
        (patient) => patient.id === state.selectedPatient.id
      );
      Object.assign(patient, { ...patient, avatar: payload.avatar });

      state.selectedPatient = {
        ...state.selectedPatient,
        avatar: payload.avatar,
      };
    },
  },
  extraReducers: {
    [fetchPatients.pending]: (state) => {
      state.patients = [];
      delete state.error;
      delete state.success;
      state.loading = "FETCH_PATIENTS";
    },
    [fetchPatients.fulfilled]: (state, action) => {
      state.success = "FETCH_PATIENTS";
      state.patients = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchPatients.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_PATIENTS",
        errorMessage: payload?.error?.message,
      };
      delete state.loading;
    },

    [createPatient.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CREATE_PATIENT";
    },
    [createPatient.fulfilled]: (state, action) => {
      state.success = "CREATE_PATIENT";
      state.patients.push(action.payload);
      delete state.loading;
      delete state.error;
    },
    [createPatient.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CREATE_PATIENT",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editPatient.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_PATIENT";
    },
    [editPatient.fulfilled]: (state, action) => {
      state.success = "EDIT_PATIENT";
      const patient = state.patients.find(
        (patient) => patient.id === action.payload.id
      );
      Object.assign(patient, action.payload);
      state.selectedPatient = { ...state.selectedPatient, ...action.payload };
      delete state.loading;
      delete state.error;
    },
    [editPatient.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_PATIENT",
        errorMessage: payload?.error,
      };
      delete state.loading;
      0;
    },

    [deletePatient.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_PATIENT";
      const position = state.patients.findIndex(
        (patient) => patient.id === action.meta.arg
      );
      state.backupPatient = Object.assign({}, state.patients[position]);
      state.backupPosition = position;
    },
    [deletePatient.fulfilled]: (state) => {
      state.success = "DELETE_PATIENT";
      state.patients.splice(state.backupPosition, 1);
      delete state.backupPatient;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deletePatient.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_PATIENT",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupPatient;
      delete state.loading;
    },

    [fetchPatientTimeline.pending]: (state) => {
      state.patientTimeline = {};
      delete state.error;
      delete state.success;
      state.loading = "FETCH_PATIENT_TIMELINE";
    },
    [fetchPatientTimeline.fulfilled]: (state, action) => {
      state.success = "FETCH_PATIENT_TIMELINE";
      state.patientTimeline = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchPatientTimeline.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_PATIENT_TIMELINE",
        errorMessage: payload?.error?.message,
      };
      delete state.loading;
    },
  },
});
export const {
  setSelectedPatient,
  clearStates,
  addToPatientTimeline,
  editPatientTimeline,
  addEntryToTimelineActivity,
  updatePatientBalance,
  changePatientProfilePicture,
} = patientSlice.actions;
export const patientActions = patientSlice.actions;
export default patientSlice.reducer;
