import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchOrganizationDetails = createAsyncThunk(
  "organization/fetchOrganizationDetails",
  async (getPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(
        `${BASE_API_URL}/gateway/organization/get-organization`,
        {
          params: getPayload,
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editOrganizationDetails = createAsyncThunk(
  "organization/editOrganizationDetails",
  async (editPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/gateway/organization/edit`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const organizationSlice = createSlice({
  name: "organization",
  initialState: {
    organizationDetails: {},
    loading: "",
    error: "",
    success: "",
  },
  reducers: {
    setOrganizationDetails: (state, action) => {
      state.organizationDetails = action.payload;
    },
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchOrganizationDetails.pending]: (state) => {
      state.organizationDetails = {};
      delete state.error;
      delete state.success;
      state.loading = "FETCH_USER_DETAILS";
    },
    [fetchOrganizationDetails.fulfilled]: (state, action) => {
      state.success = "FETCH_USER_DETAILS";
      state.organizationDetails = action.payload;
      delete state.loading;
      delete state.error;
    },
    [fetchOrganizationDetails.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_USER_DETAILS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },
    [editOrganizationDetails.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_USER_DETAILS";
    },
    [editOrganizationDetails.fulfilled]: (state, action) => {
      state.success = "EDIT_USER_DETAILS";
      // const member = state.members.find(
      //   (member) => member.id === action.payload.id
      // );
      // delete state.tempNote;
      // Object.assign(state.organizationDetails, action.payload);
      state.organizationDetails = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editOrganizationDetails.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_USER_DETAILS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },
  },
});

export const {
  clearStates,
  setOrganizationDetails,
} = organizationSlice.actions;
export default organizationSlice.reducer;
