import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import Axios from "axios";
import { BASE_API_URL } from "../../utils";

export const fetchPrescriptions = createAsyncThunk(
  "prescriptions/fetchPrescriptions",
  async (fetchPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.get(`${BASE_API_URL}/billing/service/get-serviceuser`, {
        params: fetchPayload,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "medispark_acccess_token"
          )}`,
        },
      });
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const createPrescription = createAsyncThunk(
  "prescriptions/createPrescription",
  async (createPayload, thunkAPI) => {
    try {
      const { data } = await Axios.post(
        `${BASE_API_URL}/billing/service/add-to-user`,
        createPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const editPrescription = createAsyncThunk(
  "prescriptions/editPrescription",
  async (editPayload, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.put(
        `${BASE_API_URL}/church/prescriptions/${editPayload.id}/`,
        editPayload,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return data;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

export const deletePrescription = createAsyncThunk(
  "prescriptions/deletePrescription",
  async (prescriptionId, thunkAPI) => {
    try {
      const {
        data: { data },
      } = await Axios.delete(
        `${BASE_API_URL}/church/prescriptions/${prescriptionId}/`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(
              "medispark_acccess_token"
            )}`,
          },
        }
      );
      return prescriptionId;
    } catch ({ response }) {
      console.log(response);
      return thunkAPI.rejectWithValue({ error: response.data });
    }
  }
);

const prescriptionSlice = createSlice({
  name: "prescriptions",
  initialState: {
    prescriptions: [],
    newestPrescription: {},
    loading: "FETCH_PRESCRIPTIONS",
    error: "",
    success: "",
  },
  reducers: {
    clearStates: (state, { payload }) => {
      delete state.loading;
      delete state.error;
      delete state.success;
    },
  },
  extraReducers: {
    [fetchPrescriptions.pending]: (state) => {
      state.newestPrescription = {};
      delete state.error;
      delete state.success;
      state.loading = "FETCH_PRESCRIPTIONS";
    },
    [fetchPrescriptions.fulfilled]: (state, action) => {
      state.success = "FETCH_PRESCRIPTIONS";
      state.newestPrescription = action.payload?.prescription;
      delete state.loading;
      delete state.error;
    },
    [fetchPrescriptions.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "FETCH_PRESCRIPTIONS",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [createPrescription.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "CREATE_PRESCRIPTION";
    },
    [createPrescription.fulfilled]: (state, action) => {
      state.success = "CREATE_PRESCRIPTION";
      state.newestPrescription = action.payload?.data;
      delete state.loading;
      delete state.error;
    },
    [createPrescription.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "CREATE_PRESCRIPTION",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [editPrescription.pending]: (state) => {
      delete state.error;
      delete state.success;
      state.loading = "EDIT_PRESCRIPTION";
    },
    [editPrescription.fulfilled]: (state, action) => {
      state.success = "EDIT_PRESCRIPTION";
      const prescription = state.prescriptions.find(
        (prescription) => prescription.id === action.payload.id
      );
      // delete state.tempNote;
      Object.assign(prescription, action.payload);
      // state.prescriptions = action.payload;
      delete state.loading;
      delete state.error;
    },
    [editPrescription.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "EDIT_PRESCRIPTION",
        errorMessage: payload?.error,
      };
      delete state.loading;
    },

    [deletePrescription.pending]: (state, action) => {
      delete state.error;
      delete state.success;
      state.loading = "DELETE_PRESCRIPTION";
      const position = state.prescriptions.findIndex(
        (prescription) => prescription.id === action.meta.arg
      );
      state.backupPrescription = Object.assign(
        {},
        state.prescriptions[position]
      );
      state.backupPosition = position;
    },
    [deletePrescription.fulfilled]: (state) => {
      state.success = "DELETE_PRESCRIPTION";
      state.prescriptions.splice(state.backupPosition, 1);
      delete state.backupPrescription;
      delete state.backupPosition;
      delete state.loading;
      delete state.error;
    },
    [deletePrescription.rejected]: (state, { payload }) => {
      state.error = {
        errorType: "DELETE_PRESCRIPTION",
        errorMessage: payload?.error,
      };
      delete state.backupPosition;
      delete state.backupPrescription;
      delete state.loading;
    },
  },
});
export const { clearStates } = prescriptionSlice.actions;
export const prescriptionActions = prescriptionSlice.actions;
export default prescriptionSlice.reducer;
